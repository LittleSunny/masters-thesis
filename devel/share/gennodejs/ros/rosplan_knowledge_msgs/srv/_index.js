
"use strict";

let KnowledgeUpdateService = require('./KnowledgeUpdateService.js')
let GetDomainPredicateDetailsService = require('./GetDomainPredicateDetailsService.js')
let GetDomainOperatorService = require('./GetDomainOperatorService.js')
let RemoveWaypoint = require('./RemoveWaypoint.js')
let AddWaypoint = require('./AddWaypoint.js')
let GetDomainAttributeService = require('./GetDomainAttributeService.js')
let KnowledgeUpdateServiceArray = require('./KnowledgeUpdateServiceArray.js')
let GetAttributeService = require('./GetAttributeService.js')
let KnowledgeQueryService = require('./KnowledgeQueryService.js')
let GetDomainTypeService = require('./GetDomainTypeService.js')
let GetInstanceService = require('./GetInstanceService.js')
let CreatePRM = require('./CreatePRM.js')
let GetDomainOperatorDetailsService = require('./GetDomainOperatorDetailsService.js')
let GenerateProblemService = require('./GenerateProblemService.js')

module.exports = {
  KnowledgeUpdateService: KnowledgeUpdateService,
  GetDomainPredicateDetailsService: GetDomainPredicateDetailsService,
  GetDomainOperatorService: GetDomainOperatorService,
  RemoveWaypoint: RemoveWaypoint,
  AddWaypoint: AddWaypoint,
  GetDomainAttributeService: GetDomainAttributeService,
  KnowledgeUpdateServiceArray: KnowledgeUpdateServiceArray,
  GetAttributeService: GetAttributeService,
  KnowledgeQueryService: KnowledgeQueryService,
  GetDomainTypeService: GetDomainTypeService,
  GetInstanceService: GetInstanceService,
  CreatePRM: CreatePRM,
  GetDomainOperatorDetailsService: GetDomainOperatorDetailsService,
  GenerateProblemService: GenerateProblemService,
};
