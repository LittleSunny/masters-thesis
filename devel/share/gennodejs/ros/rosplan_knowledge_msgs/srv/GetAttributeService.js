// Auto-generated. Do not edit!

// (in-package rosplan_knowledge_msgs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

let KnowledgeItem = require('../msg/KnowledgeItem.js');

//-----------------------------------------------------------

class GetAttributeServiceRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.predicate_name = null;
    }
    else {
      if (initObj.hasOwnProperty('predicate_name')) {
        this.predicate_name = initObj.predicate_name
      }
      else {
        this.predicate_name = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetAttributeServiceRequest
    // Serialize message field [predicate_name]
    bufferOffset = _serializer.string(obj.predicate_name, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetAttributeServiceRequest
    let len;
    let data = new GetAttributeServiceRequest(null);
    // Deserialize message field [predicate_name]
    data.predicate_name = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.predicate_name.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_knowledge_msgs/GetAttributeServiceRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '130dad30028f4055312b63b35c17d1c3';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    
    
    string predicate_name
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetAttributeServiceRequest(null);
    if (msg.predicate_name !== undefined) {
      resolved.predicate_name = msg.predicate_name;
    }
    else {
      resolved.predicate_name = ''
    }

    return resolved;
    }
};

class GetAttributeServiceResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.attributes = null;
    }
    else {
      if (initObj.hasOwnProperty('attributes')) {
        this.attributes = initObj.attributes
      }
      else {
        this.attributes = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetAttributeServiceResponse
    // Serialize message field [attributes]
    // Serialize the length for message field [attributes]
    bufferOffset = _serializer.uint32(obj.attributes.length, buffer, bufferOffset);
    obj.attributes.forEach((val) => {
      bufferOffset = KnowledgeItem.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetAttributeServiceResponse
    let len;
    let data = new GetAttributeServiceResponse(null);
    // Deserialize message field [attributes]
    // Deserialize array length for message field [attributes]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.attributes = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.attributes[i] = KnowledgeItem.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    object.attributes.forEach((val) => {
      length += KnowledgeItem.getMessageSize(val);
    });
    return length + 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_knowledge_msgs/GetAttributeServiceResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '4346d5c15a13ae8ec3736496416e2ac4';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    rosplan_knowledge_msgs/KnowledgeItem[] attributes
    
    
    ================================================================================
    MSG: rosplan_knowledge_msgs/KnowledgeItem
    # A knowledge item used to represent a piece of the world model in ROSPlan
    uint8 INSTANCE = 0
    uint8 FACT = 1
    uint8 FUNCTION = 2
    
    uint8 knowledge_type
    
    # instance knowledge_type
    string instance_type
    string instance_name
    
    # attribute knowledge_type
    string attribute_name
    diagnostic_msgs/KeyValue[] values
    
    # function value
    float64 function_value
    
    # negative of positive
    bool is_negative
    ================================================================================
    MSG: diagnostic_msgs/KeyValue
    string key # what to label this value when viewing
    string value # a value to track over time
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetAttributeServiceResponse(null);
    if (msg.attributes !== undefined) {
      resolved.attributes = new Array(msg.attributes.length);
      for (let i = 0; i < resolved.attributes.length; ++i) {
        resolved.attributes[i] = KnowledgeItem.Resolve(msg.attributes[i]);
      }
    }
    else {
      resolved.attributes = []
    }

    return resolved;
    }
};

module.exports = {
  Request: GetAttributeServiceRequest,
  Response: GetAttributeServiceResponse,
  md5sum() { return 'bf3939829dd290d472fffb68a41256ec'; },
  datatype() { return 'rosplan_knowledge_msgs/GetAttributeService'; }
};
