// Auto-generated. Do not edit!

// (in-package rosplan_knowledge_msgs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let KnowledgeItem = require('../msg/KnowledgeItem.js');

//-----------------------------------------------------------


//-----------------------------------------------------------

class KnowledgeUpdateServiceArrayRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.update_type = null;
      this.knowledge = null;
    }
    else {
      if (initObj.hasOwnProperty('update_type')) {
        this.update_type = initObj.update_type
      }
      else {
        this.update_type = 0;
      }
      if (initObj.hasOwnProperty('knowledge')) {
        this.knowledge = initObj.knowledge
      }
      else {
        this.knowledge = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type KnowledgeUpdateServiceArrayRequest
    // Serialize message field [update_type]
    bufferOffset = _serializer.uint8(obj.update_type, buffer, bufferOffset);
    // Serialize message field [knowledge]
    // Serialize the length for message field [knowledge]
    bufferOffset = _serializer.uint32(obj.knowledge.length, buffer, bufferOffset);
    obj.knowledge.forEach((val) => {
      bufferOffset = KnowledgeItem.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type KnowledgeUpdateServiceArrayRequest
    let len;
    let data = new KnowledgeUpdateServiceArrayRequest(null);
    // Deserialize message field [update_type]
    data.update_type = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [knowledge]
    // Deserialize array length for message field [knowledge]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.knowledge = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.knowledge[i] = KnowledgeItem.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    object.knowledge.forEach((val) => {
      length += KnowledgeItem.getMessageSize(val);
    });
    return length + 5;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_knowledge_msgs/KnowledgeUpdateServiceArrayRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '84baa11f46067de28d0a200709674319';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    
    uint8 ADD_KNOWLEDGE = 0
    uint8 ADD_GOAL = 1
    uint8 REMOVE_KNOWLEDGE = 2
    uint8 REMOVE_GOAL = 3
    
    uint8 update_type
    
    rosplan_knowledge_msgs/KnowledgeItem[] knowledge
    
    ================================================================================
    MSG: rosplan_knowledge_msgs/KnowledgeItem
    # A knowledge item used to represent a piece of the world model in ROSPlan
    uint8 INSTANCE = 0
    uint8 FACT = 1
    uint8 FUNCTION = 2
    
    uint8 knowledge_type
    
    # instance knowledge_type
    string instance_type
    string instance_name
    
    # attribute knowledge_type
    string attribute_name
    diagnostic_msgs/KeyValue[] values
    
    # function value
    float64 function_value
    
    # negative of positive
    bool is_negative
    ================================================================================
    MSG: diagnostic_msgs/KeyValue
    string key # what to label this value when viewing
    string value # a value to track over time
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new KnowledgeUpdateServiceArrayRequest(null);
    if (msg.update_type !== undefined) {
      resolved.update_type = msg.update_type;
    }
    else {
      resolved.update_type = 0
    }

    if (msg.knowledge !== undefined) {
      resolved.knowledge = new Array(msg.knowledge.length);
      for (let i = 0; i < resolved.knowledge.length; ++i) {
        resolved.knowledge[i] = KnowledgeItem.Resolve(msg.knowledge[i]);
      }
    }
    else {
      resolved.knowledge = []
    }

    return resolved;
    }
};

// Constants for message
KnowledgeUpdateServiceArrayRequest.Constants = {
  ADD_KNOWLEDGE: 0,
  ADD_GOAL: 1,
  REMOVE_KNOWLEDGE: 2,
  REMOVE_GOAL: 3,
}

class KnowledgeUpdateServiceArrayResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.success = null;
    }
    else {
      if (initObj.hasOwnProperty('success')) {
        this.success = initObj.success
      }
      else {
        this.success = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type KnowledgeUpdateServiceArrayResponse
    // Serialize message field [success]
    bufferOffset = _serializer.bool(obj.success, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type KnowledgeUpdateServiceArrayResponse
    let len;
    let data = new KnowledgeUpdateServiceArrayResponse(null);
    // Deserialize message field [success]
    data.success = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_knowledge_msgs/KnowledgeUpdateServiceArrayResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '358e233cde0c8a8bcfea4ce193f8fc15';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool success
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new KnowledgeUpdateServiceArrayResponse(null);
    if (msg.success !== undefined) {
      resolved.success = msg.success;
    }
    else {
      resolved.success = false
    }

    return resolved;
    }
};

module.exports = {
  Request: KnowledgeUpdateServiceArrayRequest,
  Response: KnowledgeUpdateServiceArrayResponse,
  md5sum() { return '837765eff2c5aff85dc1654a7e74e555'; },
  datatype() { return 'rosplan_knowledge_msgs/KnowledgeUpdateServiceArray'; }
};
