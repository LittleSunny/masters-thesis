// Auto-generated. Do not edit!

// (in-package rosplan_knowledge_msgs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

let DomainOperator = require('../msg/DomainOperator.js');

//-----------------------------------------------------------

class GetDomainOperatorDetailsServiceRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.name = null;
    }
    else {
      if (initObj.hasOwnProperty('name')) {
        this.name = initObj.name
      }
      else {
        this.name = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetDomainOperatorDetailsServiceRequest
    // Serialize message field [name]
    bufferOffset = _serializer.string(obj.name, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetDomainOperatorDetailsServiceRequest
    let len;
    let data = new GetDomainOperatorDetailsServiceRequest(null);
    // Deserialize message field [name]
    data.name = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.name.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_knowledge_msgs/GetDomainOperatorDetailsServiceRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'c1f3d28f1b044c871e6eff2e9fc3c667';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    
    string name
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetDomainOperatorDetailsServiceRequest(null);
    if (msg.name !== undefined) {
      resolved.name = msg.name;
    }
    else {
      resolved.name = ''
    }

    return resolved;
    }
};

class GetDomainOperatorDetailsServiceResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.op = null;
    }
    else {
      if (initObj.hasOwnProperty('op')) {
        this.op = initObj.op
      }
      else {
        this.op = new DomainOperator();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GetDomainOperatorDetailsServiceResponse
    // Serialize message field [op]
    bufferOffset = DomainOperator.serialize(obj.op, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GetDomainOperatorDetailsServiceResponse
    let len;
    let data = new GetDomainOperatorDetailsServiceResponse(null);
    // Deserialize message field [op]
    data.op = DomainOperator.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += DomainOperator.getMessageSize(object.op);
    return length;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_knowledge_msgs/GetDomainOperatorDetailsServiceResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '052890ba8f53a90bccc7aeb79087a057';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    rosplan_knowledge_msgs/DomainOperator op
    
    
    ================================================================================
    MSG: rosplan_knowledge_msgs/DomainOperator
    # A knowledge item used to represent an operator in the domain.
    # (1) name and parameters
    rosplan_knowledge_msgs/DomainFormula formula
    
    # (2) duration constraint
    
    
    # (3) effect lists
    rosplan_knowledge_msgs/DomainFormula[] at_start_add_effects
    rosplan_knowledge_msgs/DomainFormula[] at_start_del_effects
    rosplan_knowledge_msgs/DomainFormula[] at_end_add_effects
    rosplan_knowledge_msgs/DomainFormula[] at_end_del_effects
    
    # (4) conditions
    rosplan_knowledge_msgs/DomainFormula[] at_start_simple_condition
    rosplan_knowledge_msgs/DomainFormula[] over_all_simple_condition
    rosplan_knowledge_msgs/DomainFormula[] at_end_simple_condition
    rosplan_knowledge_msgs/DomainFormula[] at_start_neg_condition
    rosplan_knowledge_msgs/DomainFormula[] over_all_neg_condition
    rosplan_knowledge_msgs/DomainFormula[] at_end_neg_condition
    
    ================================================================================
    MSG: rosplan_knowledge_msgs/DomainFormula
    # A knowledge item used to represent an atomic formula from the domain.
    # typed_parameters matches label -> type
    string name
    diagnostic_msgs/KeyValue[] typed_parameters
    
    ================================================================================
    MSG: diagnostic_msgs/KeyValue
    string key # what to label this value when viewing
    string value # a value to track over time
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GetDomainOperatorDetailsServiceResponse(null);
    if (msg.op !== undefined) {
      resolved.op = DomainOperator.Resolve(msg.op)
    }
    else {
      resolved.op = new DomainOperator()
    }

    return resolved;
    }
};

module.exports = {
  Request: GetDomainOperatorDetailsServiceRequest,
  Response: GetDomainOperatorDetailsServiceResponse,
  md5sum() { return '8eaa82c5a9043a8eaaeb8014eb9d9795'; },
  datatype() { return 'rosplan_knowledge_msgs/GetDomainOperatorDetailsService'; }
};
