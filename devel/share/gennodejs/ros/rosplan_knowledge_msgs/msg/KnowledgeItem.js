// Auto-generated. Do not edit!

// (in-package rosplan_knowledge_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let diagnostic_msgs = _finder('diagnostic_msgs');

//-----------------------------------------------------------

class KnowledgeItem {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.knowledge_type = null;
      this.instance_type = null;
      this.instance_name = null;
      this.attribute_name = null;
      this.values = null;
      this.function_value = null;
      this.is_negative = null;
    }
    else {
      if (initObj.hasOwnProperty('knowledge_type')) {
        this.knowledge_type = initObj.knowledge_type
      }
      else {
        this.knowledge_type = 0;
      }
      if (initObj.hasOwnProperty('instance_type')) {
        this.instance_type = initObj.instance_type
      }
      else {
        this.instance_type = '';
      }
      if (initObj.hasOwnProperty('instance_name')) {
        this.instance_name = initObj.instance_name
      }
      else {
        this.instance_name = '';
      }
      if (initObj.hasOwnProperty('attribute_name')) {
        this.attribute_name = initObj.attribute_name
      }
      else {
        this.attribute_name = '';
      }
      if (initObj.hasOwnProperty('values')) {
        this.values = initObj.values
      }
      else {
        this.values = [];
      }
      if (initObj.hasOwnProperty('function_value')) {
        this.function_value = initObj.function_value
      }
      else {
        this.function_value = 0.0;
      }
      if (initObj.hasOwnProperty('is_negative')) {
        this.is_negative = initObj.is_negative
      }
      else {
        this.is_negative = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type KnowledgeItem
    // Serialize message field [knowledge_type]
    bufferOffset = _serializer.uint8(obj.knowledge_type, buffer, bufferOffset);
    // Serialize message field [instance_type]
    bufferOffset = _serializer.string(obj.instance_type, buffer, bufferOffset);
    // Serialize message field [instance_name]
    bufferOffset = _serializer.string(obj.instance_name, buffer, bufferOffset);
    // Serialize message field [attribute_name]
    bufferOffset = _serializer.string(obj.attribute_name, buffer, bufferOffset);
    // Serialize message field [values]
    // Serialize the length for message field [values]
    bufferOffset = _serializer.uint32(obj.values.length, buffer, bufferOffset);
    obj.values.forEach((val) => {
      bufferOffset = diagnostic_msgs.msg.KeyValue.serialize(val, buffer, bufferOffset);
    });
    // Serialize message field [function_value]
    bufferOffset = _serializer.float64(obj.function_value, buffer, bufferOffset);
    // Serialize message field [is_negative]
    bufferOffset = _serializer.bool(obj.is_negative, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type KnowledgeItem
    let len;
    let data = new KnowledgeItem(null);
    // Deserialize message field [knowledge_type]
    data.knowledge_type = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [instance_type]
    data.instance_type = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [instance_name]
    data.instance_name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [attribute_name]
    data.attribute_name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [values]
    // Deserialize array length for message field [values]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.values = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.values[i] = diagnostic_msgs.msg.KeyValue.deserialize(buffer, bufferOffset)
    }
    // Deserialize message field [function_value]
    data.function_value = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [is_negative]
    data.is_negative = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.instance_type.length;
    length += object.instance_name.length;
    length += object.attribute_name.length;
    object.values.forEach((val) => {
      length += diagnostic_msgs.msg.KeyValue.getMessageSize(val);
    });
    return length + 26;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rosplan_knowledge_msgs/KnowledgeItem';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a4264640d228a4b57e9b41de1d4d7474';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # A knowledge item used to represent a piece of the world model in ROSPlan
    uint8 INSTANCE = 0
    uint8 FACT = 1
    uint8 FUNCTION = 2
    
    uint8 knowledge_type
    
    # instance knowledge_type
    string instance_type
    string instance_name
    
    # attribute knowledge_type
    string attribute_name
    diagnostic_msgs/KeyValue[] values
    
    # function value
    float64 function_value
    
    # negative of positive
    bool is_negative
    ================================================================================
    MSG: diagnostic_msgs/KeyValue
    string key # what to label this value when viewing
    string value # a value to track over time
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new KnowledgeItem(null);
    if (msg.knowledge_type !== undefined) {
      resolved.knowledge_type = msg.knowledge_type;
    }
    else {
      resolved.knowledge_type = 0
    }

    if (msg.instance_type !== undefined) {
      resolved.instance_type = msg.instance_type;
    }
    else {
      resolved.instance_type = ''
    }

    if (msg.instance_name !== undefined) {
      resolved.instance_name = msg.instance_name;
    }
    else {
      resolved.instance_name = ''
    }

    if (msg.attribute_name !== undefined) {
      resolved.attribute_name = msg.attribute_name;
    }
    else {
      resolved.attribute_name = ''
    }

    if (msg.values !== undefined) {
      resolved.values = new Array(msg.values.length);
      for (let i = 0; i < resolved.values.length; ++i) {
        resolved.values[i] = diagnostic_msgs.msg.KeyValue.Resolve(msg.values[i]);
      }
    }
    else {
      resolved.values = []
    }

    if (msg.function_value !== undefined) {
      resolved.function_value = msg.function_value;
    }
    else {
      resolved.function_value = 0.0
    }

    if (msg.is_negative !== undefined) {
      resolved.is_negative = msg.is_negative;
    }
    else {
      resolved.is_negative = false
    }

    return resolved;
    }
};

// Constants for message
KnowledgeItem.Constants = {
  INSTANCE: 0,
  FACT: 1,
  FUNCTION: 2,
}

module.exports = KnowledgeItem;
