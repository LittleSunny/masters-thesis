// Auto-generated. Do not edit!

// (in-package rosplan_knowledge_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let KnowledgeItem = require('./KnowledgeItem.js');

//-----------------------------------------------------------

class Filter {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.function = null;
      this.knowledge_items = null;
    }
    else {
      if (initObj.hasOwnProperty('function')) {
        this.function = initObj.function
      }
      else {
        this.function = 0;
      }
      if (initObj.hasOwnProperty('knowledge_items')) {
        this.knowledge_items = initObj.knowledge_items
      }
      else {
        this.knowledge_items = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Filter
    // Serialize message field [function]
    bufferOffset = _serializer.uint8(obj.function, buffer, bufferOffset);
    // Serialize message field [knowledge_items]
    // Serialize the length for message field [knowledge_items]
    bufferOffset = _serializer.uint32(obj.knowledge_items.length, buffer, bufferOffset);
    obj.knowledge_items.forEach((val) => {
      bufferOffset = KnowledgeItem.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Filter
    let len;
    let data = new Filter(null);
    // Deserialize message field [function]
    data.function = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [knowledge_items]
    // Deserialize array length for message field [knowledge_items]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.knowledge_items = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.knowledge_items[i] = KnowledgeItem.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    object.knowledge_items.forEach((val) => {
      length += KnowledgeItem.getMessageSize(val);
    });
    return length + 5;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rosplan_knowledge_msgs/Filter';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ffaa5312f028f19664069486a077b599';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint8 CLEAR = 0
    uint8 ADD = 1
    uint8 REMOVE = 2
    
    uint8 function
    KnowledgeItem[] knowledge_items
    
    ================================================================================
    MSG: rosplan_knowledge_msgs/KnowledgeItem
    # A knowledge item used to represent a piece of the world model in ROSPlan
    uint8 INSTANCE = 0
    uint8 FACT = 1
    uint8 FUNCTION = 2
    
    uint8 knowledge_type
    
    # instance knowledge_type
    string instance_type
    string instance_name
    
    # attribute knowledge_type
    string attribute_name
    diagnostic_msgs/KeyValue[] values
    
    # function value
    float64 function_value
    
    # negative of positive
    bool is_negative
    ================================================================================
    MSG: diagnostic_msgs/KeyValue
    string key # what to label this value when viewing
    string value # a value to track over time
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Filter(null);
    if (msg.function !== undefined) {
      resolved.function = msg.function;
    }
    else {
      resolved.function = 0
    }

    if (msg.knowledge_items !== undefined) {
      resolved.knowledge_items = new Array(msg.knowledge_items.length);
      for (let i = 0; i < resolved.knowledge_items.length; ++i) {
        resolved.knowledge_items[i] = KnowledgeItem.Resolve(msg.knowledge_items[i]);
      }
    }
    else {
      resolved.knowledge_items = []
    }

    return resolved;
    }
};

// Constants for message
Filter.Constants = {
  CLEAR: 0,
  ADD: 1,
  REMOVE: 2,
}

module.exports = Filter;
