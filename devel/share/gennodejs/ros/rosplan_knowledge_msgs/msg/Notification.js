// Auto-generated. Do not edit!

// (in-package rosplan_knowledge_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let KnowledgeItem = require('./KnowledgeItem.js');

//-----------------------------------------------------------

class Notification {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.function = null;
      this.knowledge_item = null;
    }
    else {
      if (initObj.hasOwnProperty('function')) {
        this.function = initObj.function
      }
      else {
        this.function = 0;
      }
      if (initObj.hasOwnProperty('knowledge_item')) {
        this.knowledge_item = initObj.knowledge_item
      }
      else {
        this.knowledge_item = new KnowledgeItem();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Notification
    // Serialize message field [function]
    bufferOffset = _serializer.uint8(obj.function, buffer, bufferOffset);
    // Serialize message field [knowledge_item]
    bufferOffset = KnowledgeItem.serialize(obj.knowledge_item, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Notification
    let len;
    let data = new Notification(null);
    // Deserialize message field [function]
    data.function = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [knowledge_item]
    data.knowledge_item = KnowledgeItem.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += KnowledgeItem.getMessageSize(object.knowledge_item);
    return length + 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rosplan_knowledge_msgs/Notification';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'dc8b02687f0e69c5faf3b5fd16f19695';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint8 ADDED = 0
    uint8 REMOVED = 1
    
    uint8 function
    KnowledgeItem knowledge_item
    
    ================================================================================
    MSG: rosplan_knowledge_msgs/KnowledgeItem
    # A knowledge item used to represent a piece of the world model in ROSPlan
    uint8 INSTANCE = 0
    uint8 FACT = 1
    uint8 FUNCTION = 2
    
    uint8 knowledge_type
    
    # instance knowledge_type
    string instance_type
    string instance_name
    
    # attribute knowledge_type
    string attribute_name
    diagnostic_msgs/KeyValue[] values
    
    # function value
    float64 function_value
    
    # negative of positive
    bool is_negative
    ================================================================================
    MSG: diagnostic_msgs/KeyValue
    string key # what to label this value when viewing
    string value # a value to track over time
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Notification(null);
    if (msg.function !== undefined) {
      resolved.function = msg.function;
    }
    else {
      resolved.function = 0
    }

    if (msg.knowledge_item !== undefined) {
      resolved.knowledge_item = KnowledgeItem.Resolve(msg.knowledge_item)
    }
    else {
      resolved.knowledge_item = new KnowledgeItem()
    }

    return resolved;
    }
};

// Constants for message
Notification.Constants = {
  ADDED: 0,
  REMOVED: 1,
}

module.exports = Notification;
