
"use strict";

let MongoQueryMsg = require('./MongoQueryMsg.js')
let MongoUpdateMsg = require('./MongoUpdateMsg.js')
let MongoInsertMsg = require('./MongoInsertMsg.js')
let MongoQuerywithProjectionMsg = require('./MongoQuerywithProjectionMsg.js')
let MongoDeleteMsg = require('./MongoDeleteMsg.js')

module.exports = {
  MongoQueryMsg: MongoQueryMsg,
  MongoUpdateMsg: MongoUpdateMsg,
  MongoInsertMsg: MongoInsertMsg,
  MongoQuerywithProjectionMsg: MongoQuerywithProjectionMsg,
  MongoDeleteMsg: MongoDeleteMsg,
};
