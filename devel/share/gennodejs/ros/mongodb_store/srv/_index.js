
"use strict";

let MongoInsert = require('./MongoInsert.js')
let GetParam = require('./GetParam.js')
let MongoFind = require('./MongoFind.js')
let SetParam = require('./SetParam.js')
let MongoUpdate = require('./MongoUpdate.js')

module.exports = {
  MongoInsert: MongoInsert,
  GetParam: GetParam,
  MongoFind: MongoFind,
  SetParam: SetParam,
  MongoUpdate: MongoUpdate,
};
