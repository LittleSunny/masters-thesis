// Auto-generated. Do not edit!

// (in-package rosplan_dispatch_msgs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class PlanningServiceRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.domain_path = null;
      this.problem_path = null;
      this.data_path = null;
      this.planner_command = null;
    }
    else {
      if (initObj.hasOwnProperty('domain_path')) {
        this.domain_path = initObj.domain_path
      }
      else {
        this.domain_path = '';
      }
      if (initObj.hasOwnProperty('problem_path')) {
        this.problem_path = initObj.problem_path
      }
      else {
        this.problem_path = '';
      }
      if (initObj.hasOwnProperty('data_path')) {
        this.data_path = initObj.data_path
      }
      else {
        this.data_path = '';
      }
      if (initObj.hasOwnProperty('planner_command')) {
        this.planner_command = initObj.planner_command
      }
      else {
        this.planner_command = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PlanningServiceRequest
    // Serialize message field [domain_path]
    bufferOffset = _serializer.string(obj.domain_path, buffer, bufferOffset);
    // Serialize message field [problem_path]
    bufferOffset = _serializer.string(obj.problem_path, buffer, bufferOffset);
    // Serialize message field [data_path]
    bufferOffset = _serializer.string(obj.data_path, buffer, bufferOffset);
    // Serialize message field [planner_command]
    bufferOffset = _serializer.string(obj.planner_command, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PlanningServiceRequest
    let len;
    let data = new PlanningServiceRequest(null);
    // Deserialize message field [domain_path]
    data.domain_path = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [problem_path]
    data.problem_path = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [data_path]
    data.data_path = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [planner_command]
    data.planner_command = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.domain_path.length;
    length += object.problem_path.length;
    length += object.data_path.length;
    length += object.planner_command.length;
    return length + 16;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_dispatch_msgs/PlanningServiceRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b7b29f3dae421b04c9202694bb71079b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    string domain_path
    string problem_path
    string data_path
    string planner_command
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PlanningServiceRequest(null);
    if (msg.domain_path !== undefined) {
      resolved.domain_path = msg.domain_path;
    }
    else {
      resolved.domain_path = ''
    }

    if (msg.problem_path !== undefined) {
      resolved.problem_path = msg.problem_path;
    }
    else {
      resolved.problem_path = ''
    }

    if (msg.data_path !== undefined) {
      resolved.data_path = msg.data_path;
    }
    else {
      resolved.data_path = ''
    }

    if (msg.planner_command !== undefined) {
      resolved.planner_command = msg.planner_command;
    }
    else {
      resolved.planner_command = ''
    }

    return resolved;
    }
};

class PlanningServiceResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
    }
    else {
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PlanningServiceResponse
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PlanningServiceResponse
    let len;
    let data = new PlanningServiceResponse(null);
    return data;
  }

  static getMessageSize(object) {
    return 0;
  }

  static datatype() {
    // Returns string type for a service object
    return 'rosplan_dispatch_msgs/PlanningServiceResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd41d8cd98f00b204e9800998ecf8427e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PlanningServiceResponse(null);
    return resolved;
    }
};

module.exports = {
  Request: PlanningServiceRequest,
  Response: PlanningServiceResponse,
  md5sum() { return 'b7b29f3dae421b04c9202694bb71079b'; },
  datatype() { return 'rosplan_dispatch_msgs/PlanningService'; }
};
