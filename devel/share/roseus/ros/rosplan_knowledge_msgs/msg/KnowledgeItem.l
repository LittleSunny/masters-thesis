;; Auto-generated. Do not edit!


(when (boundp 'rosplan_knowledge_msgs::KnowledgeItem)
  (if (not (find-package "ROSPLAN_KNOWLEDGE_MSGS"))
    (make-package "ROSPLAN_KNOWLEDGE_MSGS"))
  (shadow 'KnowledgeItem (find-package "ROSPLAN_KNOWLEDGE_MSGS")))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM"))

(in-package "ROS")
;;//! \htmlinclude KnowledgeItem.msg.html
(if (not (find-package "DIAGNOSTIC_MSGS"))
  (ros::roseus-add-msgs "diagnostic_msgs"))


(intern "*INSTANCE*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM"))
(shadow '*INSTANCE* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM"))
(defconstant rosplan_knowledge_msgs::KnowledgeItem::*INSTANCE* 0)
(intern "*FACT*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM"))
(shadow '*FACT* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM"))
(defconstant rosplan_knowledge_msgs::KnowledgeItem::*FACT* 1)
(intern "*FUNCTION*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM"))
(shadow '*FUNCTION* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEITEM"))
(defconstant rosplan_knowledge_msgs::KnowledgeItem::*FUNCTION* 2)
(defclass rosplan_knowledge_msgs::KnowledgeItem
  :super ros::object
  :slots (_knowledge_type _instance_type _instance_name _attribute_name _values _function_value _is_negative ))

(defmethod rosplan_knowledge_msgs::KnowledgeItem
  (:init
   (&key
    ((:knowledge_type __knowledge_type) 0)
    ((:instance_type __instance_type) "")
    ((:instance_name __instance_name) "")
    ((:attribute_name __attribute_name) "")
    ((:values __values) (let (r) (dotimes (i 0) (push (instance diagnostic_msgs::KeyValue :init) r)) r))
    ((:function_value __function_value) 0.0)
    ((:is_negative __is_negative) nil)
    )
   (send-super :init)
   (setq _knowledge_type (round __knowledge_type))
   (setq _instance_type (string __instance_type))
   (setq _instance_name (string __instance_name))
   (setq _attribute_name (string __attribute_name))
   (setq _values __values)
   (setq _function_value (float __function_value))
   (setq _is_negative __is_negative)
   self)
  (:knowledge_type
   (&optional __knowledge_type)
   (if __knowledge_type (setq _knowledge_type __knowledge_type)) _knowledge_type)
  (:instance_type
   (&optional __instance_type)
   (if __instance_type (setq _instance_type __instance_type)) _instance_type)
  (:instance_name
   (&optional __instance_name)
   (if __instance_name (setq _instance_name __instance_name)) _instance_name)
  (:attribute_name
   (&optional __attribute_name)
   (if __attribute_name (setq _attribute_name __attribute_name)) _attribute_name)
  (:values
   (&rest __values)
   (if (keywordp (car __values))
       (send* _values __values)
     (progn
       (if __values (setq _values (car __values)))
       _values)))
  (:function_value
   (&optional __function_value)
   (if __function_value (setq _function_value __function_value)) _function_value)
  (:is_negative
   (&optional __is_negative)
   (if __is_negative (setq _is_negative __is_negative)) _is_negative)
  (:serialization-length
   ()
   (+
    ;; uint8 _knowledge_type
    1
    ;; string _instance_type
    4 (length _instance_type)
    ;; string _instance_name
    4 (length _instance_name)
    ;; string _attribute_name
    4 (length _attribute_name)
    ;; diagnostic_msgs/KeyValue[] _values
    (apply #'+ (send-all _values :serialization-length)) 4
    ;; float64 _function_value
    8
    ;; bool _is_negative
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _knowledge_type
       (write-byte _knowledge_type s)
     ;; string _instance_type
       (write-long (length _instance_type) s) (princ _instance_type s)
     ;; string _instance_name
       (write-long (length _instance_name) s) (princ _instance_name s)
     ;; string _attribute_name
       (write-long (length _attribute_name) s) (princ _attribute_name s)
     ;; diagnostic_msgs/KeyValue[] _values
     (write-long (length _values) s)
     (dolist (elem _values)
       (send elem :serialize s)
       )
     ;; float64 _function_value
       (sys::poke _function_value (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; bool _is_negative
       (if _is_negative (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _knowledge_type
     (setq _knowledge_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; string _instance_type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _instance_type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _instance_name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _instance_name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _attribute_name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _attribute_name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; diagnostic_msgs/KeyValue[] _values
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _values (let (r) (dotimes (i n) (push (instance diagnostic_msgs::KeyValue :init) r)) r))
     (dolist (elem- _values)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; float64 _function_value
     (setq _function_value (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; bool _is_negative
     (setq _is_negative (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get rosplan_knowledge_msgs::KnowledgeItem :md5sum-) "a4264640d228a4b57e9b41de1d4d7474")
(setf (get rosplan_knowledge_msgs::KnowledgeItem :datatype-) "rosplan_knowledge_msgs/KnowledgeItem")
(setf (get rosplan_knowledge_msgs::KnowledgeItem :definition-)
      "# A knowledge item used to represent a piece of the world model in ROSPlan
uint8 INSTANCE = 0
uint8 FACT = 1
uint8 FUNCTION = 2

uint8 knowledge_type

# instance knowledge_type
string instance_type
string instance_name

# attribute knowledge_type
string attribute_name
diagnostic_msgs/KeyValue[] values

# function value
float64 function_value

# negative of positive
bool is_negative
================================================================================
MSG: diagnostic_msgs/KeyValue
string key # what to label this value when viewing
string value # a value to track over time

")



(provide :rosplan_knowledge_msgs/KnowledgeItem "a4264640d228a4b57e9b41de1d4d7474")


