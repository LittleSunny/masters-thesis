;; Auto-generated. Do not edit!


(when (boundp 'rosplan_knowledge_msgs::Filter)
  (if (not (find-package "ROSPLAN_KNOWLEDGE_MSGS"))
    (make-package "ROSPLAN_KNOWLEDGE_MSGS"))
  (shadow 'Filter (find-package "ROSPLAN_KNOWLEDGE_MSGS")))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER"))

(in-package "ROS")
;;//! \htmlinclude Filter.msg.html


(intern "*CLEAR*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER"))
(shadow '*CLEAR* (find-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER"))
(defconstant rosplan_knowledge_msgs::Filter::*CLEAR* 0)
(intern "*ADD*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER"))
(shadow '*ADD* (find-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER"))
(defconstant rosplan_knowledge_msgs::Filter::*ADD* 1)
(intern "*REMOVE*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER"))
(shadow '*REMOVE* (find-package "ROSPLAN_KNOWLEDGE_MSGS::FILTER"))
(defconstant rosplan_knowledge_msgs::Filter::*REMOVE* 2)
(defclass rosplan_knowledge_msgs::Filter
  :super ros::object
  :slots (_function _knowledge_items ))

(defmethod rosplan_knowledge_msgs::Filter
  (:init
   (&key
    ((:function __function) 0)
    ((:knowledge_items __knowledge_items) (let (r) (dotimes (i 0) (push (instance rosplan_knowledge_msgs::KnowledgeItem :init) r)) r))
    )
   (send-super :init)
   (setq _function (round __function))
   (setq _knowledge_items __knowledge_items)
   self)
  (:function
   (&optional __function)
   (if __function (setq _function __function)) _function)
  (:knowledge_items
   (&rest __knowledge_items)
   (if (keywordp (car __knowledge_items))
       (send* _knowledge_items __knowledge_items)
     (progn
       (if __knowledge_items (setq _knowledge_items (car __knowledge_items)))
       _knowledge_items)))
  (:serialization-length
   ()
   (+
    ;; uint8 _function
    1
    ;; rosplan_knowledge_msgs/KnowledgeItem[] _knowledge_items
    (apply #'+ (send-all _knowledge_items :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _function
       (write-byte _function s)
     ;; rosplan_knowledge_msgs/KnowledgeItem[] _knowledge_items
     (write-long (length _knowledge_items) s)
     (dolist (elem _knowledge_items)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _function
     (setq _function (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; rosplan_knowledge_msgs/KnowledgeItem[] _knowledge_items
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _knowledge_items (let (r) (dotimes (i n) (push (instance rosplan_knowledge_msgs::KnowledgeItem :init) r)) r))
     (dolist (elem- _knowledge_items)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get rosplan_knowledge_msgs::Filter :md5sum-) "ffaa5312f028f19664069486a077b599")
(setf (get rosplan_knowledge_msgs::Filter :datatype-) "rosplan_knowledge_msgs/Filter")
(setf (get rosplan_knowledge_msgs::Filter :definition-)
      "uint8 CLEAR = 0
uint8 ADD = 1
uint8 REMOVE = 2

uint8 function
KnowledgeItem[] knowledge_items

================================================================================
MSG: rosplan_knowledge_msgs/KnowledgeItem
# A knowledge item used to represent a piece of the world model in ROSPlan
uint8 INSTANCE = 0
uint8 FACT = 1
uint8 FUNCTION = 2

uint8 knowledge_type

# instance knowledge_type
string instance_type
string instance_name

# attribute knowledge_type
string attribute_name
diagnostic_msgs/KeyValue[] values

# function value
float64 function_value

# negative of positive
bool is_negative
================================================================================
MSG: diagnostic_msgs/KeyValue
string key # what to label this value when viewing
string value # a value to track over time

")



(provide :rosplan_knowledge_msgs/Filter "ffaa5312f028f19664069486a077b599")


