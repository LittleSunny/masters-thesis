;; Auto-generated. Do not edit!


(when (boundp 'rosplan_knowledge_msgs::Notification)
  (if (not (find-package "ROSPLAN_KNOWLEDGE_MSGS"))
    (make-package "ROSPLAN_KNOWLEDGE_MSGS"))
  (shadow 'Notification (find-package "ROSPLAN_KNOWLEDGE_MSGS")))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::NOTIFICATION")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::NOTIFICATION"))

(in-package "ROS")
;;//! \htmlinclude Notification.msg.html


(intern "*ADDED*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::NOTIFICATION"))
(shadow '*ADDED* (find-package "ROSPLAN_KNOWLEDGE_MSGS::NOTIFICATION"))
(defconstant rosplan_knowledge_msgs::Notification::*ADDED* 0)
(intern "*REMOVED*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::NOTIFICATION"))
(shadow '*REMOVED* (find-package "ROSPLAN_KNOWLEDGE_MSGS::NOTIFICATION"))
(defconstant rosplan_knowledge_msgs::Notification::*REMOVED* 1)
(defclass rosplan_knowledge_msgs::Notification
  :super ros::object
  :slots (_function _knowledge_item ))

(defmethod rosplan_knowledge_msgs::Notification
  (:init
   (&key
    ((:function __function) 0)
    ((:knowledge_item __knowledge_item) (instance rosplan_knowledge_msgs::KnowledgeItem :init))
    )
   (send-super :init)
   (setq _function (round __function))
   (setq _knowledge_item __knowledge_item)
   self)
  (:function
   (&optional __function)
   (if __function (setq _function __function)) _function)
  (:knowledge_item
   (&rest __knowledge_item)
   (if (keywordp (car __knowledge_item))
       (send* _knowledge_item __knowledge_item)
     (progn
       (if __knowledge_item (setq _knowledge_item (car __knowledge_item)))
       _knowledge_item)))
  (:serialization-length
   ()
   (+
    ;; uint8 _function
    1
    ;; rosplan_knowledge_msgs/KnowledgeItem _knowledge_item
    (send _knowledge_item :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _function
       (write-byte _function s)
     ;; rosplan_knowledge_msgs/KnowledgeItem _knowledge_item
       (send _knowledge_item :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _function
     (setq _function (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; rosplan_knowledge_msgs/KnowledgeItem _knowledge_item
     (send _knowledge_item :deserialize buf ptr-) (incf ptr- (send _knowledge_item :serialization-length))
   ;;
   self)
  )

(setf (get rosplan_knowledge_msgs::Notification :md5sum-) "dc8b02687f0e69c5faf3b5fd16f19695")
(setf (get rosplan_knowledge_msgs::Notification :datatype-) "rosplan_knowledge_msgs/Notification")
(setf (get rosplan_knowledge_msgs::Notification :definition-)
      "uint8 ADDED = 0
uint8 REMOVED = 1

uint8 function
KnowledgeItem knowledge_item

================================================================================
MSG: rosplan_knowledge_msgs/KnowledgeItem
# A knowledge item used to represent a piece of the world model in ROSPlan
uint8 INSTANCE = 0
uint8 FACT = 1
uint8 FUNCTION = 2

uint8 knowledge_type

# instance knowledge_type
string instance_type
string instance_name

# attribute knowledge_type
string attribute_name
diagnostic_msgs/KeyValue[] values

# function value
float64 function_value

# negative of positive
bool is_negative
================================================================================
MSG: diagnostic_msgs/KeyValue
string key # what to label this value when viewing
string value # a value to track over time

")



(provide :rosplan_knowledge_msgs/Notification "dc8b02687f0e69c5faf3b5fd16f19695")


