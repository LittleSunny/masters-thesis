;; Auto-generated. Do not edit!


(when (boundp 'rosplan_knowledge_msgs::KnowledgeUpdateService)
  (if (not (find-package "ROSPLAN_KNOWLEDGE_MSGS"))
    (make-package "ROSPLAN_KNOWLEDGE_MSGS"))
  (shadow 'KnowledgeUpdateService (find-package "ROSPLAN_KNOWLEDGE_MSGS")))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICE")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICE"))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICERESPONSE")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICERESPONSE"))

(in-package "ROS")





(intern "*ADD_KNOWLEDGE*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(shadow '*ADD_KNOWLEDGE* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest::*ADD_KNOWLEDGE* 0)
(intern "*ADD_GOAL*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(shadow '*ADD_GOAL* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest::*ADD_GOAL* 1)
(intern "*REMOVE_KNOWLEDGE*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(shadow '*REMOVE_KNOWLEDGE* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest::*REMOVE_KNOWLEDGE* 2)
(intern "*REMOVE_GOAL*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(shadow '*REMOVE_GOAL* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest::*REMOVE_GOAL* 3)
(defclass rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest
  :super ros::object
  :slots (_update_type _knowledge ))

(defmethod rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest
  (:init
   (&key
    ((:update_type __update_type) 0)
    ((:knowledge __knowledge) (instance rosplan_knowledge_msgs::KnowledgeItem :init))
    )
   (send-super :init)
   (setq _update_type (round __update_type))
   (setq _knowledge __knowledge)
   self)
  (:update_type
   (&optional __update_type)
   (if __update_type (setq _update_type __update_type)) _update_type)
  (:knowledge
   (&rest __knowledge)
   (if (keywordp (car __knowledge))
       (send* _knowledge __knowledge)
     (progn
       (if __knowledge (setq _knowledge (car __knowledge)))
       _knowledge)))
  (:serialization-length
   ()
   (+
    ;; uint8 _update_type
    1
    ;; rosplan_knowledge_msgs/KnowledgeItem _knowledge
    (send _knowledge :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _update_type
       (write-byte _update_type s)
     ;; rosplan_knowledge_msgs/KnowledgeItem _knowledge
       (send _knowledge :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _update_type
     (setq _update_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; rosplan_knowledge_msgs/KnowledgeItem _knowledge
     (send _knowledge :deserialize buf ptr-) (incf ptr- (send _knowledge :serialization-length))
   ;;
   self)
  )

(defclass rosplan_knowledge_msgs::KnowledgeUpdateServiceResponse
  :super ros::object
  :slots (_success ))

(defmethod rosplan_knowledge_msgs::KnowledgeUpdateServiceResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional __success)
   (if __success (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass rosplan_knowledge_msgs::KnowledgeUpdateService
  :super ros::object
  :slots ())

(setf (get rosplan_knowledge_msgs::KnowledgeUpdateService :md5sum-) "837765eff2c5aff85dc1654a7e74e555")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateService :datatype-) "rosplan_knowledge_msgs/KnowledgeUpdateService")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateService :request) rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest)
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateService :response) rosplan_knowledge_msgs::KnowledgeUpdateServiceResponse)

(defmethod rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest
  (:response () (instance rosplan_knowledge_msgs::KnowledgeUpdateServiceResponse :init)))

(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest :md5sum-) "837765eff2c5aff85dc1654a7e74e555")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest :datatype-) "rosplan_knowledge_msgs/KnowledgeUpdateServiceRequest")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceRequest :definition-)
      "

uint8 ADD_KNOWLEDGE = 0
uint8 ADD_GOAL = 1
uint8 REMOVE_KNOWLEDGE = 2
uint8 REMOVE_GOAL = 3

uint8 update_type

rosplan_knowledge_msgs/KnowledgeItem knowledge

================================================================================
MSG: rosplan_knowledge_msgs/KnowledgeItem
# A knowledge item used to represent a piece of the world model in ROSPlan
uint8 INSTANCE = 0
uint8 FACT = 1
uint8 FUNCTION = 2

uint8 knowledge_type

# instance knowledge_type
string instance_type
string instance_name

# attribute knowledge_type
string attribute_name
diagnostic_msgs/KeyValue[] values

# function value
float64 function_value

# negative of positive
bool is_negative
================================================================================
MSG: diagnostic_msgs/KeyValue
string key # what to label this value when viewing
string value # a value to track over time
---
bool success

")

(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceResponse :md5sum-) "837765eff2c5aff85dc1654a7e74e555")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceResponse :datatype-) "rosplan_knowledge_msgs/KnowledgeUpdateServiceResponse")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceResponse :definition-)
      "

uint8 ADD_KNOWLEDGE = 0
uint8 ADD_GOAL = 1
uint8 REMOVE_KNOWLEDGE = 2
uint8 REMOVE_GOAL = 3

uint8 update_type

rosplan_knowledge_msgs/KnowledgeItem knowledge

================================================================================
MSG: rosplan_knowledge_msgs/KnowledgeItem
# A knowledge item used to represent a piece of the world model in ROSPlan
uint8 INSTANCE = 0
uint8 FACT = 1
uint8 FUNCTION = 2

uint8 knowledge_type

# instance knowledge_type
string instance_type
string instance_name

# attribute knowledge_type
string attribute_name
diagnostic_msgs/KeyValue[] values

# function value
float64 function_value

# negative of positive
bool is_negative
================================================================================
MSG: diagnostic_msgs/KeyValue
string key # what to label this value when viewing
string value # a value to track over time
---
bool success

")



(provide :rosplan_knowledge_msgs/KnowledgeUpdateService "837765eff2c5aff85dc1654a7e74e555")


