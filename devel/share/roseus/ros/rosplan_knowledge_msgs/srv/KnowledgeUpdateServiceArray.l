;; Auto-generated. Do not edit!


(when (boundp 'rosplan_knowledge_msgs::KnowledgeUpdateServiceArray)
  (if (not (find-package "ROSPLAN_KNOWLEDGE_MSGS"))
    (make-package "ROSPLAN_KNOWLEDGE_MSGS"))
  (shadow 'KnowledgeUpdateServiceArray (find-package "ROSPLAN_KNOWLEDGE_MSGS")))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAY")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAY"))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(unless (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYRESPONSE")
  (make-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYRESPONSE"))

(in-package "ROS")





(intern "*ADD_KNOWLEDGE*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(shadow '*ADD_KNOWLEDGE* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest::*ADD_KNOWLEDGE* 0)
(intern "*ADD_GOAL*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(shadow '*ADD_GOAL* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest::*ADD_GOAL* 1)
(intern "*REMOVE_KNOWLEDGE*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(shadow '*REMOVE_KNOWLEDGE* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest::*REMOVE_KNOWLEDGE* 2)
(intern "*REMOVE_GOAL*" (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(shadow '*REMOVE_GOAL* (find-package "ROSPLAN_KNOWLEDGE_MSGS::KNOWLEDGEUPDATESERVICEARRAYREQUEST"))
(defconstant rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest::*REMOVE_GOAL* 3)
(defclass rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest
  :super ros::object
  :slots (_update_type _knowledge ))

(defmethod rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest
  (:init
   (&key
    ((:update_type __update_type) 0)
    ((:knowledge __knowledge) (let (r) (dotimes (i 0) (push (instance rosplan_knowledge_msgs::KnowledgeItem :init) r)) r))
    )
   (send-super :init)
   (setq _update_type (round __update_type))
   (setq _knowledge __knowledge)
   self)
  (:update_type
   (&optional __update_type)
   (if __update_type (setq _update_type __update_type)) _update_type)
  (:knowledge
   (&rest __knowledge)
   (if (keywordp (car __knowledge))
       (send* _knowledge __knowledge)
     (progn
       (if __knowledge (setq _knowledge (car __knowledge)))
       _knowledge)))
  (:serialization-length
   ()
   (+
    ;; uint8 _update_type
    1
    ;; rosplan_knowledge_msgs/KnowledgeItem[] _knowledge
    (apply #'+ (send-all _knowledge :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _update_type
       (write-byte _update_type s)
     ;; rosplan_knowledge_msgs/KnowledgeItem[] _knowledge
     (write-long (length _knowledge) s)
     (dolist (elem _knowledge)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _update_type
     (setq _update_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; rosplan_knowledge_msgs/KnowledgeItem[] _knowledge
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _knowledge (let (r) (dotimes (i n) (push (instance rosplan_knowledge_msgs::KnowledgeItem :init) r)) r))
     (dolist (elem- _knowledge)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(defclass rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayResponse
  :super ros::object
  :slots (_success ))

(defmethod rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional __success)
   (if __success (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass rosplan_knowledge_msgs::KnowledgeUpdateServiceArray
  :super ros::object
  :slots ())

(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArray :md5sum-) "837765eff2c5aff85dc1654a7e74e555")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArray :datatype-) "rosplan_knowledge_msgs/KnowledgeUpdateServiceArray")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArray :request) rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest)
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArray :response) rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayResponse)

(defmethod rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest
  (:response () (instance rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayResponse :init)))

(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest :md5sum-) "837765eff2c5aff85dc1654a7e74e555")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest :datatype-) "rosplan_knowledge_msgs/KnowledgeUpdateServiceArrayRequest")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayRequest :definition-)
      "

uint8 ADD_KNOWLEDGE = 0
uint8 ADD_GOAL = 1
uint8 REMOVE_KNOWLEDGE = 2
uint8 REMOVE_GOAL = 3

uint8 update_type

rosplan_knowledge_msgs/KnowledgeItem[] knowledge

================================================================================
MSG: rosplan_knowledge_msgs/KnowledgeItem
# A knowledge item used to represent a piece of the world model in ROSPlan
uint8 INSTANCE = 0
uint8 FACT = 1
uint8 FUNCTION = 2

uint8 knowledge_type

# instance knowledge_type
string instance_type
string instance_name

# attribute knowledge_type
string attribute_name
diagnostic_msgs/KeyValue[] values

# function value
float64 function_value

# negative of positive
bool is_negative
================================================================================
MSG: diagnostic_msgs/KeyValue
string key # what to label this value when viewing
string value # a value to track over time
---
bool success

")

(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayResponse :md5sum-) "837765eff2c5aff85dc1654a7e74e555")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayResponse :datatype-) "rosplan_knowledge_msgs/KnowledgeUpdateServiceArrayResponse")
(setf (get rosplan_knowledge_msgs::KnowledgeUpdateServiceArrayResponse :definition-)
      "

uint8 ADD_KNOWLEDGE = 0
uint8 ADD_GOAL = 1
uint8 REMOVE_KNOWLEDGE = 2
uint8 REMOVE_GOAL = 3

uint8 update_type

rosplan_knowledge_msgs/KnowledgeItem[] knowledge

================================================================================
MSG: rosplan_knowledge_msgs/KnowledgeItem
# A knowledge item used to represent a piece of the world model in ROSPlan
uint8 INSTANCE = 0
uint8 FACT = 1
uint8 FUNCTION = 2

uint8 knowledge_type

# instance knowledge_type
string instance_type
string instance_name

# attribute knowledge_type
string attribute_name
diagnostic_msgs/KeyValue[] values

# function value
float64 function_value

# negative of positive
bool is_negative
================================================================================
MSG: diagnostic_msgs/KeyValue
string key # what to label this value when viewing
string value # a value to track over time
---
bool success

")



(provide :rosplan_knowledge_msgs/KnowledgeUpdateServiceArray "837765eff2c5aff85dc1654a7e74e555")


