collect_covariance.txt
collect_plans.txt
domain_pred_update_RRT.cpp
domain_pred_update_RRT.errors
domain_pred_update_RRT.goals
domain_pred_update_RRT.h
domain_pred_update_RRT.m
domain_pred_update_RRT.pddl
domain_pred_update_RRT_planner
domain_pred_update_RRT.properties
domain_pred_update_RRT.startstates
domain_pred_update_RRT.transitions
final_states.txt
landmark.txt
plan_14230625
plan_exec.pddl
plan.pddl
problem_pred_update_RRT.pddl
problem_pred_update_RRT_plan.pddl
ROStrajectory.txt
waypoints_rrt.txt

======================================================

DiNo version 1.1
Discretised Nonlinear Heuristic Planner for PDDL+ models with continous processes and events

Copyright (C) 2015
W. Piotrowski, M. Fox, D. Long, D. Magazzeni, F. Mercorio

Call with the -c flag or read the license file for terms
and conditions of use.
Run this program with "-h" for the list of options.
Send bug reports and comments to wiktor.piotrowski@kcl.ac.uk

======================================================

======================================================
Planning configuration

* Source domain: /home/sunny/catkin_ws/Tests/LTA-2018/case_2/domain_pred_update_RRT.pddl
* Source problem: /home/sunny/catkin_ws/Tests/LTA-2018/case_2/problem_pred_update_RRT.pddl
* Planning Mode: Feasible Plan
* SRPG time horizon: 20
* Output format: PDDL+
* Epsilon separation: 0.001
* Output target: "/home/sunny/catkin_ws/Tests/LTA-2018/case_2/problem_pred_update_RRT_plan.pddl"

* Model: /home/sunny/catkin_ws/Tests/LTA-2018/case_2/domain_pred_update_RRT
* State size 93429 bits (rounded to 11680 bytes).
* Allocated memory: 1000 Megabytes

**  Time Discretisation = 1.000000
**  Digits for representing the integer part of a real =  5.000000
**  Digits for representing the fractional part of a real =  4
======================================================
DEBUG SUNNY EXTERNAL
SUNNY DEBUG this is external solver name /home/sunny/catkin_ws/DiNo/ex/ext_lib/build/libext_lib.so
/home/sunny/catkin_ws/Tests/LTA-2018/case_2/waypoints_rrt.txt
/home/sunny/catkin_ws/Tests/LTA-2018/case_2/landmark.txt
/home/sunny/catkin_ws/Tests/LTA-2018/case_2/problem_pred_update_RRT.pddl

=== Analyzing model... ===============================

* State Space Expansion Algorithm: SRPG+.
  with symmetry algorithm 3 -- Heuristic Small Memory Normalization
  with permutation trial limit 10.
* Maximum SRPG time horizon: 20.
* Maximum size of the state space: 112061 states.
  with states hash-compressed to 40 bits.

START H VAL: 7.000
0

======================================================

Model exploration complete (in 468.68 seconds).
	8224 actions fired
	1 start states
	6934 reachable states
	1 goals found


=== Building model dynamics... =======================

* Transition Graph mode: Memory Image
* Maximum size of graph: 112046 transitions.


======================================================

Model dynamics rebuilding complete (in 468.76 seconds).
	6934 states
	8224 transitions
	out degree: min 0 max 7 avg 1.19


=== Finding control paths... =========================

* Search Algorithm: Feasible Plan.


======================================================

Control paths calculation complete (in 468.76 seconds).
	6934 states
	25 controllable


=== Collecting plans... ==============================

thisi is in build_plan_from
PLAN: S0-R91-S4-R0-S15-R0-S16-R0-S17-R0-S18-R0-S19-R0-S20-R0-S21-R0-S22-R0-S23-R0-S24-R11-S25-R0-S28-R0-S197-R458-S409-R0-S685-R0-S1026-R0-S1493-R0-S2098-R18-S2813-R721-S4581-R0-S6929-R0-S6930-R0-S6931-R1-S6932
external_collect_plans

======================================================

Plan(s) generation complete (in 468.80 seconds).
	1 plans
	plan length (actions): min 25 max 25 avg 25.00
	plan duration (time): min 0 max 19 avg 19.00
	plan weight: min 0 max 19 avg 19.00


=== Writing final results... =========================

* Output format: PDDL+
* Output target: "/home/sunny/catkin_ws/Tests/LTA-2018/case_2/problem_pred_update_RRT_plan.pddl".


======================================================

Results Written (in 484.26 seconds).


