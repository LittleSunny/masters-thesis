domain: file "/home/sunny/catkin_ws/Tests/LTA-2018/case_2/domain_pred_update_RRT.pddl"
problem: file "/home/sunny/catkin_ws/Tests/LTA-2018/case_2/problem_pred_update_RRT.pddl"
message: " Time Discretisation = 1.000000"
message: " Digits for representing the integer part of a real =  5.000000"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(10,4);
	integer: -1000..1000;

	 TIME_type: real(12,7);

	covariance : Enum {dummy1,dummy2};
	robot : Enum {kenny};
	waypoint : Enum {endpoint,wp0,wp1,wp10,wp11,wp12,wp13,wp14,wp15,wp16,wp17,wp18,wp19,wp2,wp20,wp21,wp22,wp23,wp24,wp25,wp26,wp27,wp28,wp29,wp3,wp30,wp31,wp32,wp33,wp34,wp35,wp36,wp37,wp38,wp4,wp5,wp6,wp7,wp8,wp9};

const 
	 T:1.000000;

	dfactor : 1.00000;
	finaltrace : 0.600000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	counter[pddlname:"counter";] :  real_type;
	cov[pddlname:"cov";] :  real_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	predict_covariance[pddlname:"predict_covariance";] :  real_type;
	relatived[pddlname:"relatived";] :  real_type;
	update_covariance[pddlname:"update_covariance";] :  real_type;


	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;
	observe[pddlname: "observe";] :  boolean;
	moving[pddlname: "moving";] : Array [robot] of Array [waypoint] of  boolean;
	connected[pddlname: "connected";] : Array [waypoint] of Array [waypoint] of  boolean;
	lessthan[pddlname: "lessthan";] : Array [covariance] of Array [covariance] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun assign_cov_event_predict_n_update(update_covariance : real_type ; ): real_type "/home/sunny/catkin_ws/Tests/LTA-2018/case_2/domain_pred_update_RRT.h" ;
externfun assign_counter_event_predict_n_update(): real_type ;
externfun increase_counter_process_control(counter : real_type ; T : real_type ; ): real_type ;
externfun decrease_relatived_process_control(relatived : real_type ; T : real_type ; dfactor : real_type ; ): real_type ;
externfun assign_relatived_action_goto_waypoint(distance : real_type ; ): real_type ;
externfun assign_counter_action_goto_waypoint(): real_type ;
externfun increase_update_covariance_action_goto_waypoint(update_covariance : real_type ; ): real_type ;
externfun increase_predict_covariance_action_goto_waypoint(predict_covariance : real_type ; ): real_type ;
procedure set_robot_at( r : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[r][wp] := value;
END;

function get_robot_at( r : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[r][wp];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;

procedure set_observe(  value : boolean);
BEGIN
	observe := value;
END;

function get_observe(): boolean;
BEGIN
	return 	observe;
END;

procedure set_moving( r : robot ; to_ : waypoint ;  value : boolean);
BEGIN
	moving[r][to_] := value;
END;

function get_moving( r : robot ; to_ : waypoint): boolean;
BEGIN
	return 	moving[r][to_];
END;

procedure set_connected( from : waypoint ; to_ : waypoint ;  value : boolean);
BEGIN
	connected[from][to_] := value;
END;

function get_connected( from : waypoint ; to_ : waypoint): boolean;
BEGIN
	return 	connected[from][to_];
END;

procedure set_lessthan( c : covariance ; f : covariance ;  value : boolean);
BEGIN
	lessthan[c][f] := value;
END;

function get_lessthan( c : covariance ; f : covariance): boolean;
BEGIN
	return 	lessthan[c][f];
END;









function predict_n_update () : boolean; 
BEGIN
IF (((( counter) > (0.00000)))) THEN 
cov := assign_cov_event_predict_n_update(update_covariance  );
counter := assign_counter_event_predict_n_update();
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;

procedure process_control (); 
BEGIN
IF (((( relatived) > (-dfactor)))) THEN 
counter := increase_counter_process_control(counter , T  );
relatived := decrease_relatived_process_control(relatived , T , dfactor  );

ENDIF ; 

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   predict_n_update_triggered :  boolean;
BEGIN
 event_triggered := true;
   predict_n_update_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! predict_n_update_triggered) then 
   predict_n_update_triggered := predict_n_update();
   event_triggered := event_triggered | predict_n_update_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_control_enabled :  boolean;
BEGIN
 process_updated := false; end_while := false;
   process_control_enabled := false;
while (!end_while) do 
    if (((( relatived) > (-dfactor))) &  !process_control_enabled) then
   process_updated := true;
   process_control();
   process_control_enabled := true;
   endif;

IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

ruleset r:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_waypoint " 
(robot_at[r][from]) & (observe) & (!(robot_at[r][to_])) & (!(visited[to_])) & (connected[from][to_]) ==> 
pddlname: " goto_waypoint"; 
BEGIN
moving[r][to_]:= true; 
robot_at[r][from]:= false; 
observe:= false; 
relatived := assign_relatived_action_goto_waypoint(distance[from][to_]  );
counter := assign_counter_action_goto_waypoint();
update_covariance := increase_update_covariance_action_goto_waypoint(update_covariance  );
predict_covariance := increase_predict_covariance_action_goto_waypoint(predict_covariance  );

END; 
END; 
END; 
END;

ruleset r:robot do 
 ruleset to_:waypoint do 
 action rule " reached " 
(moving[r][to_]) & ((( relatived) <= (0.00000))) ==> 
pddlname: " reached"; 
BEGIN
robot_at[r][to_]:= true; 
visited[to_]:= true; 
observe:= true; 
moving[r][to_]:= false; 

END; 
END; 
END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for r : robot do 
  for wp : waypoint do 
    set_robot_at(r,wp, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   set_observe(false);

   for r : robot do 
     for to_ : waypoint do 
       set_moving(r,to_, false);
END; END;  -- close for
   for from : waypoint do 
     for to_ : waypoint do 
       set_connected(from,to_, false);
END; END;  -- close for
   for c : covariance do 
     for f : covariance do 
       set_lessthan(c,f, false);
END; END;  -- close for
   for wp1 : waypoint do 
     for wp2 : waypoint do 
       distance[wp1][wp2] := 0.0 ;
END; END;  -- close for
   cov := 0.0 ;

   counter := 0.0 ;

   update_covariance := 0.0 ;

   predict_covariance := 0.0 ;

   relatived := 0.0 ;



connected[endpoint][wp11]:= true; 
connected[endpoint][wp17]:= true; 
connected[endpoint][wp18]:= true; 
connected[endpoint][wp19]:= true; 
connected[endpoint][wp20]:= true; 
connected[endpoint][wp21]:= true; 
connected[endpoint][wp23]:= true; 
connected[endpoint][wp25]:= true; 
connected[endpoint][wp29]:= true; 
connected[endpoint][wp30]:= true; 
connected[endpoint][wp33]:= true; 
connected[endpoint][wp34]:= true; 
connected[endpoint][wp35]:= true; 
connected[endpoint][wp36]:= true; 
connected[endpoint][wp6]:= true; 
connected[endpoint][wp8]:= true; 
connected[wp0][wp1]:= true; 
connected[wp0][wp5]:= true; 
connected[wp0][wp6]:= true; 
connected[wp0][wp12]:= true; 
connected[wp0][wp17]:= true; 
connected[wp0][wp19]:= true; 
connected[wp0][wp21]:= true; 
connected[wp0][wp29]:= true; 
connected[wp0][wp31]:= true; 
connected[wp0][wp33]:= true; 
connected[wp0][wp34]:= true; 
connected[wp0][wp36]:= true; 
connected[wp0][wp38]:= true; 
connected[wp1][wp0]:= true; 
connected[wp1][wp2]:= true; 
connected[wp1][wp9]:= true; 
connected[wp10][wp2]:= true; 
connected[wp10][wp27]:= true; 
connected[wp11][wp6]:= true; 
connected[wp11][wp13]:= true; 
connected[wp11][endpoint]:= true; 
connected[wp12][wp0]:= true; 
connected[wp13][wp11]:= true; 
connected[wp13][wp14]:= true; 
connected[wp13][wp16]:= true; 
connected[wp14][wp13]:= true; 
connected[wp14][wp15]:= true; 
connected[wp14][wp18]:= true; 
connected[wp14][wp26]:= true; 
connected[wp14][wp32]:= true; 
connected[wp15][wp14]:= true; 
connected[wp15][wp22]:= true; 
connected[wp16][wp13]:= true; 
connected[wp17][wp0]:= true; 
connected[wp17][wp20]:= true; 
connected[wp17][wp23]:= true; 
connected[wp17][wp30]:= true; 
connected[wp17][wp35]:= true; 
connected[wp17][endpoint]:= true; 
connected[wp18][wp14]:= true; 
connected[wp18][endpoint]:= true; 
connected[wp19][wp0]:= true; 
connected[wp19][endpoint]:= true; 
connected[wp2][wp1]:= true; 
connected[wp2][wp3]:= true; 
connected[wp2][wp4]:= true; 
connected[wp2][wp7]:= true; 
connected[wp2][wp10]:= true; 
connected[wp20][wp17]:= true; 
connected[wp20][endpoint]:= true; 
connected[wp21][wp0]:= true; 
connected[wp21][endpoint]:= true; 
connected[wp22][wp15]:= true; 
connected[wp23][wp17]:= true; 
connected[wp23][endpoint]:= true; 
connected[wp24][wp9]:= true; 
connected[wp25][wp3]:= true; 
connected[wp25][endpoint]:= true; 
connected[wp26][wp14]:= true; 
connected[wp27][wp10]:= true; 
connected[wp28][wp9]:= true; 
connected[wp29][wp0]:= true; 
connected[wp29][endpoint]:= true; 
connected[wp3][wp2]:= true; 
connected[wp3][wp8]:= true; 
connected[wp3][wp25]:= true; 
connected[wp3][wp37]:= true; 
connected[wp30][wp17]:= true; 
connected[wp30][endpoint]:= true; 
connected[wp31][wp0]:= true; 
connected[wp32][wp14]:= true; 
connected[wp33][wp0]:= true; 
connected[wp33][endpoint]:= true; 
connected[wp34][wp0]:= true; 
connected[wp34][endpoint]:= true; 
connected[wp35][wp17]:= true; 
connected[wp35][endpoint]:= true; 
connected[wp36][wp0]:= true; 
connected[wp36][endpoint]:= true; 
connected[wp37][wp3]:= true; 
connected[wp38][wp0]:= true; 
connected[wp4][wp2]:= true; 
connected[wp5][wp0]:= true; 
connected[wp6][wp0]:= true; 
connected[wp6][wp11]:= true; 
connected[wp6][endpoint]:= true; 
connected[wp7][wp2]:= true; 
connected[wp8][wp3]:= true; 
connected[wp8][endpoint]:= true; 
connected[wp9][wp1]:= true; 
connected[wp9][wp24]:= true; 
connected[wp9][wp28]:= true; 
observe:= true; 
robot_at[kenny][wp0]:= true; 
cov := 1.22000;
distance[endpoint][wp11] := 6.73647;
distance[endpoint][wp17] := 0.854400;
distance[endpoint][wp18] := 4.34885;
distance[endpoint][wp19] := 2.24555;
distance[endpoint][wp20] := 0.960469;
distance[endpoint][wp21] := 5.68177;
distance[endpoint][wp23] := 2.06155;
distance[endpoint][wp25] := 4.51221;
distance[endpoint][wp29] := 7.50000;
distance[endpoint][wp30] := 2.65801;
distance[endpoint][wp33] := 3.38009;
distance[endpoint][wp34] := 6.31269;
distance[endpoint][wp35] := 1.50083;
distance[endpoint][wp36] := 3.20663;
distance[endpoint][wp6] := 4.25000;
distance[endpoint][wp8] := 3.40918;
distance[wp0][wp1] := 2.46221;
distance[wp0][wp5] := 1.36565;
distance[wp0][wp6] := 6.20000;
distance[wp0][wp12] := 1.80000;
distance[wp0][wp17] := 9.65466;
distance[wp0][wp19] := 8.26226;
distance[wp0][wp21] := 4.83735;
distance[wp0][wp29] := 2.95000;
distance[wp0][wp31] := 1.04043;
distance[wp0][wp33] := 7.11425;
distance[wp0][wp34] := 4.16923;
distance[wp0][wp36] := 7.32462;
distance[wp0][wp38] := 3.71080;
distance[wp1][wp0] := 2.46221;
distance[wp1][wp2] := 1.27769;
distance[wp1][wp9] := 2.13600;
distance[wp10][wp2] := 8.00016;
distance[wp10][wp27] := 1.92354;
distance[wp11][wp6] := 2.54804;
distance[wp11][wp13] := 2.08387;
distance[wp11][endpoint] := 6.73647;
distance[wp12][wp0] := 1.80000;
distance[wp13][wp11] := 2.08387;
distance[wp13][wp14] := 1.00623;
distance[wp13][wp16] := 1.04043;
distance[wp14][wp13] := 1.00623;
distance[wp14][wp15] := 1.29035;
distance[wp14][wp18] := 4.85824;
distance[wp14][wp26] := 3.65000;
distance[wp14][wp32] := 2.46221;
distance[wp15][wp14] := 1.29035;
distance[wp15][wp22] := 1.39014;
distance[wp16][wp13] := 1.04043;
distance[wp17][wp0] := 9.65466;
distance[wp17][wp20] := 1.06888;
distance[wp17][wp23] := 2.20000;
distance[wp17][wp30] := 2.25056;
distance[wp17][wp35] := 1.00125;
distance[wp17][endpoint] := 0.854400;
distance[wp18][wp14] := 4.85824;
distance[wp18][endpoint] := 4.34885;
distance[wp19][wp0] := 8.26226;
distance[wp19][endpoint] := 2.24555;
distance[wp2][wp1] := 1.27769;
distance[wp2][wp3] := 6.00521;
distance[wp2][wp4] := 4.60679;
distance[wp2][wp7] := 9.35334;
distance[wp2][wp10] := 8.00016;
distance[wp20][wp17] := 1.06888;
distance[wp20][endpoint] := 0.960469;
distance[wp21][wp0] := 4.83735;
distance[wp21][endpoint] := 5.68177;
distance[wp22][wp15] := 1.39014;
distance[wp23][wp17] := 2.20000;
distance[wp23][endpoint] := 2.06155;
distance[wp24][wp9] := 1.66883;
distance[wp25][wp3] := 3.73162;
distance[wp25][endpoint] := 4.51221;
distance[wp26][wp14] := 3.65000;
distance[wp27][wp10] := 1.92354;
distance[wp28][wp9] := 1.01242;
distance[wp29][wp0] := 2.95000;
distance[wp29][endpoint] := 7.50000;
distance[wp3][wp2] := 6.00521;
distance[wp3][wp8] := 4.80234;
distance[wp3][wp25] := 3.73162;
distance[wp3][wp37] := 1.09659;
distance[wp30][wp17] := 2.25056;
distance[wp30][endpoint] := 2.65801;
distance[wp31][wp0] := 1.04043;
distance[wp32][wp14] := 2.46221;
distance[wp33][wp0] := 7.11425;
distance[wp33][endpoint] := 3.38009;
distance[wp34][wp0] := 4.16923;
distance[wp34][endpoint] := 6.31269;
distance[wp35][wp17] := 1.00125;
distance[wp35][endpoint] := 1.50083;
distance[wp36][wp0] := 7.32462;
distance[wp36][endpoint] := 3.20663;
distance[wp37][wp3] := 1.09659;
distance[wp38][wp0] := 3.71080;
distance[wp4][wp2] := 4.60679;
distance[wp5][wp0] := 1.36565;
distance[wp6][wp0] := 6.20000;
distance[wp6][wp11] := 2.54804;
distance[wp6][endpoint] := 4.25000;
distance[wp7][wp2] := 9.35334;
distance[wp8][wp3] := 4.80234;
distance[wp8][endpoint] := 3.40918;
distance[wp9][wp1] := 2.13600;
distance[wp9][wp24] := 1.66883;
distance[wp9][wp28] := 1.01242;
predict_covariance := 0.00000;
relatived := 0.00000;
update_covariance := 1.22000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (robot_at[kenny][endpoint]) & ((( cov) < (finaltrace)))& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


