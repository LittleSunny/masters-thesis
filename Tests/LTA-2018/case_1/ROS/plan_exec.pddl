antony
collect_covariance.txt
collect_plans.txt
domain_pred_update_RRT.cpp
domain_pred_update_RRT.h
domain_pred_update_RRT.m
domain_pred_update_RRT.pddl
domain_pred_update_RRT_planner
final_states.txt
landmark.txt
plan_exec.pddl
plan.pddl
problem_pred_update_RRT.pddl
problem_pred_update_RRT_plan.pddl
waypoints_rrt.txt

======================================================

DiNo version 1.1
Discretised Nonlinear Heuristic Planner for PDDL+ models with continous processes and events

Copyright (C) 2015
W. Piotrowski, M. Fox, D. Long, D. Magazzeni, F. Mercorio

Call with the -c flag or read the license file for terms
and conditions of use.
Run this program with "-h" for the list of options.
Send bug reports and comments to wiktor.piotrowski@kcl.ac.uk

======================================================

======================================================
Planning configuration

* Source domain: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.pddl
* Source problem: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT.pddl
* Planning Mode: Feasible Plan
* SRPG time horizon: 20
* Output format: PDDL+
* Epsilon separation: 0.001
* Output target: "/home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT_plan.pddl"

* Model: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT
* State size 93429 bits (rounded to 11680 bytes).
* Allocated memory: 1000 Megabytes

**  Time Discretisation = 1.000000
**  Digits for representing the integer part of a real =  5.000000
**  Digits for representing the fractional part of a real =  4
======================================================
DEBUG SUNNY EXTERNAL
SUNNY DEBUG this is external solver name /home/sunny/catkin_ws/DiNo/ex/ext_lib/build/libext_lib.so
/home/sunny/catkin_ws/Tests/LTA-2018/case_1/waypoints_rrt.txt
/home/sunny/catkin_ws/Tests/LTA-2018/case_1/landmark.txt
/home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT.pddl

=== Analyzing model... ===============================

* State Space Expansion Algorithm: SRPG+.
  with symmetry algorithm 3 -- Heuristic Small Memory Normalization
  with permutation trial limit 10.
* Maximum SRPG time horizon: 20.
* Maximum size of the state space: 112061 states.
  with states hash-compressed to 40 bits.

START H VAL: 7.000
0

======================================================

Model exploration complete (in 369.02 seconds).
	7385 actions fired
	1 start states
	6184 reachable states
	1 goals found


=== Building model dynamics... =======================

* Transition Graph mode: Memory Image
* Maximum size of graph: 112046 transitions.


======================================================

Model dynamics rebuilding complete (in 369.14 seconds).
	6184 states
	7385 transitions
	out degree: min 0 max 7 avg 1.19


=== Finding control paths... =========================

* Search Algorithm: Feasible Plan.


======================================================

Control paths calculation complete (in 369.14 seconds).
	6184 states
	28 controllable


=== Collecting plans... ==============================

thisi is in build_plan_from
PLAN: S0-R83-S2-R0-S278-R0-S516-R0-S996-R0-S1566-R3-S2266-R156-S3208-R0-S4473-R0-S6151-R0-S6152-R0-S6153-R0-S6154-R36-S6155-R1450-S6157-R0-S6160-R0-S6161-R0-S6162-R0-S6163-R0-S6164-R0-S6165-R10-S6166-R0-S6169-R401-S6170-R0-S6178-R0-S6179-R0-S6180-R0-S6181-R1-S6182
external_collect_plans

======================================================

Plan(s) generation complete (in 369.14 seconds).
	1 plans
	plan length (actions): min 28 max 28 avg 28.00
	plan duration (time): min 0 max 20 avg 20.00
	plan weight: min 0 max 20 avg 20.00


=== Writing final results... =========================

* Output format: PDDL+
* Output target: "/home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT_plan.pddl".


======================================================

Results Written (in 384.42 seconds).


