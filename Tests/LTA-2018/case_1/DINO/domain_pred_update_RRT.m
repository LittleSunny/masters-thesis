domain: file "/home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.pddl"
problem: file "/home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT.pddl"
message: " Time Discretisation = 1.000000"
message: " Digits for representing the integer part of a real =  5.000000"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(10,4);
	integer: -1000..1000;

	 TIME_type: real(12,7);

	covariance : Enum {dummy1,dummy2};
	robot : Enum {kenny};
	waypoint : Enum {endpoint,wp0,wp1,wp10,wp11,wp12,wp13,wp14,wp15,wp16,wp17,wp18,wp19,wp2,wp20,wp21,wp22,wp23,wp24,wp25,wp26,wp27,wp28,wp29,wp3,wp30,wp31,wp32,wp33,wp34,wp35,wp36,wp37,wp38,wp4,wp5,wp6,wp7,wp8,wp9};

const 
	 T:1.000000;

	dfactor : 1.00000;
	finaltrace : 0.300000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	counter[pddlname:"counter";] :  real_type;
	cov[pddlname:"cov";] :  real_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	predict_covariance[pddlname:"predict_covariance";] :  real_type;
	relatived[pddlname:"relatived";] :  real_type;
	update_covariance[pddlname:"update_covariance";] :  real_type;


	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;
	observe[pddlname: "observe";] :  boolean;
	moving[pddlname: "moving";] : Array [robot] of Array [waypoint] of  boolean;
	connected[pddlname: "connected";] : Array [waypoint] of Array [waypoint] of  boolean;
	lessthan[pddlname: "lessthan";] : Array [covariance] of Array [covariance] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun assign_cov_event_predict_n_update(update_covariance : real_type ; ): real_type "/home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.h" ;
externfun assign_counter_event_predict_n_update(): real_type ;
externfun increase_counter_process_control(counter : real_type ; T : real_type ; ): real_type ;
externfun decrease_relatived_process_control(relatived : real_type ; T : real_type ; dfactor : real_type ; ): real_type ;
externfun assign_relatived_action_goto_waypoint(distance : real_type ; ): real_type ;
externfun assign_counter_action_goto_waypoint(): real_type ;
externfun increase_update_covariance_action_goto_waypoint(update_covariance : real_type ; ): real_type ;
externfun increase_predict_covariance_action_goto_waypoint(predict_covariance : real_type ; ): real_type ;
procedure set_robot_at( r : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[r][wp] := value;
END;

function get_robot_at( r : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[r][wp];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;

procedure set_observe(  value : boolean);
BEGIN
	observe := value;
END;

function get_observe(): boolean;
BEGIN
	return 	observe;
END;

procedure set_moving( r : robot ; to_ : waypoint ;  value : boolean);
BEGIN
	moving[r][to_] := value;
END;

function get_moving( r : robot ; to_ : waypoint): boolean;
BEGIN
	return 	moving[r][to_];
END;

procedure set_connected( from : waypoint ; to_ : waypoint ;  value : boolean);
BEGIN
	connected[from][to_] := value;
END;

function get_connected( from : waypoint ; to_ : waypoint): boolean;
BEGIN
	return 	connected[from][to_];
END;

procedure set_lessthan( c : covariance ; f : covariance ;  value : boolean);
BEGIN
	lessthan[c][f] := value;
END;

function get_lessthan( c : covariance ; f : covariance): boolean;
BEGIN
	return 	lessthan[c][f];
END;









function predict_n_update () : boolean; 
BEGIN
IF (((( counter) > (0.00000)))) THEN 
cov := assign_cov_event_predict_n_update(update_covariance  );
counter := assign_counter_event_predict_n_update();
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;

procedure process_control (); 
BEGIN
IF (((( relatived) > (-dfactor)))) THEN 
counter := increase_counter_process_control(counter , T  );
relatived := decrease_relatived_process_control(relatived , T , dfactor  );

ENDIF ; 

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   predict_n_update_triggered :  boolean;
BEGIN
 event_triggered := true;
   predict_n_update_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! predict_n_update_triggered) then 
   predict_n_update_triggered := predict_n_update();
   event_triggered := event_triggered | predict_n_update_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_control_enabled :  boolean;
BEGIN
 process_updated := false; end_while := false;
   process_control_enabled := false;
while (!end_while) do 
    if (((( relatived) > (-dfactor))) &  !process_control_enabled) then
   process_updated := true;
   process_control();
   process_control_enabled := true;
   endif;

IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

ruleset r:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_waypoint " 
(robot_at[r][from]) & (observe) & (!(robot_at[r][to_])) & (!(visited[to_])) & (connected[from][to_]) ==> 
pddlname: " goto_waypoint"; 
BEGIN
moving[r][to_]:= true; 
robot_at[r][from]:= false; 
observe:= false; 
relatived := assign_relatived_action_goto_waypoint(distance[from][to_]  );
counter := assign_counter_action_goto_waypoint();
update_covariance := increase_update_covariance_action_goto_waypoint(update_covariance  );
predict_covariance := increase_predict_covariance_action_goto_waypoint(predict_covariance  );

END; 
END; 
END; 
END;

ruleset r:robot do 
 ruleset to_:waypoint do 
 action rule " reached " 
(moving[r][to_]) & ((( relatived) <= (0.00000))) ==> 
pddlname: " reached"; 
BEGIN
robot_at[r][to_]:= true; 
visited[to_]:= true; 
observe:= true; 
moving[r][to_]:= false; 

END; 
END; 
END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for r : robot do 
  for wp : waypoint do 
    set_robot_at(r,wp, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   set_observe(false);

   for r : robot do 
     for to_ : waypoint do 
       set_moving(r,to_, false);
END; END;  -- close for
   for from : waypoint do 
     for to_ : waypoint do 
       set_connected(from,to_, false);
END; END;  -- close for
   for c : covariance do 
     for f : covariance do 
       set_lessthan(c,f, false);
END; END;  -- close for
   for wp1 : waypoint do 
     for wp2 : waypoint do 
       distance[wp1][wp2] := 0.0 ;
END; END;  -- close for
   cov := 0.0 ;

   counter := 0.0 ;

   update_covariance := 0.0 ;

   predict_covariance := 0.0 ;

   relatived := 0.0 ;



connected[endpoint][wp12]:= true; 
connected[endpoint][wp14]:= true; 
connected[endpoint][wp16]:= true; 
connected[endpoint][wp18]:= true; 
connected[endpoint][wp19]:= true; 
connected[endpoint][wp24]:= true; 
connected[endpoint][wp25]:= true; 
connected[endpoint][wp26]:= true; 
connected[endpoint][wp27]:= true; 
connected[endpoint][wp28]:= true; 
connected[endpoint][wp31]:= true; 
connected[endpoint][wp36]:= true; 
connected[endpoint][wp37]:= true; 
connected[endpoint][wp6]:= true; 
connected[endpoint][wp8]:= true; 
connected[wp0][wp1]:= true; 
connected[wp0][wp2]:= true; 
connected[wp0][wp3]:= true; 
connected[wp0][wp6]:= true; 
connected[wp0][wp8]:= true; 
connected[wp0][wp12]:= true; 
connected[wp0][wp18]:= true; 
connected[wp0][wp20]:= true; 
connected[wp0][wp21]:= true; 
connected[wp0][wp24]:= true; 
connected[wp0][wp27]:= true; 
connected[wp0][wp29]:= true; 
connected[wp0][wp31]:= true; 
connected[wp1][wp0]:= true; 
connected[wp1][wp4]:= true; 
connected[wp1][wp5]:= true; 
connected[wp1][wp13]:= true; 
connected[wp1][wp15]:= true; 
connected[wp1][wp30]:= true; 
connected[wp10][wp6]:= true; 
connected[wp10][wp11]:= true; 
connected[wp10][wp22]:= true; 
connected[wp10][wp23]:= true; 
connected[wp10][wp28]:= true; 
connected[wp11][wp10]:= true; 
connected[wp12][wp0]:= true; 
connected[wp12][wp33]:= true; 
connected[wp12][wp35]:= true; 
connected[wp12][wp38]:= true; 
connected[wp12][endpoint]:= true; 
connected[wp13][wp1]:= true; 
connected[wp14][wp3]:= true; 
connected[wp14][endpoint]:= true; 
connected[wp15][wp1]:= true; 
connected[wp16][wp5]:= true; 
connected[wp16][endpoint]:= true; 
connected[wp17][wp7]:= true; 
connected[wp18][wp0]:= true; 
connected[wp18][wp19]:= true; 
connected[wp18][wp36]:= true; 
connected[wp18][endpoint]:= true; 
connected[wp19][wp18]:= true; 
connected[wp19][wp25]:= true; 
connected[wp19][wp26]:= true; 
connected[wp19][endpoint]:= true; 
connected[wp2][wp0]:= true; 
connected[wp20][wp0]:= true; 
connected[wp21][wp0]:= true; 
connected[wp22][wp10]:= true; 
connected[wp23][wp10]:= true; 
connected[wp24][wp0]:= true; 
connected[wp24][endpoint]:= true; 
connected[wp25][wp19]:= true; 
connected[wp25][endpoint]:= true; 
connected[wp26][wp19]:= true; 
connected[wp26][endpoint]:= true; 
connected[wp27][wp0]:= true; 
connected[wp27][endpoint]:= true; 
connected[wp28][wp10]:= true; 
connected[wp28][endpoint]:= true; 
connected[wp29][wp0]:= true; 
connected[wp29][wp34]:= true; 
connected[wp3][wp0]:= true; 
connected[wp3][wp14]:= true; 
connected[wp30][wp1]:= true; 
connected[wp31][wp0]:= true; 
connected[wp31][endpoint]:= true; 
connected[wp32][wp7]:= true; 
connected[wp33][wp12]:= true; 
connected[wp34][wp29]:= true; 
connected[wp35][wp12]:= true; 
connected[wp36][wp18]:= true; 
connected[wp36][wp37]:= true; 
connected[wp36][endpoint]:= true; 
connected[wp37][wp36]:= true; 
connected[wp37][endpoint]:= true; 
connected[wp38][wp12]:= true; 
connected[wp4][wp1]:= true; 
connected[wp5][wp1]:= true; 
connected[wp5][wp7]:= true; 
connected[wp5][wp16]:= true; 
connected[wp6][wp0]:= true; 
connected[wp6][wp9]:= true; 
connected[wp6][wp10]:= true; 
connected[wp6][endpoint]:= true; 
connected[wp7][wp5]:= true; 
connected[wp7][wp17]:= true; 
connected[wp7][wp32]:= true; 
connected[wp8][wp0]:= true; 
connected[wp8][endpoint]:= true; 
connected[wp9][wp6]:= true; 
observe:= true; 
robot_at[kenny][wp0]:= true; 
cov := 1.22000;
distance[endpoint][wp12] := 6.66521;
distance[endpoint][wp14] := 7.42041;
distance[endpoint][wp16] := 3.45145;
distance[endpoint][wp18] := 2.22767;
distance[endpoint][wp19] := 0.919239;
distance[endpoint][wp24] := 4.78174;
distance[endpoint][wp25] := 2.35637;
distance[endpoint][wp26] := 2.50000;
distance[endpoint][wp27] := 1.61555;
distance[endpoint][wp28] := 4.38463;
distance[endpoint][wp31] := 4.27376;
distance[endpoint][wp36] := 1.34164;
distance[endpoint][wp37] := 3.29621;
distance[endpoint][wp6] := 5.60803;
distance[endpoint][wp8] := 3.20351;
distance[wp0][wp1] := 3.34701;
distance[wp0][wp2] := 4.36606;
distance[wp0][wp3] := 2.05548;
distance[wp0][wp6] := 4.85927;
distance[wp0][wp8] := 7.25155;
distance[wp0][wp12] := 3.82655;
distance[wp0][wp18] := 8.25742;
distance[wp0][wp20] := 1.50083;
distance[wp0][wp21] := 1.34164;
distance[wp0][wp24] := 5.72647;
distance[wp0][wp27] := 8.97009;
distance[wp0][wp29] := 2.22036;
distance[wp0][wp31] := 6.21631;
distance[wp1][wp0] := 3.34701;
distance[wp1][wp4] := 3.39485;
distance[wp1][wp5] := 4.35718;
distance[wp1][wp13] := 2.21923;
distance[wp1][wp15] := 8.45946;
distance[wp1][wp30] := 1.05119;
distance[wp10][wp6] := 3.80526;
distance[wp10][wp11] := 2.68002;
distance[wp10][wp22] := 1.20934;
distance[wp10][wp23] := 4.01528;
distance[wp10][wp28] := 5.31836;
distance[wp11][wp10] := 2.68002;
distance[wp12][wp0] := 3.82655;
distance[wp12][wp33] := 1.04043;
distance[wp12][wp35] := 2.21923;
distance[wp12][wp38] := 4.15843;
distance[wp12][endpoint] := 6.66521;
distance[wp13][wp1] := 2.21923;
distance[wp14][wp3] := 1.07703;
distance[wp14][endpoint] := 7.42041;
distance[wp15][wp1] := 8.45946;
distance[wp16][wp5] := 5.40023;
distance[wp16][endpoint] := 3.45145;
distance[wp17][wp7] := 1.60702;
distance[wp18][wp0] := 8.25742;
distance[wp18][wp19] := 1.57877;
distance[wp18][wp36] := 2.22767;
distance[wp18][endpoint] := 2.22767;
distance[wp19][wp18] := 1.57877;
distance[wp19][wp25] := 2.90043;
distance[wp19][wp26] := 1.75071;
distance[wp19][endpoint] := 0.919239;
distance[wp2][wp0] := 4.36606;
distance[wp20][wp0] := 1.50083;
distance[wp21][wp0] := 1.34164;
distance[wp22][wp10] := 1.20934;
distance[wp23][wp10] := 4.01528;
distance[wp24][wp0] := 5.72647;
distance[wp24][endpoint] := 4.78174;
distance[wp25][wp19] := 2.90043;
distance[wp25][endpoint] := 2.35637;
distance[wp26][wp19] := 1.75071;
distance[wp26][endpoint] := 2.50000;
distance[wp27][wp0] := 8.97009;
distance[wp27][endpoint] := 1.61555;
distance[wp28][wp10] := 5.31836;
distance[wp28][endpoint] := 4.38463;
distance[wp29][wp0] := 2.22036;
distance[wp29][wp34] := 2.81114;
distance[wp3][wp0] := 2.05548;
distance[wp3][wp14] := 1.07703;
distance[wp30][wp1] := 1.05119;
distance[wp31][wp0] := 6.21631;
distance[wp31][endpoint] := 4.27376;
distance[wp32][wp7] := 1.01242;
distance[wp33][wp12] := 1.04043;
distance[wp34][wp29] := 2.81114;
distance[wp35][wp12] := 2.21923;
distance[wp36][wp18] := 2.22767;
distance[wp36][wp37] := 2.05061;
distance[wp36][endpoint] := 1.34164;
distance[wp37][wp36] := 2.05061;
distance[wp37][endpoint] := 3.29621;
distance[wp38][wp12] := 4.15843;
distance[wp4][wp1] := 3.39485;
distance[wp5][wp1] := 4.35718;
distance[wp5][wp7] := 2.87141;
distance[wp5][wp16] := 5.40023;
distance[wp6][wp0] := 4.85927;
distance[wp6][wp9] := 2.30489;
distance[wp6][wp10] := 3.80526;
distance[wp6][endpoint] := 5.60803;
distance[wp7][wp5] := 2.87141;
distance[wp7][wp17] := 1.60702;
distance[wp7][wp32] := 1.01242;
distance[wp8][wp0] := 7.25155;
distance[wp8][endpoint] := 3.20351;
distance[wp9][wp6] := 2.30489;
predict_covariance := 0.00000;
relatived := 0.00000;
update_covariance := 1.22000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (robot_at[kenny][endpoint]) & ((( cov) < (finaltrace)))& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


