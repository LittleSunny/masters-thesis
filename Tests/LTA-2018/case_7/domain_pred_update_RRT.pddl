(define (domain landmark)

(:requirements :typing :durative-actions :fluents :time :strips  :disjunctive-preconditions :durative-actions
:negative-preconditions :timed-initial-literals)

(:types
  waypoint
  robot
  covariance
)

(:predicates 
       (robot_at ?r - robot ?wp - waypoint)
       (visited ?wp - waypoint)
       (observe)
       (moving ?r - robot ?to - waypoint)
       (connected ?from ?to - waypoint)
       (lessthan ?c ?f - covariance)   
)

(:functions (distance ?wp1 ?wp2 - waypoint) (cov) (counter) (update_covariance) (predict_covariance) (relativeD)  (dFactor)
       (finalTrace)
)  
;; relativeD- a variable to store the distance between wps, dFactor- distance factor, to see how many times kalman prediction to be done
;; cov is the initial covarianc trace, finalTrace- the required trace upon reaching the goal state

;; Move between any two waypoints, along the straight line between the two waypoints

(:action goto_waypoint
  :parameters (?r - robot ?from ?to - waypoint)
  :precondition  (and (robot_at ?r ?from) (observe) (not(robot_at ?r ?to)) (not (visited ?to )) (connected ?from ?to)) 
  :effect (and (not (robot_at ?r ?from)) 
            (assign (relativeD) (distance ?from ?to))
            (moving ?r ?to)
            (not (observe))
            (assign (counter) 0)
            (increase (update_covariance) 0)
            (increase (predict_covariance) 0)      
            )           
)

(:action reached
    :parameters(?r - robot ?to - waypoint)
    :precondition(and (moving ?r ?to) (<= (relativeD) 0))
    :effect(and (robot_at ?r ?to) (visited ?to) (not (moving ?r ?to)) (observe))
)   
  

(:event predict_n_update
  ;;:parameters(?r - robot ?to - waypoint)
  :parameters()
    :precondition(and (> (counter) 0) )
    :effect (and 
              (assign (cov) (update_covariance))
              (assign (counter) 0)
        )
)

;; to calculate the number of prediction steps needed
(:process control
  :parameters()
  :precondition (and  (> (relativeD) (-dFactor)) )
  :effect (and  (decrease (relativeD) (* #t (dFactor)))   (increase (counter) (* #t 1))
)
)
)
