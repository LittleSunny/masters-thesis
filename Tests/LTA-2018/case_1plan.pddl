Run with the -l flag or read the license file for terms
and conditions of use.
Run this program with "-h" for the list of options.
Bugs, questions, and comments should be directed to
"wiktor.piotrowski@kcl.ac.uk".

DiNo/UPMurphi compiler last compiled date: Jul  3 2018
===========================================================================


 Start DiNo translation... 

 ----- CONFIG FILE ----- 
PDDL Domain File: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.pddl
PDDL Problem File: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT.pddl
The output model will be written on file: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.m
The output external function file will be written on file: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.h
 ----- DOMAIN SETTINGS ----- 
 Time discretisation: 1.000000
 Real number: (Integer): 5.000000 digits for the integer part
 Real number: (Fractional) 5.000000 digits for the fractional part


 ...Translation completed 


Errors: 0, warnings: 0

===========================================================================
DiNo Release 1.1
Discretised Nonlinear Heuristic Planner for PDDL+ models with continous processes and events.

DiNo Release 1.1 :
Copyright (C) 2015: W. Piotrowski, M. Fox, D. Long, D. Magazzeni, F. Mercorio.
DiNo Release 1.1 is based on UPMurphi release 3.0.

Universal Planner Murphi Release 3.0 :
Copyright (C) 2007 - 2015: G. Della Penna, B. Intrigila, D. Magazzeni, F. Mercorio.
Universal Planner Murphi Release 3.0 is based on CMurphi release 5.4.

CMurphi Release 5.4 :
Copyright (C) 2001 - 2003 by E. Tronci, G. Della Penna, B. Intrigila, I. Melatti, M. Zilli.
CMurphi Release 5.4 is based on Murphi release 3.1.

Murphi Release 3.1 :
Copyright (C) 1992 - 1999 by the Board of Trustees of
Leland Stanford Junior University.

===========================================================================
PDDL domain: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.pddl (found)
PDDL problem: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT.pddl (found)
DiNo model: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.m (found)
C++ source: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.cpp (found)
Executable planner: /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT_planner (found)
Compiling PDDL to DiNo model, please wait...
system command executed is: "/home/sunny/catkin_ws/DiNo/bin/pddl2upm" /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.pddl /home/sunny/catkin_ws/Tests/LTA-2018/case_1/problem_pred_update_RRT.pddl --custom 1.000000 5.000000 5.000000 
PDDL compilation successful, no errors
DiNo model generated in file /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.m
Compiling model...
Model compilation successful, no errors
C++ source code generated in file /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT.cpp
Compiling executable planner, please wait...
Planner compilation successful, no errors
Executable planner generated in file /home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT_planner
Call .//home/sunny/catkin_ws/Tests/LTA-2018/case_1/domain_pred_update_RRT_planner to execute the planner with default options
