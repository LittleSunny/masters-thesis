(define (domain car)
(:requirements :typing :durative-actions :fluents :time :strips  :disjunctive-preconditions :durative-actions
:negative-preconditions :timed-initial-literals)
(:types
	waypoint
	robot
)
(:predicates (running) (stopped) (engineBlown) (transmission_fine) (goal_reached) 
(robot_at ?r - robot ?wp - waypoint)
	(visited ?wp - waypoint)
)

(:functions (distance ?wp1 ?wp2 - waypoint) (cov) (constant) )

;; Move between any two waypoints, avoiding terrain
(:action goto_waypoint
	:parameters (?r - robot ?from ?to - waypoint)
	:precondition  (and (robot_at ?r ?from) )
	:effect (and (not (robot_at ?r ?from)) (decrease (cov) (/  (distance ?from ?to)(constant))) (robot_at ?r ?to))
	
)
(:action goto_goal
	:parameters (?r - robot ?from ?to - waypoint)
	:precondition  (and (robot_at ?r ?from) (< (cov) 5 ))
	:effect (and (not (robot_at ?r ?from)) (increase (cov) 5) (robot_at ?r ?to) (visited ?to))
	
)


(:action stop
:parameters(?wp - waypoint)
:precondition(and (< (cov) 10) (visited ?wp) )
:effect(and(goal_reached))
)

)
(define (problem car_prob)
    (:domain car)
    (:objects
    kenny - robot
    wp0 wp1 wp2 wp3 wp4 wp5 - waypoint
)
    (:init 
    (= (distance wp1 wp0) 10)
    (= (distance wp1 wp2) 10)
    (= (distance wp2 wp0) 14.14)
    (= (distance wp2 wp1) 10)
    (= (distance wp0 wp1) 10)
    (= (distance wp0 wp2) 14.14) 
    (= (distance wp0 wp3) 10)
    (= (distance wp1 wp3) 20)
    (= (distance wp3 wp1) 20)
    (= (distance wp3 wp0) 10)
    (= (distance wp3 wp2) 10)
    (= (distance wp2 wp3) 10)
    (= (distance wp4 wp0) 5)
    (= (distance wp0 wp4) 5)
    (= (distance wp4 wp1) 8)
    (= (distance wp1 wp4) 8)
    (= (distance wp4 wp2) 4)
    (= (distance wp2 wp4) 4)
    (= (distance wp4 wp3) 24)
    (= (distance wp3 wp4) 24)
    (= (distance wp5 wp0) 16)
    (= (distance wp0 wp5) 16)
    (= (distance wp5 wp1) 24)
    (= (distance wp1 wp5) 24)
    (= (distance wp5 wp2) 30)
    (= (distance wp2 wp5) 30)
    (= (distance wp5 wp3) 20)
    (= (distance wp3 wp5) 20)
    (= (distance wp5 wp4) 10)
    (= (distance wp4 wp5) 10) 
    (robot_at kenny wp0)
    (= (cov) 50)
    (= (constant) 1)
    )
     (:goal (and (visited wp2) (goal_reached) ))
     (:metric minimize(total-time))
)

