/******************************
  Program "car.m" compiled by "DiNo Release 1.1"

  DiNo Last Compiled date: "May 14 2018"
 ******************************/

/********************
  Parameter
 ********************/
#define DINO_VERSION "DiNo Release 1.1"
#define MURPHI_DATE "May 14 2018"
#define PROTOCOL_NAME "car"
#define DOMAIN_FILENAME "car.pddl"
#define PROBLEM_FILENAME "prob01.pddl"
#define DISCRETIZATION 0.100000
#define VAL_PATHNAME "/home/sunny/catkin_ws/DiNo/src/DiNo/../VAL-master/validate"
#define BITS_IN_WORLD 1589
#define HASHC
#define HAS_CLOCK
const char * const modelmessages[] = { " Time Discretisation = 0.1"," Digits for representing the integer part of a real =  5"," Digits for representing the fractional part of a real =  4" };
const int modelmessagecount = 3;

/********************
  Include
 ********************/
#include "upm_prolog.hpp"

/********************
  Decl declaration
 ********************/

class mu_1_real_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_real_type& val) { return mu__real::operator=((double) val); };
  mu_1_real_type (const char *name, int os): mu__real(7,4,40,name, os) {};
  mu_1_real_type (void): mu__real(7,4,40) {};
  mu_1_real_type (double val): mu__real(7,4,40,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_real_type mu_1_real_type_undefined_var;

class mu_1_integer: public mu__long
{
 public:
  inline int operator=(int val) { return mu__long::operator=(val); };
  inline int operator=(const mu_1_integer& val) { return mu__long::operator=((int) val); };
  mu_1_integer (const char *name, int os): mu__long(-1000, 1000, 11, name, os) {};
  mu_1_integer (void): mu__long(-1000, 1000, 11) {};
  mu_1_integer (int val): mu__long(-1000, 1000, 11, "Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%d",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of subrange decl ***/
mu_1_integer mu_1_integer_undefined_var;

class mu_1_TIME_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_TIME_type& val) { return mu__real::operator=((double) val); };
  mu_1_TIME_type (const char *name, int os): mu__real(7,2,40,name, os) {};
  mu_1_TIME_type (void): mu__real(7,2,40) {};
  mu_1_TIME_type (double val): mu__real(7,2,40,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_TIME_type mu_1_TIME_type_undefined_var;

class mu_1_robot: public mu__byte
{
 public:
  inline int operator=(int val) { return value(val); };
  inline int operator=(const mu_1_robot& val) { return value(val.value()); };
  static const char *values[];
  friend ostream& operator<< (ostream& s, mu_1_robot& val)
  {
    if (val.defined())
      return ( s << mu_1_robot::values[ int(val) - 1] );
    else return ( s << "Undefined" );
  };

  mu_1_robot (const char *name, int os): mu__byte(1, 1, 1, name, os) {};
  mu_1_robot (void): mu__byte(1, 1, 1) {};
  mu_1_robot (int val): mu__byte(1, 1, 1, "Parameter or function result.", 0)
  {
     operator=(val);
  };
  const char * Name() { return values[ value() -1]; };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
  virtual void print(FILE *target, const char *separator)
  {
    if (defined())
    fprintf(target,"%s: %s%s",name,values[ value() -1],separator); 
    else
    fprintf(target,"%s: Undefined%s",name,separator); 
  };
};

const char *mu_1_robot::values[] = {"kenny",NULL };

/*** end of enum declaration ***/
mu_1_robot mu_1_robot_undefined_var;

class mu_1_waypoint: public mu__byte
{
 public:
  inline int operator=(int val) { return value(val); };
  inline int operator=(const mu_1_waypoint& val) { return value(val.value()); };
  static const char *values[];
  friend ostream& operator<< (ostream& s, mu_1_waypoint& val)
  {
    if (val.defined())
      return ( s << mu_1_waypoint::values[ int(val) - 2] );
    else return ( s << "Undefined" );
  };

  mu_1_waypoint (const char *name, int os): mu__byte(2, 7, 3, name, os) {};
  mu_1_waypoint (void): mu__byte(2, 7, 3) {};
  mu_1_waypoint (int val): mu__byte(2, 7, 3, "Parameter or function result.", 0)
  {
     operator=(val);
  };
  const char * Name() { return values[ value() -2]; };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
  virtual void print(FILE *target, const char *separator)
  {
    if (defined())
    fprintf(target,"%s: %s%s",name,values[ value() -2],separator); 
    else
    fprintf(target,"%s: Undefined%s",name,separator); 
  };
};

const char *mu_1_waypoint::values[] = {"wp0","wp1","wp2","wp3","wp4","wp5",NULL };

/*** end of enum declaration ***/
mu_1_waypoint mu_1_waypoint_undefined_var;

class mu_1__type_0/*:public mu_1__type_super*/
{
 public:
  mu_1_real_type array[ 6 ]; 
#define awesome_mu_00_mu_1_real_type_mu_1__type_0 1 
 public:
  char *name;
  char longname[BUFFER_SIZE/4];
  void set_self( const char *n, int os);
  void set_self_2( const char *n, const char *n2, int os);
  void set_self_ar( const char *n, const char *n2, int os);
  mu_1__type_0 (const char *n, int os) { set_self(n, os); };
  mu_1__type_0 ( void ) {};
  virtual ~mu_1__type_0 ();
  mu_1_real_type& operator[] (int index) /* const */
  {
#ifndef NO_RUN_TIME_CHECKING
    if ( ( index >= 2 ) && ( index <= 7 ) )
      return array[ index - 2 ];
    else {
      if (index==UNDEFVAL) 
	Error.Error("Indexing to %s using an undefined value.", name);
      else
	Error.Error("%d not in index range of %s.", index, name);
      return array[0];
    }
#else
    return array[ index - 2 ];
#endif
  };
  mu_1__type_0& operator= (const mu_1__type_0& from)
  {
    for (int i = 0; i < 6; i++)
      array[i].value(from.array[i].value());
    return *this;
  }

friend int CompareWeight(mu_1__type_0& a, mu_1__type_0& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = CompareWeight(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
friend int Compare(mu_1__type_0& a, mu_1__type_0& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = Compare(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort()
  {
    for (int i=0; i<6; i++)
      array[i].MultisetSort();
  }
  void print_statistic()
  {
    for (int i=0; i<6; i++)
      array[i].print_statistic();
  }
  void clear() { for (int i = 0; i < 6; i++) array[i].clear(); };

  void undefine() { for (int i = 0; i < 6; i++) array[i].undefine(); };

  void reset() { for (int i = 0; i < 6; i++) array[i].reset(); };

  void to_state(state *thestate)
  {
    for (int i = 0; i < 6; i++)
      array[i].to_state(thestate);
  };

  std::vector<mu_0_boolean*> bool_array() {

	std::vector<mu_0_boolean*> barr;
	#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_0
		for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
			std::string stype = typeid(array[ix]).name();
			if (stype.compare("12mu_0_boolean") == 0)
				barr.push_back(&(array[ix]));
 		}
		return barr;

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_0
		return barr; 

	#else 
		#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_0
			return barr;
	   		

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu_0_boolean*> temp_b = array[ix].bool_array();
				 barr.insert(barr.end(), temp_b.begin(), temp_b.end());
 			}
			return barr;
	   		
		#endif 
	#endif 
}
  std::vector<mu__real*> num_array() {

	std::vector<mu__real*> narr;
	#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_0
	for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
		std::string stype = typeid(array[ix]).name();
		if (stype.compare("14mu_1_real_type") == 0)
			narr.push_back(&(array[ix]));
 	}
		return narr;
	

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_0
		return narr; 

	#else 
		#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_0
			return narr;

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu__real*> temp_n = array[ix].num_array();
				 narr.insert(narr.end(), temp_n.begin(), temp_n.end());
 			}
			return narr;
	   		

		#endif 
	#endif 
}
  void print(FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print(target,separator); };

  void print_diff(state *prevstate, FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print_diff(prevstate,target,separator);
  };
};

  void mu_1__type_0::set_self_ar( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    int l1 = strlen(n1), l2 = strlen(n2);
    strcpy( longname, n1 );
    longname[l1] = '[';
    strcpy( longname+l1+1, n2 );
    longname[l1+l2+1] = ']';
    longname[l1+l2+2] = 0;
    set_self( longname, os );
  };
  void mu_1__type_0::set_self_2( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    strcpy( longname, n1 );
    strcat( longname, n2 );
    set_self( longname, os );
  };
  void mu_1__type_0::set_self( const char *n, int os)
  {
    int i=0;
    name = (char *)n;

if (n) array[i].set_self_ar(n,"wp0", i * 40 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp1", i * 40 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp2", i * 40 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp3", i * 40 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp4", i * 40 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp5", i * 40 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
  }
mu_1__type_0::~mu_1__type_0()
{
}
/*** end array declaration ***/
mu_1__type_0 mu_1__type_0_undefined_var;

class mu_1__type_1/*:public mu_1__type_super*/
{
 public:
  mu_1__type_0 array[ 6 ]; 
#define awesome_mu_00_mu_1__type_0_mu_1__type_1 1 
 public:
  char *name;
  char longname[BUFFER_SIZE/4];
  void set_self( const char *n, int os);
  void set_self_2( const char *n, const char *n2, int os);
  void set_self_ar( const char *n, const char *n2, int os);
  mu_1__type_1 (const char *n, int os) { set_self(n, os); };
  mu_1__type_1 ( void ) {};
  virtual ~mu_1__type_1 ();
  mu_1__type_0& operator[] (int index) /* const */
  {
#ifndef NO_RUN_TIME_CHECKING
    if ( ( index >= 2 ) && ( index <= 7 ) )
      return array[ index - 2 ];
    else {
      if (index==UNDEFVAL) 
	Error.Error("Indexing to %s using an undefined value.", name);
      else
	Error.Error("%d not in index range of %s.", index, name);
      return array[0];
    }
#else
    return array[ index - 2 ];
#endif
  };
  mu_1__type_1& operator= (const mu_1__type_1& from)
  {
    for (int i = 0; i < 6; i++)
      array[i] = from.array[i];
    return *this;
  }

friend int CompareWeight(mu_1__type_1& a, mu_1__type_1& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = CompareWeight(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
friend int Compare(mu_1__type_1& a, mu_1__type_1& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = Compare(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort()
  {
    for (int i=0; i<6; i++)
      array[i].MultisetSort();
  }
  void print_statistic()
  {
    for (int i=0; i<6; i++)
      array[i].print_statistic();
  }
  void clear() { for (int i = 0; i < 6; i++) array[i].clear(); };

  void undefine() { for (int i = 0; i < 6; i++) array[i].undefine(); };

  void reset() { for (int i = 0; i < 6; i++) array[i].reset(); };

  void to_state(state *thestate)
  {
    for (int i = 0; i < 6; i++)
      array[i].to_state(thestate);
  };

  std::vector<mu_0_boolean*> bool_array() {

	std::vector<mu_0_boolean*> barr;
	#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_1
		for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
			std::string stype = typeid(array[ix]).name();
			if (stype.compare("12mu_0_boolean") == 0)
				barr.push_back(&(array[ix]));
 		}
		return barr;

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_1
		return barr; 

	#else 
		#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_1
			return barr;
	   		

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu_0_boolean*> temp_b = array[ix].bool_array();
				 barr.insert(barr.end(), temp_b.begin(), temp_b.end());
 			}
			return barr;
	   		
		#endif 
	#endif 
}
  std::vector<mu__real*> num_array() {

	std::vector<mu__real*> narr;
	#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_1
	for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
		std::string stype = typeid(array[ix]).name();
		if (stype.compare("14mu_1_real_type") == 0)
			narr.push_back(&(array[ix]));
 	}
		return narr;
	

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_1
		return narr; 

	#else 
		#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_1
			return narr;

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu__real*> temp_n = array[ix].num_array();
				 narr.insert(narr.end(), temp_n.begin(), temp_n.end());
 			}
			return narr;
	   		

		#endif 
	#endif 
}
  void print(FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print(target,separator); };

  void print_diff(state *prevstate, FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print_diff(prevstate,target,separator);
  };
};

  void mu_1__type_1::set_self_ar( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    int l1 = strlen(n1), l2 = strlen(n2);
    strcpy( longname, n1 );
    longname[l1] = '[';
    strcpy( longname+l1+1, n2 );
    longname[l1+l2+1] = ']';
    longname[l1+l2+2] = 0;
    set_self( longname, os );
  };
  void mu_1__type_1::set_self_2( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    strcpy( longname, n1 );
    strcat( longname, n2 );
    set_self( longname, os );
  };
  void mu_1__type_1::set_self( const char *n, int os)
  {
    int i=0;
    name = (char *)n;

if (n) array[i].set_self_ar(n,"wp0", i * 240 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp1", i * 240 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp2", i * 240 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp3", i * 240 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp4", i * 240 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp5", i * 240 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
  }
mu_1__type_1::~mu_1__type_1()
{
}
/*** end array declaration ***/
mu_1__type_1 mu_1__type_1_undefined_var;

class mu_1__type_2/*:public mu_1__type_super*/
{
 public:
  mu_0_boolean array[ 6 ]; 
#define awesome_mu_00_mu_0_boolean_mu_1__type_2 1 
 public:
  char *name;
  char longname[BUFFER_SIZE/4];
  void set_self( const char *n, int os);
  void set_self_2( const char *n, const char *n2, int os);
  void set_self_ar( const char *n, const char *n2, int os);
  mu_1__type_2 (const char *n, int os) { set_self(n, os); };
  mu_1__type_2 ( void ) {};
  virtual ~mu_1__type_2 ();
  mu_0_boolean& operator[] (int index) /* const */
  {
#ifndef NO_RUN_TIME_CHECKING
    if ( ( index >= 2 ) && ( index <= 7 ) )
      return array[ index - 2 ];
    else {
      if (index==UNDEFVAL) 
	Error.Error("Indexing to %s using an undefined value.", name);
      else
	Error.Error("%d not in index range of %s.", index, name);
      return array[0];
    }
#else
    return array[ index - 2 ];
#endif
  };
  mu_1__type_2& operator= (const mu_1__type_2& from)
  {
    for (int i = 0; i < 6; i++)
      array[i].value(from.array[i].value());
    return *this;
  }

friend int CompareWeight(mu_1__type_2& a, mu_1__type_2& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = CompareWeight(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
friend int Compare(mu_1__type_2& a, mu_1__type_2& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = Compare(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort()
  {
    for (int i=0; i<6; i++)
      array[i].MultisetSort();
  }
  void print_statistic()
  {
    for (int i=0; i<6; i++)
      array[i].print_statistic();
  }
  void clear() { for (int i = 0; i < 6; i++) array[i].clear(); };

  void undefine() { for (int i = 0; i < 6; i++) array[i].undefine(); };

  void reset() { for (int i = 0; i < 6; i++) array[i].reset(); };

  void to_state(state *thestate)
  {
    for (int i = 0; i < 6; i++)
      array[i].to_state(thestate);
  };

  std::vector<mu_0_boolean*> bool_array() {

	std::vector<mu_0_boolean*> barr;
	#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_2
		for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
			std::string stype = typeid(array[ix]).name();
			if (stype.compare("12mu_0_boolean") == 0)
				barr.push_back(&(array[ix]));
 		}
		return barr;

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_2
		return barr; 

	#else 
		#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_2
			return barr;
	   		

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu_0_boolean*> temp_b = array[ix].bool_array();
				 barr.insert(barr.end(), temp_b.begin(), temp_b.end());
 			}
			return barr;
	   		
		#endif 
	#endif 
}
  std::vector<mu__real*> num_array() {

	std::vector<mu__real*> narr;
	#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_2
	for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
		std::string stype = typeid(array[ix]).name();
		if (stype.compare("14mu_1_real_type") == 0)
			narr.push_back(&(array[ix]));
 	}
		return narr;
	

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_2
		return narr; 

	#else 
		#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_2
			return narr;

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu__real*> temp_n = array[ix].num_array();
				 narr.insert(narr.end(), temp_n.begin(), temp_n.end());
 			}
			return narr;
	   		

		#endif 
	#endif 
}
  void print(FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print(target,separator); };

  void print_diff(state *prevstate, FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print_diff(prevstate,target,separator);
  };
};

  void mu_1__type_2::set_self_ar( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    int l1 = strlen(n1), l2 = strlen(n2);
    strcpy( longname, n1 );
    longname[l1] = '[';
    strcpy( longname+l1+1, n2 );
    longname[l1+l2+1] = ']';
    longname[l1+l2+2] = 0;
    set_self( longname, os );
  };
  void mu_1__type_2::set_self_2( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    strcpy( longname, n1 );
    strcat( longname, n2 );
    set_self( longname, os );
  };
  void mu_1__type_2::set_self( const char *n, int os)
  {
    int i=0;
    name = (char *)n;

if (n) array[i].set_self_ar(n,"wp0", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp1", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp2", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp3", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp4", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp5", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
  }
mu_1__type_2::~mu_1__type_2()
{
}
/*** end array declaration ***/
mu_1__type_2 mu_1__type_2_undefined_var;

class mu_1__type_3/*:public mu_1__type_super*/
{
 public:
  mu_1__type_2 array[ 1 ]; 
#define awesome_mu_00_mu_1__type_2_mu_1__type_3 1 
 public:
  char *name;
  char longname[BUFFER_SIZE/4];
  void set_self( const char *n, int os);
  void set_self_2( const char *n, const char *n2, int os);
  void set_self_ar( const char *n, const char *n2, int os);
  mu_1__type_3 (const char *n, int os) { set_self(n, os); };
  mu_1__type_3 ( void ) {};
  virtual ~mu_1__type_3 ();
  mu_1__type_2& operator[] (int index) /* const */
  {
#ifndef NO_RUN_TIME_CHECKING
    if ( ( index >= 1 ) && ( index <= 1 ) )
      return array[ index - 1 ];
    else {
      if (index==UNDEFVAL) 
	Error.Error("Indexing to %s using an undefined value.", name);
      else
	Error.Error("%d not in index range of %s.", index, name);
      return array[0];
    }
#else
    return array[ index - 1 ];
#endif
  };
  mu_1__type_3& operator= (const mu_1__type_3& from)
  {
      array[0] = from.array[0];
    return *this;
  }

friend int CompareWeight(mu_1__type_3& a, mu_1__type_3& b)
  {
    int w;
    for (int i=0; i<1; i++) {
      w = CompareWeight(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
friend int Compare(mu_1__type_3& a, mu_1__type_3& b)
  {
    int w;
    for (int i=0; i<1; i++) {
      w = Compare(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort()
  {
    for (int i=0; i<1; i++)
      array[i].MultisetSort();
  }
  void print_statistic()
  {
    for (int i=0; i<1; i++)
      array[i].print_statistic();
  }
  void clear() { for (int i = 0; i < 1; i++) array[i].clear(); };

  void undefine() { for (int i = 0; i < 1; i++) array[i].undefine(); };

  void reset() { for (int i = 0; i < 1; i++) array[i].reset(); };

  void to_state(state *thestate)
  {
    for (int i = 0; i < 1; i++)
      array[i].to_state(thestate);
  };

  std::vector<mu_0_boolean*> bool_array() {

	std::vector<mu_0_boolean*> barr;
	#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_3
		for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
			std::string stype = typeid(array[ix]).name();
			if (stype.compare("12mu_0_boolean") == 0)
				barr.push_back(&(array[ix]));
 		}
		return barr;

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_3
		return barr; 

	#else 
		#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_3
			return barr;
	   		

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu_0_boolean*> temp_b = array[ix].bool_array();
				 barr.insert(barr.end(), temp_b.begin(), temp_b.end());
 			}
			return barr;
	   		
		#endif 
	#endif 
}
  std::vector<mu__real*> num_array() {

	std::vector<mu__real*> narr;
	#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_3
	for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
		std::string stype = typeid(array[ix]).name();
		if (stype.compare("14mu_1_real_type") == 0)
			narr.push_back(&(array[ix]));
 	}
		return narr;
	

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_3
		return narr; 

	#else 
		#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_3
			return narr;

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu__real*> temp_n = array[ix].num_array();
				 narr.insert(narr.end(), temp_n.begin(), temp_n.end());
 			}
			return narr;
	   		

		#endif 
	#endif 
}
  void print(FILE *target, const char *separator)
  {
    for (int i = 0; i < 1; i++)
      array[i].print(target,separator); };

  void print_diff(state *prevstate, FILE *target, const char *separator)
  {
    for (int i = 0; i < 1; i++)
      array[i].print_diff(prevstate,target,separator);
  };
};

  void mu_1__type_3::set_self_ar( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    int l1 = strlen(n1), l2 = strlen(n2);
    strcpy( longname, n1 );
    longname[l1] = '[';
    strcpy( longname+l1+1, n2 );
    longname[l1+l2+1] = ']';
    longname[l1+l2+2] = 0;
    set_self( longname, os );
  };
  void mu_1__type_3::set_self_2( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    strcpy( longname, n1 );
    strcat( longname, n2 );
    set_self( longname, os );
  };
  void mu_1__type_3::set_self( const char *n, int os)
  {
    int i=0;
    name = (char *)n;

if (n) array[i].set_self_ar(n,"kenny", i * 12 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
  }
mu_1__type_3::~mu_1__type_3()
{
}
/*** end array declaration ***/
mu_1__type_3 mu_1__type_3_undefined_var;

class mu_1__type_4/*:public mu_1__type_super*/
{
 public:
  mu_0_boolean array[ 6 ]; 
#define awesome_mu_00_mu_0_boolean_mu_1__type_4 1 
 public:
  char *name;
  char longname[BUFFER_SIZE/4];
  void set_self( const char *n, int os);
  void set_self_2( const char *n, const char *n2, int os);
  void set_self_ar( const char *n, const char *n2, int os);
  mu_1__type_4 (const char *n, int os) { set_self(n, os); };
  mu_1__type_4 ( void ) {};
  virtual ~mu_1__type_4 ();
  mu_0_boolean& operator[] (int index) /* const */
  {
#ifndef NO_RUN_TIME_CHECKING
    if ( ( index >= 2 ) && ( index <= 7 ) )
      return array[ index - 2 ];
    else {
      if (index==UNDEFVAL) 
	Error.Error("Indexing to %s using an undefined value.", name);
      else
	Error.Error("%d not in index range of %s.", index, name);
      return array[0];
    }
#else
    return array[ index - 2 ];
#endif
  };
  mu_1__type_4& operator= (const mu_1__type_4& from)
  {
    for (int i = 0; i < 6; i++)
      array[i].value(from.array[i].value());
    return *this;
  }

friend int CompareWeight(mu_1__type_4& a, mu_1__type_4& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = CompareWeight(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
friend int Compare(mu_1__type_4& a, mu_1__type_4& b)
  {
    int w;
    for (int i=0; i<6; i++) {
      w = Compare(a.array[i], b.array[i]);
      if (w!=0) return w;
    }
    return 0;
  }
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort()
  {
    for (int i=0; i<6; i++)
      array[i].MultisetSort();
  }
  void print_statistic()
  {
    for (int i=0; i<6; i++)
      array[i].print_statistic();
  }
  void clear() { for (int i = 0; i < 6; i++) array[i].clear(); };

  void undefine() { for (int i = 0; i < 6; i++) array[i].undefine(); };

  void reset() { for (int i = 0; i < 6; i++) array[i].reset(); };

  void to_state(state *thestate)
  {
    for (int i = 0; i < 6; i++)
      array[i].to_state(thestate);
  };

  std::vector<mu_0_boolean*> bool_array() {

	std::vector<mu_0_boolean*> barr;
	#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_4
		for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
			std::string stype = typeid(array[ix]).name();
			if (stype.compare("12mu_0_boolean") == 0)
				barr.push_back(&(array[ix]));
 		}
		return barr;

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_4
		return barr; 

	#else 
		#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_4
			return barr;
	   		

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu_0_boolean*> temp_b = array[ix].bool_array();
				 barr.insert(barr.end(), temp_b.begin(), temp_b.end());
 			}
			return barr;
	   		
		#endif 
	#endif 
}
  std::vector<mu__real*> num_array() {

	std::vector<mu__real*> narr;
	#ifdef awesome_mu_00_mu_1_real_type_mu_1__type_4
	for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
		std::string stype = typeid(array[ix]).name();
		if (stype.compare("14mu_1_real_type") == 0)
			narr.push_back(&(array[ix]));
 	}
		return narr;
	

	#elif awesome_mu_00_mu_1_TIME_type_mu_1__type_4
		return narr; 

	#else 
		#ifdef awesome_mu_00_mu_0_boolean_mu_1__type_4
			return narr;

		#else 
			for (int ix = 0; ix < (sizeof((array))/sizeof(*(array))); ix++){
				 std::vector<mu__real*> temp_n = array[ix].num_array();
				 narr.insert(narr.end(), temp_n.begin(), temp_n.end());
 			}
			return narr;
	   		

		#endif 
	#endif 
}
  void print(FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print(target,separator); };

  void print_diff(state *prevstate, FILE *target, const char *separator)
  {
    for (int i = 0; i < 6; i++)
      array[i].print_diff(prevstate,target,separator);
  };
};

  void mu_1__type_4::set_self_ar( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    int l1 = strlen(n1), l2 = strlen(n2);
    strcpy( longname, n1 );
    longname[l1] = '[';
    strcpy( longname+l1+1, n2 );
    longname[l1+l2+1] = ']';
    longname[l1+l2+2] = 0;
    set_self( longname, os );
  };
  void mu_1__type_4::set_self_2( const char *n1, const char *n2, int os ) {
    if (n1 == NULL) {set_self(NULL, 0); return;}
    strcpy( longname, n1 );
    strcat( longname, n2 );
    set_self( longname, os );
  };
  void mu_1__type_4::set_self( const char *n, int os)
  {
    int i=0;
    name = (char *)n;

if (n) array[i].set_self_ar(n,"wp0", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp1", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp2", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp3", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp4", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
if (n) array[i].set_self_ar(n,"wp5", i * 2 + os); else array[i].set_self_ar(NULL, NULL, 0); i++;
  }
mu_1__type_4::~mu_1__type_4()
{
}
/*** end array declaration ***/
mu_1__type_4 mu_1__type_4_undefined_var;

const int mu_kenny = 1;
const int mu_wp0 = 2;
const int mu_wp1 = 3;
const int mu_wp2 = 4;
const int mu_wp3 = 5;
const int mu_wp4 = 6;
const int mu_wp5 = 7;
const double mu_T = +1.000000e-01;
const double mu_constant = +1.000000e+00;
/*** Variable declaration ***/
mu_0_boolean mu_all_event_true("all_event_true",0);

/*** Variable declaration ***/
mu_1_integer mu_h_n("h_n",2);

/*** Variable declaration ***/
mu_1_integer mu_g_n("g_n",13);

/*** Variable declaration ***/
mu_1_integer mu_f_n("f_n",24);

/*** Variable declaration ***/
mu_1_TIME_type mu_TIME("TIME",35);

/*** Variable declaration ***/
mu_1_real_type mu_cov("cov",75);

/*** Variable declaration ***/
mu_1__type_1 mu_distance("distance",115);

/*** Variable declaration ***/
mu_0_boolean mu_running("running",1555);

/*** Variable declaration ***/
mu_0_boolean mu_stopped("stopped",1557);

/*** Variable declaration ***/
mu_0_boolean mu_engineblown("engineblown",1559);

/*** Variable declaration ***/
mu_0_boolean mu_transmission_fine("transmission_fine",1561);

/*** Variable declaration ***/
mu_0_boolean mu_goal_reached("goal_reached",1563);

/*** Variable declaration ***/
mu_1__type_3 mu_robot_at("robot_at",1565);

/*** Variable declaration ***/
mu_1__type_4 mu_visited("visited",1577);


#include "car.h"

void mu_set_running(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_running.undefine();
else
  mu_running = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_running()
{
return mu_running;
	Error.Error("The end of function get_running reached without returning values.");
};
/*** end function declaration ***/

void mu_set_stopped(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_stopped.undefine();
else
  mu_stopped = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_stopped()
{
return mu_stopped;
	Error.Error("The end of function get_stopped reached without returning values.");
};
/*** end function declaration ***/

void mu_set_engineblown(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_engineblown.undefine();
else
  mu_engineblown = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_engineblown()
{
return mu_engineblown;
	Error.Error("The end of function get_engineblown reached without returning values.");
};
/*** end function declaration ***/

void mu_set_transmission_fine(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_transmission_fine.undefine();
else
  mu_transmission_fine = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_transmission_fine()
{
return mu_transmission_fine;
	Error.Error("The end of function get_transmission_fine reached without returning values.");
};
/*** end function declaration ***/

void mu_set_goal_reached(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_goal_reached.undefine();
else
  mu_goal_reached = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_goal_reached()
{
return mu_goal_reached;
	Error.Error("The end of function get_goal_reached reached without returning values.");
};
/*** end function declaration ***/

void mu_set_robot_at(const mu_1_robot& mu_r, const mu_1_waypoint& mu_wp, const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_robot_at[mu_r][mu_wp].undefine();
else
  mu_robot_at[mu_r][mu_wp] = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_robot_at(const mu_1_robot& mu_r,const mu_1_waypoint& mu_wp)
{
return mu_robot_at[mu_r][mu_wp];
	Error.Error("The end of function get_robot_at reached without returning values.");
};
/*** end function declaration ***/

void mu_set_visited(const mu_1_waypoint& mu_wp, const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_visited[mu_wp].undefine();
else
  mu_visited[mu_wp] = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_visited(const mu_1_waypoint& mu_wp)
{
return mu_visited[mu_wp];
	Error.Error("The end of function get_visited reached without returning values.");
};
/*** end function declaration ***/

void mu_event_check()
{
/*** Variable declaration ***/
mu_0_boolean mu_event_triggered("event_triggered",0);

mu_event_triggered = mu_true;
{
  bool mu__while_expr_6;  mu__while_expr_6 = mu_event_triggered;
int mu__counter_5 = 0;
while (mu__while_expr_6) {
if ( ++mu__counter_5 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
mu_event_triggered = mu_false;
};
mu__while_expr_6 = mu_event_triggered;
}
};
};
/*** end procedure declaration ***/

mu_0_boolean mu_DAs_violate_duration()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_duration_violated("DA_duration_violated",0);

mu_DA_duration_violated = mu_false;
return mu_DA_duration_violated;
	Error.Error("The end of function DAs_violate_duration reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_DAs_ongoing_in_goal_state()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_still_ongoing("DA_still_ongoing",0);

mu_DA_still_ongoing = mu_false;
return mu_DA_still_ongoing;
	Error.Error("The end of function DAs_ongoing_in_goal_state reached without returning values.");
};
/*** end function declaration ***/

void mu_apply_continuous_change()
{
/*** Variable declaration ***/
mu_0_boolean mu_process_updated("process_updated",0);

/*** Variable declaration ***/
mu_0_boolean mu_end_while("end_while",2);

mu_process_updated = mu_false;
mu_end_while = mu_false;
{
  bool mu__while_expr_8;  mu__while_expr_8 = !(mu_end_while);
int mu__counter_7 = 0;
while (mu__while_expr_8) {
if ( ++mu__counter_7 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
if ( !(mu_process_updated) )
{
mu_end_while = mu_true;
}
else
{
mu_process_updated = mu_false;
}
};
mu__while_expr_8 = !(mu_end_while);
}
};
};
/*** end procedure declaration ***/





/********************
  The world
 ********************/
void world_class::clear()
{
  mu_all_event_true.clear();
  mu_h_n.clear();
  mu_g_n.clear();
  mu_f_n.clear();
  mu_TIME.clear();
  mu_cov.clear();
  mu_distance.clear();
  mu_running.clear();
  mu_stopped.clear();
  mu_engineblown.clear();
  mu_transmission_fine.clear();
  mu_goal_reached.clear();
  mu_robot_at.clear();
  mu_visited.clear();
}
void world_class::undefine()
{
  mu_all_event_true.undefine();
  mu_h_n.undefine();
  mu_g_n.undefine();
  mu_f_n.undefine();
  mu_TIME.undefine();
  mu_cov.undefine();
  mu_distance.undefine();
  mu_running.undefine();
  mu_stopped.undefine();
  mu_engineblown.undefine();
  mu_transmission_fine.undefine();
  mu_goal_reached.undefine();
  mu_robot_at.undefine();
  mu_visited.undefine();
}
void world_class::reset()
{
  mu_all_event_true.reset();
  mu_h_n.reset();
  mu_g_n.reset();
  mu_f_n.reset();
  mu_TIME.reset();
  mu_cov.reset();
  mu_distance.reset();
  mu_running.reset();
  mu_stopped.reset();
  mu_engineblown.reset();
  mu_transmission_fine.reset();
  mu_goal_reached.reset();
  mu_robot_at.reset();
  mu_visited.reset();
}
std::vector<mu_0_boolean*> world_class::get_mu_bools()
{
	  std::vector<mu_0_boolean*> awesome;

      awesome.push_back(&(mu_all_event_true));
      awesome.push_back(&(mu_running));
      awesome.push_back(&(mu_stopped));
      awesome.push_back(&(mu_engineblown));
      awesome.push_back(&(mu_transmission_fine));
      awesome.push_back(&(mu_goal_reached));
    return awesome; 
}
std::vector<mu_0_boolean*> world_class::get_mu_bool_arrays()
{
	  std::vector<mu_0_boolean*> var_arrays;
   std::vector<mu_0_boolean*> interm;

      interm = mu_distance.bool_array();
		if (interm.size() > 0) var_arrays.insert(var_arrays.end(), interm.begin(), interm.end());
      interm = mu_robot_at.bool_array();
		if (interm.size() > 0) var_arrays.insert(var_arrays.end(), interm.begin(), interm.end());
      interm = mu_visited.bool_array();
		if (interm.size() > 0) var_arrays.insert(var_arrays.end(), interm.begin(), interm.end());
    return var_arrays; 
}
std::vector<mu__real*> world_class::get_mu_nums()
{
	  std::vector<mu__real*> awesome;

      awesome.push_back(&(mu_cov));
    return awesome; 
}
std::vector<mu__real*> world_class::get_mu_num_arrays()
{
	  std::vector<mu__real*> var_arrays;
   std::vector<mu__real*> interm;

      interm = mu_distance.num_array();
		if (interm.size() > 0) var_arrays.insert(var_arrays.end(), interm.begin(), interm.end());
      interm = mu_robot_at.num_array();
		if (interm.size() > 0) var_arrays.insert(var_arrays.end(), interm.begin(), interm.end());
      interm = mu_visited.num_array();
		if (interm.size() > 0) var_arrays.insert(var_arrays.end(), interm.begin(), interm.end());
    return var_arrays; 
}
//WP WP WP WP WP
double world_class::get_f_val()
{
  double f_val = mu_f_n.value();
  return f_val;
}

//WP WP WP WP WP
void world_class::fire_processes()
{
}

//WP WP WP WP WP
void world_class::fire_processes_plus()
{
}

//WP WP WP WP WP
void world_class::fire_processes_minus()
{
}

//WP WP WP WP WP
void world_class::set_f_val()
{
  double f_val = mu_g_n.value() + mu_h_n.value();
  mu_f_n.value(f_val);
}

//WP WP WP WP WP
double world_class::get_h_val()
{
  double h_val = mu_h_n.value();
  return h_val;
}

//WP WP WP WP WP
void world_class::set_h_val()
{
  //	NON-HEURISTIC SEARCH
  // double h_val = 0; 

  //	FF RPG
  //upm_rpg::getInstance().clear_all();
  //double h_val = upm_rpg::getInstance().compute_rpg();


  //	NUMERIC RPG
  //upm_numeric_rpg::getInstance().clear_all();
  //double h_val = upm_numeric_rpg::getInstance().compute_rpg();

  //	TEMPORAL RPG
  upm_staged_rpg::getInstance().clear_all();
  double h_val = upm_staged_rpg::getInstance().compute_rpg();

  mu_h_n.value(h_val);
}

//WP WP WP WP WP
void world_class::set_h_val(int hp)
{
  double h_val = hp; 
  mu_h_n.value(h_val);
}

//WP WP WP WP WP
double world_class::get_g_val()
{
  double g_val = mu_g_n.value();
  return g_val;
}

//WP WP WP WP WP
void world_class::set_g_val(double g_val)
{
  mu_g_n.value(g_val);
}

void world_class::print(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print(target, separator);
  mu_h_n.print(target, separator);
  mu_g_n.print(target, separator);
  mu_f_n.print(target, separator);
  mu_TIME.print(target, separator);
  mu_cov.print(target, separator);
  mu_distance.print(target, separator);
  mu_running.print(target, separator);
  mu_stopped.print(target, separator);
  mu_engineblown.print(target, separator);
  mu_transmission_fine.print(target, separator);
  mu_goal_reached.print(target, separator);
  mu_robot_at.print(target, separator);
  mu_visited.print(target, separator);
    num_calls--;
}
}
void world_class::pddlprint(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_TIME.print(target, separator);
  mu_cov.print(target, separator);
  mu_distance.print(target, separator);
  mu_running.print(target, separator);
  mu_stopped.print(target, separator);
  mu_engineblown.print(target, separator);
  mu_transmission_fine.print(target, separator);
  mu_goal_reached.print(target, separator);
  mu_robot_at.print(target, separator);
  mu_visited.print(target, separator);
    num_calls--;
}
}
double world_class::get_clock_value()
{
  return mu_TIME.value();
}
void world_class::print_statistic()
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print_statistic();
  mu_h_n.print_statistic();
  mu_g_n.print_statistic();
  mu_f_n.print_statistic();
  mu_TIME.print_statistic();
  mu_cov.print_statistic();
  mu_distance.print_statistic();
  mu_running.print_statistic();
  mu_stopped.print_statistic();
  mu_engineblown.print_statistic();
  mu_transmission_fine.print_statistic();
  mu_goal_reached.print_statistic();
  mu_robot_at.print_statistic();
  mu_visited.print_statistic();
    num_calls--;
}
}
void world_class::print_diff(state *prevstate, FILE *target, const char *separator)
{
  if ( prevstate != NULL )
  {
    mu_all_event_true.print_diff(prevstate,target,separator);
    mu_h_n.print_diff(prevstate,target,separator);
    mu_g_n.print_diff(prevstate,target,separator);
    mu_f_n.print_diff(prevstate,target,separator);
    mu_TIME.print_diff(prevstate,target,separator);
    mu_cov.print_diff(prevstate,target,separator);
    mu_distance.print_diff(prevstate,target,separator);
    mu_running.print_diff(prevstate,target,separator);
    mu_stopped.print_diff(prevstate,target,separator);
    mu_engineblown.print_diff(prevstate,target,separator);
    mu_transmission_fine.print_diff(prevstate,target,separator);
    mu_goal_reached.print_diff(prevstate,target,separator);
    mu_robot_at.print_diff(prevstate,target,separator);
    mu_visited.print_diff(prevstate,target,separator);
  }
  else
print(target,separator);
}
void world_class::to_state(state *newstate)
{
  mu_all_event_true.to_state( newstate );
  mu_h_n.to_state( newstate );
  mu_g_n.to_state( newstate );
  mu_f_n.to_state( newstate );
  mu_TIME.to_state( newstate );
  mu_cov.to_state( newstate );
  mu_distance.to_state( newstate );
  mu_running.to_state( newstate );
  mu_stopped.to_state( newstate );
  mu_engineblown.to_state( newstate );
  mu_transmission_fine.to_state( newstate );
  mu_goal_reached.to_state( newstate );
  mu_robot_at.to_state( newstate );
  mu_visited.to_state( newstate );
}
void world_class::setstate(state *thestate)
{
}


/********************
  Rule declarations
 ********************/
/******************** RuleBase0 ********************/
class RuleBase0
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" time passing ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
    return mu_true;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;



    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;



    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;



    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;



    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_TIME));  // (mu_TIME) + (mu_T) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_TIME)); //  (mu_TIME) + (mu_T) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 0;
    while (what_rule < 1 )
      {
	if ( ( TRUE  ) ) {
	      if (mu_true) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 0;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_TIME = (mu_TIME) + (mu_T);
mu_event_check (  );
mu_apply_continuous_change (  );
mu_event_check (  );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_TIME = (mu_TIME) + (mu_T);


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 1;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return Duration(r);
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( time passing )");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Clock;
  };

};
/******************** RuleBase1 ********************/
class RuleBase1
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
    return tsprintf(" stop , wp:%s", mu_wp.Name());
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
bool mu__boolexpr9;
  if (!((mu_cov) < (1.000000e+01))) mu__boolexpr9 = FALSE ;
  else {
  mu__boolexpr9 = (mu_visited[mu_wp]) ; 
}
    return mu__boolexpr9;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

bool mu__boolexpr10;
  if (!((mu_cov) < (1.000000e+01))) mu__boolexpr10 = FALSE ;
  else {
  mu__boolexpr10 = (mu_visited[mu_wp]) ; 
}

 		if (std::string(typeid(mu_visited[mu_wp]).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_visited[mu_wp])); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

bool mu__boolexpr11;
  if (!((mu_cov) < (1.000000e+01))) mu__boolexpr11 = FALSE ;
  else {
  mu__boolexpr11 = (mu_visited[mu_wp]) ; 
}

 	if (std::string(typeid(mu_cov).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_cov), std::make_pair(1.000000e+01, 4))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
 	if (std::string(typeid(mu_cov).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_cov)); 
 		if (std::string(typeid(mu_visited[mu_wp]).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_visited[mu_wp])); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

bool mu__boolexpr12;
  if (!((mu_cov) < (1.000000e+01))) mu__boolexpr12 = FALSE ;
  else {
  mu__boolexpr12 = (mu_visited[mu_wp]) ; 
}

 		if (std::string(typeid(mu_visited[mu_wp]).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_visited[mu_wp]), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

bool mu__boolexpr13;
  if (!((mu_cov) < (1.000000e+01))) mu__boolexpr13 = FALSE ;
  else {
  mu__boolexpr13 = (mu_visited[mu_wp]) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;

    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;


    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;

    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

    aeffs.push_back(&(mu_goal_reached)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;

    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

    inter_effs.insert(std::make_pair(&(mu_goal_reached), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
    aeffs.push_back(&(mu_goal_reached)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 1;
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
    while (what_rule < 7 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr14;
  if (!((mu_cov) < (1.000000e+01))) mu__boolexpr14 = FALSE ;
  else {
  mu__boolexpr14 = (mu_visited[mu_wp]) ; 
}
	      if (mu__boolexpr14) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 1;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
mu_goal_reached = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

mu_goal_reached = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

mu_goal_reached = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

mu_goal_reached = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;



  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_wp;
    mu_wp.value((r % 6) + 2);
    r = r / 6;
    return tsprintf("( stop %s)", mu_wp.Name());
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase2 ********************/
class RuleBase2
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return tsprintf(" goto_goal , to_:%s, from:%s, r:%s", mu_to_.Name(), mu_from.Name(), mu_r.Name());
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
bool mu__boolexpr15;
  if (!(mu_robot_at[mu_r][mu_from])) mu__boolexpr15 = FALSE ;
  else {
  mu__boolexpr15 = ((mu_cov) < (5.000000e+00)) ; 
}
    return mu__boolexpr15;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

bool mu__boolexpr16;
  if (!(mu_robot_at[mu_r][mu_from])) mu__boolexpr16 = FALSE ;
  else {
  mu__boolexpr16 = ((mu_cov) < (5.000000e+00)) ; 
}

 		if (std::string(typeid(mu_robot_at[mu_r][mu_from]).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_robot_at[mu_r][mu_from])); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

bool mu__boolexpr17;
  if (!(mu_robot_at[mu_r][mu_from])) mu__boolexpr17 = FALSE ;
  else {
  mu__boolexpr17 = ((mu_cov) < (5.000000e+00)) ; 
}

 	if (std::string(typeid(mu_cov).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_cov), std::make_pair(5.000000e+00, 4))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
 	if (std::string(typeid(mu_cov).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_cov)); 
 		if (std::string(typeid(mu_robot_at[mu_r][mu_from]).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_robot_at[mu_r][mu_from])); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

bool mu__boolexpr18;
  if (!(mu_robot_at[mu_r][mu_from])) mu__boolexpr18 = FALSE ;
  else {
  mu__boolexpr18 = ((mu_cov) < (5.000000e+00)) ; 
}

 		if (std::string(typeid(mu_robot_at[mu_r][mu_from]).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_robot_at[mu_r][mu_from]), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

bool mu__boolexpr19;
  if (!(mu_robot_at[mu_r][mu_from])) mu__boolexpr19 = FALSE ;
  else {
  mu__boolexpr19 = ((mu_cov) < (5.000000e+00)) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;

    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

    effs.push_back(&(mu_cov));  // increase_cov_action_goto_goal( mu_cov ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;

    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

    aeffs.push_back(&(mu_robot_at[mu_r][mu_to_])); //  mu_true 
    aeffs.push_back(&(mu_visited[mu_to_])); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;

    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

    inter_effs.insert(std::make_pair(&(mu_robot_at[mu_r][mu_to_]), 1)); //  mu_true 
    inter_effs.insert(std::make_pair(&(mu_visited[mu_to_]), 1)); //  mu_true 
    inter_effs.insert(std::make_pair(&(mu_robot_at[mu_r][mu_from]), 0)); //  mu_false 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    aeffs.push_back(&(mu_robot_at[mu_r][mu_to_])); //  mu_true 
    aeffs.push_back(&(mu_visited[mu_to_])); //  mu_true 
    aeffs.push_back(&(mu_cov)); //  increase_cov_action_goto_goal( mu_cov ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 7;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    while (what_rule < 43 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr20;
  if (!(mu_robot_at[mu_r][mu_from])) mu__boolexpr20 = FALSE ;
  else {
  mu__boolexpr20 = ((mu_cov) < (5.000000e+00)) ; 
}
	      if (mu__boolexpr20) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 6;
	}
	else
	  what_rule += 6;
    r = what_rule - 7;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
mu_robot_at[mu_r][mu_to_] = mu_true;
mu_visited[mu_to_] = mu_true;
mu_robot_at[mu_r][mu_from] = mu_false;
mu_cov = increase_cov_action_goto_goal( mu_cov );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

mu_robot_at[mu_r][mu_to_] = mu_true;
mu_visited[mu_to_] = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

mu_robot_at[mu_r][mu_to_] = mu_true;
mu_visited[mu_to_] = mu_true;
mu_cov = increase_cov_action_goto_goal( mu_cov );


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

mu_robot_at[mu_r][mu_to_] = mu_true;
mu_visited[mu_to_] = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;



  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return tsprintf("( goto_goal %s %s %s)", mu_r.Name(), mu_from.Name(), mu_to_.Name());
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase3 ********************/
class RuleBase3
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return tsprintf(" goto_waypoint , to_:%s, from:%s, r:%s", mu_to_.Name(), mu_from.Name(), mu_r.Name());
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return mu_robot_at[mu_r][mu_from];
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;


 		if (std::string(typeid(mu_robot_at[mu_r][mu_from]).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_robot_at[mu_r][mu_from])); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;



    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
 		if (std::string(typeid(mu_robot_at[mu_r][mu_from]).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_robot_at[mu_r][mu_from])); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;


 		if (std::string(typeid(mu_robot_at[mu_r][mu_from]).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_robot_at[mu_r][mu_from]), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;



    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;

    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

    effs.push_back(&(mu_cov));  // decrease_cov_action_goto_waypoint( mu_cov, mu_distance[mu_from][mu_to_], (double)mu_constant ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;

    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

    aeffs.push_back(&(mu_robot_at[mu_r][mu_to_])); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;

    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

    inter_effs.insert(std::make_pair(&(mu_robot_at[mu_r][mu_to_]), 1)); //  mu_true 
    inter_effs.insert(std::make_pair(&(mu_robot_at[mu_r][mu_from]), 0)); //  mu_false 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    aeffs.push_back(&(mu_robot_at[mu_r][mu_to_])); //  mu_true 
    aeffs.push_back(&(mu_cov)); //  decrease_cov_action_goto_waypoint( mu_cov, mu_distance[mu_from][mu_to_], (double)mu_constant ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 43;
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    while (what_rule < 79 )
      {
	if ( ( TRUE  ) ) {
	      if (mu_robot_at[mu_r][mu_from]) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 6;
	}
	else
	  what_rule += 6;
    r = what_rule - 43;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
mu_robot_at[mu_r][mu_to_] = mu_true;
mu_robot_at[mu_r][mu_from] = mu_false;
mu_cov = decrease_cov_action_goto_waypoint( mu_cov, mu_distance[mu_from][mu_to_], (double)mu_constant );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

mu_robot_at[mu_r][mu_to_] = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

mu_robot_at[mu_r][mu_to_] = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

mu_robot_at[mu_r][mu_to_] = mu_true;
mu_cov = decrease_cov_action_goto_waypoint( mu_cov, mu_distance[mu_from][mu_to_], (double)mu_constant );


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {


    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;



  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    static mu_1_waypoint mu_to_;
    mu_to_.value((r % 6) + 2);
    r = r / 6;
    static mu_1_waypoint mu_from;
    mu_from.value((r % 6) + 2);
    r = r / 6;
    static mu_1_robot mu_r;
    mu_r.value((r % 1) + 1);
    r = r / 1;
    return tsprintf("( goto_waypoint %s %s %s)", mu_r.Name(), mu_from.Name(), mu_to_.Name());
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
class NextStateGenerator
{
  RuleBase0 R0;
  RuleBase1 R1;
  RuleBase2 R2;
  RuleBase3 R3;
public:
void SetNextEnabledRule(RULE_INDEX_TYPE & what_rule)
{
  category = CONDITION;
  if (what_rule<1)
    { R0.NextRule(what_rule);
      if (what_rule<1) return; }
  if (what_rule>=1 && what_rule<7)
    { R1.NextRule(what_rule);
      if (what_rule<7) return; }
  if (what_rule>=7 && what_rule<43)
    { R2.NextRule(what_rule);
      if (what_rule<43) return; }
  if (what_rule>=43 && what_rule<79)
    { R3.NextRule(what_rule);
      if (what_rule<79) return; }
}
bool Condition(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.Condition(r-0);
  if (r>=1 && r<=6) return R1.Condition(r-1);
  if (r>=7 && r<=42) return R2.Condition(r-7);
  if (r>=43 && r<=78) return R3.Condition(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking condition for nonexisting rule.");
}
std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.bool_precond_array(r-0);
  if (r>=1 && r<=6) return R1.bool_precond_array(r-1);
  if (r>=7 && r<=42) return R2.bool_precond_array(r-7);
  if (r>=43 && r<=78) return R3.bool_precond_array(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::map<mu__real*, std::pair<double,int> > num_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.num_precond_array(r-0);
  if (r>=1 && r<=6) return R1.num_precond_array(r-1);
  if (r>=7 && r<=42) return R2.num_precond_array(r-7);
  if (r>=43 && r<=78) return R3.num_precond_array(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.all_precond_array(r-0);
  if (r>=1 && r<=6) return R1.all_precond_array(r-1);
  if (r>=7 && r<=42) return R2.all_precond_array(r-7);
  if (r>=43 && r<=78) return R3.all_precond_array(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.precond_bool_interference(r-0);
  if (r>=1 && r<=6) return R1.precond_bool_interference(r-1);
  if (r>=7 && r<=42) return R2.precond_bool_interference(r-7);
  if (r>=43 && r<=78) return R3.precond_bool_interference(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.temporal_constraints(r-0);
  if (r>=1 && r<=6) return R1.temporal_constraints(r-1);
  if (r>=7 && r<=42) return R2.temporal_constraints(r-7);
  if (r>=43 && r<=78) return R3.temporal_constraints(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_bool_interference(r-0);
  if (r>=1 && r<=6) return R1.effects_bool_interference(r-1);
  if (r>=7 && r<=42) return R2.effects_bool_interference(r-7);
  if (r>=43 && r<=78) return R3.effects_bool_interference(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_add_bool_array(r-0);
  if (r>=1 && r<=6) return R1.effects_add_bool_array(r-1);
  if (r>=7 && r<=42) return R2.effects_add_bool_array(r-7);
  if (r>=43 && r<=78) return R3.effects_add_bool_array(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_num_array(r-0);
  if (r>=1 && r<=6) return R1.effects_num_array(r-1);
  if (r>=7 && r<=42) return R2.effects_num_array(r-7);
  if (r>=43 && r<=78) return R3.effects_num_array(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_all_array(r-0);
  if (r>=1 && r<=6) return R1.effects_all_array(r-1);
  if (r>=7 && r<=42) return R2.effects_all_array(r-7);
  if (r>=43 && r<=78) return R3.effects_all_array(r-43);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
void Code(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code(r-0); return; } 
  if (r>=1 && r<=6) { R1.Code(r-1); return; } 
  if (r>=7 && r<=42) { R2.Code(r-7); return; } 
  if (r>=43 && r<=78) { R3.Code(r-43); return; } 
}
void Code_ff(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_ff(r-0); return; } 
  if (r>=1 && r<=6) { R1.Code_ff(r-1); return; } 
  if (r>=7 && r<=42) { R2.Code_ff(r-7); return; } 
  if (r>=43 && r<=78) { R3.Code_ff(r-43); return; } 
}
void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_plus(r-0); return; } 
  if (r>=1 && r<=6) { R1.Code_numeric_ff_plus(r-1); return; } 
  if (r>=7 && r<=42) { R2.Code_numeric_ff_plus(r-7); return; } 
  if (r>=43 && r<=78) { R3.Code_numeric_ff_plus(r-43); return; } 
}
void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_minus(r-0); return; } 
  if (r>=1 && r<=6) { R1.Code_numeric_ff_minus(r-1); return; } 
  if (r>=7 && r<=42) { R2.Code_numeric_ff_minus(r-7); return; } 
  if (r>=43 && r<=78) { R3.Code_numeric_ff_minus(r-43); return; } 
}
mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_rule_clock_started(r-0); } 
  if (r>=1 && r<=6) { return R1.get_rule_clock_started(r-1); } 
  if (r>=7 && r<=42) { return R2.get_rule_clock_started(r-7); } 
  if (r>=43 && r<=78) { return R3.get_rule_clock_started(r-43); } 
}
std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_clocks(r-0); } 
  if (r>=1 && r<=6) { return R1.get_clocks(r-1); } 
  if (r>=7 && r<=42) { return R2.get_clocks(r-7); } 
  if (r>=43 && r<=78) { return R3.get_clocks(r-43); } 
}
int Priority(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.Priority(); } 
  if (r>=1 && r<=6) { return R1.Priority(); } 
  if (r>=7 && r<=42) { return R2.Priority(); } 
  if (r>=43 && r<=78) { return R3.Priority(); } 
}
char * Name(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Name(r-0);
  if (r>=1 && r<=6) return R1.Name(r-1);
  if (r>=7 && r<=42) return R2.Name(r-7);
  if (r>=43 && r<=78) return R3.Name(r-43);
  return NULL;
}
int Duration(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Duration(r-0);
  if (r>=1 && r<=6) return R1.Duration(r-1);
  if (r>=7 && r<=42) return R2.Duration(r-7);
  if (r>=43 && r<=78) return R3.Duration(r-43);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
int Weight(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Weight(r-0);
  if (r>=1 && r<=6) return R1.Weight(r-1);
  if (r>=7 && r<=42) return R2.Weight(r-7);
  if (r>=43 && r<=78) return R3.Weight(r-43);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
 char * PDDLName(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLName(r-0);
  if (r>=1 && r<=6) return R1.PDDLName(r-1);
  if (r>=7 && r<=42) return R2.PDDLName(r-7);
  if (r>=43 && r<=78) return R3.PDDLName(r-43);
  return NULL;
}
RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLClass(r-0);
  if (r>=1 && r<=6) return R1.PDDLClass(r-1);
  if (r>=7 && r<=42) return R2.PDDLClass(r-7);
  if (r>=43 && r<=78) return R3.PDDLClass(r-43);
Error.Notrace("Internal: NextStateGenerator -- querying PDDL class for nonexisting rule.");
}
};
const RULE_INDEX_TYPE numrules = 79;

/********************
  parameter
 ********************/
#define RULES_IN_WORLD 79


/********************
  Startstate records
 ********************/
/******************** StartStateBase0 ********************/
class StartStateBase0
{
public:
  char * Name(unsigned short r)
  {
    return tsprintf("start");
  }
  void Code(unsigned short r)
  {
mu_TIME = 0.000000e+00;
mu_set_running ( mu_false );
mu_set_stopped ( mu_false );
mu_set_engineblown ( mu_false );
mu_set_transmission_fine ( mu_false );
mu_set_goal_reached ( mu_false );
{
for(int mu_r = 1; mu_r <= 1; mu_r++) {
{
for(int mu_wp = 2; mu_wp <= 7; mu_wp++) {
mu_set_robot_at ( mu_r, mu_wp, mu_false );
};
};
};
};
{
for(int mu_wp = 2; mu_wp <= 7; mu_wp++) {
mu_set_visited ( mu_wp, mu_false );
};
};
{
for(int mu_wp1 = 2; mu_wp1 <= 7; mu_wp1++) {
{
for(int mu_wp2 = 2; mu_wp2 <= 7; mu_wp2++) {
mu_distance[mu_wp1][mu_wp2] = 0.000000e+00;
};
};
};
};
mu_cov = 0.000000e+00;
mu_robot_at[mu_kenny][mu_wp0] = mu_true;
mu_distance[mu_wp1][mu_wp0] = 1.000000e+01;
mu_distance[mu_wp1][mu_wp2] = 1.000000e+01;
mu_distance[mu_wp2][mu_wp0] = 1.414000e+01;
mu_distance[mu_wp2][mu_wp1] = 1.000000e+01;
mu_distance[mu_wp0][mu_wp1] = 1.000000e+01;
mu_distance[mu_wp0][mu_wp2] = 1.414000e+01;
mu_distance[mu_wp0][mu_wp3] = 1.000000e+01;
mu_distance[mu_wp1][mu_wp3] = 2.000000e+01;
mu_distance[mu_wp3][mu_wp1] = 2.000000e+01;
mu_distance[mu_wp3][mu_wp0] = 1.000000e+01;
mu_distance[mu_wp3][mu_wp2] = 1.000000e+01;
mu_distance[mu_wp2][mu_wp3] = 1.000000e+01;
mu_distance[mu_wp4][mu_wp0] = 5.000000e+00;
mu_distance[mu_wp0][mu_wp4] = 5.000000e+00;
mu_distance[mu_wp4][mu_wp1] = 8.000000e+00;
mu_distance[mu_wp1][mu_wp4] = 8.000000e+00;
mu_distance[mu_wp4][mu_wp2] = 4.000000e+00;
mu_distance[mu_wp2][mu_wp4] = 4.000000e+00;
mu_distance[mu_wp4][mu_wp3] = 2.400000e+01;
mu_distance[mu_wp3][mu_wp4] = 2.400000e+01;
mu_distance[mu_wp5][mu_wp0] = 1.600000e+01;
mu_distance[mu_wp0][mu_wp5] = 1.600000e+01;
mu_distance[mu_wp5][mu_wp1] = 2.400000e+01;
mu_distance[mu_wp1][mu_wp5] = 2.400000e+01;
mu_distance[mu_wp5][mu_wp2] = 3.000000e+01;
mu_distance[mu_wp2][mu_wp5] = 3.000000e+01;
mu_distance[mu_wp5][mu_wp3] = 2.000000e+01;
mu_distance[mu_wp3][mu_wp5] = 2.000000e+01;
mu_distance[mu_wp5][mu_wp4] = 1.000000e+01;
mu_distance[mu_wp4][mu_wp5] = 1.000000e+01;
mu_cov = 5.000000e+01;
mu_all_event_true = mu_true;
mu_g_n = 0;
mu_h_n = 0;
mu_f_n = 0;
  };

};
class StartStateGenerator
{
  StartStateBase0 S0;
public:
void Code(unsigned short r)
{
  if (r<=0) { S0.Code(r-0); return; }
}
char * Name(unsigned short r)
{
  if (r<=0) return S0.Name(r-0);
  return NULL;
}
};
const rulerec startstates[] = {
{ NULL, NULL, NULL, FALSE},
};
unsigned short StartStateManager::numstartstates = 1;

/********************
  Goal records
 ********************/

// WP WP WP GOAL
int mu__goal_21() // Goal "enjoy"
{
bool mu__boolexpr22;
bool mu__boolexpr23;
  if (!(mu_visited[mu_wp2])) mu__boolexpr23 = FALSE ;
  else {
  mu__boolexpr23 = (mu_goal_reached) ; 
}
  if (!(mu__boolexpr23)) mu__boolexpr22 = FALSE ;
  else {
  mu__boolexpr22 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}
return mu__boolexpr22;
};

  std::set<mu_0_boolean*> get_bool_goal_conditions()
  {
    std::set<mu_0_boolean*> bool_goal_conds;
bool mu__boolexpr24;
  if (!(mu_visited[mu_wp2])) mu__boolexpr24 = FALSE ;
  else {
  mu__boolexpr24 = (mu_goal_reached) ; 
}
bool mu__boolexpr25;
bool mu__boolexpr26;
  if (!(mu_visited[mu_wp2])) mu__boolexpr26 = FALSE ;
  else {
  mu__boolexpr26 = (mu_goal_reached) ; 
}
  if (!(mu__boolexpr26)) mu__boolexpr25 = FALSE ;
  else {
  mu__boolexpr25 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}
bool mu__boolexpr27;
  if (!(mu_visited[mu_wp2])) mu__boolexpr27 = FALSE ;
  else {
  mu__boolexpr27 = (mu_goal_reached) ; 
}

 if (std::string(typeid(mu_goal_reached).name()).compare("12mu_0_boolean") == 0)
		bool_goal_conds.insert(&(mu_goal_reached)); 
 if (std::string(typeid(mu_visited[mu_wp2]).name()).compare("12mu_0_boolean") == 0)
		bool_goal_conds.insert(&(mu_visited[mu_wp2])); 

    return bool_goal_conds;
  }

  std::map<mu__real*, std::pair<double, int> > get_numeric_goal_conditions()
  {
    std::map<mu__real*, std::pair<double, int> > numeric_goal_conds;

    return numeric_goal_conds;
  }

bool mu__condition_28() // Condition for Rule "enjoy"
{
  return mu__goal_21( );
}

bool mu__goal__00(){ return mu__condition_28(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/


// WP WP WP GOAL
const rulerec goals[] = {
{"enjoy", &mu__condition_28, NULL, },
};
const unsigned short numgoals = 1;

/********************
  Metric related stuff
 ********************/
const short metric = -1;

/********************
  Invariant records
 ********************/
int mu__invariant_29() // Invariant "todo bien"
{
bool mu__boolexpr30;
  if (!(mu_all_event_true)) mu__boolexpr30 = FALSE ;
  else {
  mu__boolexpr30 = (!(mu_DAs_violate_duration(  ))) ; 
}
return mu__boolexpr30;
};

bool mu__condition_31() // Condition for Rule "todo bien"
{
  return mu__invariant_29( );
}

bool mu__goal__01(){ return mu__condition_31(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/

const rulerec invariants[] = {
{"todo bien", &mu__condition_31, NULL, },
};
const unsigned short numinvariants = 1;

/********************
  Normal/Canonicalization for scalarset
 ********************/
/*
goal_reached:NoScalarset
engineblown:NoScalarset
running:NoScalarset
cov:NoScalarset
f_n:NoScalarset
h_n:NoScalarset
all_event_true:NoScalarset
g_n:NoScalarset
TIME:NoScalarset
distance:NoScalarset
stopped:NoScalarset
transmission_fine:NoScalarset
robot_at:NoScalarset
visited:NoScalarset
*/

/********************
Code for symmetry
 ********************/

/********************
 Permutation Set Class
 ********************/
class PermSet
{
public:
  // book keeping
  enum PresentationType {Simple, Explicit};
  PresentationType Presentation;

  void ResetToSimple();
  void ResetToExplicit();
  void SimpleToExplicit();
  void SimpleToOne();
  bool NextPermutation();

  void Print_in_size()
  { unsigned long ret=0; for (unsigned long i=0; i<count; i++) if (in[i]) ret++; cout << "in_size:" << ret << "\n"; }


  /********************
   Simple and efficient representation
   ********************/
  bool AlreadyOnlyOneRemain;
  bool MoreThanOneRemain();


  /********************
   Explicit representation
  ********************/
  unsigned long size;
  unsigned long count;
  // in will be of product of factorial sizes for fast canonicalize
  // in will be of size 1 for reduced local memory canonicalize
  bool * in;

  // auxiliary for explicit representation

  // in/perm/revperm will be of factorial size for fast canonicalize
  // they will be of size 1 for reduced local memory canonicalize
  // second range will be size of the scalarset
  // procedure for explicit representation
  // General procedure
  PermSet();
  bool In(int i) const { return in[i]; };
  void Add(int i) { for (int j=0; j<i; j++) in[j] = FALSE;};
  void Remove(int i) { in[i] = FALSE; };
};
bool PermSet::MoreThanOneRemain()
{
  int i,j;
  if (AlreadyOnlyOneRemain)
    return FALSE;
  else {
  }
  AlreadyOnlyOneRemain = TRUE;
  return FALSE;
}
PermSet::PermSet()
: Presentation(Simple)
{
  int i,j,k;
  if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];

    // Set perm and revperm

    // setting up combination of permutations
    // for different scalarset
    int carry;
    size = 1;
    count = 1;
    for (i=0; i<1; i++)
      {
        carry = 1;
        in[i]= TRUE;
    }
  }
  else
  {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];
  in[0] = TRUE;
  }
}
void PermSet::ResetToSimple()
{
  int i;

  AlreadyOnlyOneRemain = FALSE;
  Presentation = Simple;
}
void PermSet::ResetToExplicit()
{
  for (int i=0; i<1; i++) in[i] = TRUE;
  Presentation = Explicit;
}
void PermSet::SimpleToExplicit()
{
  int i,j,k;
  int start, class_size;

  // Setup range for mapping

  // To be In or not to be

  // setup explicit representation 
  // Set perm and revperm
  for (i=0; i<1; i++)
    {
      in[i] = TRUE;
    }
  Presentation = Explicit;
  if (args->test_parameter1.value==0) Print_in_size();
}
void PermSet::SimpleToOne()
{
  int i,j,k;
  int class_size;
  int start;


  // Setup range for mapping
  Presentation = Explicit;
}
bool PermSet::NextPermutation()
{
  bool nexted = FALSE;
  int start, end; 
  int class_size;
  int temp;
  int j,k;

  // algorithm
  // for each class
  //   if forall in the same class reverse_sorted, 
  //     { sort again; goto next class }
  //   else
  //     {
  //       nexted = TRUE;
  //       for (j from l to r)
  // 	       if (for all j+ are reversed sorted)
  // 	         {
  // 	           swap j, j+1
  // 	           sort all j+ again
  // 	           break;
  // 	         }
  //     }
if (!nexted) return FALSE;
  return TRUE;
}

/********************
 Symmetry Class
 ********************/
class SymmetryClass
{
  PermSet Perm;
  bool BestInitialized;
  state BestPermutedState;

  // utilities
  void SetBestResult(int i, state* temp);
  void ResetBestResult() {BestInitialized = FALSE;};

public:
  // initializer
  SymmetryClass() : Perm(), BestInitialized(FALSE) {};
  ~SymmetryClass() {};

  void Normalize(state* s);

  void Exhaustive_Fast_Canonicalize(state *s);
  void Heuristic_Fast_Canonicalize(state *s);
  void Heuristic_Small_Mem_Canonicalize(state *s);
  void Heuristic_Fast_Normalize(state *s);

  void MultisetSort(state* s);
};


/********************
 Symmetry Class Members
 ********************/
void SymmetryClass::MultisetSort(state* s)
{
        mu_goal_reached.MultisetSort();
        mu_engineblown.MultisetSort();
        mu_running.MultisetSort();
        mu_cov.MultisetSort();
        mu_f_n.MultisetSort();
        mu_h_n.MultisetSort();
        mu_all_event_true.MultisetSort();
        mu_g_n.MultisetSort();
        mu_TIME.MultisetSort();
        mu_distance.MultisetSort();
        mu_stopped.MultisetSort();
        mu_transmission_fine.MultisetSort();
        mu_robot_at.MultisetSort();
        mu_visited.MultisetSort();
}
void SymmetryClass::Normalize(state* s)
{
  switch (args->sym_alg.mode) {
  case argsym_alg::Exhaustive_Fast_Canonicalize:
    Exhaustive_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Canonicalize:
    Heuristic_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Small_Mem_Canonicalize:
    Heuristic_Small_Mem_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Normalize:
    Heuristic_Fast_Normalize(s);
    break;
  default:
    Heuristic_Fast_Canonicalize(s);
  }
}

/********************
 Permute and Canonicalize function for different types
 ********************/
void mu_1_real_type::Permute(PermSet& Perm, int i) {};
void mu_1_real_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_real_type::Canonicalize(PermSet& Perm) {};
void mu_1_real_type::SimpleLimit(PermSet& Perm) {};
void mu_1_real_type::ArrayLimit(PermSet& Perm) {};
void mu_1_real_type::Limit(PermSet& Perm) {};
void mu_1_real_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };
void mu_1_integer::Permute(PermSet& Perm, int i) {};
void mu_1_integer::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_integer::Canonicalize(PermSet& Perm) {};
void mu_1_integer::SimpleLimit(PermSet& Perm) {};
void mu_1_integer::ArrayLimit(PermSet& Perm) {};
void mu_1_integer::Limit(PermSet& Perm) {};
void mu_1_integer::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for subrange type.\n"); };
void mu_1_TIME_type::Permute(PermSet& Perm, int i) {};
void mu_1_TIME_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_TIME_type::Canonicalize(PermSet& Perm) {};
void mu_1_TIME_type::SimpleLimit(PermSet& Perm) {};
void mu_1_TIME_type::ArrayLimit(PermSet& Perm) {};
void mu_1_TIME_type::Limit(PermSet& Perm) {};
void mu_1_TIME_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };
void mu_1_robot::Permute(PermSet& Perm, int i) {};
void mu_1_robot::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_robot::Canonicalize(PermSet& Perm) {};
void mu_1_robot::SimpleLimit(PermSet& Perm) {};
void mu_1_robot::ArrayLimit(PermSet& Perm) {};
void mu_1_robot::Limit(PermSet& Perm) {};
void mu_1_robot::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for enum type.\n"); };
void mu_1_waypoint::Permute(PermSet& Perm, int i) {};
void mu_1_waypoint::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_waypoint::Canonicalize(PermSet& Perm) {};
void mu_1_waypoint::SimpleLimit(PermSet& Perm) {};
void mu_1_waypoint::ArrayLimit(PermSet& Perm) {};
void mu_1_waypoint::Limit(PermSet& Perm) {};
void mu_1_waypoint::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for enum type.\n"); };
void mu_1__type_0::Permute(PermSet& Perm, int i)
{
  static mu_1__type_0 temp("Permute_mu_1__type_0",-1);
  int j;
  for (j=0; j<6; j++)
    array[j].Permute(Perm, i);
};
void mu_1__type_0::SimpleCanonicalize(PermSet& Perm)
{ Error.Error("Internal: Simple Canonicalization of Scalarset Array\n"); };
void mu_1__type_0::Canonicalize(PermSet& Perm){};
void mu_1__type_0::SimpleLimit(PermSet& Perm){}
void mu_1__type_0::ArrayLimit(PermSet& Perm) {}
void mu_1__type_0::Limit(PermSet& Perm){}
void mu_1__type_0::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for scalarset array.\n"); };
void mu_1__type_1::Permute(PermSet& Perm, int i)
{
  static mu_1__type_1 temp("Permute_mu_1__type_1",-1);
  int j;
  for (j=0; j<6; j++)
    array[j].Permute(Perm, i);
};
void mu_1__type_1::SimpleCanonicalize(PermSet& Perm)
{ Error.Error("Internal: Simple Canonicalization of Scalarset Array\n"); };
void mu_1__type_1::Canonicalize(PermSet& Perm){};
void mu_1__type_1::SimpleLimit(PermSet& Perm){}
void mu_1__type_1::ArrayLimit(PermSet& Perm) {}
void mu_1__type_1::Limit(PermSet& Perm){}
void mu_1__type_1::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for scalarset array.\n"); };
void mu_1__type_2::Permute(PermSet& Perm, int i)
{
  static mu_1__type_2 temp("Permute_mu_1__type_2",-1);
  int j;
  for (j=0; j<6; j++)
    array[j].Permute(Perm, i);
};
void mu_1__type_2::SimpleCanonicalize(PermSet& Perm)
{ Error.Error("Internal: Simple Canonicalization of Scalarset Array\n"); };
void mu_1__type_2::Canonicalize(PermSet& Perm){};
void mu_1__type_2::SimpleLimit(PermSet& Perm){}
void mu_1__type_2::ArrayLimit(PermSet& Perm) {}
void mu_1__type_2::Limit(PermSet& Perm){}
void mu_1__type_2::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for scalarset array.\n"); };
void mu_1__type_3::Permute(PermSet& Perm, int i)
{
  static mu_1__type_3 temp("Permute_mu_1__type_3",-1);
  int j;
  for (j=0; j<1; j++)
    array[j].Permute(Perm, i);
};
void mu_1__type_3::SimpleCanonicalize(PermSet& Perm)
{ Error.Error("Internal: Simple Canonicalization of Scalarset Array\n"); };
void mu_1__type_3::Canonicalize(PermSet& Perm){};
void mu_1__type_3::SimpleLimit(PermSet& Perm){}
void mu_1__type_3::ArrayLimit(PermSet& Perm) {}
void mu_1__type_3::Limit(PermSet& Perm){}
void mu_1__type_3::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for scalarset array.\n"); };
void mu_1__type_4::Permute(PermSet& Perm, int i)
{
  static mu_1__type_4 temp("Permute_mu_1__type_4",-1);
  int j;
  for (j=0; j<6; j++)
    array[j].Permute(Perm, i);
};
void mu_1__type_4::SimpleCanonicalize(PermSet& Perm)
{ Error.Error("Internal: Simple Canonicalization of Scalarset Array\n"); };
void mu_1__type_4::Canonicalize(PermSet& Perm){};
void mu_1__type_4::SimpleLimit(PermSet& Perm){}
void mu_1__type_4::ArrayLimit(PermSet& Perm) {}
void mu_1__type_4::Limit(PermSet& Perm){}
void mu_1__type_4::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for scalarset array.\n"); };

/********************
 Auxiliary function for error trace printing
 ********************/
bool match(state* ns, StatePtr p)
{
  unsigned int i;
  static PermSet Perm;
  static state temp;
  StateCopy(&temp, ns);
  if (args->symmetry_reduction.value)
    {
      if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {
        Perm.ResetToExplicit();
        for (i=0; i<Perm.count; i++)
          if (Perm.In(i))
            {
              if (ns != workingstate)
                  StateCopy(workingstate, ns);
              
              mu_goal_reached.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_goal_reached.MultisetSort();
              mu_engineblown.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_engineblown.MultisetSort();
              mu_running.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_running.MultisetSort();
              mu_cov.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_cov.MultisetSort();
              mu_f_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_h_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_all_event_true.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_g_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_TIME.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_distance.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_distance.MultisetSort();
              mu_stopped.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_stopped.MultisetSort();
              mu_transmission_fine.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_transmission_fine.MultisetSort();
              mu_robot_at.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_robot_at.MultisetSort();
              mu_visited.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_visited.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
      else {
        Perm.ResetToSimple();
        Perm.SimpleToOne();
        if (ns != workingstate)
          StateCopy(workingstate, ns);

          mu_goal_reached.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_goal_reached.MultisetSort();
          mu_engineblown.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_engineblown.MultisetSort();
          mu_running.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_running.MultisetSort();
          mu_cov.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_cov.MultisetSort();
          mu_f_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_f_n.MultisetSort();
          mu_h_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_h_n.MultisetSort();
          mu_all_event_true.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_all_event_true.MultisetSort();
          mu_g_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_g_n.MultisetSort();
          mu_TIME.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_TIME.MultisetSort();
          mu_distance.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_distance.MultisetSort();
          mu_stopped.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_stopped.MultisetSort();
          mu_transmission_fine.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_transmission_fine.MultisetSort();
          mu_robot_at.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_robot_at.MultisetSort();
          mu_visited.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_visited.MultisetSort();
        if (p.compare(workingstate)) {
          StateCopy(workingstate,&temp); return TRUE; }

        while (Perm.NextPermutation())
          {
            if (ns != workingstate)
              StateCopy(workingstate, ns);
              
              mu_goal_reached.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_goal_reached.MultisetSort();
              mu_engineblown.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_engineblown.MultisetSort();
              mu_running.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_running.MultisetSort();
              mu_cov.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_cov.MultisetSort();
              mu_f_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_h_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_all_event_true.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_g_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_TIME.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_distance.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_distance.MultisetSort();
              mu_stopped.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_stopped.MultisetSort();
              mu_transmission_fine.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_transmission_fine.MultisetSort();
              mu_robot_at.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_robot_at.MultisetSort();
              mu_visited.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_visited.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
    }
  if (!args->symmetry_reduction.value
      && args->multiset_reduction.value)
    {
      if (ns != workingstate)
          StateCopy(workingstate, ns);
      mu_goal_reached.MultisetSort();
      mu_engineblown.MultisetSort();
      mu_running.MultisetSort();
      mu_cov.MultisetSort();
      mu_f_n.MultisetSort();
      mu_h_n.MultisetSort();
      mu_all_event_true.MultisetSort();
      mu_g_n.MultisetSort();
      mu_TIME.MultisetSort();
      mu_distance.MultisetSort();
      mu_stopped.MultisetSort();
      mu_transmission_fine.MultisetSort();
      mu_robot_at.MultisetSort();
      mu_visited.MultisetSort();
      if (p.compare(workingstate)) {
        StateCopy(workingstate,&temp); return TRUE; }
      StateCopy(workingstate,&temp);
      return FALSE;
    }
  return (p.compare(ns));
}

/********************
 Canonicalization by fast exhaustive generation of
 all permutations
 ********************/
void SymmetryClass::Exhaustive_Fast_Canonicalize(state* s)
{
  unsigned int i;
  static state temp;
  Perm.ResetToExplicit();

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_goal_reached.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_goal_reached.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_engineblown.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_engineblown.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_running.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_running.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_cov.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_cov.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_f_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_f_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_h_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_h_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_all_event_true.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_all_event_true.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_g_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_g_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_TIME.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_TIME.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_distance.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_distance.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_stopped.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_stopped.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_transmission_fine.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_transmission_fine.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_robot_at.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_robot_at.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_visited.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_visited.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 ********************/
void SymmetryClass::Heuristic_Fast_Canonicalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 and use less local memory
 ********************/
void SymmetryClass::Heuristic_Small_Mem_Canonicalize(state* s)
{
  unsigned long cycle;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Normalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and for all other variables, pick any remaining permutation
 ********************/
void SymmetryClass::Heuristic_Fast_Normalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
  Include
 ********************/
#include "upm_epilog.hpp"
