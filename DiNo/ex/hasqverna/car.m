domain: file "car.pddl"
problem: file "prob01.pddl"
message: " Time Discretisation = 0.1"
message: " Digits for representing the integer part of a real =  5"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(7,4);
	integer: -1000..1000;

	 TIME_type: real(7,2);

	robot : Enum {kenny};
	waypoint : Enum {wp0,wp1,wp2,wp3,wp4,wp5};

const 
	 T:0.1;

	constant : 1.00000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	cov[pddlname:"cov";] :  real_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;


	running[pddlname: "running";] :  boolean;
	stopped[pddlname: "stopped";] :  boolean;
	engineblown[pddlname: "engineblown";] :  boolean;
	transmission_fine[pddlname: "transmission_fine";] :  boolean;
	goal_reached[pddlname: "goal_reached";] :  boolean;
	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun decrease_cov_action_goto_waypoint(cov : real_type ; distance : real_type ; constant : real_type ; ): real_type "car.h" ;
externfun increase_cov_action_goto_goal(cov : real_type ; ): real_type ;
procedure set_running(  value : boolean);
BEGIN
	running := value;
END;

function get_running(): boolean;
BEGIN
	return 	running;
END;

procedure set_stopped(  value : boolean);
BEGIN
	stopped := value;
END;

function get_stopped(): boolean;
BEGIN
	return 	stopped;
END;

procedure set_engineblown(  value : boolean);
BEGIN
	engineblown := value;
END;

function get_engineblown(): boolean;
BEGIN
	return 	engineblown;
END;

procedure set_transmission_fine(  value : boolean);
BEGIN
	transmission_fine := value;
END;

function get_transmission_fine(): boolean;
BEGIN
	return 	transmission_fine;
END;

procedure set_goal_reached(  value : boolean);
BEGIN
	goal_reached := value;
END;

function get_goal_reached(): boolean;
BEGIN
	return 	goal_reached;
END;

procedure set_robot_at( r : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[r][wp] := value;
END;

function get_robot_at( r : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[r][wp];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;






procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
BEGIN
 event_triggered := true;
while (event_triggered) do 
 event_triggered := false;
END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;BEGIN
 process_updated := false; end_while := false;
while (!end_while) do 
 IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

ruleset r:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_waypoint " 
(robot_at[r][from]) ==> 
pddlname: " goto_waypoint"; 
BEGIN
robot_at[r][to_]:= true; 
robot_at[r][from]:= false; 
cov := decrease_cov_action_goto_waypoint(cov , distance[from][to_] , constant  );

END; 
END; 
END; 
END;

ruleset r:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_goal " 
(robot_at[r][from]) & ((( cov) < (5.00000))) ==> 
pddlname: " goto_goal"; 
BEGIN
robot_at[r][to_]:= true; 
visited[to_]:= true; 
robot_at[r][from]:= false; 
cov := increase_cov_action_goto_goal(cov  );

END; 
END; 
END; 
END;

ruleset wp:waypoint do 
 action rule " stop " 
((( cov) < (10.0000))) & (visited[wp]) ==> 
pddlname: " stop"; 
BEGIN
goal_reached:= true; 

END; 
END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
set_running(false);

   set_stopped(false);

   set_engineblown(false);

   set_transmission_fine(false);

   set_goal_reached(false);

   for r : robot do 
     for wp : waypoint do 
       set_robot_at(r,wp, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   for wp1 : waypoint do 
     for wp2 : waypoint do 
       distance[wp1][wp2] := 0.0 ;
END; END;  -- close for
   cov := 0.0 ;


robot_at[kenny][wp0]:= true; 
distance[wp1][wp0] := 10.0000;
distance[wp1][wp2] := 10.0000;
distance[wp2][wp0] := 14.1400;
distance[wp2][wp1] := 10.0000;
distance[wp0][wp1] := 10.0000;
distance[wp0][wp2] := 14.1400;
distance[wp0][wp3] := 10.0000;
distance[wp1][wp3] := 20.0000;
distance[wp3][wp1] := 20.0000;
distance[wp3][wp0] := 10.0000;
distance[wp3][wp2] := 10.0000;
distance[wp2][wp3] := 10.0000;
distance[wp4][wp0] := 5.00000;
distance[wp0][wp4] := 5.00000;
distance[wp4][wp1] := 8.00000;
distance[wp1][wp4] := 8.00000;
distance[wp4][wp2] := 4.00000;
distance[wp2][wp4] := 4.00000;
distance[wp4][wp3] := 24.0000;
distance[wp3][wp4] := 24.0000;
distance[wp5][wp0] := 16.0000;
distance[wp0][wp5] := 16.0000;
distance[wp5][wp1] := 24.0000;
distance[wp1][wp5] := 24.0000;
distance[wp5][wp2] := 30.0000;
distance[wp2][wp5] := 30.0000;
distance[wp5][wp3] := 20.0000;
distance[wp3][wp5] := 20.0000;
distance[wp5][wp4] := 10.0000;
distance[wp4][wp5] := 10.0000;
cov := 50.0000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (visited[wp2]) & (goal_reached)& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


