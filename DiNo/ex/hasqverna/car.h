#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

double round_k_digits(double n, unsigned k){
	double prec = pow(0.1,k);
	double round = (n>0) ? (n+prec/2) : (n-prec/2);
	return round-fmod(round,prec);
}

double ext_assignment(double n){
	return round_k_digits(n,2);
}

double decrease_cov_action_goto_waypoint(double cov, double distance, double constant ) {
	 return round_k_digits(cov-((distance) / (constant)),2); 
}

double increase_cov_action_goto_goal(double cov ) {
	 return round_k_digits(cov+(5.00000),2); 
}

