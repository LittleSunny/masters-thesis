(define (domain car)
(:requirements :typing :durative-actions :fluents :time :strips  :disjunctive-preconditions :durative-actions
:negative-preconditions :timed-initial-literals)
(:types
	waypoint
	robot
)
(:predicates (running) (stopped) (engineBlown) (transmission_fine) (goal_reached) 
(robot_at ?r - robot ?wp - waypoint)
	(visited ?wp - waypoint)
)

(:functions (distance ?wp1 ?wp2 - waypoint) (cov) (constant) )

;; Move between any two waypoints, avoiding terrain
(:action goto_waypoint
	:parameters (?r - robot ?from ?to - waypoint)
	:precondition  (and (robot_at ?r ?from) )
	:effect (and (not (robot_at ?r ?from)) (decrease (cov) (/  (distance ?from ?to)(constant))) (robot_at ?r ?to))
	
)
(:action goto_goal
	:parameters (?r - robot ?from ?to - waypoint)
	:precondition  (and (robot_at ?r ?from) (< (cov) 5 ))
	:effect (and (not (robot_at ?r ?from)) (increase (cov) 5) (robot_at ?r ?to) (visited ?to))
	
)


(:action stop
:parameters(?wp - waypoint)
:precondition(and (< (cov) 10) (visited ?wp) )
:effect(and(goal_reached))
)

)
