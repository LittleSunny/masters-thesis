domain: file "generator.pddl"
problem: file "prob01.pddl"
message: " Time Discretisation = 0.1"
message: " Digits for representing the integer part of a real =  5"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(7,4);
	integer: -1000..1000;

	 TIME_type: real(7,2);

	generator : Enum {gen};
	tank : Enum {tank1,tank2,tank3};

const 
	 T:0.1;


var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	capacity[pddlname:"capacity";] : Array [generator] of  real_type;
	fuellevel[pddlname:"fuellevel";] : Array [generator] of  real_type;
	generate_clock_started[pddlname:"generate";] : Array [generator] of  boolean ;
	generate_clock [pddlname:"generate";] : Array [generator] of  TIME_type ;
	refuel_clock_started[pddlname:"refuel";] : Array [generator] of Array [tank] of  boolean ;
	refuel_clock [pddlname:"refuel";] : Array [generator] of Array [tank] of  TIME_type ;


	refueling[pddlname: "refueling";] : Array [generator] of  boolean;
	generator_ran[pddlname: "generator-ran";] :  boolean;
	available[pddlname: "available";] : Array [tank] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun decrease_fuellevel_duraction_process_generate(fuellevel : real_type ; T : real_type ; ): real_type "generator.h" ;
externfun increase_fuellevel_duraction_process_refuel(fuellevel : real_type ; T : real_type ; ): real_type ;
procedure set_refueling( g : generator ;  value : boolean);
BEGIN
	refueling[g] := value;
END;

function get_refueling( g : generator): boolean;
BEGIN
	return 	refueling[g];
END;

procedure set_generator_ran(  value : boolean);
BEGIN
	generator_ran := value;
END;

function get_generator_ran(): boolean;
BEGIN
	return 	generator_ran;
END;

procedure set_available( t : tank ;  value : boolean);
BEGIN
	available[t] := value;
END;

function get_available( t : tank): boolean;
BEGIN
	return 	available[t];
END;



procedure process_generate( g : generator);
BEGIN
	 IF (generate_clock_started[g]) THEN 
		 generate_clock[g]:= generate_clock[g] + T ;
fuellevel[g] := decrease_fuellevel_duraction_process_generate(fuellevel[g] , T  );
	 ENDIF;

END;
procedure process_refuel( g : generator; t : tank);
BEGIN
	 IF (refuel_clock_started[g][t]) THEN 
		 refuel_clock[g][t]:= refuel_clock[g][t] + T ;
fuellevel[g] := increase_fuellevel_duraction_process_refuel(fuellevel[g] , T  );
	 ENDIF;

END;
function event_generate_failure( g : generator) : boolean; 
BEGIN
	 IF (generate_clock_started[g])& !((( fuellevel[g]) >= (0.00000))) THEN 
		 generate_clock[g]:= generate_clock[g]+ T ;
		 all_event_true := false ;
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;
function event_refuel_failure( g : generator; t : tank) : boolean; 
BEGIN
	 IF (refuel_clock_started[g][t])& !((true) & ((( fuellevel[g]) < (capacity[g])))) THEN 
		 refuel_clock[g][t]:= refuel_clock[g][t]+ T ;
		 all_event_true := false ;
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;


procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   event_generate_failure_triggered :  Array [generator] of  boolean;
   event_refuel_failure_triggered :  Array [generator] of  Array [tank] of  boolean;
BEGIN
 event_triggered := true;
   for g : generator do 
       event_generate_failure_triggered[g] := false;
       END; -- close for
   for g : generator do 
     for t : tank do 
           event_refuel_failure_triggered[g][t] := false;
           END;END; -- close for
while (event_triggered) do 
 event_triggered := false;
     for g : generator do 
       if(! event_generate_failure_triggered[g]) then 
       event_generate_failure_triggered[g] := event_generate_failure(g);
       event_triggered := event_triggered | event_generate_failure_triggered[g]; 
       endif;
END; -- close for
       for g : generator do 
         for t : tank do 
           if(! event_refuel_failure_triggered[g][t]) then 
           event_refuel_failure_triggered[g][t] := event_refuel_failure(g,t);
           event_triggered := event_triggered | event_refuel_failure_triggered[g][t]; 
           endif;
END;END; -- close for
END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;
for g : generator do 
if (generate_clock[g] > 1000.00) then return true;
 endif;
END; -- close for 
  for g : generator do 
    for t : tank do 
if (refuel_clock[g][t] > 10.0000) then return true;
 endif;
END; -- close for 
END; -- close for 

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;
for g : generator do 
if (	generate_clock_started[g] = true) then return true;
 endif;
END; -- close for 
  for g : generator do 
    for t : tank do 
if (	refuel_clock_started[g][t] = true) then return true;
 endif;
END; -- close for 
END; -- close for 

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_generate_enabled :  Array [generator] of  boolean;
   process_refuel_enabled :  Array [generator] of  Array [tank] of  boolean;
BEGIN
 process_updated := false; end_while := false;
   for g : generator do 
       process_generate_enabled[g] := false;
       END; -- close for
   for g : generator do 
     for t : tank do 
           process_refuel_enabled[g][t] := false;
           END;END; -- close for
while (!end_while) do 
      for g : generator do 
       if (true & generate_clock_started[g] &  !process_generate_enabled[g]) then
       process_updated := true;
       process_generate(g);
       process_generate_enabled[g] := true;
       endif;
END; -- close for
       for g : generator do 
         for t : tank do 
           if ((true)  & refuel_clock_started[g][t] &  !process_refuel_enabled[g][t]) then
           process_updated := true;
           process_refuel(g,t);
           process_refuel_enabled[g][t] := true;
           endif;
END;END; -- close for
IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;



ruleset g:generator do 
 durative_start rule " generate_start " 
( !generate_clock_started[g])  & all_event_true ==> 
pddlname: " generate"; 
BEGIN
generate_clock_started[g]:= true;

END; 
END; 



ruleset g:generator do 
 durative_end rule " generate_end " 
( generate_clock_started[g]) & (( generate_clock[g]) = (1000.00))  & ((generate_clock[g])  > 0.0) & all_event_true ==> 
pddlname: " generate"; 
BEGIN
generate_clock_started[g]:= false;
generate_clock[g]:= 0.0;
generator_ran:= true; 

END; 
END; 




ruleset g:generator do 
 ruleset t:tank do 
 durative_start rule " refuel_start " 
( !refuel_clock_started[g][t]) & (available[t])  & all_event_true ==> 
pddlname: " refuel"; 
BEGIN
refuel_clock_started[g][t]:= true;
available[t]:= false; 
refueling[g]:= true; 

END; 
END; 
END; 



ruleset g:generator do 
 ruleset t:tank do 
 durative_end rule " refuel_end " 
( refuel_clock_started[g][t]) & (( refuel_clock[g][t]) = (10.0000))   & ((refuel_clock[g][t])  > 0.0) & all_event_true ==> 
pddlname: " refuel"; 
BEGIN
refuel_clock_started[g][t]:= false;
refuel_clock[g][t]:= 0.0;
refueling[g]:= false; 

END; 
END; 
END; 


clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for g : generator do 
  set_refueling(g, false);
END;  -- close for
   set_generator_ran(false);

   for t : tank do 
     set_available(t, false);
END;  -- close for
   for g : generator do 
     fuellevel[g] := 0.0 ;
END;  -- close for
   for g : generator do 
     capacity[g] := 0.0 ;
END;  -- close for
available[tank1]:= true; 
fuellevel[gen] := 980.000;
capacity[gen] := 1000.00;

-- durative action "generate" clock initialization
 for g : generator do 
   generate_clock_started[g]:= false;
  generate_clock[g]:= 0.0;
END; -- for ends

-- durative action "refuel" clock initialization
 for g : generator do 
   for t : tank do 
     refuel_clock_started[g][t]:= false;
    refuel_clock[g][t]:= 0.0;
END; END; -- for ends

all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 generator_ran& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


