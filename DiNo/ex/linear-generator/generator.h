#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

double round_k_digits(double n, unsigned k){
	double prec = pow(0.1,k);
	double round = (n>0) ? (n+prec/2) : (n-prec/2);
	return round-fmod(round,prec);
}

double ext_assignment(double n){
	return round_k_digits(n,2);
}

double decrease_fuellevel_duraction_process_generate(double fuellevel, double T ) {
	 return round_k_digits(fuellevel-(( T ) * (1.00000)),2); 
}

double increase_fuellevel_duraction_process_refuel(double fuellevel, double T ) {
	 return round_k_digits(fuellevel+(( T ) * (2.00000)),2); 
}

