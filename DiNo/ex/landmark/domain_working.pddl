(define (domain landmark)

(:requirements :typing :durative-actions :fluents :time :strips  :disjunctive-preconditions :durative-actions
:negative-preconditions :timed-initial-literals)

(:types
  waypoint
  robot
)

(:predicates (moving) 
       (robot_at ?r - robot ?wp - waypoint)
       (visited ?wp - waypoint)
       (observe)
)

(:functions (distance ?wp1 ?wp2 - waypoint) (cov) (dFactor) (finalTrace) (relativeD) (counter)
)  
;; relativeD- a variable to store the distance between wps, dFactor- distance factor, to see how many times kalman prediction to be done
;; cov is the initial covarianc trace, finalTrace- the required trace upon reaching the goal state

;; Move between any two waypoints, along the straight line between the two waypoints

(:action goto_waypoint
  :parameters (?r - robot ?from ?to - waypoint)
  :precondition  (and (robot_at ?r ?from) (not (observe)) (moving)) 
  :effect (and (not (robot_at ?r ?from)) (robot_at ?r ?to)
         (assign (relativeD) (distance ?from ?to))
         (assign (counter) 0)
         (not (moving))          
)
)

(:action prediction
  :parameters (?r - robot ?to - waypoint)
  :precondition  (and (> (relativeD) 0) (< (relativeD) (dFactor)) (> (counter) 0)) 
  :effect (and (increase (cov)  1) )
)

(:action update
  :parameters ()
  :precondition  (and (> (relativeD) 0) (< (relativeD) (dFactor)))
  :effect (and 
         (decrease (cov) 1 )
                     (moving)
)
)


;; to calculate the number of prediction steps needed
(:process control
  :parameters()
  :precondition (and (not (moving)) )
  :effect (and  (decrease (relativeD) (* #t (dFactor))) 
)
)
)
