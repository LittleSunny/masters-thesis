(define (domain landmark)

(:requirements :typing :durative-actions :fluents :time :strips  :disjunctive-preconditions :durative-actions
:negative-preconditions :timed-initial-literals)

(:types
  waypoint
  robot
)

(:predicates 
       (robot_at ?r - robot ?wp - waypoint)
       (visited ?wp - waypoint)
       (observe)
       (moving ?r - robot ?to - waypoint)
    
)

(:functions (distance ?wp1 ?wp2 - waypoint) (cov) (dFactor) (finalTrace) (relativeD) (counter) (predict_covariance) (update_covariance)
)  
;; relativeD- a variable to store the distance between wps, dFactor- distance factor, to see how many times kalman prediction to be done
;; cov is the initial covarianc trace, finalTrace- the required trace upon reaching the goal state

;; Move between any two waypoints, along the straight line between the two waypoints

(:action goto_waypoint
  :parameters (?r - robot ?from ?to - waypoint)
  :precondition  (and (robot_at ?r ?from) (observe) (not(robot_at ?r ?to)) (not (visited ?to ))) 
  :effect (and (not (robot_at ?r ?from)) 
            (assign (relativeD) (distance ?from ?to))
            (moving ?r ?to)
            (not (observe))
            (assign (counter) 0)
            (increase (predict_covariance) 0)
            (increase (update_covariance) 0)
            )           
)

(:action reached
    :parameters(?r - robot ?to - waypoint)
    :precondition(and (moving ?r ?to) (< (relativeD) (dFactor)))
    :effect(and (robot_at ?r ?to) (visited ?to) (not (moving ?r ?to)))
)   
  

;;(:action prediction
  ;;:parameters ()
  ;;:precondition  (and (> (relativeD) 0) (< (relativeD) (dFactor)) ) 
  ;;:effect (and 
    ;;                 (increase (cov) 1)
;;)
;;)

(:event prediction
  ;;:parameters(?r - robot ?to - waypoint)
  :parameters()
    :precondition(and (= (counter) 1) )
    :effect (and 
               ;;(increase (cov) 1)
               (increase (cov) (predict_covariance))
              (assign (counter) 0)
        )
)

(:event update
  :parameters (?r - robot ?to - waypoint)
  ;;:precondition  (and (> (relativeD) 0) (< (relativeD) (dFactor)) )
  :precondition  (and (robot_at ?r ?to) (not(observe)) )
  :effect (and 
         ;;(decrease (cov) 5)
          (decrease (cov) (update_covariance))
         (observe)
                     )
)


;; to calculate the number of prediction steps needed
(:process control
  :parameters()
  :precondition (and  (> (relativeD) 0) )
  :effect (and  (decrease (relativeD) (* #t (dFactor)))   (increase (counter) (* #t 1))
)
)
)
