#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

double round_k_digits(double n, unsigned k){
	double prec = pow(0.1,k);
	double round = (n>0) ? (n+prec/2) : (n-prec/2);
	return round-fmod(round,prec);
}

double ext_assignment(double n){
	return round_k_digits(n,5.000000);
}

double increase_cov_event_prediction(double cov, double predict_covariance ) {
	 return round_k_digits(cov+(predict_covariance),5.000000); 
}

double assign_counter_event_prediction() {
	 return round_k_digits(0.00000,5.000000); 
}

double increase_counter_process_control(double counter, double T ) {
	 return round_k_digits(counter+(( T ) * (1.00000)),5.000000); 
}

double decrease_relatived_process_control(double relatived, double T, double dfactor ) {
	 return round_k_digits(relatived-(( T ) * (dfactor)),5.000000); 
}

double assign_relatived_action_goto_waypoint(double distance ) {
	 return round_k_digits(distance,5.000000); 
}

double assign_counter_action_goto_waypoint() {
	 return round_k_digits(0.00000,5.000000); 
}

double increase_predict_covariance_action_goto_waypoint(double predict_covariance ) {
	 return round_k_digits(predict_covariance+(0.00000),5.000000); 
}

double increase_update_covariance_action_goto_waypoint(double update_covariance ) {
	 return round_k_digits(update_covariance+(0.00000),5.000000); 
}

double decrease_cov_action_update(double cov, double update_covariance ) {
	 return round_k_digits(cov-(update_covariance),5.000000); 
}

