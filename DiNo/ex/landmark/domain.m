domain: file "domain.pddl"
problem: file "prob01.pddl"
message: " Time Discretisation = 1.000000"
message: " Digits for representing the integer part of a real =  5.000000"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(10,4);
	integer: -1000..1000;

	 TIME_type: real(12,7);

	robot : Enum {kenny};
	waypoint : Enum {wp0,wp1,wp2,wp3,wp4,wp5};

const 
	 T:1.000000;

	dfactor : 0.500000;
	finaltrace : 0.200000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	counter[pddlname:"counter";] :  real_type;
	cov[pddlname:"cov";] :  real_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	predict_covariance[pddlname:"predict_covariance";] :  real_type;
	relatived[pddlname:"relatived";] :  real_type;
	update_covariance[pddlname:"update_covariance";] :  real_type;


	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;
	observe[pddlname: "observe";] :  boolean;
	moving[pddlname: "moving";] : Array [robot] of Array [waypoint] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun increase_cov_event_prediction(cov : real_type ; predict_covariance : real_type ; ): real_type "domain.h" ;
externfun assign_counter_event_prediction(): real_type ;
externfun increase_counter_process_control(counter : real_type ; T : real_type ; ): real_type ;
externfun decrease_relatived_process_control(relatived : real_type ; T : real_type ; dfactor : real_type ; ): real_type ;
externfun assign_relatived_action_goto_waypoint(distance : real_type ; ): real_type ;
externfun assign_counter_action_goto_waypoint(): real_type ;
externfun increase_predict_covariance_action_goto_waypoint(predict_covariance : real_type ; ): real_type ;
externfun increase_update_covariance_action_goto_waypoint(update_covariance : real_type ; ): real_type ;
externfun decrease_cov_action_update(cov : real_type ; update_covariance : real_type ; ): real_type ;
procedure set_robot_at( r : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[r][wp] := value;
END;

function get_robot_at( r : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[r][wp];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;

procedure set_observe(  value : boolean);
BEGIN
	observe := value;
END;

function get_observe(): boolean;
BEGIN
	return 	observe;
END;

procedure set_moving( r : robot ; to_ : waypoint ;  value : boolean);
BEGIN
	moving[r][to_] := value;
END;

function get_moving( r : robot ; to_ : waypoint): boolean;
BEGIN
	return 	moving[r][to_];
END;









function prediction () : boolean; 
BEGIN
IF (((( counter) > (0.00000)))) THEN 
cov := increase_cov_event_prediction(cov , predict_covariance  );
counter := assign_counter_event_prediction();
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;

procedure process_control (); 
BEGIN
IF (((( relatived) > (-dfactor)))) THEN 
counter := increase_counter_process_control(counter , T  );
relatived := decrease_relatived_process_control(relatived , T , dfactor  );

ENDIF ; 

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   prediction_triggered :  boolean;
BEGIN
 event_triggered := true;
   prediction_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! prediction_triggered) then 
   prediction_triggered := prediction();
   event_triggered := event_triggered | prediction_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_control_enabled :  boolean;
BEGIN
 process_updated := false; end_while := false;
   process_control_enabled := false;
while (!end_while) do 
    if (((( relatived) > (-dfactor))) &  !process_control_enabled) then
   process_updated := true;
   process_control();
   process_control_enabled := true;
   endif;

IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

ruleset r:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_waypoint " 
(robot_at[r][from]) & (observe) & (!(robot_at[r][to_])) & (!(visited[to_])) ==> 
pddlname: " goto_waypoint"; 
BEGIN
moving[r][to_]:= true; 
robot_at[r][from]:= false; 
observe:= false; 
relatived := assign_relatived_action_goto_waypoint(distance[from][to_]  );
counter := assign_counter_action_goto_waypoint();
predict_covariance := increase_predict_covariance_action_goto_waypoint(predict_covariance  );
update_covariance := increase_update_covariance_action_goto_waypoint(update_covariance  );

END; 
END; 
END; 
END;

ruleset r:robot do 
 ruleset to_:waypoint do 
 action rule " reached " 
(moving[r][to_]) & ((( relatived) <= (0.00000))) ==> 
pddlname: " reached"; 
BEGIN
robot_at[r][to_]:= true; 
visited[to_]:= true; 
moving[r][to_]:= false; 

END; 
END; 
END;

ruleset r:robot do 
 ruleset to_:waypoint do 
 action rule " update " 
(robot_at[r][to_]) & (!(observe)) ==> 
pddlname: " update"; 
BEGIN
observe:= true; 
cov := decrease_cov_action_update(cov , update_covariance  );

END; 
END; 
END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for r : robot do 
  for wp : waypoint do 
    set_robot_at(r,wp, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   set_observe(false);

   for r : robot do 
     for to_ : waypoint do 
       set_moving(r,to_, false);
END; END;  -- close for
   for wp1 : waypoint do 
     for wp2 : waypoint do 
       distance[wp1][wp2] := 0.0 ;
END; END;  -- close for
   cov := 0.0 ;



   relatived := 0.0 ;

   counter := 0.0 ;

   predict_covariance := 0.0 ;

   update_covariance := 0.0 ;

robot_at[kenny][wp0]:= true; 
observe:= true; 
distance[wp0][wp1] := 4.00000;
distance[wp0][wp2] := 3.00000;
distance[wp0][wp3] := 2.82843;
distance[wp0][wp4] := 6.32456;
distance[wp0][wp5] := 5.09902;
distance[wp1][wp0] := 4.00000;
distance[wp1][wp2] := 5.00000;
distance[wp1][wp3] := 2.82843;
distance[wp1][wp4] := 8.48528;
distance[wp1][wp5] := 5.83095;
distance[wp2][wp0] := 3.00000;
distance[wp2][wp1] := 5.00000;
distance[wp2][wp3] := 2.23607;
distance[wp2][wp4] := 3.60555;
distance[wp2][wp5] := 2.23607;
distance[wp3][wp0] := 2.82843;
distance[wp3][wp1] := 2.82843;
distance[wp3][wp2] := 2.23607;
distance[wp3][wp4] := 5.65685;
distance[wp3][wp5] := 3.16228;
distance[wp4][wp0] := 6.32456;
distance[wp4][wp1] := 8.48528;
distance[wp4][wp2] := 3.60555;
distance[wp4][wp3] := 5.65685;
distance[wp4][wp5] := 3.16228;
distance[wp5][wp0] := 5.09902;
distance[wp5][wp1] := 5.83095;
distance[wp5][wp2] := 2.23607;
distance[wp5][wp3] := 3.16228;
distance[wp5][wp4] := 3.16228;
cov := 0.820000;
relatived := 0.00000;
predict_covariance := 0.820000;
update_covariance := 0.820000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (robot_at[kenny][wp5]) & ((( cov) < (finaltrace)))& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


