(define (domain landmark_update)

(:requirements :typing :durative-actions :fluents :time :strips  :disjunctive-preconditions :durative-actions
:negative-preconditions :timed-initial-literals)

(:types
  waypoint
  landmark
  robot
)

(:predicates 
       (robot_at ?r - robot ?wp - waypoint)
       (visited ?wp - waypoint)
       ;;(observe)
       (moving ?r - robot ?to - waypoint)
    
)

(:functions (distance ?wp1 ?wp2 - waypoint) (cov) (dFactor) (finalTrace) (relativeD) (counter) (predict_covariance) (update_covariance) (sensorRange) 
            (land_dist ?wp- waypoint ?lm-landmark) (lm_number) (totalTime)
)  
;; relativeD- a variable to store the distance between wps, dFactor- distance factor, to see how many times kalman prediction to be done
;; cov is the initial covarianc trace, finalTrace- the required trace upon reaching the goal state

;; Move between any two waypoints, along the straight line between the two waypoints

(:action goto_waypoint
  :parameters (?r - robot ?from ?to - waypoint)
  :precondition  (and (robot_at ?r ?from) (not(robot_at ?r ?to)) (not (visited ?to ))) 
  :effect (and (not (robot_at ?r ?from)) 
            (assign (relativeD) (distance ?from ?to))
            (moving ?r ?to)
            (assign (counter) 0)
            (assign (lm_number) 5)
            (increase (predict_covariance) 0)
            (increase (update_covariance) 0)
            )           
)

(:action reached
    :parameters(?r - robot ?to - waypoint)
    :precondition(and (moving ?r ?to) (<= (relativeD) 0))
    :effect(and (robot_at ?r ?to) (visited ?to) (not (moving ?r ?to)))
)   
  

(:event prediction
  ;;:parameters(?r - robot ?to - waypoint)
    :parameters()
    :precondition(and (> (counter) 0) )
    :effect (and (increase (cov) (predict_covariance))
                 (assign (counter) 0)
                 (assign (lm_number) 5)
)
)

(:event update
  :parameters (?r - robot ?to - waypoint)
  :precondition  (and (> (lm_number) 0) )
;;  :precondition  (and (robot_at ?r ?to) )
  :effect (and 
           (decrease (cov) (update_covariance))
           (assign (lm_number) (- (lm_number) 1))
)
)

;; to calculate the number of prediction steps needed
(:process control
  :parameters()
  :precondition (and  (> (relativeD) (-dFactor)) (= (lm_number) 0) )
  :effect (and  (decrease (relativeD) (* #t (dFactor)))   (increase (counter) (* #t 1))
                (increase (totalTime) (* #t 1)) 
)
)

(:process lm_check
  :parameters()
  :precondition (and  (> (relativeD) (-dFactor)) (> (lm_number) 0) )
  :effect (and  (increase (totalTime) (* #t 1))
)
)
)
