(define (problem landmark_prb)
(:domain landmark)
    (:objects
    kenny - robot
    wp0 wp1  - waypoint
)
    (:init 
    (= (distance wp0 wp1) 4)
    (= (distance wp1 wp0) 4)
   
    (robot_at kenny wp0)
    (= (cov) 0.82)
    (= (relativeD) 0)
    (= (finalTrace) 0.4)
    (= (dFactor) 1)
    (observe)
    (= (predict_covariance) 0.82)
    (= (update_covariance) 0.82)
    )
     (:goal (and (robot_at kenny wp1) (< (cov ) finalTrace)))
     (:metric minimize(total-time))
)
