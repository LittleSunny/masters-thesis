#!/usr/bin/env sh
#cd $(dirname $0)
count=1
discretize=1
mkdir -p "outputplans"
while [ $discretize -lt 5 ]
do
  while [ $count -lt 3 ]
  do
  discretizefactor=$(echo "0.4*$discretize" | bc)
  counter=$(echo "12*$count" | bc)
  th_val="th$counter"
  filename="outputplans/plan_$discretizefactor&$th_val.pddl"
  ../../bin/dino domain.pddl prob01.pddl --force --custom $discretizefactor 5 5
  ./domain_planner -$th_val -ext ../ext_lib/build/libext_lib.so -format:pddlvv -output $filename
  count=`expr $count + 1`
  done
  count=1
  discretize=`expr $discretize + 1`
done
