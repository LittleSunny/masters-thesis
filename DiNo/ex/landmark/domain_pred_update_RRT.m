domain: file "domain_pred_update_RRT.pddl"
problem: file "problem_pred_update_RRT.pddl"
message: " Time Discretisation = 1.000000"
message: " Digits for representing the integer part of a real =  5.000000"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(10,4);
	integer: -1000..1000;

	 TIME_type: real(12,7);

	covariance : Enum {dummy1,dummy2};
	robot : Enum {kenny};
	waypoint : Enum {endpoint,wp0,wp1,wp10,wp11,wp12,wp13,wp14,wp15,wp16,wp17,wp18,wp19,wp2,wp20,wp21,wp22,wp23,wp24,wp25,wp26,wp27,wp28,wp29,wp3,wp4,wp5,wp6,wp7,wp8,wp9};

const 
	 T:1.000000;

	dfactor : 0.500000;
	finaltrace : 0.500000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	counter[pddlname:"counter";] :  real_type;
	cov[pddlname:"cov";] :  real_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	predict_covariance[pddlname:"predict_covariance";] :  real_type;
	relatived[pddlname:"relatived";] :  real_type;
	update_covariance[pddlname:"update_covariance";] :  real_type;


	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;
	observe[pddlname: "observe";] :  boolean;
	moving[pddlname: "moving";] : Array [robot] of Array [waypoint] of  boolean;
	connected[pddlname: "connected";] : Array [waypoint] of Array [waypoint] of  boolean;
	lessthan[pddlname: "lessthan";] : Array [covariance] of Array [covariance] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun assign_cov_event_predict_n_update(update_covariance : real_type ; ): real_type "domain_pred_update_RRT.h" ;
externfun assign_counter_event_predict_n_update(): real_type ;
externfun increase_counter_process_control(counter : real_type ; T : real_type ; ): real_type ;
externfun decrease_relatived_process_control(relatived : real_type ; T : real_type ; dfactor : real_type ; ): real_type ;
externfun assign_relatived_action_goto_waypoint(distance : real_type ; ): real_type ;
externfun assign_counter_action_goto_waypoint(): real_type ;
externfun increase_update_covariance_action_goto_waypoint(update_covariance : real_type ; ): real_type ;
externfun increase_predict_covariance_action_goto_waypoint(predict_covariance : real_type ; ): real_type ;
procedure set_robot_at( r : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[r][wp] := value;
END;

function get_robot_at( r : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[r][wp];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;

procedure set_observe(  value : boolean);
BEGIN
	observe := value;
END;

function get_observe(): boolean;
BEGIN
	return 	observe;
END;

procedure set_moving( r : robot ; to_ : waypoint ;  value : boolean);
BEGIN
	moving[r][to_] := value;
END;

function get_moving( r : robot ; to_ : waypoint): boolean;
BEGIN
	return 	moving[r][to_];
END;

procedure set_connected( from : waypoint ; to_ : waypoint ;  value : boolean);
BEGIN
	connected[from][to_] := value;
END;

function get_connected( from : waypoint ; to_ : waypoint): boolean;
BEGIN
	return 	connected[from][to_];
END;

procedure set_lessthan( c : covariance ; f : covariance ;  value : boolean);
BEGIN
	lessthan[c][f] := value;
END;

function get_lessthan( c : covariance ; f : covariance): boolean;
BEGIN
	return 	lessthan[c][f];
END;









function predict_n_update () : boolean; 
BEGIN
IF (((( counter) > (0.00000)))) THEN 
cov := assign_cov_event_predict_n_update(update_covariance  );
counter := assign_counter_event_predict_n_update();
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;

procedure process_control (); 
BEGIN
IF (((( relatived) > (-dfactor)))) THEN 
counter := increase_counter_process_control(counter , T  );
relatived := decrease_relatived_process_control(relatived , T , dfactor  );

ENDIF ; 

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   predict_n_update_triggered :  boolean;
BEGIN
 event_triggered := true;
   predict_n_update_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! predict_n_update_triggered) then 
   predict_n_update_triggered := predict_n_update();
   event_triggered := event_triggered | predict_n_update_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_control_enabled :  boolean;
BEGIN
 process_updated := false; end_while := false;
   process_control_enabled := false;
while (!end_while) do 
    if (((( relatived) > (-dfactor))) &  !process_control_enabled) then
   process_updated := true;
   process_control();
   process_control_enabled := true;
   endif;

IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

ruleset r:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_waypoint " 
(robot_at[r][from]) & (observe) & (!(robot_at[r][to_])) & (!(visited[to_])) & (connected[from][to_]) ==> 
pddlname: " goto_waypoint"; 
BEGIN
moving[r][to_]:= true; 
robot_at[r][from]:= false; 
observe:= false; 
relatived := assign_relatived_action_goto_waypoint(distance[from][to_]  );
counter := assign_counter_action_goto_waypoint();
update_covariance := increase_update_covariance_action_goto_waypoint(update_covariance  );
predict_covariance := increase_predict_covariance_action_goto_waypoint(predict_covariance  );

END; 
END; 
END; 
END;

ruleset r:robot do 
 ruleset to_:waypoint do 
 action rule " reached " 
(moving[r][to_]) & ((( relatived) <= (0.00000))) ==> 
pddlname: " reached"; 
BEGIN
robot_at[r][to_]:= true; 
visited[to_]:= true; 
observe:= true; 
moving[r][to_]:= false; 

END; 
END; 
END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for r : robot do 
  for wp : waypoint do 
    set_robot_at(r,wp, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   set_observe(false);

   for r : robot do 
     for to_ : waypoint do 
       set_moving(r,to_, false);
END; END;  -- close for
   for from : waypoint do 
     for to_ : waypoint do 
       set_connected(from,to_, false);
END; END;  -- close for
   for c : covariance do 
     for f : covariance do 
       set_lessthan(c,f, false);
END; END;  -- close for
   for wp1 : waypoint do 
     for wp2 : waypoint do 
       distance[wp1][wp2] := 0.0 ;
END; END;  -- close for
   cov := 0.0 ;

   counter := 0.0 ;

   update_covariance := 0.0 ;

   predict_covariance := 0.0 ;

   relatived := 0.0 ;



connected[endpoint][wp0]:= true; 
connected[endpoint][wp10]:= true; 
connected[endpoint][wp11]:= true; 
connected[endpoint][wp13]:= true; 
connected[endpoint][wp17]:= true; 
connected[endpoint][wp19]:= true; 
connected[endpoint][wp21]:= true; 
connected[endpoint][wp22]:= true; 
connected[endpoint][wp23]:= true; 
connected[endpoint][wp25]:= true; 
connected[endpoint][wp26]:= true; 
connected[endpoint][wp29]:= true; 
connected[endpoint][wp6]:= true; 
connected[endpoint][wp8]:= true; 
connected[wp0][wp1]:= true; 
connected[wp0][wp5]:= true; 
connected[wp0][wp6]:= true; 
connected[wp0][wp7]:= true; 
connected[wp0][wp9]:= true; 
connected[wp0][wp11]:= true; 
connected[wp0][wp14]:= true; 
connected[wp0][wp16]:= true; 
connected[wp0][wp19]:= true; 
connected[wp0][wp21]:= true; 
connected[wp0][wp22]:= true; 
connected[wp0][wp26]:= true; 
connected[wp0][wp27]:= true; 
connected[wp0][wp28]:= true; 
connected[wp0][endpoint]:= true; 
connected[wp1][wp0]:= true; 
connected[wp1][wp2]:= true; 
connected[wp1][wp3]:= true; 
connected[wp1][wp12]:= true; 
connected[wp1][wp18]:= true; 
connected[wp10][wp8]:= true; 
connected[wp10][endpoint]:= true; 
connected[wp11][wp0]:= true; 
connected[wp11][wp29]:= true; 
connected[wp11][endpoint]:= true; 
connected[wp12][wp1]:= true; 
connected[wp13][wp6]:= true; 
connected[wp13][endpoint]:= true; 
connected[wp14][wp0]:= true; 
connected[wp15][wp3]:= true; 
connected[wp16][wp0]:= true; 
connected[wp16][wp17]:= true; 
connected[wp17][wp16]:= true; 
connected[wp17][endpoint]:= true; 
connected[wp18][wp1]:= true; 
connected[wp19][wp0]:= true; 
connected[wp19][endpoint]:= true; 
connected[wp2][wp1]:= true; 
connected[wp20][wp3]:= true; 
connected[wp21][wp0]:= true; 
connected[wp21][wp24]:= true; 
connected[wp21][endpoint]:= true; 
connected[wp22][wp0]:= true; 
connected[wp22][endpoint]:= true; 
connected[wp23][wp9]:= true; 
connected[wp23][wp25]:= true; 
connected[wp23][endpoint]:= true; 
connected[wp24][wp21]:= true; 
connected[wp25][wp23]:= true; 
connected[wp25][endpoint]:= true; 
connected[wp26][wp0]:= true; 
connected[wp26][endpoint]:= true; 
connected[wp27][wp0]:= true; 
connected[wp28][wp0]:= true; 
connected[wp29][wp11]:= true; 
connected[wp29][endpoint]:= true; 
connected[wp3][wp1]:= true; 
connected[wp3][wp4]:= true; 
connected[wp3][wp15]:= true; 
connected[wp3][wp20]:= true; 
connected[wp4][wp3]:= true; 
connected[wp5][wp0]:= true; 
connected[wp6][wp0]:= true; 
connected[wp6][wp8]:= true; 
connected[wp6][wp13]:= true; 
connected[wp6][endpoint]:= true; 
connected[wp7][wp0]:= true; 
connected[wp8][wp6]:= true; 
connected[wp8][wp10]:= true; 
connected[wp8][endpoint]:= true; 
connected[wp9][wp0]:= true; 
connected[wp9][wp23]:= true; 
observe:= true; 
robot_at[kenny][wp0]:= true; 
cov := 1.22000;
distance[endpoint][wp0] := 2.56223;
distance[endpoint][wp10] := 1.69189;
distance[endpoint][wp11] := 0.585235;
distance[endpoint][wp13] := 3.45145;
distance[endpoint][wp17] := 5.41872;
distance[endpoint][wp19] := 0.756637;
distance[endpoint][wp21] := 3.62836;
distance[endpoint][wp22] := 4.62871;
distance[endpoint][wp23] := 3.19531;
distance[endpoint][wp25] := 5.82087;
distance[endpoint][wp26] := 2.12603;
distance[endpoint][wp29] := 1.36473;
distance[endpoint][wp6] := 5.50273;
distance[endpoint][wp8] := 3.45579;
distance[wp0][wp1] := 4.28077;
distance[wp0][wp5] := 3.50036;
distance[wp0][wp6] := 2.94364;
distance[wp0][wp7] := 4.54670;
distance[wp0][wp9] := 1.93132;
distance[wp0][wp11] := 1.97800;
distance[wp0][wp14] := 1.30096;
distance[wp0][wp16] := 6.03013;
distance[wp0][wp19] := 2.78792;
distance[wp0][wp21] := 1.10000;
distance[wp0][wp22] := 2.14009;
distance[wp0][wp26] := 4.51054;
distance[wp0][wp27] := 1.06888;
distance[wp0][wp28] := 2.60192;
distance[wp0][endpoint] := 2.56223;
distance[wp1][wp0] := 4.28077;
distance[wp1][wp2] := 2.70046;
distance[wp1][wp3] := 1.72627;
distance[wp1][wp12] := 1.54029;
distance[wp1][wp18] := 2.90259;
distance[wp10][wp8] := 2.77308;
distance[wp10][endpoint] := 1.69189;
distance[wp11][wp0] := 1.97800;
distance[wp11][wp29] := 1.37295;
distance[wp11][endpoint] := 0.585235;
distance[wp12][wp1] := 1.54029;
distance[wp13][wp6] := 4.67467;
distance[wp13][endpoint] := 3.45145;
distance[wp14][wp0] := 1.30096;
distance[wp15][wp3] := 1.90394;
distance[wp16][wp0] := 6.03013;
distance[wp16][wp17] := 2.09881;
distance[wp17][wp16] := 2.09881;
distance[wp17][endpoint] := 5.41872;
distance[wp18][wp1] := 2.90259;
distance[wp19][wp0] := 2.78792;
distance[wp19][endpoint] := 0.756637;
distance[wp2][wp1] := 2.70046;
distance[wp20][wp3] := 3.85519;
distance[wp21][wp0] := 1.10000;
distance[wp21][wp24] := 3.20975;
distance[wp21][endpoint] := 3.62836;
distance[wp22][wp0] := 2.14009;
distance[wp22][endpoint] := 4.62871;
distance[wp23][wp9] := 2.88531;
distance[wp23][wp25] := 2.66693;
distance[wp23][endpoint] := 3.19531;
distance[wp24][wp21] := 3.20975;
distance[wp25][wp23] := 2.66693;
distance[wp25][endpoint] := 5.82087;
distance[wp26][wp0] := 4.51054;
distance[wp26][endpoint] := 2.12603;
distance[wp27][wp0] := 1.06888;
distance[wp28][wp0] := 2.60192;
distance[wp29][wp11] := 1.37295;
distance[wp29][endpoint] := 1.36473;
distance[wp3][wp1] := 1.72627;
distance[wp3][wp4] := 1.05475;
distance[wp3][wp15] := 1.90394;
distance[wp3][wp20] := 3.85519;
distance[wp4][wp3] := 1.05475;
distance[wp5][wp0] := 3.50036;
distance[wp6][wp0] := 2.94364;
distance[wp6][wp8] := 5.64646;
distance[wp6][wp13] := 4.67467;
distance[wp6][endpoint] := 5.50273;
distance[wp7][wp0] := 4.54670;
distance[wp8][wp6] := 5.64646;
distance[wp8][wp10] := 2.77308;
distance[wp8][endpoint] := 3.45579;
distance[wp9][wp0] := 1.93132;
distance[wp9][wp23] := 2.88531;
predict_covariance := 0.00000;
relatived := 0.00000;
update_covariance := 1.22000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (robot_at[kenny][endpoint]) & ((( cov) < (finaltrace)))& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


