(define (domain domain1)

(:requirements :typing :durative-actions :fluents :time
:negative-preconditions :timed-initial-literals :conditional-effects)


(:predicates (rewarded) (mission_completed)
             
)

(:functions (pos) (ob) (goal) (reward) (running_time)

)

;; right now just to keep track of time to change obstacle
(:process grid
  :parameters()
  :precondition (and (not (mission_completed)))
  :effect (and  (increase (running_time) (* #t 1))
)
)

;; collect reward if at (reward)
(:event getreward
:parameters ()
:precondition (and (not (rewarded))(= (pos) (reward) ) )
:effect (rewarded)
) 

;; to generate obstacles
(:event generate_obstacle
:parameters ()
:precondition (and (not (mission_completed)) (= (running_time) 2))
:effect (and (assign (ob) [1,3]) )
) 


(:action right
	:parameters ()
	:precondition (and (not (= (pos[2]) (ob[2])))  (not(= (ob_x) (+ (pos_x) 1)))(not (mission_completed)) )  
	:precondition (and (not (= (pos) ([ob[1]+1, ob[2]) )(not (mission_completed)) )                         
        :effect (and (assign (pos_x) (+ (pos_x) 1))
)
)



(:action left
	:parameters ()
	:precondition (and (not (= (pos_y) (ob_y)))  (not(= (ob_x) (- (pos_x) 1))) (not (mission_completed)) )               
	:effect (and (assign (pos_x) (- (pos_x) 1))
)
)


(:action up
	:parameters ()
	:precondition (and (not (= (pos_x) (ob_x)))  (not(= (ob_y) (+ (pos_y) 1))) (not (mission_completed)) )
	:effect (and (assign (pos_y) (+ (pos_y) 1)) 
)
)

(:action down
	:parameters ()
	:precondition (and (not (= (pos_x) (ob_x)))  (not(= (ob_y) (- (pos_y) 1))) (not (mission_completed)) ) 
	:effect (and (assign (pos_y) (- (pos_y) 1)) 
)
)



(:action stop
	:parameters ()
	:precondition (and (= (pos_x) (goal_x) ) (= (pos_y) (goal_y) ) (rewarded))
	:effect (mission_completed)
)

)
