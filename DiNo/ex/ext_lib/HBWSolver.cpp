/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2015  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "HBWSolver.h"
#include "ExternalSolver.h"
#include <map>
#include <string>
#include "armadillo"
#include <initializer_list>

using namespace std;
using namespace arma;

extern "C" ExternalSolver* create_object(){
    return new HBWSolver();
}

extern "C" void destroy_object(ExternalSolver *externalSolver){
    delete externalSolver;
}

HBWSolver::HBWSolver()
{

}

HBWSolver::~HBWSolver()
{

}

void HBWSolver::loadSolver(string* parameters, int n)
{
    string landmarkParameters = parameters[0];
    char const *x[]={"covariance"};
    char const *y[]={"relatived"}
    //external file "finalTrace", "dFactor", "distance"};
    
    //ToDO : parseParameters(landmarkParameters);
    affected = list<string>(x,x+1);// parameters
    dependencies = list<string>(y,y+1);
}

std::map< string, double > HBWSolver::callExternalSolver(std::map< string, double > initialState, bool isHeuristic)
{
    map<string,double> toReturn;
    map<string, double>::iterator iSIt = initialState.begin();
    map<string, double>::iterator isEnd = initialState.end();
    double covariance;
    map<string, string> covarianceResults;
    for(;iSIt!=isEnd;++iSIt){
        string parameter = iSIt->first;
        string function = iSIt->first;
        double value = iSIt->second;
        /*if(function =="cov:"){
               covariance = value;
            }
        int n=function.find("distance");
        if(n!=-1){
            ......
            function == "distance"*/
        if(function == "relatived")
        {
            EKF.distance= value;
        }
        /*else if(function = "cov")
        {    covarianceResults = 

        }   */  
    }
    calculateEKF();
 
          
    return covariance;
    
}

list< string > HBWSolver::getParameters()
{
return affected;
}

list< string > HBWSolver::getDependencies()
{
return dependencies;
}

void HBWSolver::parseParameters(string parameters)
{
    string line;
    int iLine = 0;
    ifstream parametersFile (parameters.c_str());
    cout <<parameters<< endl;
    std::initializer_list<string> row = {"a","b","c"};
    std::vector<std::initializer_list<string>> distance_vector{{begin_waypoint, end_waypoint, distance}};
    if (parametersFile.is_open())
    {
        while (getline(parametersFile,line)){
           std::istringstream iss(line);
           string a, b, c, d, e;
           /* if (!(iss >> a >> b)){
               break;
           }*/
               //external file "finalTrace", "dFactor", "distance"};
           iss >> a >> b;
           if (a == "(=" && b == "(distance")
           {
            iss>>a,b,c,d,e;
           
             d = d.substr (0,e.length()-1);// remove the last bracket
              e = e.substr (0,e.length()-1);// remove the last bracket
               row = {c, d, e};
            distance_vector.push_back(row);
            }
            else if (a == "(= " && b == "(dFactor)")
             {
                iss>>a,b,c;
                EKF kalman;
                c = c.substr (0,c.length()-1);
                kalman.dFactor = c;
             }  
              else if (a == "(= " && b == "(finalTrace)")
             {
                iss>>a,b,c;
                EKF kalman;
                 c = c.substr (0,c.length()-1);
                kalman.finalTrace = c;
             }  
         
           iLine++;
       }
    }else{
        cout << "Unable to open the file" << endl;
    }
    //need to find the distance push thing
     //value = atoi(myString.c_str())
}

void HBWSolver::startEKF(const DataPoint& data){

  this->timestamp = data.get_timestamp();
  VectorXd x = data.get_state();
  this->KF.start(this->n, x, this->P, this->F, this->Q);
  this->initialized = true;
}


void HBWSolver::updateQ(const double dt){

  const double dt2 = dt * dt;
  const double dt3 = dt * dt2;
  const double dt4 = dt * dt3;

  const double r11 = dt4 * this->ax / 4;
  const double r13 = dt3 * this->ax / 2;
  const double r22 = dt4 * this->ay / 4;
  const double r24 = dt3 * this->ay / 2;
  const double r31 = dt3 * this->ax / 2;
  const double r33 = dt2 * this->ax;
  const double r42 = dt3 * this->ay / 2;
  const double r44 = dt2 * this->ay;

  this->Q << r11, 0.0, r13, 0.0,
             0.0, r22, 0.0, r24,
             r31, 0.0, r33, 0.0,
             0.0, r42, 0.0, r44;

  this->KF.setQ(Q);
}

void HBWSolver::calculateEKF()
{
      /**************************************************************************
   * PREDICTION STEP
   **************************************************************************/
    this->start(data);
    const double dt = (data.get_timestamp() - this->timestamp) / 1.e6;
  this->timestamp = data.get_timestamp();

  this->updateQ(dt);
  this->KF.updateF(dt);
  this->KF.predict();

  /**************************************************************************
   * UPDATE STEP
   **************************************************************************/
  const VectorXd z = data.get();
  const VectorXd x = this->KF.get();

  VectorXd Hx;
  MatrixXd R;
  MatrixXd H;

  if (data.get_type() == DataPointType::RADAR){

    VectorXd s = data.get_state();
    H = calculate_jacobian(s);
    Hx = convert_cartesian_to_polar(x);
    R =  this->radar_R;

  } else if (data.get_type() == DataPointType::LIDAR){

    H = this->lidar_H;
    Hx = this->lidar_H * x;
    R = this->lidar_R;
  }

  this->KF.update(z, H, Hx, R);
}

/*void HBWSolver::calculateHeight()
{
    cout << "calculating height " << endl;
    mat coefficient;
    mat y;
    int nVariables = columns.size();
    coefficient.reshape(nVariables,nVariables);
    y.reshape(nVariables,1);
    
    map<string,Column>::iterator clIt = columns.begin();
    map<string,Column>::iterator clEnd = columns.end();
    // first constraint
    for(int i=0;i<nVariables;i++,clIt++){
        Column col = clIt->second;
        coefficient.at(0,i) = col.a;
    }
    y.at(0) = totalV;
    //other constraints;
    clIt = columns.begin();
    clIt++;
    for(int i=1;i<nVariables;i++,clIt++){
        Column col2 = clIt->second;
        Column col1 = (--clIt)->second;
        ++clIt;
        cout << "column1 " << col1.name << " column2 " << col2.name << "rho" <<totalrho <<endl;
        coefficient.at(i,i-1) = totalrho;
        coefficient.at(i,i) = -totalrho;
        y.at(i)=col2.p/col2.a-col1.p/col1.a;
    }
   // mat result = inv(coefficient)*y;
    mat result = solve(coefficient,y);
    cout << result<<endl;
    clIt = columns.begin();
    for(int i=0;i<nVariables;i++,clIt++){
        string name = clIt->first;
        columns[name].d = result.at(i);        
    }
}*/
