/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2015  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "landmarkSolver.h"
#include "ExternalSolver.h"
#include <map>
#include <string>
#include "armadillo"
#include <initializer_list>

using namespace std;
using namespace arma;

extern "C" ExternalSolver* create_object(){
    return new landmarkSolver();
}

extern "C" void destroy_object(ExternalSolver *externalSolver){
    delete externalSolver;
}

landmarkSolver::landmarkSolver()
{

}

landmarkSolver::~landmarkSolver()
{

}

void landmarkSolver::loadSolver(string* parameters, int n, double discretization)
{
    
    string waypoint_file = parameters[1];
    string problem_file = parameters[0];    
    string landmark_file = parameters[2];

    //string landmark_file = "/home/sunny/catkin_ws/DiNo/ex/landmark/landmark.txt";
     //cout<<"calling waypoint file "<< waypoint_file <<endl;

    char const *x[]={"covariance"};
    char const *y[]={"relatived"};
    //external file "finalTrace", "dFactor", "distance"};
    
   
    parse_waypoint_Parameters(waypoint_file);
    parse_landmark_Parameters(landmark_file);
    parseParameters(problem_file);

  /*  //test
    landmarks["wp0"] = vector <double> {2,3,0};
    landmarks["wp1"] = vector <double> {4,6, 0};
    landmarks["wp2"] = vector <double> {6,9, M_PI/3};
    landmarks["wp3"] = vector <double> {8,8, 0};
    landmarks["wp4"] = vector <double> {9,10, M_PI/4};
    landmarks["wp5"] = vector <double> {6,8, M_PI/6};*/

    map<string, vector<double>>::iterator mit= waypoint.begin();
    for (mit; mit!= waypoint.end(); mit++)
    {
          std::stringstream ss;
          ss<<mit->first<<" x:" << mit->second[0] << " , y: " << mit->second[1] << " , rotation: " << mit->second[2]<<"\n";
          //cout << ss.str()<<endl;
        }
    
    affected = list<string>(x,x+1);// parameters
    dependencies = list<string>(y,y+1);
    discretization2 = discretization;
    //cout<<discretization2<<endl;
    }

std::map< string, double > landmarkSolver::callExternalSolver(std::map< string, double > initialState, string rule)
{ 
     //cout<<"workin on rule"<< rule<<endl;
    // cout<<"in external call"<<endl;
    unsigned long state_id = initialState["previous_state_id"];
    kalman = ekf_state[state_id];//restore the value
    kalman.time = initialState["time"];
    //cout<<"previous state"<< kalman.state<<endl;
    map<string,double> toReturn;
    //map<string, double>::iterator iSIt = initialState.begin();
    //map<string, double>::iterator isEnd = initialState.end();
    //double covariance;
    /*map<string, string> covarianceResults;
    for(;iSIt!=isEnd;++iSIt){
        string parameter = iSIt->first;
        string function = iSIt->first;
        double value = iSIt->second;
    
        if(function == "relatived")
        {
            kalman.distance= value;
        }
        //visited waypoint 
        //covariance
        /*else if(function = "cov")
        {    covarianceResults = 

        }  */ 
     

    toReturn["cov"]= initialState["cov"];
    //toReturn["predict_covariance"]= initialState["predict_covariance"];
    toReturn["update_covariance"]= initialState["cov"];
    /*if predict then predictEKF();*/
    if(rule.find("goto_waypoint") != -1)
    {
     
       string a,b,waypoint_to,waypoint_from,e;
       istringstream iss(rule);
       iss >> a >> b >> waypoint_to >> waypoint_from >> e;
       waypoint_to = waypoint_to.substr(waypoint_to.find(":")+1,waypoint_to.find(",")-waypoint_to.find(":")-1);
       waypoint_from = waypoint_from.substr(waypoint_from.find(":")+1,waypoint_from.find(",")-waypoint_from.find(":")-1);
       //cout << "Rule: " << a << ", waypoint_to: " << waypoint_to << ", waypoint_from: " << waypoint_from << ", robot: " << e <<  endl;
       kalman.from = waypoint_from;
       kalman.to = waypoint_to;
       /*VectorXd x(3);
       x(0) = waypoint[waypoint_from].at(0);
       x(1) = waypoint[waypoint_from].at(1);
       x(2) = waypoint[waypoint_from].at(2);
       kalman.KF_state.setx(x);
*/

        string destination = kalman.to;
        //cout<<"new destination" << kalman.to<<endl;
        string origin = kalman.from;
        vector<double> state = waypoint[destination];
         double distance_betn;
        //extracting distance 
        for (int i=0; i< distance_vector.size(); i++)
        {
           if (distance_vector.at(i).at(0) == origin && distance_vector.at(i).at(1) == destination)  
           {
              distance_betn = stod( distance_vector.at(i).at(2));
              //cout<<distance_betn<<endl;
              kalman.times_predicted = 0;
           }
        }
        double nom_of_predict = distance_betn/kalman.dFactor; //later include descritization 
        int whole_predict = (int) floor(nom_of_predict);

        if ((nom_of_predict- whole_predict) >0)
        {
          kalman.no_of_predictions = whole_predict+1;
          kalman.extra = true;
        }
        else
        {
          kalman.no_of_predictions = whole_predict;
          kalman.extra = false;
        }
        kalman.moving = true;

        
    }
    if(rule.find("time passing") != -1)
    { 
      if(kalman.moving)
      {   /*
          checkupdate();
          if(kalman.updatable)
          updateEKF();

          if(kalman.predictable)
          {*/

            VectorXd previous_state(3);
            previous_state = kalman.KF_state.get(); 
            double previous_state_trace = previous_state(0)+ previous_state(1)+ previous_state(2);
            predictEKF();
            kalman.times_predicted++;
            double new_cov = kalman.KF_state.getP().trace();
            MatrixXd system_cov = MatrixXd(3, 3);
            system_cov = kalman.KF_state.getP();
            float test_system_cov = (sqrt(system_cov(0,0)) +sqrt(system_cov(1,1))+sqrt(system_cov(2,2)));

        


              VectorXd new_state(3);  
              new_state = kalman.KF_state.get();
            double new_state_trace = new_state(0)+ new_state(1)+ new_state(2);
            double diff_in_state = previous_state_trace- new_state_trace;

            //cout<<"previous covariance"<<toReturn["cov"]<<endl;
            //cout<<"predict_covariance"<<new_cov<<endl;
           // toReturn["predict_covariance"]= new_cov-toReturn["predict_covariane"];
            toReturn["predict_covariance"]= new_cov-toReturn["cov"];
            //if(toReturn["predict_covariance"]< 0 && fabs(toReturn["predict_covariance"]-0)>0.06 )// error margin for floating point error
           // cout<<" testing system covariance" <<test_system_cov<<endl;
            //exit(0);
            /*if(toReturn["predict_covariance"] < 1e-6)
            {
              exit(0);
            }*/
           
            

            //if(toReturn["predict_covariance"]< 0 && fabs(toReturn["predict_covariance"]-0)>test_system_cov)// error margin for floating point error
            //{
             if (toReturn["predict_covariance"] < 1e-6 )
                toReturn["predict_covariance"] = abs(toReturn["predict_covariance"])+0;
            if((toReturn["predict_covariance"])< 0 && fabs(diff_in_state+0)>test_system_cov)// error margin for floating point error
            {
               cout<<"diff_in_state"<< diff_in_state<<endl;
              cout<<" testing system covariance" <<test_system_cov<<endl;
              cout<< toReturn["predict_covariance"]<<endl;
              cout<< kalman.KF_state.getP()<<endl;

              exit(0);
            }
          //}
          //toReturn["predict_covariance"] = new_cov;
            map<string, vector<double>>::iterator mit= landmarks.begin();
            for (mit; mit!= landmarks.end(); mit++)
            {
                vector<double> landmark = mit->second;

                 updateEKF(landmark);

            }
    

            
            double update_cov = kalman.KF_state.getP().trace();
       // toReturn["update_covariance"]= abs(new_cov-toReturn["update_covariance"]);
        //toReturn["update_covariance"]= fabs(new_cov-toReturn["cov"]);
            toReturn["update_covariance"]= update_cov;
           //cout<<setprecision(10)<<update_cov<<endl;
      
     
      }
      
      //cout<<"time passing covariance: "<< toReturn["predict_covairance"]<<endl;
    }
    /*if (rule.find("update") != -1)
    {
      updateEKF();
        double new_cov = kalman.KF_state.getP().trace();
       // toReturn["update_covariance"]= abs(new_cov-toReturn["update_covariance"]);
        toReturn["update_covariance"]= fabs(new_cov-toReturn["cov"]);
      
      //cout<<"previous covariance"<<toReturn["predict_covariance"];
      //cout<<"update covariance: "<< toReturn["update_covariance"]<<endl;
      kalman.moving = false;

    }*/
    if (rule.find("reached") != -1)
    {
       kalman.moving = false;
    }
             
    //return result;
   //toReturn["cov"]= ;
    return toReturn;
    
}


list< string > landmarkSolver::getParameters()
{
return affected;
}

list< string > landmarkSolver::getDependencies()
{
return dependencies;
}
void landmarkSolver::saveState(unsigned long state_id)
{
  kalman.state = state_id;
  ekf_state[state_id] = kalman;
}

void landmarkSolver::parseParameters(string parameters)
{
    string line;
    double value;
    int iLine = 0;
    ifstream parametersFile (parameters.c_str());
   
    cout <<parameters<< endl;
    //std::initializer_list<string> row = {"a","b","c"};
    vector<string> row; 
    bool starting_set = false;
    //std::vector<std::initializer_list<string>> {{"begin_waypoint", "end_waypoint", "distance"}};
    if (parametersFile.is_open())
    {
        while (getline(parametersFile,line)){
           std::istringstream iss(line);
           string a, b, c, d, e;
            /*if (!(iss >> a >> b>>c >>d>> e)){
               //continue;
           }*/
               //external file "finalTrace", "dFactor", "distance"};
           //iss >> a >> b;
           if(line.find("dFactor")!= -1)
            {
              iss>>a>>b>>c;
              if (a == "(=" && b == "(dFactor)")
              {
                           
                c = c.substr (0,c.length()-1);

                //cout <<b<< " "<<c<<endl;
                value = atof(c.c_str());
                kalman.dFactor = value;
               } 
            }
           else if (line.find("distance")!= -1)
           {
              iss>>a>>b>>c>>d>>e;
              if (a == "(=" && b == "(distance")
              {
                iss>>a>>b>>c>>d>>e;
               
                 d = d.substr (0,d.length()-1);// remove the last bracket
                  e = e.substr (0,e.length()-1);// remove the last bracket
                   row = vector<string> {c, d, e};
                distance_vector.push_back(row);
               //cout <<c<< " "<<d<<" " <<e<< " "<<endl;
              }
            }

          

            else if(line.find("finalTrace")!= -1)
            {
              iss>>a>>b>>c;
              if (a == "(=" && b == "(finalTrace)")
              {
                
                
                 c = c.substr (0,c.length()-1);
                // cout <<b<< " "<<c<<endl;
                value = atof(c.c_str());
               kalman.finalTrace = value;
              } 
            }

            else if((line.find("robot_at")!= -1) && (starting_set== false))   
             {
                iss>>a>>b>>c; 
                c = c.substr (0,c.length()-1);
                starting_position = c;
                starting_set = true;
            
              } 
            
          }
    startEKF();
    }else{
        cout << "Unable to open the file" << endl;
    }
    //need to find the distance push thing
     //value = atoi(myString.c_str())
}

void landmarkSolver::parse_waypoint(double pose[], string &waypoint_name, std::string line) {

    int curr,next;
    curr=line.find("[");
    
    waypoint_name = line.substr(0,curr).c_str();
    
    curr=curr+1;
    next=line.find(",",curr);

    pose[0] = (double)atof(line.substr(curr,next-curr).c_str());
    curr=next+1; next=line.find(",",curr);

    pose[1] = (double)atof(line.substr(curr,next-curr).c_str());
    curr=next+1; next=line.find("]",curr);

    pose[2] = (double)atof(line.substr(curr,next-curr).c_str());
    
  }


  void landmarkSolver::parse_landmark(double pose[], string &landmark_name, std::string line) {

    int curr,next;
    curr=line.find("[");
    
    landmark_name = line.substr(0,curr).c_str();
    
    curr=curr+1;
    next=line.find(",",curr);

    pose[0] = (double)atof(line.substr(curr,next-curr).c_str());
    curr=next+1; next=line.find(",",curr);

    pose[1] = (double)atof(line.substr(curr,next-curr).c_str());
    curr=next+1; next=line.find("]",curr);

    pose[2] = (double)atof(line.substr(curr,next-curr).c_str());
    
  }

void landmarkSolver::parse_waypoint_Parameters(string parameters)
{
    string line;
    double value;
    int iLine = 0;
    ifstream parametersFile (parameters.c_str());
   
    cout <<parameters<< endl;
    //std::initializer_list<string> row = {"a","b","c"};
    //std::vector<std::initializer_list<string>> distance_vector{{"begin_waypoint", "end_waypoint", "distance"}};
    if (parametersFile.is_open())
    {
      int curr,next;
        while (getline(parametersFile,line)){
          curr=line.find("[");
          std::string name = line.substr(0,curr);
          //geometry_msgs::PoseStamped pose;
          double pose[3];
          std::string wp;
          parse_waypoint(pose, wp, line);
          waypoint[wp]= vector<double> {pose[0], pose[1], pose[2]};
        }  
          
    }else{
        cout << "Unable to open the file" << endl;
    }
}


void landmarkSolver::parse_landmark_Parameters(string parameters)
{
    string line;
    double value;
    int iLine = 0;
    ifstream parametersFile (parameters.c_str());
   
    cout <<parameters<< endl;
    //std::initializer_list<string> row = {"a","b","c"};
    //std::vector<std::initializer_list<string>> distance_vector{{"begin_waypoint", "end_waypoint", "distance"}};
    if (parametersFile.is_open())
    {
      int curr,next;
        while (getline(parametersFile,line)){
          curr=line.find("[");
          std::string name = line.substr(0,curr);
          //geometry_msgs::PoseStamped pose;
          double pose[3];
          std::string wp;
          parse_landmark(pose, wp, line);
          landmarks[wp]= vector<double> {pose[0], pose[1], pose[2]};
        }  
          
    }else{
        cout << "Unable to open the file" << endl;
    }
}



void landmarkSolver::startEKF(){

  //this->timestamp = data.get_timestamp();
  VectorXd x(3);
   VectorXd alpha1(4);
    alpha1(0) = 0.05*0.05;
    alpha1(1) = 0.005*0.005;
    //alpha1(0) = 0.5*0.5;
    //alpha1(1) = 0.5*0.5;
    alpha1(2) = 0.1*0.1;
    alpha1(3) = 0.01*0.01;
    alpha = alpha1;

  MatrixXd P = MatrixXd(3, 3);
  MatrixXd F = MatrixXd(3, 3);
  MatrixXd Q = MatrixXd(3, 3);

  //temp stuff
  MatrixXd V = MatrixXd(3, 3);
  MatrixXd M = MatrixXd(3, 3);
 /* MatrixXd T = MartixXd(3, 3);
  MatrixXd M = MatrixXd(3, 3);*/

  //x(0)= 0; x(1)= 0; x(2)=0;
  x(0) = waypoint[starting_position].at(0);
  x(1) = waypoint[starting_position].at(1);
  x(2) = waypoint[starting_position].at(2);

//cout<< "Sunny test" << starting_position<< endl;

  /*P << 0.4, 0.0, 0.0,
               0.0, 0.4, 0.0,
             0.0, 0.0, 0.02;*/
   P << 0.6, 0.0, 0.0,
               0.0, 0.6, 0.0,
             0.0, 0.0, 0.02;

  V<< (-1*kalman.dFactor* sin(x(2))), cos(x(2)), 0,
       kalman.dFactor* cos(x(2)), sin(x(2)), 0,
       1,0,1;

  M<< alpha(1)* kalman.dFactor* kalman.dFactor, 0, 0,
      0, alpha(2)* kalman.dFactor* kalman.dFactor, 0,
      0, 0, alpha(1)*kalman.dFactor* kalman.dFactor;



  //add theta factor to the (0,2) (1,2)
  /*F << 1.0, 0,kalman.dFactor,
        0,1.0,kalman.dFactor,
        0, 0, 1;*/


        F << 1.0, 0, (-1*kalman.dFactor*sin(x(2))),
        0,1.0,kalman.dFactor*cos(x(2)),
        0, 0, 1;


  Q << V*M*V.transpose();
   //KF.start(3, x, P, F, Q);
   kalman.KF_state.start(3,x,P,F,Q);
   kalman.state= 0;
   ekf_state[0] = kalman;
}
/*

void landmarkSolver::updateQ(const double dt){

  const double dt2 = dt * dt;
  const double dt3 = dt * dt2;
  const double dt4 = dt * dt3;

  const double r11 = dt4 * this->ax / 4;
  const double r13 = dt3 * this->ax / 2;
  const double r22 = dt4 * this->ay / 4;
  const double r24 = dt3 * this->ay / 2;
  const double r31 = dt3 * this->ax / 2;
  const double r33 = dt2 * this->ax;
  const double r42 = dt3 * this->ay / 2;
  const double r44 = dt2 * this->ay;

  this->Q << r11, 0.0, r13, 0.0,
             0.0, r22, 0.0, r24,
             r31, 0.0, r33, 0.0,
             0.0, r42, 0.0, r44;

  this->KF.setQ(Q);
}
*/
void landmarkSolver::predictEKF()
{
      /**************************************************************************
   * PREDICTION STEP
   **************************************************************************/
  //KF.updateF(kalman.dFactor);

  MatrixXd V = MatrixXd(3, 3);
  MatrixXd M = MatrixXd(3, 3);
  MatrixXd Q = MatrixXd(3, 3);
  VectorXd x(3);
  x = kalman.KF_state.get();

  string destination = kalman.to;
  string origin = kalman.from;

  vector<double> state = waypoint[destination];

  //cout<<"initialized matrices for predict origin"<< origin<<endl;
  //cout<<"initialized matrices for predict destination"<< destination<<endl;


  double control1 = 0;
  double control3 = 0;
  double control2 = kalman.dFactor*discretization2;
   //double control2 = kalman.dFactor;
  
  //if(kalman.times_predicted == 0)
 /* cout <<"states" <<state.at(0)<<" , "<<state.at(1)<<" ," <<state.at(2)<<endl;
  cout<<"current post" <<x(0)<< " , "<<x(1)<< " ," <<x(2) <<endl;
  cout<<"x-old "<<x<<endl;*/
  
    if ((abs(state.at(1)- x(1)) <= kalman.finalTrace  ) && (abs(state.at(0)- x(0)) <= kalman.finalTrace))
    {
      control1 = atan2(state.at(1),state.at(0)) - x(2);
      //control1 = 0;
      //cout<<"check atan2 for only rotation"<< atan2(state.at(1),state.at(0))<<endl;
      //  control1 = x(2) - atan2(state.at(1),state.at(0));
    }
    else
    {
      control1 = atan2((state.at(1)- x(1)),(state.at(0)- x(0))) - x(2);
      //control1 = atan2(x(1)-(state.at(1)),(x(0)-(state.at(0)))) - x(2);
      //cout<<"check atan2"<< atan2((state.at(1)- x(1)),(state.at(0)- x(0))) <<endl;
      //control1 = x(2) - 
    }
      //cout<<"control1 before checck angle"<<control1<<endl;

    //control1 = check_angle(control1);

  //cout<<"control1 after check angle"<<control1<<endl;

 
  double check_distance = fabs(sqrt((state.at(0)-x(0)) *(state.at(0)-x(0))+ ((state.at(1)-x(1))*(state.at(1)-x(1)))));
      if(check_distance < kalman.dFactor*discretization2)
 //if(check_distance < kalman.finalTrace)
     {
        control3 = state.at(2)-x(2)-control1;
        //control3 = state.at(2)-control1;

        control3 = check_angle(control3);
        control2 = fabs(sqrt((state.at(0)-x(0)) *(state.at(0)-x(0))+ ((state.at(1)-x(1))*(state.at(1)-x(1)))))*discretization2;
        // control2 = fabs(sqrt((state.at(0)-x(0)) *(state.at(0)-x(0))+ ((state.at(1)-x(1))*(state.at(1)-x(1)))));

       // cout<<"test this"<<endl;
      //exit(0);
      }
     // else
      //{
        x(2)= x(2)+control1;
     // }

  
  /*
  double theta = x(2)+control1;
  theta = check_angle(theta);*/
  

 // x(2) = check_angle(x(2))();
  double theta = check_angle(x(2));


  /*cout<<"theta for the updateF "<< theta<<endl;
  cout<<"control1 "<<control1<<endl;
  cout<<"control2 "<<control2<<endl;
  cout<<"control3 "<<control3<<endl;

  cout<<"previous F"<<kalman.KF_state.getF()<<endl;
  cout<<"previous P"<<kalman.KF_state.getP()<<endl;*/

  kalman.KF_state.updateF(control2, theta);

  //cout<<"new F"<<kalman.KF_state.getF()<<endl;


  V<< -1*control2* sin(theta), cos(theta), 0,
       control2* cos(theta), sin(theta), 0,
       1,0,1;

  M<< alpha(0)* control1* control1+ alpha(1)* control2* control2, 0, 0,
      0, alpha(2)* control2* control2+ (alpha(3)*((control1*control1)+(control3*control3))), 0,
      0, 0, alpha(1)*control2* control2+ alpha(0)*control3* control3;

      //alpha(1)*control2* control2+ alpha(0)*control3* control3

  Q = V*M*V.transpose();
  //cout<<"Q "<<Q<<endl;


  //the non linear formula used
  kalman.KF_state.setQ(Q);
   
  x(0) = x(0)+ control2*cos(theta);
  x(1) = x(1)+ control2*sin(theta);
  x(2) = x(2)+ control3;
 
  x(2) = check_angle(x(2));
  //cout<<"x-new "<<x<<endl;


  kalman.KF_state.setx(x);
  kalman.KF_state.predict();
  //cout<<"new P"<<kalman.KF_state.getP()<<endl;

}

double landmarkSolver::check_angle(double angle)
{
  while( angle< - M_PI)
  {  angle = angle+2*M_PI;
  }
  while( angle>  M_PI)
  {  angle = angle-2*M_PI;
  }
  return angle;

}


void landmarkSolver::updateEKF(vector<double> landmark)
{
    /**************************************************************************
   * UPDATE STEP
   **************************************************************************/
 // const VectorXd z = data.get();
  VectorXd state = kalman.KF_state.get();

  VectorXd Hx(2);
  MatrixXd R = MatrixXd(2, 2);
  MatrixXd H = MatrixXd(2, 3);
  VectorXd z(2);

 
  R<< 0.025, 0,
      0, 0.001;

  //string destination = kalman.to;
  //vector<double> landmark = landmarks[destination];


 
 //double range = sqrt((state(0)-landmark.at(0))* (state(0)-landmark.at(0))+ ((state(1)-landmark.at(1))* (state(1)-landmark.at(1))));
 double range = sqrt((landmark.at(0)-state(0))*(landmark.at(0)-state(0))+ ((landmark.at(1)-state(1))* (landmark.at(1)-state(1))));
 //double bearing = atan2((landmark.at(1)-state(1)),(landmark.at(0)-state(0)))- landmark.at(2);
 double bearing = atan2((landmark.at(1)-state(1)),(landmark.at(0)-state(0)))- state(2);    

//if (range <1 && bearing> -M_PI/2  && bearing < M_PI/2)
//if (range <1)
 double start_angle = state(2) - M_PI/2;
 double end_angle = state(2)+ M_PI/2;
 // hardcoded for now for the 3rd landmark, need to generalize using unit vectors   
int can_sense = 1;

if (landmark.at(0) - 5.45 < 1e-3){
          if (landmark.at(0) - state(0) > 0){
             can_sense = 1;
         } else {
   can_sense = 0; 
}
}

if (range <3 && bearing >= start_angle  && bearing <= end_angle && can_sense==1)
{
    //exit(0);
/*H<< -(state(0)-landmark.at(0))/range, -(state(1)-landmark.at(1))/range, 0,
     (state(1)-landmark.at(1))/(range*range), -(state(0)-landmark.at(0))/(range *range), -1;
*/
H<< -(landmark.at(0)-state(0))/range, -(landmark.at(1)-state(1))/range, 0,
      (state(1)-landmark.at(1))/(range*range), -(state(0)-landmark.at(0))/(range *range), -1;

float random1 = static_cast <float> (rand())/static_cast <float>(RAND_MAX);
float random2 = -1 + static_cast <float> (rand())/static_cast <float>(RAND_MAX/2);
/*
float random1 = 0.0;
float random2 = 0.0;
*/
z(0) = range+ random1/12;
z(1) = bearing+ random2* M_PI/12;


Hx(0) = range;
Hx(1) = bearing;

  kalman.KF_state.update(z, H, Hx, R);
} 
}

void landmarkSolver::Collect_Plans(std::vector<unsigned long> v, double time)
{
 // cout<<"this is working in the correct plans"<<endl;
  string plan_file = "./collect_plans.txt";
  string cov_file = "./collect_covariance.txt";
  string total_cov = "./final_states.txt";

  ofstream out_file;
  ofstream cov_handle;
  ofstream total_handle;

  out_file.open(plan_file);
  cov_handle.open(cov_file);
  total_handle.open(total_cov);

  out_file<<"state,time,x,y,theta\n";
  cov_handle<<"state,time,cov\n";
  double cov = 0;

  for (int i = 1; i<v.size(); i++)
  {
    unsigned long stateid = v.at(i);
    kalman = ekf_state[stateid];
    VectorXd x(3);
    x = kalman.KF_state.get();
    out_file<<stateid<<","<<kalman.time<<","<<x(0)<<","<<x(1)<<","<<x(2)<<"\n";

    cov = kalman.KF_state.getP().trace();
    cov_handle<<stateid<<","<<kalman.time<<","<<cov<<"\n";

  }  

  //cout<<"this is working in the middle correct plans"<<endl;
  //double totalcov= v.at(v.size()-1);
  double totalstate= v.at(0);
  //double cmptime = v.at(1);
  total_handle<<"total states:"<<totalstate<<"\n";
  total_handle<<"total covariance:"<< cov<<"\n";
  total_handle<<"computation time:"<< time<<"\n";
  out_file.close();
  cov_handle.close();
  total_handle.close();
}