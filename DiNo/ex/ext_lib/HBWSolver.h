/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2015  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef HBWSOLVER_H
#define HBWSOLVER_H

#include "ExternalSolver.h"
#include "kalmanfilter.h"
#include <string>


using namespace std;

struct EKF{
    double covariance;
    double relativedistance;
    double finalTrace;
    double dFactor;
    double distance;

};

class HBWSolver : public ExternalSolver
{
public:
    HBWSolver();
    ~HBWSolver();
    virtual void loadSolver(string* parameters, int n);
    virtual map<string,double> callExternalSolver(map<string,double> initialState, bool isHeuristic); 
    virtual  list<string> getParameters();
    virtual  list<string> getDependencies();
    KalmanFilter KF;
//private:
    list<string> affected;
    list<string> dependencies;
    std::vector<std::initializer_list<string>> distance_vector;
    
    map<string,Column> columns;
    void parseParameters(string parameters);
    void calculateEKF();
    void updateQ();
    void startEKF();



    
};

#endif // HBWSOLVER_H
