/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2015  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef landmarkSolver_H
#define landmarkSolver_H

#include "ExternalSolver.h"
#include "kalmanfilter.h"
#include <string>

//#include <geometry_msgs/Twist.h>
//#include "geometry_msgs/PoseStamped.h"
//#include <geometry_msgs>
#include <sstream>
#include <iomanip>
#include <vector>


using namespace std;

struct EKF{
    double relativedistance;
    double finalTrace;
    double dFactor;
    double distance;
    KalmanFilter KF_state;
    string from;
    string to;
    int times_predicted;
    int no_of_predictions;
    bool extra;
    int state; //current
    bool moving;
    double time;


};

class landmarkSolver : public ExternalSolver
{
public:
    landmarkSolver();
    ~landmarkSolver();
    virtual void loadSolver(string* parameters, int n, double discretization);
    virtual map<string,double> callExternalSolver(map<string,double> initialState, string rule); 
    virtual  list<string> getParameters();
    virtual  list<string> getDependencies();
    virtual void saveState(unsigned long state_id);
    virtual void Collect_Plans(std::vector<unsigned long> v, double time);
    
    EKF kalman;
   
    map<unsigned long,EKF> ekf_state; //store EKF for each state
    map<string, vector<double>> waypoint;
//private:
    list<string> affected;
    list<string> dependencies;
    //std::vector<std::initializer_list<string>> distance_vector;
    vector <vector <string> > distance_vector;
    string starting_position;
    void parseParameters(string parameters);
    void predictEKF();
    void updateEKF(vector<double> landmark);
    void updateQ();
    void startEKF();
    void parse_waypoint_Parameters(string parameters);
    void parse_landmark_Parameters(string parameters);
    void parse_waypoint(double point[], string &wp,  string line);
    void parse_landmark(double point[], string &landmarks,  string line);
    double check_angle(double angle);
    double discretization2;


    VectorXd alpha; 

    map<string, vector<double>> landmarks;


    
};

#endif // landmarkSolver_H
