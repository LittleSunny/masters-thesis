(define (problem turtlebot_demo_task)
(:domain turtlebot_demo)
(:objects
    kenny - robot
    wp0 wp1 - waypoint
)
(:init
    (robot_at kenny wp0)
)
(:goal (and
    (visited wp0)
    (visited wp1)
   )))
