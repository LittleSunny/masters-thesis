#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

double round_k_digits(double n, unsigned k){
	double prec = pow(0.1,k);
	double round = (n>0) ? (n+prec/2) : (n-prec/2);
	return round-fmod(round,prec);
}

double ext_assignment(double n){
	return round_k_digits(n,2);
}

double assign_init_x_action_right(double init_x ) {
	 return round_k_digits((init_x) + (1.00000),2); 
}

double assign_init_x_action_left(double init_x ) {
	 return round_k_digits((init_x) - (1.00000),2); 
}

double assign_init_y_action_up(double init_y ) {
	 return round_k_digits((init_y) + (1.00000),2); 
}

double assign_init_y_action_down(double init_y ) {
	 return round_k_digits((init_y) - (1.00000),2); 
}

