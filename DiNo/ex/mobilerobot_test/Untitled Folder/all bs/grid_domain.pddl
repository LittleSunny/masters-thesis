(define (domain grid_domain)

(:requirements :typing :durative-actions :fluents :time
:negative-preconditions :timed-initial-literals)


(:predicates (reward) (mission_completed)

)

(:functions (init_x) (init_y) 

)

(:event getreward
:parameters ()
:precondition (and (not (reward))(= (init_x) 5) (= (init_y) 5))
:effect (and (reward))
) 

(:action right
	:parameters ()
	:precondition (and (not (mission_completed)))
	:effect (and (assign (init_x) (+ (init_x) 1)))
)

(:action left
	:parameters ()
	:precondition (and (not (mission_completed)))
	:effect (and (assign (init_x) (- (init_x) 1)))
)

(:action up
	:parameters ()
	:precondition (and (not (mission_completed)))
	:effect (and (assign (init_y) (+ (init_y) 1)))
)

(:action down
	:parameters ()
	:precondition (and (not (mission_completed)))
	:effect (and (assign (init_y) (- (init_y) 1)))
)

(:action stop
	:parameters ()
	:precondition (and (= (init_x) 2) (= (init_y) 2) (reward))
	:effect (mission_completed)
)

)
