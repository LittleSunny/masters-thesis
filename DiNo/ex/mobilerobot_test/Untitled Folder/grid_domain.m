domain: file "grid_domain.pddl"
problem: file "grid_problem.pddl"
message: " Time Discretisation = 0.1"
message: " Digits for representing the integer part of a real =  5"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(7,4);
	integer: -1000..1000;

	 TIME_type: real(7,2);


const 
	 T:0.1;


var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	init_x[pddlname:"init_x";] :  real_type;
	init_y[pddlname:"init_y";] :  real_type;


	reward[pddlname: "reward";] :  boolean;
	mission_completed[pddlname: "mission_completed";] :  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun assign_init_x_action_right(init_x : real_type ; ): real_type "grid_domain.h" ;
externfun assign_init_x_action_left(init_x : real_type ; ): real_type ;
externfun assign_init_y_action_up(init_y : real_type ; ): real_type ;
externfun assign_init_y_action_down(init_y : real_type ; ): real_type ;
procedure set_reward(  value : boolean);
BEGIN
	reward := value;
END;

function get_reward(): boolean;
BEGIN
	return 	reward;
END;

procedure set_mission_completed(  value : boolean);
BEGIN
	mission_completed := value;
END;

function get_mission_completed(): boolean;
BEGIN
	return 	mission_completed;
END;



function getreward () : boolean; 
BEGIN
IF ((!(reward)) & ((( init_x) = (5.00000))) & ((( init_y) = (5.00000)))) THEN 
reward:= true; 
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   getreward_triggered :  boolean;
BEGIN
 event_triggered := true;
   getreward_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! getreward_triggered) then 
   getreward_triggered := getreward();
   event_triggered := event_triggered | getreward_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;BEGIN
 process_updated := false; end_while := false;
while (!end_while) do 
 IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

action rule " right " 
(!(mission_completed)) ==> 
pddlname: " right"; 
BEGIN
init_x := assign_init_x_action_right(init_x  );

END;

action rule " left " 
(!(mission_completed)) ==> 
pddlname: " left"; 
BEGIN
init_x := assign_init_x_action_left(init_x  );

END;

action rule " up " 
(!(mission_completed)) ==> 
pddlname: " up"; 
BEGIN
init_y := assign_init_y_action_up(init_y  );

END;

action rule " down " 
(!(mission_completed)) ==> 
pddlname: " down"; 
BEGIN
init_y := assign_init_y_action_down(init_y  );

END;

action rule " stop " 
((( init_x) = (2.00000))) & ((( init_y) = (2.00000))) & (reward) ==> 
pddlname: " stop"; 
BEGIN
mission_completed:= true; 

END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
set_reward(false);

   set_mission_completed(false);

   init_x := 0.0 ;

   init_y := 0.0 ;

reward:= false; 
mission_completed:= false; 
init_x := 0.00000;
init_y := 0.00000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (mission_completed)& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


