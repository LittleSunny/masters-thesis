/******************************
  Program "grid_domain_obs.m" compiled by "DiNo Release 1.1"

  DiNo Last Compiled date: "Mar 13 2018"
 ******************************/

/********************
  Parameter
 ********************/
#define DINO_VERSION "DiNo Release 1.1"
#define MURPHI_DATE "Mar 13 2018"
#define PROTOCOL_NAME "grid_domain_obs"
#define DOMAIN_FILENAME "grid_domain_obs.pddl"
#define PROBLEM_FILENAME "grid_problem_obs.pddl"
#define DISCRETIZATION 1.000000
#define VAL_PATHNAME "/home/sunny/catkin_ws/DiNo/src/DiNo/../VAL-master/validate"
#define BITS_IN_WORLD 607
#define HASHC
#define HAS_CLOCK
const char * const modelmessages[] = { " Time Discretisation = 1.000000"," Digits for representing the integer part of a real =  5.000000"," Digits for representing the fractional part of a real =  4" };
const int modelmessagecount = 3;

/********************
  Include
 ********************/
#include "upm_prolog.hpp"

/********************
  Decl declaration
 ********************/

class mu_1_real_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_real_type& val) { return mu__real::operator=((double) val); };
  mu_1_real_type (const char *name, int os): mu__real(9,4,48,name, os) {};
  mu_1_real_type (void): mu__real(9,4,48) {};
  mu_1_real_type (double val): mu__real(9,4,48,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_real_type mu_1_real_type_undefined_var;

class mu_1_integer: public mu__long
{
 public:
  inline int operator=(int val) { return mu__long::operator=(val); };
  inline int operator=(const mu_1_integer& val) { return mu__long::operator=((int) val); };
  mu_1_integer (const char *name, int os): mu__long(-1000, 1000, 11, name, os) {};
  mu_1_integer (void): mu__long(-1000, 1000, 11) {};
  mu_1_integer (int val): mu__long(-1000, 1000, 11, "Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%d",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of subrange decl ***/
mu_1_integer mu_1_integer_undefined_var;

class mu_1_TIME_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_TIME_type& val) { return mu__real::operator=((double) val); };
  mu_1_TIME_type (const char *name, int os): mu__real(12,7,64,name, os) {};
  mu_1_TIME_type (void): mu__real(12,7,64) {};
  mu_1_TIME_type (double val): mu__real(12,7,64,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_TIME_type mu_1_TIME_type_undefined_var;

const double mu_T = +1.000000e+00;
const double mu_goal_x = +2.000000e+00;
const double mu_goal_y = +2.000000e+00;
const double mu_reward_x = +4.000000e+00;
const double mu_reward_y = +4.000000e+00;
/*** Variable declaration ***/
mu_0_boolean mu_all_event_true("all_event_true",0);

/*** Variable declaration ***/
mu_1_integer mu_h_n("h_n",2);

/*** Variable declaration ***/
mu_1_integer mu_g_n("g_n",13);

/*** Variable declaration ***/
mu_1_integer mu_f_n("f_n",24);

/*** Variable declaration ***/
mu_1_TIME_type mu_TIME("TIME",35);

/*** Variable declaration ***/
mu_1_real_type mu_ob_x("ob_x",99);

/*** Variable declaration ***/
mu_1_real_type mu_ob_y("ob_y",147);

/*** Variable declaration ***/
mu_1_real_type mu_pos_x("pos_x",195);

/*** Variable declaration ***/
mu_1_real_type mu_pos_y("pos_y",243);

/*** Variable declaration ***/
mu_1_real_type mu_running_time("running_time",291);

/*** Variable declaration ***/
mu_0_boolean mu_right_clock_started("right_clock_started",339);

/*** Variable declaration ***/
mu_1_TIME_type mu_right_clock("right_clock",341);

/*** Variable declaration ***/
mu_0_boolean mu_left_clock_started("left_clock_started",405);

/*** Variable declaration ***/
mu_1_TIME_type mu_left_clock("left_clock",407);

/*** Variable declaration ***/
mu_0_boolean mu_up_clock_started("up_clock_started",471);

/*** Variable declaration ***/
mu_1_TIME_type mu_up_clock("up_clock",473);

/*** Variable declaration ***/
mu_0_boolean mu_down_clock_started("down_clock_started",537);

/*** Variable declaration ***/
mu_1_TIME_type mu_down_clock("down_clock",539);

/*** Variable declaration ***/
mu_0_boolean mu_reward("reward",603);

/*** Variable declaration ***/
mu_0_boolean mu_mission_completed("mission_completed",605);


#include "grid_domain_obs.h"

void mu_set_reward(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_reward.undefine();
else
  mu_reward = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_reward()
{
return mu_reward;
	Error.Error("The end of function get_reward reached without returning values.");
};
/*** end function declaration ***/

void mu_set_mission_completed(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_mission_completed.undefine();
else
  mu_mission_completed = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_mission_completed()
{
return mu_mission_completed;
	Error.Error("The end of function get_mission_completed reached without returning values.");
};
/*** end function declaration ***/

void mu_process_right()
{
if ( mu_right_clock_started )
{
mu_right_clock = (mu_right_clock) + (mu_T);
}
};
/*** end procedure declaration ***/

void mu_process_left()
{
if ( mu_left_clock_started )
{
mu_left_clock = (mu_left_clock) + (mu_T);
}
};
/*** end procedure declaration ***/

void mu_process_up()
{
if ( mu_up_clock_started )
{
mu_up_clock = (mu_up_clock) + (mu_T);
}
};
/*** end procedure declaration ***/

void mu_process_down()
{
if ( mu_down_clock_started )
{
mu_down_clock = (mu_down_clock) + (mu_T);
}
};
/*** end procedure declaration ***/

mu_0_boolean mu_event_right_failure()
{
bool mu__boolexpr0;
  if (!(mu_right_clock_started)) mu__boolexpr0 = FALSE ;
  else {
bool mu__boolexpr1;
bool mu__boolexpr2;
  if (!((mu_pos_y) == (mu_ob_y))) mu__boolexpr2 = FALSE ;
  else {
  mu__boolexpr2 = ((mu_ob_x) == ((mu_pos_x) + (1.000000e+00))) ; 
}
  if (!(!(mu__boolexpr2))) mu__boolexpr1 = FALSE ;
  else {
  mu__boolexpr1 = (!(mu_mission_completed)) ; 
}
  mu__boolexpr0 = (!(mu__boolexpr1)) ; 
}
if ( mu__boolexpr0 )
{
mu_right_clock = (mu_right_clock) + (mu_T);
mu_all_event_true = mu_false;
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function event_right_failure reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_event_left_failure()
{
bool mu__boolexpr3;
  if (!(mu_left_clock_started)) mu__boolexpr3 = FALSE ;
  else {
bool mu__boolexpr4;
bool mu__boolexpr5;
  if (!((mu_pos_y) == (mu_ob_y))) mu__boolexpr5 = FALSE ;
  else {
  mu__boolexpr5 = ((mu_ob_x) == ((mu_pos_x) - (1.000000e+00))) ; 
}
  if (!(!(mu__boolexpr5))) mu__boolexpr4 = FALSE ;
  else {
  mu__boolexpr4 = (!(mu_mission_completed)) ; 
}
  mu__boolexpr3 = (!(mu__boolexpr4)) ; 
}
if ( mu__boolexpr3 )
{
mu_left_clock = (mu_left_clock) + (mu_T);
mu_all_event_true = mu_false;
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function event_left_failure reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_event_up_failure()
{
bool mu__boolexpr6;
  if (!(mu_up_clock_started)) mu__boolexpr6 = FALSE ;
  else {
bool mu__boolexpr7;
bool mu__boolexpr8;
  if (!((mu_pos_x) == (mu_ob_x))) mu__boolexpr8 = FALSE ;
  else {
  mu__boolexpr8 = ((mu_ob_y) == ((mu_pos_y) + (1.000000e+00))) ; 
}
  if (!(!(mu__boolexpr8))) mu__boolexpr7 = FALSE ;
  else {
  mu__boolexpr7 = (!(mu_mission_completed)) ; 
}
  mu__boolexpr6 = (!(mu__boolexpr7)) ; 
}
if ( mu__boolexpr6 )
{
mu_up_clock = (mu_up_clock) + (mu_T);
mu_all_event_true = mu_false;
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function event_up_failure reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_event_down_failure()
{
bool mu__boolexpr9;
  if (!(mu_down_clock_started)) mu__boolexpr9 = FALSE ;
  else {
bool mu__boolexpr10;
bool mu__boolexpr11;
  if (!((mu_pos_x) == (mu_ob_x))) mu__boolexpr11 = FALSE ;
  else {
  mu__boolexpr11 = ((mu_ob_y) == ((mu_pos_y) - (1.000000e+00))) ; 
}
  if (!(!(mu__boolexpr11))) mu__boolexpr10 = FALSE ;
  else {
  mu__boolexpr10 = (!(mu_mission_completed)) ; 
}
  mu__boolexpr9 = (!(mu__boolexpr10)) ; 
}
if ( mu__boolexpr9 )
{
mu_down_clock = (mu_down_clock) + (mu_T);
mu_all_event_true = mu_false;
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function event_down_failure reached without returning values.");
};
/*** end function declaration ***/

void mu_process_grid()
{
if ( !(mu_mission_completed) )
{
mu_running_time = increase_running_time_process_grid( mu_running_time, (double)mu_T );
}
};
/*** end procedure declaration ***/

mu_0_boolean mu_getreward()
{
bool mu__boolexpr12;
bool mu__boolexpr13;
  if (!(!(mu_reward))) mu__boolexpr13 = FALSE ;
  else {
  mu__boolexpr13 = ((mu_pos_x) == (mu_reward_x)) ; 
}
  if (!(mu__boolexpr13)) mu__boolexpr12 = FALSE ;
  else {
  mu__boolexpr12 = ((mu_pos_y) == (mu_reward_y)) ; 
}
if ( mu__boolexpr12 )
{
mu_reward = mu_true;
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function getreward reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_generate_obstacle()
{
bool mu__boolexpr14;
  if (!(!(mu_mission_completed))) mu__boolexpr14 = FALSE ;
  else {
  mu__boolexpr14 = ((mu_running_time) == (2.000000e+00)) ; 
}
if ( mu__boolexpr14 )
{
mu_ob_x = assign_ob_x_event_generate_obstacle(  );
mu_ob_y = assign_ob_y_event_generate_obstacle(  );
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function generate_obstacle reached without returning values.");
};
/*** end function declaration ***/

void mu_event_check()
{
/*** Variable declaration ***/
mu_0_boolean mu_event_triggered("event_triggered",0);

/*** Variable declaration ***/
mu_0_boolean mu_getreward_triggered("getreward_triggered",2);

/*** Variable declaration ***/
mu_0_boolean mu_generate_obstacle_triggered("generate_obstacle_triggered",4);

/*** Variable declaration ***/
mu_0_boolean mu_event_right_failure_triggered("event_right_failure_triggered",6);

/*** Variable declaration ***/
mu_0_boolean mu_event_left_failure_triggered("event_left_failure_triggered",8);

/*** Variable declaration ***/
mu_0_boolean mu_event_up_failure_triggered("event_up_failure_triggered",10);

/*** Variable declaration ***/
mu_0_boolean mu_event_down_failure_triggered("event_down_failure_triggered",12);

mu_event_triggered = mu_true;
mu_getreward_triggered = mu_false;
mu_generate_obstacle_triggered = mu_false;
mu_event_right_failure_triggered = mu_false;
mu_event_left_failure_triggered = mu_false;
mu_event_up_failure_triggered = mu_false;
mu_event_down_failure_triggered = mu_false;
{
  bool mu__while_expr_16;  mu__while_expr_16 = mu_event_triggered;
int mu__counter_15 = 0;
while (mu__while_expr_16) {
if ( ++mu__counter_15 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
mu_event_triggered = mu_false;
if ( !(mu_getreward_triggered) )
{
mu_getreward_triggered = mu_getreward(  );
bool mu__boolexpr17;
  if (mu_event_triggered) mu__boolexpr17 = TRUE ;
  else {
  mu__boolexpr17 = (mu_getreward_triggered) ; 
}
mu_event_triggered = mu__boolexpr17;
}
if ( !(mu_generate_obstacle_triggered) )
{
mu_generate_obstacle_triggered = mu_generate_obstacle(  );
bool mu__boolexpr18;
  if (mu_event_triggered) mu__boolexpr18 = TRUE ;
  else {
  mu__boolexpr18 = (mu_generate_obstacle_triggered) ; 
}
mu_event_triggered = mu__boolexpr18;
}
if ( !(mu_event_right_failure_triggered) )
{
mu_event_right_failure_triggered = mu_event_right_failure(  );
bool mu__boolexpr19;
  if (mu_event_triggered) mu__boolexpr19 = TRUE ;
  else {
  mu__boolexpr19 = (mu_event_right_failure_triggered) ; 
}
mu_event_triggered = mu__boolexpr19;
}
if ( !(mu_event_left_failure_triggered) )
{
mu_event_left_failure_triggered = mu_event_left_failure(  );
bool mu__boolexpr20;
  if (mu_event_triggered) mu__boolexpr20 = TRUE ;
  else {
  mu__boolexpr20 = (mu_event_left_failure_triggered) ; 
}
mu_event_triggered = mu__boolexpr20;
}
if ( !(mu_event_up_failure_triggered) )
{
mu_event_up_failure_triggered = mu_event_up_failure(  );
bool mu__boolexpr21;
  if (mu_event_triggered) mu__boolexpr21 = TRUE ;
  else {
  mu__boolexpr21 = (mu_event_up_failure_triggered) ; 
}
mu_event_triggered = mu__boolexpr21;
}
if ( !(mu_event_down_failure_triggered) )
{
mu_event_down_failure_triggered = mu_event_down_failure(  );
bool mu__boolexpr22;
  if (mu_event_triggered) mu__boolexpr22 = TRUE ;
  else {
  mu__boolexpr22 = (mu_event_down_failure_triggered) ; 
}
mu_event_triggered = mu__boolexpr22;
}
};
mu__while_expr_16 = mu_event_triggered;
}
};
};
/*** end procedure declaration ***/

mu_0_boolean mu_DAs_violate_duration()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_duration_violated("DA_duration_violated",0);

mu_DA_duration_violated = mu_false;
if ( (mu_right_clock) > (1.000000e+00) )
{
return mu_true;
}
if ( (mu_left_clock) > (1.000000e+00) )
{
return mu_true;
}
if ( (mu_up_clock) > (1.000000e+00) )
{
return mu_true;
}
if ( (mu_down_clock) > (1.000000e+00) )
{
return mu_true;
}
return mu_DA_duration_violated;
	Error.Error("The end of function DAs_violate_duration reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_DAs_ongoing_in_goal_state()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_still_ongoing("DA_still_ongoing",0);

mu_DA_still_ongoing = mu_false;
if ( (mu_right_clock_started) == (mu_true) )
{
return mu_true;
}
if ( (mu_left_clock_started) == (mu_true) )
{
return mu_true;
}
if ( (mu_up_clock_started) == (mu_true) )
{
return mu_true;
}
if ( (mu_down_clock_started) == (mu_true) )
{
return mu_true;
}
return mu_DA_still_ongoing;
	Error.Error("The end of function DAs_ongoing_in_goal_state reached without returning values.");
};
/*** end function declaration ***/

void mu_apply_continuous_change()
{
/*** Variable declaration ***/
mu_0_boolean mu_process_updated("process_updated",0);

/*** Variable declaration ***/
mu_0_boolean mu_end_while("end_while",2);

/*** Variable declaration ***/
mu_0_boolean mu_process_grid_enabled("process_grid_enabled",4);

/*** Variable declaration ***/
mu_0_boolean mu_process_right_enabled("process_right_enabled",6);

/*** Variable declaration ***/
mu_0_boolean mu_process_left_enabled("process_left_enabled",8);

/*** Variable declaration ***/
mu_0_boolean mu_process_up_enabled("process_up_enabled",10);

/*** Variable declaration ***/
mu_0_boolean mu_process_down_enabled("process_down_enabled",12);

mu_process_updated = mu_false;
mu_end_while = mu_false;
mu_process_grid_enabled = mu_false;
mu_process_right_enabled = mu_false;
mu_process_left_enabled = mu_false;
mu_process_up_enabled = mu_false;
mu_process_down_enabled = mu_false;
{
  bool mu__while_expr_24;  mu__while_expr_24 = !(mu_end_while);
int mu__counter_23 = 0;
while (mu__while_expr_24) {
if ( ++mu__counter_23 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
bool mu__boolexpr25;
  if (!(!(mu_mission_completed))) mu__boolexpr25 = FALSE ;
  else {
  mu__boolexpr25 = (!(mu_process_grid_enabled)) ; 
}
if ( mu__boolexpr25 )
{
mu_process_updated = mu_true;
mu_process_grid (  );
mu_process_grid_enabled = mu_true;
}
bool mu__boolexpr26;
bool mu__boolexpr27;
  if (!(mu_true)) mu__boolexpr27 = FALSE ;
  else {
  mu__boolexpr27 = (mu_right_clock_started) ; 
}
  if (!(mu__boolexpr27)) mu__boolexpr26 = FALSE ;
  else {
  mu__boolexpr26 = (!(mu_process_right_enabled)) ; 
}
if ( mu__boolexpr26 )
{
mu_process_updated = mu_true;
mu_process_right (  );
mu_process_right_enabled = mu_true;
}
bool mu__boolexpr28;
bool mu__boolexpr29;
  if (!(mu_true)) mu__boolexpr29 = FALSE ;
  else {
  mu__boolexpr29 = (mu_left_clock_started) ; 
}
  if (!(mu__boolexpr29)) mu__boolexpr28 = FALSE ;
  else {
  mu__boolexpr28 = (!(mu_process_left_enabled)) ; 
}
if ( mu__boolexpr28 )
{
mu_process_updated = mu_true;
mu_process_left (  );
mu_process_left_enabled = mu_true;
}
bool mu__boolexpr30;
bool mu__boolexpr31;
  if (!(mu_true)) mu__boolexpr31 = FALSE ;
  else {
  mu__boolexpr31 = (mu_up_clock_started) ; 
}
  if (!(mu__boolexpr31)) mu__boolexpr30 = FALSE ;
  else {
  mu__boolexpr30 = (!(mu_process_up_enabled)) ; 
}
if ( mu__boolexpr30 )
{
mu_process_updated = mu_true;
mu_process_up (  );
mu_process_up_enabled = mu_true;
}
bool mu__boolexpr32;
bool mu__boolexpr33;
  if (!(mu_true)) mu__boolexpr33 = FALSE ;
  else {
  mu__boolexpr33 = (mu_down_clock_started) ; 
}
  if (!(mu__boolexpr33)) mu__boolexpr32 = FALSE ;
  else {
  mu__boolexpr32 = (!(mu_process_down_enabled)) ; 
}
if ( mu__boolexpr32 )
{
mu_process_updated = mu_true;
mu_process_down (  );
mu_process_down_enabled = mu_true;
}
if ( !(mu_process_updated) )
{
mu_end_while = mu_true;
}
else
{
mu_process_updated = mu_false;
}
};
mu__while_expr_24 = !(mu_end_while);
}
};
};
/*** end procedure declaration ***/





/********************
  The world
 ********************/
void world_class::clear()
{
  mu_all_event_true.clear();
  mu_h_n.clear();
  mu_g_n.clear();
  mu_f_n.clear();
  mu_TIME.clear();
  mu_ob_x.clear();
  mu_ob_y.clear();
  mu_pos_x.clear();
  mu_pos_y.clear();
  mu_running_time.clear();
  mu_right_clock_started.clear();
  mu_right_clock.clear();
  mu_left_clock_started.clear();
  mu_left_clock.clear();
  mu_up_clock_started.clear();
  mu_up_clock.clear();
  mu_down_clock_started.clear();
  mu_down_clock.clear();
  mu_reward.clear();
  mu_mission_completed.clear();
}
void world_class::undefine()
{
  mu_all_event_true.undefine();
  mu_h_n.undefine();
  mu_g_n.undefine();
  mu_f_n.undefine();
  mu_TIME.undefine();
  mu_ob_x.undefine();
  mu_ob_y.undefine();
  mu_pos_x.undefine();
  mu_pos_y.undefine();
  mu_running_time.undefine();
  mu_right_clock_started.undefine();
  mu_right_clock.undefine();
  mu_left_clock_started.undefine();
  mu_left_clock.undefine();
  mu_up_clock_started.undefine();
  mu_up_clock.undefine();
  mu_down_clock_started.undefine();
  mu_down_clock.undefine();
  mu_reward.undefine();
  mu_mission_completed.undefine();
}
void world_class::reset()
{
  mu_all_event_true.reset();
  mu_h_n.reset();
  mu_g_n.reset();
  mu_f_n.reset();
  mu_TIME.reset();
  mu_ob_x.reset();
  mu_ob_y.reset();
  mu_pos_x.reset();
  mu_pos_y.reset();
  mu_running_time.reset();
  mu_right_clock_started.reset();
  mu_right_clock.reset();
  mu_left_clock_started.reset();
  mu_left_clock.reset();
  mu_up_clock_started.reset();
  mu_up_clock.reset();
  mu_down_clock_started.reset();
  mu_down_clock.reset();
  mu_reward.reset();
  mu_mission_completed.reset();
}
std::vector<mu_0_boolean*> world_class::get_mu_bools()
{
	  std::vector<mu_0_boolean*> awesome;

      awesome.push_back(&(mu_all_event_true));
      awesome.push_back(&(mu_right_clock_started));
      awesome.push_back(&(mu_left_clock_started));
      awesome.push_back(&(mu_up_clock_started));
      awesome.push_back(&(mu_down_clock_started));
      awesome.push_back(&(mu_reward));
      awesome.push_back(&(mu_mission_completed));
    return awesome; 
}
std::vector<mu_0_boolean*> world_class::get_mu_bool_arrays()
{
	  std::vector<mu_0_boolean*> var_arrays;
   std::vector<mu_0_boolean*> interm;

    return var_arrays; 
}
std::vector<mu__real*> world_class::get_mu_nums()
{
	  std::vector<mu__real*> awesome;

      awesome.push_back(&(mu_ob_x));
      awesome.push_back(&(mu_ob_y));
      awesome.push_back(&(mu_pos_x));
      awesome.push_back(&(mu_pos_y));
      awesome.push_back(&(mu_running_time));
    return awesome; 
}
std::vector<mu__real*> world_class::get_mu_num_arrays()
{
	  std::vector<mu__real*> var_arrays;
   std::vector<mu__real*> interm;

    return var_arrays; 
}
//WP WP WP WP WP
double world_class::get_f_val()
{
  double f_val = mu_f_n.value();
  return f_val;
}

//WP WP WP WP WP
void world_class::fire_processes()
{
			mu_process_down();

			mu_process_grid();

			mu_process_left();

			mu_process_right();

			mu_process_up();

}

//WP WP WP WP WP
void world_class::fire_processes_plus()
{
{



 if (mu_down_clock_started) 


	{
		mu_down_clock = (mu_down_clock) + (mu_T); 
	}


}
{



 if (mu_left_clock_started) 


	{
		mu_left_clock = (mu_left_clock) + (mu_T); 
	}


}
{



 if (mu_right_clock_started) 


	{
		mu_right_clock = (mu_right_clock) + (mu_T); 
	}


}
{



 if (!(mu_mission_completed)) 


	{
		mu_running_time = increase_running_time_process_grid( mu_running_time, (double)mu_T ); 
	}


}
{



 if (mu_up_clock_started) 


	{
		mu_up_clock = (mu_up_clock) + (mu_T); 
	}


}
}

//WP WP WP WP WP
void world_class::fire_processes_minus()
{
{



 if (mu_right_clock_started) 


	{
	}


}
}

//WP WP WP WP WP
void world_class::set_f_val()
{
  double f_val = mu_g_n.value() + mu_h_n.value();
  mu_f_n.value(f_val);
}

//WP WP WP WP WP
double world_class::get_h_val()
{
  double h_val = mu_h_n.value();
  return h_val;
}

//WP WP WP WP WP
void world_class::set_h_val()
{
  //	NON-HEURISTIC SEARCH
  // double h_val = 0; 

  //	FF RPG
  //upm_rpg::getInstance().clear_all();
  //double h_val = upm_rpg::getInstance().compute_rpg();


  //	NUMERIC RPG
  //upm_numeric_rpg::getInstance().clear_all();
  //double h_val = upm_numeric_rpg::getInstance().compute_rpg();

  //	TEMPORAL RPG
  upm_staged_rpg::getInstance().clear_all();
  double h_val = upm_staged_rpg::getInstance().compute_rpg();

  mu_h_n.value(h_val);
}

//WP WP WP WP WP
void world_class::set_h_val(int hp)
{
  double h_val = hp; 
  mu_h_n.value(h_val);
}

//WP WP WP WP WP
double world_class::get_g_val()
{
  double g_val = mu_g_n.value();
  return g_val;
}

//WP WP WP WP WP
void world_class::set_g_val(double g_val)
{
  mu_g_n.value(g_val);
}

void world_class::print(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print(target, separator);
  mu_h_n.print(target, separator);
  mu_g_n.print(target, separator);
  mu_f_n.print(target, separator);
  mu_TIME.print(target, separator);
  mu_ob_x.print(target, separator);
  mu_ob_y.print(target, separator);
  mu_pos_x.print(target, separator);
  mu_pos_y.print(target, separator);
  mu_running_time.print(target, separator);
  mu_right_clock_started.print(target, separator);
  mu_right_clock.print(target, separator);
  mu_left_clock_started.print(target, separator);
  mu_left_clock.print(target, separator);
  mu_up_clock_started.print(target, separator);
  mu_up_clock.print(target, separator);
  mu_down_clock_started.print(target, separator);
  mu_down_clock.print(target, separator);
  mu_reward.print(target, separator);
  mu_mission_completed.print(target, separator);
    num_calls--;
}
}
void world_class::pddlprint(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_TIME.print(target, separator);
  mu_ob_x.print(target, separator);
  mu_ob_y.print(target, separator);
  mu_pos_x.print(target, separator);
  mu_pos_y.print(target, separator);
  mu_running_time.print(target, separator);
  mu_right_clock_started.print(target, separator);
  mu_right_clock.print(target, separator);
  mu_left_clock_started.print(target, separator);
  mu_left_clock.print(target, separator);
  mu_up_clock_started.print(target, separator);
  mu_up_clock.print(target, separator);
  mu_down_clock_started.print(target, separator);
  mu_down_clock.print(target, separator);
  mu_reward.print(target, separator);
  mu_mission_completed.print(target, separator);
    num_calls--;
}
}
double world_class::get_clock_value()
{
  return mu_TIME.value();
}
void world_class::print_statistic()
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print_statistic();
  mu_h_n.print_statistic();
  mu_g_n.print_statistic();
  mu_f_n.print_statistic();
  mu_TIME.print_statistic();
  mu_ob_x.print_statistic();
  mu_ob_y.print_statistic();
  mu_pos_x.print_statistic();
  mu_pos_y.print_statistic();
  mu_running_time.print_statistic();
  mu_right_clock_started.print_statistic();
  mu_right_clock.print_statistic();
  mu_left_clock_started.print_statistic();
  mu_left_clock.print_statistic();
  mu_up_clock_started.print_statistic();
  mu_up_clock.print_statistic();
  mu_down_clock_started.print_statistic();
  mu_down_clock.print_statistic();
  mu_reward.print_statistic();
  mu_mission_completed.print_statistic();
    num_calls--;
}
}
void world_class::print_diff(state *prevstate, FILE *target, const char *separator)
{
  if ( prevstate != NULL )
  {
    mu_all_event_true.print_diff(prevstate,target,separator);
    mu_h_n.print_diff(prevstate,target,separator);
    mu_g_n.print_diff(prevstate,target,separator);
    mu_f_n.print_diff(prevstate,target,separator);
    mu_TIME.print_diff(prevstate,target,separator);
    mu_ob_x.print_diff(prevstate,target,separator);
    mu_ob_y.print_diff(prevstate,target,separator);
    mu_pos_x.print_diff(prevstate,target,separator);
    mu_pos_y.print_diff(prevstate,target,separator);
    mu_running_time.print_diff(prevstate,target,separator);
    mu_right_clock_started.print_diff(prevstate,target,separator);
    mu_right_clock.print_diff(prevstate,target,separator);
    mu_left_clock_started.print_diff(prevstate,target,separator);
    mu_left_clock.print_diff(prevstate,target,separator);
    mu_up_clock_started.print_diff(prevstate,target,separator);
    mu_up_clock.print_diff(prevstate,target,separator);
    mu_down_clock_started.print_diff(prevstate,target,separator);
    mu_down_clock.print_diff(prevstate,target,separator);
    mu_reward.print_diff(prevstate,target,separator);
    mu_mission_completed.print_diff(prevstate,target,separator);
  }
  else
print(target,separator);
}
void world_class::to_state(state *newstate)
{
  mu_all_event_true.to_state( newstate );
  mu_h_n.to_state( newstate );
  mu_g_n.to_state( newstate );
  mu_f_n.to_state( newstate );
  mu_TIME.to_state( newstate );
  mu_ob_x.to_state( newstate );
  mu_ob_y.to_state( newstate );
  mu_pos_x.to_state( newstate );
  mu_pos_y.to_state( newstate );
  mu_running_time.to_state( newstate );
  mu_right_clock_started.to_state( newstate );
  mu_right_clock.to_state( newstate );
  mu_left_clock_started.to_state( newstate );
  mu_left_clock.to_state( newstate );
  mu_up_clock_started.to_state( newstate );
  mu_up_clock.to_state( newstate );
  mu_down_clock_started.to_state( newstate );
  mu_down_clock.to_state( newstate );
  mu_reward.to_state( newstate );
  mu_mission_completed.to_state( newstate );
}
void world_class::setstate(state *thestate)
{
}


/********************
  Rule declarations
 ********************/
/******************** RuleBase0 ********************/
class RuleBase0
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" time passing ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
    return mu_true;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;



    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;



    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;



    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;



    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_TIME));  // (mu_TIME) + (mu_T) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_TIME)); //  (mu_TIME) + (mu_T) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 0;
    while (what_rule < 1 )
      {
	if ( ( TRUE  ) ) {
	      if (mu_true) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 0;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_TIME = (mu_TIME) + (mu_T);
mu_event_check (  );
mu_apply_continuous_change (  );
mu_event_check (  );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_TIME = (mu_TIME) + (mu_T);


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 1;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return Duration(r);
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( time passing )");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Clock;
  };

};
/******************** RuleBase1 ********************/
class RuleBase1
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" stop ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr34;
bool mu__boolexpr35;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr35 = FALSE ;
  else {
  mu__boolexpr35 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr35)) mu__boolexpr34 = FALSE ;
  else {
  mu__boolexpr34 = (mu_reward) ; 
}
    return mu__boolexpr34;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr36;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr36 = FALSE ;
  else {
  mu__boolexpr36 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr37;
bool mu__boolexpr38;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr38 = FALSE ;
  else {
  mu__boolexpr38 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr38)) mu__boolexpr37 = FALSE ;
  else {
  mu__boolexpr37 = (mu_reward) ; 
}
bool mu__boolexpr39;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr39 = FALSE ;
  else {
  mu__boolexpr39 = ((mu_pos_y) == (mu_goal_y)) ; 
}

 		if (std::string(typeid(mu_reward).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_reward)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr40;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr40 = FALSE ;
  else {
  mu__boolexpr40 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr41;
bool mu__boolexpr42;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr42 = FALSE ;
  else {
  mu__boolexpr42 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr42)) mu__boolexpr41 = FALSE ;
  else {
  mu__boolexpr41 = (mu_reward) ; 
}
bool mu__boolexpr43;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr43 = FALSE ;
  else {
  mu__boolexpr43 = ((mu_pos_y) == (mu_goal_y)) ; 
}

 	if (std::string(typeid(mu_pos_x).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_pos_x), std::make_pair(mu_goal_x, 0))); 
	} 
 	if (std::string(typeid(mu_pos_y).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_pos_y), std::make_pair(mu_goal_y, 0))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_pos_x).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_pos_x)); 
 	if (std::string(typeid(mu_pos_y).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_pos_y)); 
 		if (std::string(typeid(mu_reward).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_reward)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr44;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr44 = FALSE ;
  else {
  mu__boolexpr44 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr45;
bool mu__boolexpr46;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr46 = FALSE ;
  else {
  mu__boolexpr46 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr46)) mu__boolexpr45 = FALSE ;
  else {
  mu__boolexpr45 = (mu_reward) ; 
}
bool mu__boolexpr47;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr47 = FALSE ;
  else {
  mu__boolexpr47 = ((mu_pos_y) == (mu_goal_y)) ; 
}

 		if (std::string(typeid(mu_reward).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_reward), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr48;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr48 = FALSE ;
  else {
  mu__boolexpr48 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr49;
bool mu__boolexpr50;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr50 = FALSE ;
  else {
  mu__boolexpr50 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr50)) mu__boolexpr49 = FALSE ;
  else {
  mu__boolexpr49 = (mu_reward) ; 
}
bool mu__boolexpr51;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr51 = FALSE ;
  else {
  mu__boolexpr51 = ((mu_pos_y) == (mu_goal_y)) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;



    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;


    aeffs.push_back(&(mu_mission_completed)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_mission_completed), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_mission_completed)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 1;
    while (what_rule < 2 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr52;
bool mu__boolexpr53;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr53 = FALSE ;
  else {
  mu__boolexpr53 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr53)) mu__boolexpr52 = FALSE ;
  else {
  mu__boolexpr52 = (mu_reward) ; 
}
	      if (mu__boolexpr52) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 1;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_mission_completed = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {



mu_mission_completed = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_mission_completed = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_mission_completed = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( stop)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase2 ********************/
class RuleBase2
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" down_end ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr54;
bool mu__boolexpr55;
bool mu__boolexpr56;
  if (!(mu_down_clock_started)) mu__boolexpr56 = FALSE ;
  else {
  mu__boolexpr56 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr56)) mu__boolexpr55 = FALSE ;
  else {
  mu__boolexpr55 = ((mu_down_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr55)) mu__boolexpr54 = FALSE ;
  else {
  mu__boolexpr54 = (mu_all_event_true) ; 
}
    return mu__boolexpr54;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr57;
bool mu__boolexpr58;
  if (!(mu_down_clock_started)) mu__boolexpr58 = FALSE ;
  else {
  mu__boolexpr58 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr58)) mu__boolexpr57 = FALSE ;
  else {
  mu__boolexpr57 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr59;
bool mu__boolexpr60;
bool mu__boolexpr61;
  if (!(mu_down_clock_started)) mu__boolexpr61 = FALSE ;
  else {
  mu__boolexpr61 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr61)) mu__boolexpr60 = FALSE ;
  else {
  mu__boolexpr60 = ((mu_down_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr60)) mu__boolexpr59 = FALSE ;
  else {
  mu__boolexpr59 = (mu_all_event_true) ; 
}
bool mu__boolexpr62;
  if (!(mu_down_clock_started)) mu__boolexpr62 = FALSE ;
  else {
  mu__boolexpr62 = ((mu_down_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr63;
bool mu__boolexpr64;
  if (!(mu_down_clock_started)) mu__boolexpr64 = FALSE ;
  else {
  mu__boolexpr64 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr64)) mu__boolexpr63 = FALSE ;
  else {
  mu__boolexpr63 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr65;
  if (!(mu_down_clock_started)) mu__boolexpr65 = FALSE ;
  else {
  mu__boolexpr65 = ((mu_down_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_down_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_down_clock_started)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr66;
bool mu__boolexpr67;
  if (!(mu_down_clock_started)) mu__boolexpr67 = FALSE ;
  else {
  mu__boolexpr67 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr67)) mu__boolexpr66 = FALSE ;
  else {
  mu__boolexpr66 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr68;
bool mu__boolexpr69;
bool mu__boolexpr70;
  if (!(mu_down_clock_started)) mu__boolexpr70 = FALSE ;
  else {
  mu__boolexpr70 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr70)) mu__boolexpr69 = FALSE ;
  else {
  mu__boolexpr69 = ((mu_down_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr69)) mu__boolexpr68 = FALSE ;
  else {
  mu__boolexpr68 = (mu_all_event_true) ; 
}
bool mu__boolexpr71;
  if (!(mu_down_clock_started)) mu__boolexpr71 = FALSE ;
  else {
  mu__boolexpr71 = ((mu_down_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr72;
bool mu__boolexpr73;
  if (!(mu_down_clock_started)) mu__boolexpr73 = FALSE ;
  else {
  mu__boolexpr73 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr73)) mu__boolexpr72 = FALSE ;
  else {
  mu__boolexpr72 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr74;
  if (!(mu_down_clock_started)) mu__boolexpr74 = FALSE ;
  else {
  mu__boolexpr74 = ((mu_down_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_down_clock).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_down_clock), std::make_pair(0.000000e+00, 3))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_down_clock).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_down_clock)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_down_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_down_clock_started)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr75;
bool mu__boolexpr76;
  if (!(mu_down_clock_started)) mu__boolexpr76 = FALSE ;
  else {
  mu__boolexpr76 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr76)) mu__boolexpr75 = FALSE ;
  else {
  mu__boolexpr75 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr77;
bool mu__boolexpr78;
bool mu__boolexpr79;
  if (!(mu_down_clock_started)) mu__boolexpr79 = FALSE ;
  else {
  mu__boolexpr79 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr79)) mu__boolexpr78 = FALSE ;
  else {
  mu__boolexpr78 = ((mu_down_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr78)) mu__boolexpr77 = FALSE ;
  else {
  mu__boolexpr77 = (mu_all_event_true) ; 
}
bool mu__boolexpr80;
  if (!(mu_down_clock_started)) mu__boolexpr80 = FALSE ;
  else {
  mu__boolexpr80 = ((mu_down_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr81;
bool mu__boolexpr82;
  if (!(mu_down_clock_started)) mu__boolexpr82 = FALSE ;
  else {
  mu__boolexpr82 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr82)) mu__boolexpr81 = FALSE ;
  else {
  mu__boolexpr81 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr83;
  if (!(mu_down_clock_started)) mu__boolexpr83 = FALSE ;
  else {
  mu__boolexpr83 = ((mu_down_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 
 		if (std::string(typeid(mu_down_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_down_clock_started), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr84;
bool mu__boolexpr85;
  if (!(mu_down_clock_started)) mu__boolexpr85 = FALSE ;
  else {
  mu__boolexpr85 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr85)) mu__boolexpr84 = FALSE ;
  else {
  mu__boolexpr84 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr86;
bool mu__boolexpr87;
bool mu__boolexpr88;
  if (!(mu_down_clock_started)) mu__boolexpr88 = FALSE ;
  else {
  mu__boolexpr88 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr88)) mu__boolexpr87 = FALSE ;
  else {
  mu__boolexpr87 = ((mu_down_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr87)) mu__boolexpr86 = FALSE ;
  else {
  mu__boolexpr86 = (mu_all_event_true) ; 
}
bool mu__boolexpr89;
  if (!(mu_down_clock_started)) mu__boolexpr89 = FALSE ;
  else {
  mu__boolexpr89 = ((mu_down_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr90;
bool mu__boolexpr91;
  if (!(mu_down_clock_started)) mu__boolexpr91 = FALSE ;
  else {
  mu__boolexpr91 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr91)) mu__boolexpr90 = FALSE ;
  else {
  mu__boolexpr90 = ((mu_down_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr92;
  if (!(mu_down_clock_started)) mu__boolexpr92 = FALSE ;
  else {
  mu__boolexpr92 = ((mu_down_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_down_clock).name()).compare("14mu_1_real_type") == 0)
			temporal_cons = std::make_pair(0.000000e+00 + mu_T, TIME_INFINITY); 

    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_down_clock));  // 0.000000e+00 
    effs.push_back(&(mu_pos_y));  // assign_pos_y_duraction_end_down( mu_pos_y ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_down_clock_started), 0)); //  mu_false 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_down_clock)); //  0.000000e+00 
    aeffs.push_back(&(mu_pos_y)); //  assign_pos_y_duraction_end_down( mu_pos_y ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 2;
    while (what_rule < 3 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr93;
bool mu__boolexpr94;
bool mu__boolexpr95;
  if (!(mu_down_clock_started)) mu__boolexpr95 = FALSE ;
  else {
  mu__boolexpr95 = ((mu_down_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr95)) mu__boolexpr94 = FALSE ;
  else {
  mu__boolexpr94 = ((mu_down_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr94)) mu__boolexpr93 = FALSE ;
  else {
  mu__boolexpr93 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr93) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 2;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_down_clock_started = mu_false;
mu_down_clock = 0.000000e+00;
mu_pos_y = assign_pos_y_duraction_end_down( mu_pos_y );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_down_clock = 0.000000e+00;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

pr.insert(std::make_pair(&(mu_down_clock_started), &(mu_down_clock))); 
return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( down)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeEnd;
  };

};
/******************** RuleBase3 ********************/
class RuleBase3
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" down_start ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr96;
  if (!(!(mu_down_clock_started))) mu__boolexpr96 = FALSE ;
  else {
  mu__boolexpr96 = (mu_all_event_true) ; 
}
    return mu__boolexpr96;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr97;
  if (!(!(mu_down_clock_started))) mu__boolexpr97 = FALSE ;
  else {
  mu__boolexpr97 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr98;
  if (!(!(mu_down_clock_started))) mu__boolexpr98 = FALSE ;
  else {
  mu__boolexpr98 = (mu_all_event_true) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr99;
  if (!(!(mu_down_clock_started))) mu__boolexpr99 = FALSE ;
  else {
  mu__boolexpr99 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_down_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_down_clock_started), 0)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr100;
  if (!(!(mu_down_clock_started))) mu__boolexpr100 = FALSE ;
  else {
  mu__boolexpr100 = (mu_all_event_true) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;



    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;


    aeffs.push_back(&(mu_down_clock_started)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_down_clock_started), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_down_clock_started)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 3;
    while (what_rule < 4 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr101;
  if (!(!(mu_down_clock_started))) mu__boolexpr101 = FALSE ;
  else {
  mu__boolexpr101 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr101) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 3;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_down_clock_started = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {



mu_down_clock_started = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_down_clock_started = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_down_clock_started = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {



return (&(mu_down_clock_started)); 


  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( down)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeStart;
  };

};
/******************** RuleBase4 ********************/
class RuleBase4
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" up_end ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr102;
bool mu__boolexpr103;
bool mu__boolexpr104;
  if (!(mu_up_clock_started)) mu__boolexpr104 = FALSE ;
  else {
  mu__boolexpr104 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr104)) mu__boolexpr103 = FALSE ;
  else {
  mu__boolexpr103 = ((mu_up_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr103)) mu__boolexpr102 = FALSE ;
  else {
  mu__boolexpr102 = (mu_all_event_true) ; 
}
    return mu__boolexpr102;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr105;
bool mu__boolexpr106;
  if (!(mu_up_clock_started)) mu__boolexpr106 = FALSE ;
  else {
  mu__boolexpr106 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr106)) mu__boolexpr105 = FALSE ;
  else {
  mu__boolexpr105 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr107;
bool mu__boolexpr108;
bool mu__boolexpr109;
  if (!(mu_up_clock_started)) mu__boolexpr109 = FALSE ;
  else {
  mu__boolexpr109 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr109)) mu__boolexpr108 = FALSE ;
  else {
  mu__boolexpr108 = ((mu_up_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr108)) mu__boolexpr107 = FALSE ;
  else {
  mu__boolexpr107 = (mu_all_event_true) ; 
}
bool mu__boolexpr110;
  if (!(mu_up_clock_started)) mu__boolexpr110 = FALSE ;
  else {
  mu__boolexpr110 = ((mu_up_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr111;
bool mu__boolexpr112;
  if (!(mu_up_clock_started)) mu__boolexpr112 = FALSE ;
  else {
  mu__boolexpr112 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr112)) mu__boolexpr111 = FALSE ;
  else {
  mu__boolexpr111 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr113;
  if (!(mu_up_clock_started)) mu__boolexpr113 = FALSE ;
  else {
  mu__boolexpr113 = ((mu_up_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_up_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_up_clock_started)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr114;
bool mu__boolexpr115;
  if (!(mu_up_clock_started)) mu__boolexpr115 = FALSE ;
  else {
  mu__boolexpr115 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr115)) mu__boolexpr114 = FALSE ;
  else {
  mu__boolexpr114 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr116;
bool mu__boolexpr117;
bool mu__boolexpr118;
  if (!(mu_up_clock_started)) mu__boolexpr118 = FALSE ;
  else {
  mu__boolexpr118 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr118)) mu__boolexpr117 = FALSE ;
  else {
  mu__boolexpr117 = ((mu_up_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr117)) mu__boolexpr116 = FALSE ;
  else {
  mu__boolexpr116 = (mu_all_event_true) ; 
}
bool mu__boolexpr119;
  if (!(mu_up_clock_started)) mu__boolexpr119 = FALSE ;
  else {
  mu__boolexpr119 = ((mu_up_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr120;
bool mu__boolexpr121;
  if (!(mu_up_clock_started)) mu__boolexpr121 = FALSE ;
  else {
  mu__boolexpr121 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr121)) mu__boolexpr120 = FALSE ;
  else {
  mu__boolexpr120 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr122;
  if (!(mu_up_clock_started)) mu__boolexpr122 = FALSE ;
  else {
  mu__boolexpr122 = ((mu_up_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_up_clock).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_up_clock), std::make_pair(0.000000e+00, 3))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_up_clock).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_up_clock)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_up_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_up_clock_started)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr123;
bool mu__boolexpr124;
  if (!(mu_up_clock_started)) mu__boolexpr124 = FALSE ;
  else {
  mu__boolexpr124 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr124)) mu__boolexpr123 = FALSE ;
  else {
  mu__boolexpr123 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr125;
bool mu__boolexpr126;
bool mu__boolexpr127;
  if (!(mu_up_clock_started)) mu__boolexpr127 = FALSE ;
  else {
  mu__boolexpr127 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr127)) mu__boolexpr126 = FALSE ;
  else {
  mu__boolexpr126 = ((mu_up_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr126)) mu__boolexpr125 = FALSE ;
  else {
  mu__boolexpr125 = (mu_all_event_true) ; 
}
bool mu__boolexpr128;
  if (!(mu_up_clock_started)) mu__boolexpr128 = FALSE ;
  else {
  mu__boolexpr128 = ((mu_up_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr129;
bool mu__boolexpr130;
  if (!(mu_up_clock_started)) mu__boolexpr130 = FALSE ;
  else {
  mu__boolexpr130 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr130)) mu__boolexpr129 = FALSE ;
  else {
  mu__boolexpr129 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr131;
  if (!(mu_up_clock_started)) mu__boolexpr131 = FALSE ;
  else {
  mu__boolexpr131 = ((mu_up_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 
 		if (std::string(typeid(mu_up_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_up_clock_started), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr132;
bool mu__boolexpr133;
  if (!(mu_up_clock_started)) mu__boolexpr133 = FALSE ;
  else {
  mu__boolexpr133 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr133)) mu__boolexpr132 = FALSE ;
  else {
  mu__boolexpr132 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr134;
bool mu__boolexpr135;
bool mu__boolexpr136;
  if (!(mu_up_clock_started)) mu__boolexpr136 = FALSE ;
  else {
  mu__boolexpr136 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr136)) mu__boolexpr135 = FALSE ;
  else {
  mu__boolexpr135 = ((mu_up_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr135)) mu__boolexpr134 = FALSE ;
  else {
  mu__boolexpr134 = (mu_all_event_true) ; 
}
bool mu__boolexpr137;
  if (!(mu_up_clock_started)) mu__boolexpr137 = FALSE ;
  else {
  mu__boolexpr137 = ((mu_up_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr138;
bool mu__boolexpr139;
  if (!(mu_up_clock_started)) mu__boolexpr139 = FALSE ;
  else {
  mu__boolexpr139 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr139)) mu__boolexpr138 = FALSE ;
  else {
  mu__boolexpr138 = ((mu_up_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr140;
  if (!(mu_up_clock_started)) mu__boolexpr140 = FALSE ;
  else {
  mu__boolexpr140 = ((mu_up_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_up_clock).name()).compare("14mu_1_real_type") == 0)
			temporal_cons = std::make_pair(0.000000e+00 + mu_T, TIME_INFINITY); 

    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_up_clock));  // 0.000000e+00 
    effs.push_back(&(mu_pos_y));  // assign_pos_y_duraction_end_up( mu_pos_y ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_up_clock_started), 0)); //  mu_false 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_up_clock)); //  0.000000e+00 
    aeffs.push_back(&(mu_pos_y)); //  assign_pos_y_duraction_end_up( mu_pos_y ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 4;
    while (what_rule < 5 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr141;
bool mu__boolexpr142;
bool mu__boolexpr143;
  if (!(mu_up_clock_started)) mu__boolexpr143 = FALSE ;
  else {
  mu__boolexpr143 = ((mu_up_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr143)) mu__boolexpr142 = FALSE ;
  else {
  mu__boolexpr142 = ((mu_up_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr142)) mu__boolexpr141 = FALSE ;
  else {
  mu__boolexpr141 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr141) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 4;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_up_clock_started = mu_false;
mu_up_clock = 0.000000e+00;
mu_pos_y = assign_pos_y_duraction_end_up( mu_pos_y );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_up_clock = 0.000000e+00;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

pr.insert(std::make_pair(&(mu_up_clock_started), &(mu_up_clock))); 
return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( up)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeEnd;
  };

};
/******************** RuleBase5 ********************/
class RuleBase5
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" up_start ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr144;
  if (!(!(mu_up_clock_started))) mu__boolexpr144 = FALSE ;
  else {
  mu__boolexpr144 = (mu_all_event_true) ; 
}
    return mu__boolexpr144;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr145;
  if (!(!(mu_up_clock_started))) mu__boolexpr145 = FALSE ;
  else {
  mu__boolexpr145 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr146;
  if (!(!(mu_up_clock_started))) mu__boolexpr146 = FALSE ;
  else {
  mu__boolexpr146 = (mu_all_event_true) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr147;
  if (!(!(mu_up_clock_started))) mu__boolexpr147 = FALSE ;
  else {
  mu__boolexpr147 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_up_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_up_clock_started), 0)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr148;
  if (!(!(mu_up_clock_started))) mu__boolexpr148 = FALSE ;
  else {
  mu__boolexpr148 = (mu_all_event_true) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;



    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;


    aeffs.push_back(&(mu_up_clock_started)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_up_clock_started), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_up_clock_started)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 5;
    while (what_rule < 6 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr149;
  if (!(!(mu_up_clock_started))) mu__boolexpr149 = FALSE ;
  else {
  mu__boolexpr149 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr149) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 5;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_up_clock_started = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {



mu_up_clock_started = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_up_clock_started = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_up_clock_started = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {



return (&(mu_up_clock_started)); 


  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( up)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeStart;
  };

};
/******************** RuleBase6 ********************/
class RuleBase6
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" left_end ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr150;
bool mu__boolexpr151;
bool mu__boolexpr152;
  if (!(mu_left_clock_started)) mu__boolexpr152 = FALSE ;
  else {
  mu__boolexpr152 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr152)) mu__boolexpr151 = FALSE ;
  else {
  mu__boolexpr151 = ((mu_left_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr151)) mu__boolexpr150 = FALSE ;
  else {
  mu__boolexpr150 = (mu_all_event_true) ; 
}
    return mu__boolexpr150;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr153;
bool mu__boolexpr154;
  if (!(mu_left_clock_started)) mu__boolexpr154 = FALSE ;
  else {
  mu__boolexpr154 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr154)) mu__boolexpr153 = FALSE ;
  else {
  mu__boolexpr153 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr155;
bool mu__boolexpr156;
bool mu__boolexpr157;
  if (!(mu_left_clock_started)) mu__boolexpr157 = FALSE ;
  else {
  mu__boolexpr157 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr157)) mu__boolexpr156 = FALSE ;
  else {
  mu__boolexpr156 = ((mu_left_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr156)) mu__boolexpr155 = FALSE ;
  else {
  mu__boolexpr155 = (mu_all_event_true) ; 
}
bool mu__boolexpr158;
  if (!(mu_left_clock_started)) mu__boolexpr158 = FALSE ;
  else {
  mu__boolexpr158 = ((mu_left_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr159;
bool mu__boolexpr160;
  if (!(mu_left_clock_started)) mu__boolexpr160 = FALSE ;
  else {
  mu__boolexpr160 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr160)) mu__boolexpr159 = FALSE ;
  else {
  mu__boolexpr159 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr161;
  if (!(mu_left_clock_started)) mu__boolexpr161 = FALSE ;
  else {
  mu__boolexpr161 = ((mu_left_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_left_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_left_clock_started)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr162;
bool mu__boolexpr163;
  if (!(mu_left_clock_started)) mu__boolexpr163 = FALSE ;
  else {
  mu__boolexpr163 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr163)) mu__boolexpr162 = FALSE ;
  else {
  mu__boolexpr162 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr164;
bool mu__boolexpr165;
bool mu__boolexpr166;
  if (!(mu_left_clock_started)) mu__boolexpr166 = FALSE ;
  else {
  mu__boolexpr166 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr166)) mu__boolexpr165 = FALSE ;
  else {
  mu__boolexpr165 = ((mu_left_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr165)) mu__boolexpr164 = FALSE ;
  else {
  mu__boolexpr164 = (mu_all_event_true) ; 
}
bool mu__boolexpr167;
  if (!(mu_left_clock_started)) mu__boolexpr167 = FALSE ;
  else {
  mu__boolexpr167 = ((mu_left_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr168;
bool mu__boolexpr169;
  if (!(mu_left_clock_started)) mu__boolexpr169 = FALSE ;
  else {
  mu__boolexpr169 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr169)) mu__boolexpr168 = FALSE ;
  else {
  mu__boolexpr168 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr170;
  if (!(mu_left_clock_started)) mu__boolexpr170 = FALSE ;
  else {
  mu__boolexpr170 = ((mu_left_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_left_clock).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_left_clock), std::make_pair(0.000000e+00, 3))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_left_clock).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_left_clock)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_left_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_left_clock_started)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr171;
bool mu__boolexpr172;
  if (!(mu_left_clock_started)) mu__boolexpr172 = FALSE ;
  else {
  mu__boolexpr172 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr172)) mu__boolexpr171 = FALSE ;
  else {
  mu__boolexpr171 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr173;
bool mu__boolexpr174;
bool mu__boolexpr175;
  if (!(mu_left_clock_started)) mu__boolexpr175 = FALSE ;
  else {
  mu__boolexpr175 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr175)) mu__boolexpr174 = FALSE ;
  else {
  mu__boolexpr174 = ((mu_left_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr174)) mu__boolexpr173 = FALSE ;
  else {
  mu__boolexpr173 = (mu_all_event_true) ; 
}
bool mu__boolexpr176;
  if (!(mu_left_clock_started)) mu__boolexpr176 = FALSE ;
  else {
  mu__boolexpr176 = ((mu_left_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr177;
bool mu__boolexpr178;
  if (!(mu_left_clock_started)) mu__boolexpr178 = FALSE ;
  else {
  mu__boolexpr178 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr178)) mu__boolexpr177 = FALSE ;
  else {
  mu__boolexpr177 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr179;
  if (!(mu_left_clock_started)) mu__boolexpr179 = FALSE ;
  else {
  mu__boolexpr179 = ((mu_left_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 
 		if (std::string(typeid(mu_left_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_left_clock_started), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr180;
bool mu__boolexpr181;
  if (!(mu_left_clock_started)) mu__boolexpr181 = FALSE ;
  else {
  mu__boolexpr181 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr181)) mu__boolexpr180 = FALSE ;
  else {
  mu__boolexpr180 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr182;
bool mu__boolexpr183;
bool mu__boolexpr184;
  if (!(mu_left_clock_started)) mu__boolexpr184 = FALSE ;
  else {
  mu__boolexpr184 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr184)) mu__boolexpr183 = FALSE ;
  else {
  mu__boolexpr183 = ((mu_left_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr183)) mu__boolexpr182 = FALSE ;
  else {
  mu__boolexpr182 = (mu_all_event_true) ; 
}
bool mu__boolexpr185;
  if (!(mu_left_clock_started)) mu__boolexpr185 = FALSE ;
  else {
  mu__boolexpr185 = ((mu_left_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr186;
bool mu__boolexpr187;
  if (!(mu_left_clock_started)) mu__boolexpr187 = FALSE ;
  else {
  mu__boolexpr187 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr187)) mu__boolexpr186 = FALSE ;
  else {
  mu__boolexpr186 = ((mu_left_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr188;
  if (!(mu_left_clock_started)) mu__boolexpr188 = FALSE ;
  else {
  mu__boolexpr188 = ((mu_left_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_left_clock).name()).compare("14mu_1_real_type") == 0)
			temporal_cons = std::make_pair(0.000000e+00 + mu_T, TIME_INFINITY); 

    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_left_clock));  // 0.000000e+00 
    effs.push_back(&(mu_pos_x));  // assign_pos_x_duraction_end_left( mu_pos_x ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_left_clock_started), 0)); //  mu_false 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_left_clock)); //  0.000000e+00 
    aeffs.push_back(&(mu_pos_x)); //  assign_pos_x_duraction_end_left( mu_pos_x ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 6;
    while (what_rule < 7 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr189;
bool mu__boolexpr190;
bool mu__boolexpr191;
  if (!(mu_left_clock_started)) mu__boolexpr191 = FALSE ;
  else {
  mu__boolexpr191 = ((mu_left_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr191)) mu__boolexpr190 = FALSE ;
  else {
  mu__boolexpr190 = ((mu_left_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr190)) mu__boolexpr189 = FALSE ;
  else {
  mu__boolexpr189 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr189) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 6;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_left_clock_started = mu_false;
mu_left_clock = 0.000000e+00;
mu_pos_x = assign_pos_x_duraction_end_left( mu_pos_x );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_left_clock = 0.000000e+00;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

pr.insert(std::make_pair(&(mu_left_clock_started), &(mu_left_clock))); 
return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( left)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeEnd;
  };

};
/******************** RuleBase7 ********************/
class RuleBase7
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" left_start ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr192;
  if (!(!(mu_left_clock_started))) mu__boolexpr192 = FALSE ;
  else {
  mu__boolexpr192 = (mu_all_event_true) ; 
}
    return mu__boolexpr192;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr193;
  if (!(!(mu_left_clock_started))) mu__boolexpr193 = FALSE ;
  else {
  mu__boolexpr193 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr194;
  if (!(!(mu_left_clock_started))) mu__boolexpr194 = FALSE ;
  else {
  mu__boolexpr194 = (mu_all_event_true) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr195;
  if (!(!(mu_left_clock_started))) mu__boolexpr195 = FALSE ;
  else {
  mu__boolexpr195 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_left_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_left_clock_started), 0)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr196;
  if (!(!(mu_left_clock_started))) mu__boolexpr196 = FALSE ;
  else {
  mu__boolexpr196 = (mu_all_event_true) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;



    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;


    aeffs.push_back(&(mu_left_clock_started)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_left_clock_started), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_left_clock_started)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 7;
    while (what_rule < 8 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr197;
  if (!(!(mu_left_clock_started))) mu__boolexpr197 = FALSE ;
  else {
  mu__boolexpr197 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr197) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 7;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_left_clock_started = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {



mu_left_clock_started = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_left_clock_started = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_left_clock_started = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {



return (&(mu_left_clock_started)); 


  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( left)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeStart;
  };

};
/******************** RuleBase8 ********************/
class RuleBase8
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" right_end ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr198;
bool mu__boolexpr199;
bool mu__boolexpr200;
  if (!(mu_right_clock_started)) mu__boolexpr200 = FALSE ;
  else {
  mu__boolexpr200 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr200)) mu__boolexpr199 = FALSE ;
  else {
  mu__boolexpr199 = ((mu_right_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr199)) mu__boolexpr198 = FALSE ;
  else {
  mu__boolexpr198 = (mu_all_event_true) ; 
}
    return mu__boolexpr198;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr201;
bool mu__boolexpr202;
  if (!(mu_right_clock_started)) mu__boolexpr202 = FALSE ;
  else {
  mu__boolexpr202 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr202)) mu__boolexpr201 = FALSE ;
  else {
  mu__boolexpr201 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr203;
bool mu__boolexpr204;
bool mu__boolexpr205;
  if (!(mu_right_clock_started)) mu__boolexpr205 = FALSE ;
  else {
  mu__boolexpr205 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr205)) mu__boolexpr204 = FALSE ;
  else {
  mu__boolexpr204 = ((mu_right_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr204)) mu__boolexpr203 = FALSE ;
  else {
  mu__boolexpr203 = (mu_all_event_true) ; 
}
bool mu__boolexpr206;
  if (!(mu_right_clock_started)) mu__boolexpr206 = FALSE ;
  else {
  mu__boolexpr206 = ((mu_right_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr207;
bool mu__boolexpr208;
  if (!(mu_right_clock_started)) mu__boolexpr208 = FALSE ;
  else {
  mu__boolexpr208 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr208)) mu__boolexpr207 = FALSE ;
  else {
  mu__boolexpr207 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr209;
  if (!(mu_right_clock_started)) mu__boolexpr209 = FALSE ;
  else {
  mu__boolexpr209 = ((mu_right_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_right_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_right_clock_started)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr210;
bool mu__boolexpr211;
  if (!(mu_right_clock_started)) mu__boolexpr211 = FALSE ;
  else {
  mu__boolexpr211 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr211)) mu__boolexpr210 = FALSE ;
  else {
  mu__boolexpr210 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr212;
bool mu__boolexpr213;
bool mu__boolexpr214;
  if (!(mu_right_clock_started)) mu__boolexpr214 = FALSE ;
  else {
  mu__boolexpr214 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr214)) mu__boolexpr213 = FALSE ;
  else {
  mu__boolexpr213 = ((mu_right_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr213)) mu__boolexpr212 = FALSE ;
  else {
  mu__boolexpr212 = (mu_all_event_true) ; 
}
bool mu__boolexpr215;
  if (!(mu_right_clock_started)) mu__boolexpr215 = FALSE ;
  else {
  mu__boolexpr215 = ((mu_right_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr216;
bool mu__boolexpr217;
  if (!(mu_right_clock_started)) mu__boolexpr217 = FALSE ;
  else {
  mu__boolexpr217 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr217)) mu__boolexpr216 = FALSE ;
  else {
  mu__boolexpr216 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr218;
  if (!(mu_right_clock_started)) mu__boolexpr218 = FALSE ;
  else {
  mu__boolexpr218 = ((mu_right_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_right_clock).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_right_clock), std::make_pair(0.000000e+00, 3))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_right_clock).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_right_clock)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 
 		if (std::string(typeid(mu_right_clock_started).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_right_clock_started)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr219;
bool mu__boolexpr220;
  if (!(mu_right_clock_started)) mu__boolexpr220 = FALSE ;
  else {
  mu__boolexpr220 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr220)) mu__boolexpr219 = FALSE ;
  else {
  mu__boolexpr219 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr221;
bool mu__boolexpr222;
bool mu__boolexpr223;
  if (!(mu_right_clock_started)) mu__boolexpr223 = FALSE ;
  else {
  mu__boolexpr223 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr223)) mu__boolexpr222 = FALSE ;
  else {
  mu__boolexpr222 = ((mu_right_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr222)) mu__boolexpr221 = FALSE ;
  else {
  mu__boolexpr221 = (mu_all_event_true) ; 
}
bool mu__boolexpr224;
  if (!(mu_right_clock_started)) mu__boolexpr224 = FALSE ;
  else {
  mu__boolexpr224 = ((mu_right_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr225;
bool mu__boolexpr226;
  if (!(mu_right_clock_started)) mu__boolexpr226 = FALSE ;
  else {
  mu__boolexpr226 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr226)) mu__boolexpr225 = FALSE ;
  else {
  mu__boolexpr225 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr227;
  if (!(mu_right_clock_started)) mu__boolexpr227 = FALSE ;
  else {
  mu__boolexpr227 = ((mu_right_clock) == (1.000000e+00)) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 
 		if (std::string(typeid(mu_right_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_right_clock_started), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr228;
bool mu__boolexpr229;
  if (!(mu_right_clock_started)) mu__boolexpr229 = FALSE ;
  else {
  mu__boolexpr229 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr229)) mu__boolexpr228 = FALSE ;
  else {
  mu__boolexpr228 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr230;
bool mu__boolexpr231;
bool mu__boolexpr232;
  if (!(mu_right_clock_started)) mu__boolexpr232 = FALSE ;
  else {
  mu__boolexpr232 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr232)) mu__boolexpr231 = FALSE ;
  else {
  mu__boolexpr231 = ((mu_right_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr231)) mu__boolexpr230 = FALSE ;
  else {
  mu__boolexpr230 = (mu_all_event_true) ; 
}
bool mu__boolexpr233;
  if (!(mu_right_clock_started)) mu__boolexpr233 = FALSE ;
  else {
  mu__boolexpr233 = ((mu_right_clock) == (1.000000e+00)) ; 
}
bool mu__boolexpr234;
bool mu__boolexpr235;
  if (!(mu_right_clock_started)) mu__boolexpr235 = FALSE ;
  else {
  mu__boolexpr235 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr235)) mu__boolexpr234 = FALSE ;
  else {
  mu__boolexpr234 = ((mu_right_clock) > (0.000000e+00)) ; 
}
bool mu__boolexpr236;
  if (!(mu_right_clock_started)) mu__boolexpr236 = FALSE ;
  else {
  mu__boolexpr236 = ((mu_right_clock) == (1.000000e+00)) ; 
}

 	if (std::string(typeid(mu_right_clock).name()).compare("14mu_1_real_type") == 0)
			temporal_cons = std::make_pair(0.000000e+00 + mu_T, TIME_INFINITY); 

    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_right_clock));  // 0.000000e+00 
    effs.push_back(&(mu_pos_x));  // assign_pos_x_duraction_end_right( mu_pos_x ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_right_clock_started), 0)); //  mu_false 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_right_clock)); //  0.000000e+00 
    aeffs.push_back(&(mu_pos_x)); //  assign_pos_x_duraction_end_right( mu_pos_x ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 8;
    while (what_rule < 9 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr237;
bool mu__boolexpr238;
bool mu__boolexpr239;
  if (!(mu_right_clock_started)) mu__boolexpr239 = FALSE ;
  else {
  mu__boolexpr239 = ((mu_right_clock) == (1.000000e+00)) ; 
}
  if (!(mu__boolexpr239)) mu__boolexpr238 = FALSE ;
  else {
  mu__boolexpr238 = ((mu_right_clock) > (0.000000e+00)) ; 
}
  if (!(mu__boolexpr238)) mu__boolexpr237 = FALSE ;
  else {
  mu__boolexpr237 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr237) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 8;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_right_clock_started = mu_false;
mu_right_clock = 0.000000e+00;
mu_pos_x = assign_pos_x_duraction_end_right( mu_pos_x );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_right_clock = 0.000000e+00;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

pr.insert(std::make_pair(&(mu_right_clock_started), &(mu_right_clock))); 
return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( right)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeEnd;
  };

};
/******************** RuleBase9 ********************/
class RuleBase9
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" right_start ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr240;
  if (!(!(mu_right_clock_started))) mu__boolexpr240 = FALSE ;
  else {
  mu__boolexpr240 = (mu_all_event_true) ; 
}
    return mu__boolexpr240;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr241;
  if (!(!(mu_right_clock_started))) mu__boolexpr241 = FALSE ;
  else {
  mu__boolexpr241 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr242;
  if (!(!(mu_right_clock_started))) mu__boolexpr242 = FALSE ;
  else {
  mu__boolexpr242 = (mu_all_event_true) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_all_event_true)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr243;
  if (!(!(mu_right_clock_started))) mu__boolexpr243 = FALSE ;
  else {
  mu__boolexpr243 = (mu_all_event_true) ; 
}

 		if (std::string(typeid(mu_right_clock_started).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_right_clock_started), 0)); 
 		if (std::string(typeid(mu_all_event_true).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_all_event_true), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr244;
  if (!(!(mu_right_clock_started))) mu__boolexpr244 = FALSE ;
  else {
  mu__boolexpr244 = (mu_all_event_true) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;



    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;


    aeffs.push_back(&(mu_right_clock_started)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_right_clock_started), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_right_clock_started)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 9;
    while (what_rule < 10 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr245;
  if (!(!(mu_right_clock_started))) mu__boolexpr245 = FALSE ;
  else {
  mu__boolexpr245 = (mu_all_event_true) ; 
}
	      if (mu__boolexpr245) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 9;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_right_clock_started = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {



mu_right_clock_started = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_right_clock_started = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_right_clock_started = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {



return (&(mu_right_clock_started)); 


  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( right)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::DurativeStart;
  };

};
class NextStateGenerator
{
  RuleBase0 R0;
  RuleBase1 R1;
  RuleBase2 R2;
  RuleBase3 R3;
  RuleBase4 R4;
  RuleBase5 R5;
  RuleBase6 R6;
  RuleBase7 R7;
  RuleBase8 R8;
  RuleBase9 R9;
public:
void SetNextEnabledRule(RULE_INDEX_TYPE & what_rule)
{
  category = CONDITION;
  if (what_rule<1)
    { R0.NextRule(what_rule);
      if (what_rule<1) return; }
  if (what_rule>=1 && what_rule<2)
    { R1.NextRule(what_rule);
      if (what_rule<2) return; }
  if (what_rule>=2 && what_rule<3)
    { R2.NextRule(what_rule);
      if (what_rule<3) return; }
  if (what_rule>=3 && what_rule<4)
    { R3.NextRule(what_rule);
      if (what_rule<4) return; }
  if (what_rule>=4 && what_rule<5)
    { R4.NextRule(what_rule);
      if (what_rule<5) return; }
  if (what_rule>=5 && what_rule<6)
    { R5.NextRule(what_rule);
      if (what_rule<6) return; }
  if (what_rule>=6 && what_rule<7)
    { R6.NextRule(what_rule);
      if (what_rule<7) return; }
  if (what_rule>=7 && what_rule<8)
    { R7.NextRule(what_rule);
      if (what_rule<8) return; }
  if (what_rule>=8 && what_rule<9)
    { R8.NextRule(what_rule);
      if (what_rule<9) return; }
  if (what_rule>=9 && what_rule<10)
    { R9.NextRule(what_rule);
      if (what_rule<10) return; }
}
bool Condition(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.Condition(r-0);
  if (r>=1 && r<=1) return R1.Condition(r-1);
  if (r>=2 && r<=2) return R2.Condition(r-2);
  if (r>=3 && r<=3) return R3.Condition(r-3);
  if (r>=4 && r<=4) return R4.Condition(r-4);
  if (r>=5 && r<=5) return R5.Condition(r-5);
  if (r>=6 && r<=6) return R6.Condition(r-6);
  if (r>=7 && r<=7) return R7.Condition(r-7);
  if (r>=8 && r<=8) return R8.Condition(r-8);
  if (r>=9 && r<=9) return R9.Condition(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking condition for nonexisting rule.");
}
std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.bool_precond_array(r-0);
  if (r>=1 && r<=1) return R1.bool_precond_array(r-1);
  if (r>=2 && r<=2) return R2.bool_precond_array(r-2);
  if (r>=3 && r<=3) return R3.bool_precond_array(r-3);
  if (r>=4 && r<=4) return R4.bool_precond_array(r-4);
  if (r>=5 && r<=5) return R5.bool_precond_array(r-5);
  if (r>=6 && r<=6) return R6.bool_precond_array(r-6);
  if (r>=7 && r<=7) return R7.bool_precond_array(r-7);
  if (r>=8 && r<=8) return R8.bool_precond_array(r-8);
  if (r>=9 && r<=9) return R9.bool_precond_array(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::map<mu__real*, std::pair<double,int> > num_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.num_precond_array(r-0);
  if (r>=1 && r<=1) return R1.num_precond_array(r-1);
  if (r>=2 && r<=2) return R2.num_precond_array(r-2);
  if (r>=3 && r<=3) return R3.num_precond_array(r-3);
  if (r>=4 && r<=4) return R4.num_precond_array(r-4);
  if (r>=5 && r<=5) return R5.num_precond_array(r-5);
  if (r>=6 && r<=6) return R6.num_precond_array(r-6);
  if (r>=7 && r<=7) return R7.num_precond_array(r-7);
  if (r>=8 && r<=8) return R8.num_precond_array(r-8);
  if (r>=9 && r<=9) return R9.num_precond_array(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.all_precond_array(r-0);
  if (r>=1 && r<=1) return R1.all_precond_array(r-1);
  if (r>=2 && r<=2) return R2.all_precond_array(r-2);
  if (r>=3 && r<=3) return R3.all_precond_array(r-3);
  if (r>=4 && r<=4) return R4.all_precond_array(r-4);
  if (r>=5 && r<=5) return R5.all_precond_array(r-5);
  if (r>=6 && r<=6) return R6.all_precond_array(r-6);
  if (r>=7 && r<=7) return R7.all_precond_array(r-7);
  if (r>=8 && r<=8) return R8.all_precond_array(r-8);
  if (r>=9 && r<=9) return R9.all_precond_array(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.precond_bool_interference(r-0);
  if (r>=1 && r<=1) return R1.precond_bool_interference(r-1);
  if (r>=2 && r<=2) return R2.precond_bool_interference(r-2);
  if (r>=3 && r<=3) return R3.precond_bool_interference(r-3);
  if (r>=4 && r<=4) return R4.precond_bool_interference(r-4);
  if (r>=5 && r<=5) return R5.precond_bool_interference(r-5);
  if (r>=6 && r<=6) return R6.precond_bool_interference(r-6);
  if (r>=7 && r<=7) return R7.precond_bool_interference(r-7);
  if (r>=8 && r<=8) return R8.precond_bool_interference(r-8);
  if (r>=9 && r<=9) return R9.precond_bool_interference(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.temporal_constraints(r-0);
  if (r>=1 && r<=1) return R1.temporal_constraints(r-1);
  if (r>=2 && r<=2) return R2.temporal_constraints(r-2);
  if (r>=3 && r<=3) return R3.temporal_constraints(r-3);
  if (r>=4 && r<=4) return R4.temporal_constraints(r-4);
  if (r>=5 && r<=5) return R5.temporal_constraints(r-5);
  if (r>=6 && r<=6) return R6.temporal_constraints(r-6);
  if (r>=7 && r<=7) return R7.temporal_constraints(r-7);
  if (r>=8 && r<=8) return R8.temporal_constraints(r-8);
  if (r>=9 && r<=9) return R9.temporal_constraints(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_bool_interference(r-0);
  if (r>=1 && r<=1) return R1.effects_bool_interference(r-1);
  if (r>=2 && r<=2) return R2.effects_bool_interference(r-2);
  if (r>=3 && r<=3) return R3.effects_bool_interference(r-3);
  if (r>=4 && r<=4) return R4.effects_bool_interference(r-4);
  if (r>=5 && r<=5) return R5.effects_bool_interference(r-5);
  if (r>=6 && r<=6) return R6.effects_bool_interference(r-6);
  if (r>=7 && r<=7) return R7.effects_bool_interference(r-7);
  if (r>=8 && r<=8) return R8.effects_bool_interference(r-8);
  if (r>=9 && r<=9) return R9.effects_bool_interference(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_add_bool_array(r-0);
  if (r>=1 && r<=1) return R1.effects_add_bool_array(r-1);
  if (r>=2 && r<=2) return R2.effects_add_bool_array(r-2);
  if (r>=3 && r<=3) return R3.effects_add_bool_array(r-3);
  if (r>=4 && r<=4) return R4.effects_add_bool_array(r-4);
  if (r>=5 && r<=5) return R5.effects_add_bool_array(r-5);
  if (r>=6 && r<=6) return R6.effects_add_bool_array(r-6);
  if (r>=7 && r<=7) return R7.effects_add_bool_array(r-7);
  if (r>=8 && r<=8) return R8.effects_add_bool_array(r-8);
  if (r>=9 && r<=9) return R9.effects_add_bool_array(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_num_array(r-0);
  if (r>=1 && r<=1) return R1.effects_num_array(r-1);
  if (r>=2 && r<=2) return R2.effects_num_array(r-2);
  if (r>=3 && r<=3) return R3.effects_num_array(r-3);
  if (r>=4 && r<=4) return R4.effects_num_array(r-4);
  if (r>=5 && r<=5) return R5.effects_num_array(r-5);
  if (r>=6 && r<=6) return R6.effects_num_array(r-6);
  if (r>=7 && r<=7) return R7.effects_num_array(r-7);
  if (r>=8 && r<=8) return R8.effects_num_array(r-8);
  if (r>=9 && r<=9) return R9.effects_num_array(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_all_array(r-0);
  if (r>=1 && r<=1) return R1.effects_all_array(r-1);
  if (r>=2 && r<=2) return R2.effects_all_array(r-2);
  if (r>=3 && r<=3) return R3.effects_all_array(r-3);
  if (r>=4 && r<=4) return R4.effects_all_array(r-4);
  if (r>=5 && r<=5) return R5.effects_all_array(r-5);
  if (r>=6 && r<=6) return R6.effects_all_array(r-6);
  if (r>=7 && r<=7) return R7.effects_all_array(r-7);
  if (r>=8 && r<=8) return R8.effects_all_array(r-8);
  if (r>=9 && r<=9) return R9.effects_all_array(r-9);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
void Code(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code(r-5); return; } 
  if (r>=6 && r<=6) { R6.Code(r-6); return; } 
  if (r>=7 && r<=7) { R7.Code(r-7); return; } 
  if (r>=8 && r<=8) { R8.Code(r-8); return; } 
  if (r>=9 && r<=9) { R9.Code(r-9); return; } 
}
void Code_ff(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_ff(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_ff(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_ff(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_ff(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code_ff(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code_ff(r-5); return; } 
  if (r>=6 && r<=6) { R6.Code_ff(r-6); return; } 
  if (r>=7 && r<=7) { R7.Code_ff(r-7); return; } 
  if (r>=8 && r<=8) { R8.Code_ff(r-8); return; } 
  if (r>=9 && r<=9) { R9.Code_ff(r-9); return; } 
}
void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_plus(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_numeric_ff_plus(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_numeric_ff_plus(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_numeric_ff_plus(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code_numeric_ff_plus(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code_numeric_ff_plus(r-5); return; } 
  if (r>=6 && r<=6) { R6.Code_numeric_ff_plus(r-6); return; } 
  if (r>=7 && r<=7) { R7.Code_numeric_ff_plus(r-7); return; } 
  if (r>=8 && r<=8) { R8.Code_numeric_ff_plus(r-8); return; } 
  if (r>=9 && r<=9) { R9.Code_numeric_ff_plus(r-9); return; } 
}
void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_minus(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_numeric_ff_minus(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_numeric_ff_minus(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_numeric_ff_minus(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code_numeric_ff_minus(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code_numeric_ff_minus(r-5); return; } 
  if (r>=6 && r<=6) { R6.Code_numeric_ff_minus(r-6); return; } 
  if (r>=7 && r<=7) { R7.Code_numeric_ff_minus(r-7); return; } 
  if (r>=8 && r<=8) { R8.Code_numeric_ff_minus(r-8); return; } 
  if (r>=9 && r<=9) { R9.Code_numeric_ff_minus(r-9); return; } 
}
mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_rule_clock_started(r-0); } 
  if (r>=1 && r<=1) { return R1.get_rule_clock_started(r-1); } 
  if (r>=2 && r<=2) { return R2.get_rule_clock_started(r-2); } 
  if (r>=3 && r<=3) { return R3.get_rule_clock_started(r-3); } 
  if (r>=4 && r<=4) { return R4.get_rule_clock_started(r-4); } 
  if (r>=5 && r<=5) { return R5.get_rule_clock_started(r-5); } 
  if (r>=6 && r<=6) { return R6.get_rule_clock_started(r-6); } 
  if (r>=7 && r<=7) { return R7.get_rule_clock_started(r-7); } 
  if (r>=8 && r<=8) { return R8.get_rule_clock_started(r-8); } 
  if (r>=9 && r<=9) { return R9.get_rule_clock_started(r-9); } 
}
std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_clocks(r-0); } 
  if (r>=1 && r<=1) { return R1.get_clocks(r-1); } 
  if (r>=2 && r<=2) { return R2.get_clocks(r-2); } 
  if (r>=3 && r<=3) { return R3.get_clocks(r-3); } 
  if (r>=4 && r<=4) { return R4.get_clocks(r-4); } 
  if (r>=5 && r<=5) { return R5.get_clocks(r-5); } 
  if (r>=6 && r<=6) { return R6.get_clocks(r-6); } 
  if (r>=7 && r<=7) { return R7.get_clocks(r-7); } 
  if (r>=8 && r<=8) { return R8.get_clocks(r-8); } 
  if (r>=9 && r<=9) { return R9.get_clocks(r-9); } 
}
int Priority(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.Priority(); } 
  if (r>=1 && r<=1) { return R1.Priority(); } 
  if (r>=2 && r<=2) { return R2.Priority(); } 
  if (r>=3 && r<=3) { return R3.Priority(); } 
  if (r>=4 && r<=4) { return R4.Priority(); } 
  if (r>=5 && r<=5) { return R5.Priority(); } 
  if (r>=6 && r<=6) { return R6.Priority(); } 
  if (r>=7 && r<=7) { return R7.Priority(); } 
  if (r>=8 && r<=8) { return R8.Priority(); } 
  if (r>=9 && r<=9) { return R9.Priority(); } 
}
char * Name(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Name(r-0);
  if (r>=1 && r<=1) return R1.Name(r-1);
  if (r>=2 && r<=2) return R2.Name(r-2);
  if (r>=3 && r<=3) return R3.Name(r-3);
  if (r>=4 && r<=4) return R4.Name(r-4);
  if (r>=5 && r<=5) return R5.Name(r-5);
  if (r>=6 && r<=6) return R6.Name(r-6);
  if (r>=7 && r<=7) return R7.Name(r-7);
  if (r>=8 && r<=8) return R8.Name(r-8);
  if (r>=9 && r<=9) return R9.Name(r-9);
  return NULL;
}
int Duration(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Duration(r-0);
  if (r>=1 && r<=1) return R1.Duration(r-1);
  if (r>=2 && r<=2) return R2.Duration(r-2);
  if (r>=3 && r<=3) return R3.Duration(r-3);
  if (r>=4 && r<=4) return R4.Duration(r-4);
  if (r>=5 && r<=5) return R5.Duration(r-5);
  if (r>=6 && r<=6) return R6.Duration(r-6);
  if (r>=7 && r<=7) return R7.Duration(r-7);
  if (r>=8 && r<=8) return R8.Duration(r-8);
  if (r>=9 && r<=9) return R9.Duration(r-9);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
int Weight(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Weight(r-0);
  if (r>=1 && r<=1) return R1.Weight(r-1);
  if (r>=2 && r<=2) return R2.Weight(r-2);
  if (r>=3 && r<=3) return R3.Weight(r-3);
  if (r>=4 && r<=4) return R4.Weight(r-4);
  if (r>=5 && r<=5) return R5.Weight(r-5);
  if (r>=6 && r<=6) return R6.Weight(r-6);
  if (r>=7 && r<=7) return R7.Weight(r-7);
  if (r>=8 && r<=8) return R8.Weight(r-8);
  if (r>=9 && r<=9) return R9.Weight(r-9);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
 char * PDDLName(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLName(r-0);
  if (r>=1 && r<=1) return R1.PDDLName(r-1);
  if (r>=2 && r<=2) return R2.PDDLName(r-2);
  if (r>=3 && r<=3) return R3.PDDLName(r-3);
  if (r>=4 && r<=4) return R4.PDDLName(r-4);
  if (r>=5 && r<=5) return R5.PDDLName(r-5);
  if (r>=6 && r<=6) return R6.PDDLName(r-6);
  if (r>=7 && r<=7) return R7.PDDLName(r-7);
  if (r>=8 && r<=8) return R8.PDDLName(r-8);
  if (r>=9 && r<=9) return R9.PDDLName(r-9);
  return NULL;
}
RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLClass(r-0);
  if (r>=1 && r<=1) return R1.PDDLClass(r-1);
  if (r>=2 && r<=2) return R2.PDDLClass(r-2);
  if (r>=3 && r<=3) return R3.PDDLClass(r-3);
  if (r>=4 && r<=4) return R4.PDDLClass(r-4);
  if (r>=5 && r<=5) return R5.PDDLClass(r-5);
  if (r>=6 && r<=6) return R6.PDDLClass(r-6);
  if (r>=7 && r<=7) return R7.PDDLClass(r-7);
  if (r>=8 && r<=8) return R8.PDDLClass(r-8);
  if (r>=9 && r<=9) return R9.PDDLClass(r-9);
Error.Notrace("Internal: NextStateGenerator -- querying PDDL class for nonexisting rule.");
}
};
const RULE_INDEX_TYPE numrules = 10;

/********************
  parameter
 ********************/
#define RULES_IN_WORLD 10


/********************
  Startstate records
 ********************/
/******************** StartStateBase0 ********************/
class StartStateBase0
{
public:
  char * Name(unsigned short r)
  {
    return tsprintf("start");
  }
  void Code(unsigned short r)
  {
mu_TIME = 0.000000e+00;
mu_set_reward ( mu_false );
mu_set_mission_completed ( mu_false );
mu_pos_x = 0.000000e+00;
mu_pos_y = 0.000000e+00;
mu_ob_x = 0.000000e+00;
mu_ob_y = 0.000000e+00;
mu_running_time = 0.000000e+00;
mu_reward = mu_false;
mu_mission_completed = mu_false;
mu_running_time = 0.000000e+00;
mu_pos_x = 0.000000e+00;
mu_pos_y = 0.000000e+00;
mu_ob_x = 1.000000e+00;
mu_ob_y = 0.000000e+00;
mu_down_clock_started = mu_false;
mu_down_clock = 0.000000e+00;
mu_left_clock_started = mu_false;
mu_left_clock = 0.000000e+00;
mu_right_clock_started = mu_false;
mu_right_clock = 0.000000e+00;
mu_up_clock_started = mu_false;
mu_up_clock = 0.000000e+00;
mu_all_event_true = mu_true;
mu_g_n = 0;
mu_h_n = 0;
mu_f_n = 0;
  };

};
class StartStateGenerator
{
  StartStateBase0 S0;
public:
void Code(unsigned short r)
{
  if (r<=0) { S0.Code(r-0); return; }
}
char * Name(unsigned short r)
{
  if (r<=0) return S0.Name(r-0);
  return NULL;
}
};
const rulerec startstates[] = {
{ NULL, NULL, NULL, FALSE},
};
unsigned short StartStateManager::numstartstates = 1;

/********************
  Goal records
 ********************/

// WP WP WP GOAL
int mu__goal_246() // Goal "enjoy"
{
bool mu__boolexpr247;
  if (!(mu_mission_completed)) mu__boolexpr247 = FALSE ;
  else {
  mu__boolexpr247 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}
return mu__boolexpr247;
};

  std::set<mu_0_boolean*> get_bool_goal_conditions()
  {
    std::set<mu_0_boolean*> bool_goal_conds;
bool mu__boolexpr248;
  if (!(mu_mission_completed)) mu__boolexpr248 = FALSE ;
  else {
  mu__boolexpr248 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}

 if (std::string(typeid(mu_mission_completed).name()).compare("12mu_0_boolean") == 0)
		bool_goal_conds.insert(&(mu_mission_completed)); 

    return bool_goal_conds;
  }

  std::map<mu__real*, std::pair<double, int> > get_numeric_goal_conditions()
  {
    std::map<mu__real*, std::pair<double, int> > numeric_goal_conds;

    return numeric_goal_conds;
  }

bool mu__condition_249() // Condition for Rule "enjoy"
{
  return mu__goal_246( );
}

bool mu__goal__00(){ return mu__condition_249(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/


// WP WP WP GOAL
const rulerec goals[] = {
{"enjoy", &mu__condition_249, NULL, },
};
const unsigned short numgoals = 1;

/********************
  Metric related stuff
 ********************/
const short metric = -1;

/********************
  Invariant records
 ********************/
int mu__invariant_250() // Invariant "todo bien"
{
bool mu__boolexpr251;
  if (!(mu_all_event_true)) mu__boolexpr251 = FALSE ;
  else {
  mu__boolexpr251 = (!(mu_DAs_violate_duration(  ))) ; 
}
return mu__boolexpr251;
};

bool mu__condition_252() // Condition for Rule "todo bien"
{
  return mu__invariant_250( );
}

bool mu__goal__01(){ return mu__condition_252(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/

const rulerec invariants[] = {
{"todo bien", &mu__condition_252, NULL, },
};
const unsigned short numinvariants = 1;

/********************
  Normal/Canonicalization for scalarset
 ********************/
/*
reward:NoScalarset
down_clock_started:NoScalarset
up_clock_started:NoScalarset
left_clock_started:NoScalarset
right_clock_started:NoScalarset
pos_y:NoScalarset
ob_y:NoScalarset
TIME:NoScalarset
g_n:NoScalarset
all_event_true:NoScalarset
h_n:NoScalarset
f_n:NoScalarset
ob_x:NoScalarset
pos_x:NoScalarset
running_time:NoScalarset
right_clock:NoScalarset
left_clock:NoScalarset
up_clock:NoScalarset
down_clock:NoScalarset
mission_completed:NoScalarset
*/

/********************
Code for symmetry
 ********************/

/********************
 Permutation Set Class
 ********************/
class PermSet
{
public:
  // book keeping
  enum PresentationType {Simple, Explicit};
  PresentationType Presentation;

  void ResetToSimple();
  void ResetToExplicit();
  void SimpleToExplicit();
  void SimpleToOne();
  bool NextPermutation();

  void Print_in_size()
  { unsigned long ret=0; for (unsigned long i=0; i<count; i++) if (in[i]) ret++; cout << "in_size:" << ret << "\n"; }


  /********************
   Simple and efficient representation
   ********************/
  bool AlreadyOnlyOneRemain;
  bool MoreThanOneRemain();


  /********************
   Explicit representation
  ********************/
  unsigned long size;
  unsigned long count;
  // in will be of product of factorial sizes for fast canonicalize
  // in will be of size 1 for reduced local memory canonicalize
  bool * in;

  // auxiliary for explicit representation

  // in/perm/revperm will be of factorial size for fast canonicalize
  // they will be of size 1 for reduced local memory canonicalize
  // second range will be size of the scalarset
  // procedure for explicit representation
  // General procedure
  PermSet();
  bool In(int i) const { return in[i]; };
  void Add(int i) { for (int j=0; j<i; j++) in[j] = FALSE;};
  void Remove(int i) { in[i] = FALSE; };
};
bool PermSet::MoreThanOneRemain()
{
  int i,j;
  if (AlreadyOnlyOneRemain)
    return FALSE;
  else {
  }
  AlreadyOnlyOneRemain = TRUE;
  return FALSE;
}
PermSet::PermSet()
: Presentation(Simple)
{
  int i,j,k;
  if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];

    // Set perm and revperm

    // setting up combination of permutations
    // for different scalarset
    int carry;
    size = 1;
    count = 1;
    for (i=0; i<1; i++)
      {
        carry = 1;
        in[i]= TRUE;
    }
  }
  else
  {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];
  in[0] = TRUE;
  }
}
void PermSet::ResetToSimple()
{
  int i;

  AlreadyOnlyOneRemain = FALSE;
  Presentation = Simple;
}
void PermSet::ResetToExplicit()
{
  for (int i=0; i<1; i++) in[i] = TRUE;
  Presentation = Explicit;
}
void PermSet::SimpleToExplicit()
{
  int i,j,k;
  int start, class_size;

  // Setup range for mapping

  // To be In or not to be

  // setup explicit representation 
  // Set perm and revperm
  for (i=0; i<1; i++)
    {
      in[i] = TRUE;
    }
  Presentation = Explicit;
  if (args->test_parameter1.value==0) Print_in_size();
}
void PermSet::SimpleToOne()
{
  int i,j,k;
  int class_size;
  int start;


  // Setup range for mapping
  Presentation = Explicit;
}
bool PermSet::NextPermutation()
{
  bool nexted = FALSE;
  int start, end; 
  int class_size;
  int temp;
  int j,k;

  // algorithm
  // for each class
  //   if forall in the same class reverse_sorted, 
  //     { sort again; goto next class }
  //   else
  //     {
  //       nexted = TRUE;
  //       for (j from l to r)
  // 	       if (for all j+ are reversed sorted)
  // 	         {
  // 	           swap j, j+1
  // 	           sort all j+ again
  // 	           break;
  // 	         }
  //     }
if (!nexted) return FALSE;
  return TRUE;
}

/********************
 Symmetry Class
 ********************/
class SymmetryClass
{
  PermSet Perm;
  bool BestInitialized;
  state BestPermutedState;

  // utilities
  void SetBestResult(int i, state* temp);
  void ResetBestResult() {BestInitialized = FALSE;};

public:
  // initializer
  SymmetryClass() : Perm(), BestInitialized(FALSE) {};
  ~SymmetryClass() {};

  void Normalize(state* s);

  void Exhaustive_Fast_Canonicalize(state *s);
  void Heuristic_Fast_Canonicalize(state *s);
  void Heuristic_Small_Mem_Canonicalize(state *s);
  void Heuristic_Fast_Normalize(state *s);

  void MultisetSort(state* s);
};


/********************
 Symmetry Class Members
 ********************/
void SymmetryClass::MultisetSort(state* s)
{
        mu_reward.MultisetSort();
        mu_down_clock_started.MultisetSort();
        mu_up_clock_started.MultisetSort();
        mu_left_clock_started.MultisetSort();
        mu_right_clock_started.MultisetSort();
        mu_pos_y.MultisetSort();
        mu_ob_y.MultisetSort();
        mu_TIME.MultisetSort();
        mu_g_n.MultisetSort();
        mu_all_event_true.MultisetSort();
        mu_h_n.MultisetSort();
        mu_f_n.MultisetSort();
        mu_ob_x.MultisetSort();
        mu_pos_x.MultisetSort();
        mu_running_time.MultisetSort();
        mu_right_clock.MultisetSort();
        mu_left_clock.MultisetSort();
        mu_up_clock.MultisetSort();
        mu_down_clock.MultisetSort();
        mu_mission_completed.MultisetSort();
}
void SymmetryClass::Normalize(state* s)
{
  switch (args->sym_alg.mode) {
  case argsym_alg::Exhaustive_Fast_Canonicalize:
    Exhaustive_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Canonicalize:
    Heuristic_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Small_Mem_Canonicalize:
    Heuristic_Small_Mem_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Normalize:
    Heuristic_Fast_Normalize(s);
    break;
  default:
    Heuristic_Fast_Canonicalize(s);
  }
}

/********************
 Permute and Canonicalize function for different types
 ********************/
void mu_1_real_type::Permute(PermSet& Perm, int i) {};
void mu_1_real_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_real_type::Canonicalize(PermSet& Perm) {};
void mu_1_real_type::SimpleLimit(PermSet& Perm) {};
void mu_1_real_type::ArrayLimit(PermSet& Perm) {};
void mu_1_real_type::Limit(PermSet& Perm) {};
void mu_1_real_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };
void mu_1_integer::Permute(PermSet& Perm, int i) {};
void mu_1_integer::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_integer::Canonicalize(PermSet& Perm) {};
void mu_1_integer::SimpleLimit(PermSet& Perm) {};
void mu_1_integer::ArrayLimit(PermSet& Perm) {};
void mu_1_integer::Limit(PermSet& Perm) {};
void mu_1_integer::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for subrange type.\n"); };
void mu_1_TIME_type::Permute(PermSet& Perm, int i) {};
void mu_1_TIME_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_TIME_type::Canonicalize(PermSet& Perm) {};
void mu_1_TIME_type::SimpleLimit(PermSet& Perm) {};
void mu_1_TIME_type::ArrayLimit(PermSet& Perm) {};
void mu_1_TIME_type::Limit(PermSet& Perm) {};
void mu_1_TIME_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };

/********************
 Auxiliary function for error trace printing
 ********************/
bool match(state* ns, StatePtr p)
{
  unsigned int i;
  static PermSet Perm;
  static state temp;
  StateCopy(&temp, ns);
  if (args->symmetry_reduction.value)
    {
      if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {
        Perm.ResetToExplicit();
        for (i=0; i<Perm.count; i++)
          if (Perm.In(i))
            {
              if (ns != workingstate)
                  StateCopy(workingstate, ns);
              
              mu_reward.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_reward.MultisetSort();
              mu_down_clock_started.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_down_clock_started.MultisetSort();
              mu_up_clock_started.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_up_clock_started.MultisetSort();
              mu_left_clock_started.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_left_clock_started.MultisetSort();
              mu_right_clock_started.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_right_clock_started.MultisetSort();
              mu_pos_y.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_pos_y.MultisetSort();
              mu_ob_y.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_ob_y.MultisetSort();
              mu_TIME.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_g_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_all_event_true.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_h_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_f_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_ob_x.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_ob_x.MultisetSort();
              mu_pos_x.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_pos_x.MultisetSort();
              mu_running_time.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_running_time.MultisetSort();
              mu_right_clock.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_right_clock.MultisetSort();
              mu_left_clock.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_left_clock.MultisetSort();
              mu_up_clock.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_up_clock.MultisetSort();
              mu_down_clock.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_down_clock.MultisetSort();
              mu_mission_completed.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_mission_completed.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
      else {
        Perm.ResetToSimple();
        Perm.SimpleToOne();
        if (ns != workingstate)
          StateCopy(workingstate, ns);

          mu_reward.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_reward.MultisetSort();
          mu_down_clock_started.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_down_clock_started.MultisetSort();
          mu_up_clock_started.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_up_clock_started.MultisetSort();
          mu_left_clock_started.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_left_clock_started.MultisetSort();
          mu_right_clock_started.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_right_clock_started.MultisetSort();
          mu_pos_y.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_pos_y.MultisetSort();
          mu_ob_y.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_ob_y.MultisetSort();
          mu_TIME.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_TIME.MultisetSort();
          mu_g_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_g_n.MultisetSort();
          mu_all_event_true.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_all_event_true.MultisetSort();
          mu_h_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_h_n.MultisetSort();
          mu_f_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_f_n.MultisetSort();
          mu_ob_x.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_ob_x.MultisetSort();
          mu_pos_x.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_pos_x.MultisetSort();
          mu_running_time.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_running_time.MultisetSort();
          mu_right_clock.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_right_clock.MultisetSort();
          mu_left_clock.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_left_clock.MultisetSort();
          mu_up_clock.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_up_clock.MultisetSort();
          mu_down_clock.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_down_clock.MultisetSort();
          mu_mission_completed.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_mission_completed.MultisetSort();
        if (p.compare(workingstate)) {
          StateCopy(workingstate,&temp); return TRUE; }

        while (Perm.NextPermutation())
          {
            if (ns != workingstate)
              StateCopy(workingstate, ns);
              
              mu_reward.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_reward.MultisetSort();
              mu_down_clock_started.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_down_clock_started.MultisetSort();
              mu_up_clock_started.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_up_clock_started.MultisetSort();
              mu_left_clock_started.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_left_clock_started.MultisetSort();
              mu_right_clock_started.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_right_clock_started.MultisetSort();
              mu_pos_y.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_pos_y.MultisetSort();
              mu_ob_y.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_ob_y.MultisetSort();
              mu_TIME.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_g_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_all_event_true.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_h_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_f_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_ob_x.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_ob_x.MultisetSort();
              mu_pos_x.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_pos_x.MultisetSort();
              mu_running_time.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_running_time.MultisetSort();
              mu_right_clock.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_right_clock.MultisetSort();
              mu_left_clock.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_left_clock.MultisetSort();
              mu_up_clock.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_up_clock.MultisetSort();
              mu_down_clock.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_down_clock.MultisetSort();
              mu_mission_completed.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_mission_completed.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
    }
  if (!args->symmetry_reduction.value
      && args->multiset_reduction.value)
    {
      if (ns != workingstate)
          StateCopy(workingstate, ns);
      mu_reward.MultisetSort();
      mu_down_clock_started.MultisetSort();
      mu_up_clock_started.MultisetSort();
      mu_left_clock_started.MultisetSort();
      mu_right_clock_started.MultisetSort();
      mu_pos_y.MultisetSort();
      mu_ob_y.MultisetSort();
      mu_TIME.MultisetSort();
      mu_g_n.MultisetSort();
      mu_all_event_true.MultisetSort();
      mu_h_n.MultisetSort();
      mu_f_n.MultisetSort();
      mu_ob_x.MultisetSort();
      mu_pos_x.MultisetSort();
      mu_running_time.MultisetSort();
      mu_right_clock.MultisetSort();
      mu_left_clock.MultisetSort();
      mu_up_clock.MultisetSort();
      mu_down_clock.MultisetSort();
      mu_mission_completed.MultisetSort();
      if (p.compare(workingstate)) {
        StateCopy(workingstate,&temp); return TRUE; }
      StateCopy(workingstate,&temp);
      return FALSE;
    }
  return (p.compare(ns));
}

/********************
 Canonicalization by fast exhaustive generation of
 all permutations
 ********************/
void SymmetryClass::Exhaustive_Fast_Canonicalize(state* s)
{
  unsigned int i;
  static state temp;
  Perm.ResetToExplicit();

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_reward.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_reward.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_down_clock_started.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_down_clock_started.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_up_clock_started.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_up_clock_started.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_left_clock_started.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_left_clock_started.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_right_clock_started.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_right_clock_started.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_pos_y.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_pos_y.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_ob_y.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_ob_y.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_TIME.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_TIME.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_g_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_g_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_all_event_true.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_all_event_true.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_h_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_h_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_f_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_f_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_ob_x.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_ob_x.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_pos_x.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_pos_x.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_running_time.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_running_time.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_right_clock.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_right_clock.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_left_clock.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_left_clock.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_up_clock.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_up_clock.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_down_clock.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_down_clock.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_mission_completed.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_mission_completed.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 ********************/
void SymmetryClass::Heuristic_Fast_Canonicalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 and use less local memory
 ********************/
void SymmetryClass::Heuristic_Small_Mem_Canonicalize(state* s)
{
  unsigned long cycle;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Normalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and for all other variables, pick any remaining permutation
 ********************/
void SymmetryClass::Heuristic_Fast_Normalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
  Include
 ********************/
#include "upm_epilog.hpp"
