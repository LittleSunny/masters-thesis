(define (domain domain1)

(:requirements :typing :durative-actions :fluents :time
:negative-preconditions :timed-initial-literals :conditional-effects)


(:predicates (reward) (mission_completed)
             
)

(:functions (pos_x) (pos_y) (ob_x) (ob_y) (goal_x) (goal_y) (reward_x) (reward_y) (running_time)

)

;; right now just to keep track of time to change obstacle
(:process grid
  :parameters()
  :precondition (and (not (mission_completed)))
  :effect (and  (increase (running_time) (* #t 1))
)
)

;; collect reward if at (reward_x, reward_y)
(:event getreward
:parameters ()
:precondition (and (not (reward))(= (pos_x) (reward_x) ) (= (pos_y) (reward_y) ) )
:effect (and (reward))
) 

;; to generate obstacles
(:event generate_obstacle
:parameters ()
:precondition (and (not (mission_completed)) (= (running_time) 2))
:effect (and (assign (ob_x) 1) (assign (ob_y) 3 ) )
) 


(:action right
	:parameters ()
	:precondition (and (not (= (pos_y) (ob_y)))  (not(= (ob_x) (+ (pos_x) 1)))(not (mission_completed)) )                        
        :effect (and (assign (pos_x) (+ (pos_x) 1))
)
)



(:action left
	:parameters ()
	:precondition (and (not (= (pos_y) (ob_y)))  (not(= (ob_x) (- (pos_x) 1))) (not (mission_completed)) )               
	:effect (and (assign (pos_x) (- (pos_x) 1))
)
)


(:action up
	:parameters ()
	:precondition (and (not (= (pos_x) (ob_x)))  (not(= (ob_y) (+ (pos_y) 1))) (not (mission_completed)) )
	:effect (and (assign (pos_y) (+ (pos_y) 1)) 
)
)

(:action down
	:parameters ()
	:precondition (and (not (= (pos_x) (ob_x)))  (not(= (ob_y) (- (pos_y) 1))) (not (mission_completed)) ) 
	:effect (and (assign (pos_y) (- (pos_y) 1)) 
)
)



(:action stop
	:parameters ()
	:precondition (and (= (pos_x) (goal_x) ) (= (pos_y) (goal_y) ) (reward))
	:effect (mission_completed)
)

)
