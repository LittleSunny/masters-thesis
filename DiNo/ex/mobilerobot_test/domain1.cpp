/******************************
  Program "domain1.m" compiled by "DiNo Release 1.1"

  DiNo Last Compiled date: "Mar 13 2018"
 ******************************/

/********************
  Parameter
 ********************/
#define DINO_VERSION "DiNo Release 1.1"
#define MURPHI_DATE "Mar 13 2018"
#define PROTOCOL_NAME "domain1"
#define DOMAIN_FILENAME "domain1.pddl"
#define PROBLEM_FILENAME "problem1.pddl"
#define DISCRETIZATION 1.000000
#define VAL_PATHNAME "/home/sunny/catkin_ws/DiNo/src/DiNo/../VAL-master/validate"
#define BITS_IN_WORLD 383
#define HASHC
#define HAS_CLOCK
const char * const modelmessages[] = { " Time Discretisation = 1.000000"," Digits for representing the integer part of a real =  5.000000"," Digits for representing the fractional part of a real =  4" };
const int modelmessagecount = 3;

/********************
  Include
 ********************/
#include "upm_prolog.hpp"

/********************
  Decl declaration
 ********************/

class mu_1_real_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_real_type& val) { return mu__real::operator=((double) val); };
  mu_1_real_type (const char *name, int os): mu__real(10,4,56,name, os) {};
  mu_1_real_type (void): mu__real(10,4,56) {};
  mu_1_real_type (double val): mu__real(10,4,56,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_real_type mu_1_real_type_undefined_var;

class mu_1_integer: public mu__long
{
 public:
  inline int operator=(int val) { return mu__long::operator=(val); };
  inline int operator=(const mu_1_integer& val) { return mu__long::operator=((int) val); };
  mu_1_integer (const char *name, int os): mu__long(-1000, 1000, 11, name, os) {};
  mu_1_integer (void): mu__long(-1000, 1000, 11) {};
  mu_1_integer (int val): mu__long(-1000, 1000, 11, "Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%d",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of subrange decl ***/
mu_1_integer mu_1_integer_undefined_var;

class mu_1_TIME_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_TIME_type& val) { return mu__real::operator=((double) val); };
  mu_1_TIME_type (const char *name, int os): mu__real(12,7,64,name, os) {};
  mu_1_TIME_type (void): mu__real(12,7,64) {};
  mu_1_TIME_type (double val): mu__real(12,7,64,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_TIME_type mu_1_TIME_type_undefined_var;

const double mu_T = +1.000000e+00;
const double mu_goal_x = +2.000000e+00;
const double mu_goal_y = +2.000000e+00;
const double mu_reward_x = +4.000000e+00;
const double mu_reward_y = +4.000000e+00;
/*** Variable declaration ***/
mu_0_boolean mu_all_event_true("all_event_true",0);

/*** Variable declaration ***/
mu_1_integer mu_h_n("h_n",2);

/*** Variable declaration ***/
mu_1_integer mu_g_n("g_n",13);

/*** Variable declaration ***/
mu_1_integer mu_f_n("f_n",24);

/*** Variable declaration ***/
mu_1_TIME_type mu_TIME("TIME",35);

/*** Variable declaration ***/
mu_1_real_type mu_ob_x("ob_x",99);

/*** Variable declaration ***/
mu_1_real_type mu_ob_y("ob_y",155);

/*** Variable declaration ***/
mu_1_real_type mu_pos_x("pos_x",211);

/*** Variable declaration ***/
mu_1_real_type mu_pos_y("pos_y",267);

/*** Variable declaration ***/
mu_1_real_type mu_running_time("running_time",323);

/*** Variable declaration ***/
mu_0_boolean mu_reward("reward",379);

/*** Variable declaration ***/
mu_0_boolean mu_mission_completed("mission_completed",381);


#include "domain1.h"

void mu_set_reward(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_reward.undefine();
else
  mu_reward = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_reward()
{
return mu_reward;
	Error.Error("The end of function get_reward reached without returning values.");
};
/*** end function declaration ***/

void mu_set_mission_completed(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_mission_completed.undefine();
else
  mu_mission_completed = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_mission_completed()
{
return mu_mission_completed;
	Error.Error("The end of function get_mission_completed reached without returning values.");
};
/*** end function declaration ***/

void mu_process_grid()
{
if ( !(mu_mission_completed) )
{
mu_running_time = increase_running_time_process_grid( mu_running_time, (double)mu_T );
}
};
/*** end procedure declaration ***/

mu_0_boolean mu_getreward()
{
bool mu__boolexpr0;
bool mu__boolexpr1;
  if (!(!(mu_reward))) mu__boolexpr1 = FALSE ;
  else {
  mu__boolexpr1 = ((mu_pos_x) == (mu_reward_x)) ; 
}
  if (!(mu__boolexpr1)) mu__boolexpr0 = FALSE ;
  else {
  mu__boolexpr0 = ((mu_pos_y) == (mu_reward_y)) ; 
}
if ( mu__boolexpr0 )
{
mu_reward = mu_true;
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function getreward reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_generate_obstacle()
{
bool mu__boolexpr2;
  if (!(!(mu_mission_completed))) mu__boolexpr2 = FALSE ;
  else {
  mu__boolexpr2 = ((mu_running_time) == (2.000000e+00)) ; 
}
if ( mu__boolexpr2 )
{
mu_ob_x = assign_ob_x_event_generate_obstacle(  );
mu_ob_y = assign_ob_y_event_generate_obstacle(  );
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function generate_obstacle reached without returning values.");
};
/*** end function declaration ***/

void mu_event_check()
{
/*** Variable declaration ***/
mu_0_boolean mu_event_triggered("event_triggered",0);

/*** Variable declaration ***/
mu_0_boolean mu_getreward_triggered("getreward_triggered",2);

/*** Variable declaration ***/
mu_0_boolean mu_generate_obstacle_triggered("generate_obstacle_triggered",4);

mu_event_triggered = mu_true;
mu_getreward_triggered = mu_false;
mu_generate_obstacle_triggered = mu_false;
{
  bool mu__while_expr_4;  mu__while_expr_4 = mu_event_triggered;
int mu__counter_3 = 0;
while (mu__while_expr_4) {
if ( ++mu__counter_3 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
mu_event_triggered = mu_false;
if ( !(mu_getreward_triggered) )
{
mu_getreward_triggered = mu_getreward(  );
bool mu__boolexpr5;
  if (mu_event_triggered) mu__boolexpr5 = TRUE ;
  else {
  mu__boolexpr5 = (mu_getreward_triggered) ; 
}
mu_event_triggered = mu__boolexpr5;
}
if ( !(mu_generate_obstacle_triggered) )
{
mu_generate_obstacle_triggered = mu_generate_obstacle(  );
bool mu__boolexpr6;
  if (mu_event_triggered) mu__boolexpr6 = TRUE ;
  else {
  mu__boolexpr6 = (mu_generate_obstacle_triggered) ; 
}
mu_event_triggered = mu__boolexpr6;
}
};
mu__while_expr_4 = mu_event_triggered;
}
};
};
/*** end procedure declaration ***/

mu_0_boolean mu_DAs_violate_duration()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_duration_violated("DA_duration_violated",0);

mu_DA_duration_violated = mu_false;
return mu_DA_duration_violated;
	Error.Error("The end of function DAs_violate_duration reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_DAs_ongoing_in_goal_state()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_still_ongoing("DA_still_ongoing",0);

mu_DA_still_ongoing = mu_false;
return mu_DA_still_ongoing;
	Error.Error("The end of function DAs_ongoing_in_goal_state reached without returning values.");
};
/*** end function declaration ***/

void mu_apply_continuous_change()
{
/*** Variable declaration ***/
mu_0_boolean mu_process_updated("process_updated",0);

/*** Variable declaration ***/
mu_0_boolean mu_end_while("end_while",2);

/*** Variable declaration ***/
mu_0_boolean mu_process_grid_enabled("process_grid_enabled",4);

mu_process_updated = mu_false;
mu_end_while = mu_false;
mu_process_grid_enabled = mu_false;
{
  bool mu__while_expr_8;  mu__while_expr_8 = !(mu_end_while);
int mu__counter_7 = 0;
while (mu__while_expr_8) {
if ( ++mu__counter_7 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
bool mu__boolexpr9;
  if (!(!(mu_mission_completed))) mu__boolexpr9 = FALSE ;
  else {
  mu__boolexpr9 = (!(mu_process_grid_enabled)) ; 
}
if ( mu__boolexpr9 )
{
mu_process_updated = mu_true;
mu_process_grid (  );
mu_process_grid_enabled = mu_true;
}
if ( !(mu_process_updated) )
{
mu_end_while = mu_true;
}
else
{
mu_process_updated = mu_false;
}
};
mu__while_expr_8 = !(mu_end_while);
}
};
};
/*** end procedure declaration ***/





/********************
  The world
 ********************/
void world_class::clear()
{
  mu_all_event_true.clear();
  mu_h_n.clear();
  mu_g_n.clear();
  mu_f_n.clear();
  mu_TIME.clear();
  mu_ob_x.clear();
  mu_ob_y.clear();
  mu_pos_x.clear();
  mu_pos_y.clear();
  mu_running_time.clear();
  mu_reward.clear();
  mu_mission_completed.clear();
}
void world_class::undefine()
{
  mu_all_event_true.undefine();
  mu_h_n.undefine();
  mu_g_n.undefine();
  mu_f_n.undefine();
  mu_TIME.undefine();
  mu_ob_x.undefine();
  mu_ob_y.undefine();
  mu_pos_x.undefine();
  mu_pos_y.undefine();
  mu_running_time.undefine();
  mu_reward.undefine();
  mu_mission_completed.undefine();
}
void world_class::reset()
{
  mu_all_event_true.reset();
  mu_h_n.reset();
  mu_g_n.reset();
  mu_f_n.reset();
  mu_TIME.reset();
  mu_ob_x.reset();
  mu_ob_y.reset();
  mu_pos_x.reset();
  mu_pos_y.reset();
  mu_running_time.reset();
  mu_reward.reset();
  mu_mission_completed.reset();
}
std::vector<mu_0_boolean*> world_class::get_mu_bools()
{
	  std::vector<mu_0_boolean*> awesome;

      awesome.push_back(&(mu_all_event_true));
      awesome.push_back(&(mu_reward));
      awesome.push_back(&(mu_mission_completed));
    return awesome; 
}
std::vector<mu_0_boolean*> world_class::get_mu_bool_arrays()
{
	  std::vector<mu_0_boolean*> var_arrays;
   std::vector<mu_0_boolean*> interm;

    return var_arrays; 
}
std::vector<mu__real*> world_class::get_mu_nums()
{
	  std::vector<mu__real*> awesome;

      awesome.push_back(&(mu_ob_x));
      awesome.push_back(&(mu_ob_y));
      awesome.push_back(&(mu_pos_x));
      awesome.push_back(&(mu_pos_y));
      awesome.push_back(&(mu_running_time));
    return awesome; 
}
std::vector<mu__real*> world_class::get_mu_num_arrays()
{
	  std::vector<mu__real*> var_arrays;
   std::vector<mu__real*> interm;

    return var_arrays; 
}
//WP WP WP WP WP
double world_class::get_f_val()
{
  double f_val = mu_f_n.value();
  return f_val;
}

//WP WP WP WP WP
void world_class::fire_processes()
{
			mu_process_grid();

}

//WP WP WP WP WP
void world_class::fire_processes_plus()
{
{



 if (!(mu_mission_completed)) 


	{
		mu_running_time = increase_running_time_process_grid( mu_running_time, (double)mu_T ); 
	}


}
}

//WP WP WP WP WP
void world_class::fire_processes_minus()
{
{



 if (!(mu_mission_completed)) 


	{
	}


}
}

//WP WP WP WP WP
void world_class::set_f_val()
{
  double f_val = mu_g_n.value() + mu_h_n.value();
  mu_f_n.value(f_val);
}

//WP WP WP WP WP
double world_class::get_h_val()
{
  double h_val = mu_h_n.value();
  return h_val;
}

//WP WP WP WP WP
void world_class::set_h_val()
{
  //	NON-HEURISTIC SEARCH
  // double h_val = 0; 

  //	FF RPG
  //upm_rpg::getInstance().clear_all();
  //double h_val = upm_rpg::getInstance().compute_rpg();


  //	NUMERIC RPG
  //upm_numeric_rpg::getInstance().clear_all();
  //double h_val = upm_numeric_rpg::getInstance().compute_rpg();

  //	TEMPORAL RPG
  upm_staged_rpg::getInstance().clear_all();
  double h_val = upm_staged_rpg::getInstance().compute_rpg();

  mu_h_n.value(h_val);
}

//WP WP WP WP WP
void world_class::set_h_val(int hp)
{
  double h_val = hp; 
  mu_h_n.value(h_val);
}

//WP WP WP WP WP
double world_class::get_g_val()
{
  double g_val = mu_g_n.value();
  return g_val;
}

//WP WP WP WP WP
void world_class::set_g_val(double g_val)
{
  mu_g_n.value(g_val);
}

void world_class::print(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print(target, separator);
  mu_h_n.print(target, separator);
  mu_g_n.print(target, separator);
  mu_f_n.print(target, separator);
  mu_TIME.print(target, separator);
  mu_ob_x.print(target, separator);
  mu_ob_y.print(target, separator);
  mu_pos_x.print(target, separator);
  mu_pos_y.print(target, separator);
  mu_running_time.print(target, separator);
  mu_reward.print(target, separator);
  mu_mission_completed.print(target, separator);
    num_calls--;
}
}
void world_class::pddlprint(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_TIME.print(target, separator);
  mu_ob_x.print(target, separator);
  mu_ob_y.print(target, separator);
  mu_pos_x.print(target, separator);
  mu_pos_y.print(target, separator);
  mu_running_time.print(target, separator);
  mu_reward.print(target, separator);
  mu_mission_completed.print(target, separator);
    num_calls--;
}
}
double world_class::get_clock_value()
{
  return mu_TIME.value();
}
void world_class::print_statistic()
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print_statistic();
  mu_h_n.print_statistic();
  mu_g_n.print_statistic();
  mu_f_n.print_statistic();
  mu_TIME.print_statistic();
  mu_ob_x.print_statistic();
  mu_ob_y.print_statistic();
  mu_pos_x.print_statistic();
  mu_pos_y.print_statistic();
  mu_running_time.print_statistic();
  mu_reward.print_statistic();
  mu_mission_completed.print_statistic();
    num_calls--;
}
}
void world_class::print_diff(state *prevstate, FILE *target, const char *separator)
{
  if ( prevstate != NULL )
  {
    mu_all_event_true.print_diff(prevstate,target,separator);
    mu_h_n.print_diff(prevstate,target,separator);
    mu_g_n.print_diff(prevstate,target,separator);
    mu_f_n.print_diff(prevstate,target,separator);
    mu_TIME.print_diff(prevstate,target,separator);
    mu_ob_x.print_diff(prevstate,target,separator);
    mu_ob_y.print_diff(prevstate,target,separator);
    mu_pos_x.print_diff(prevstate,target,separator);
    mu_pos_y.print_diff(prevstate,target,separator);
    mu_running_time.print_diff(prevstate,target,separator);
    mu_reward.print_diff(prevstate,target,separator);
    mu_mission_completed.print_diff(prevstate,target,separator);
  }
  else
print(target,separator);
}
void world_class::to_state(state *newstate)
{
  mu_all_event_true.to_state( newstate );
  mu_h_n.to_state( newstate );
  mu_g_n.to_state( newstate );
  mu_f_n.to_state( newstate );
  mu_TIME.to_state( newstate );
  mu_ob_x.to_state( newstate );
  mu_ob_y.to_state( newstate );
  mu_pos_x.to_state( newstate );
  mu_pos_y.to_state( newstate );
  mu_running_time.to_state( newstate );
  mu_reward.to_state( newstate );
  mu_mission_completed.to_state( newstate );
}
void world_class::setstate(state *thestate)
{
}


/********************
  Rule declarations
 ********************/
/******************** RuleBase0 ********************/
class RuleBase0
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" time passing ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
    return mu_true;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;



    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;



    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;



    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;



    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_TIME));  // (mu_TIME) + (mu_T) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_TIME)); //  (mu_TIME) + (mu_T) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 0;
    while (what_rule < 1 )
      {
	if ( ( TRUE  ) ) {
	      if (mu_true) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 0;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_TIME = (mu_TIME) + (mu_T);
mu_event_check (  );
mu_apply_continuous_change (  );
mu_event_check (  );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_TIME = (mu_TIME) + (mu_T);


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 1;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return Duration(r);
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( time passing )");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Clock;
  };

};
/******************** RuleBase1 ********************/
class RuleBase1
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" stop ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr10;
bool mu__boolexpr11;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr11 = FALSE ;
  else {
  mu__boolexpr11 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr11)) mu__boolexpr10 = FALSE ;
  else {
  mu__boolexpr10 = (mu_reward) ; 
}
    return mu__boolexpr10;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr12;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr12 = FALSE ;
  else {
  mu__boolexpr12 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr13;
bool mu__boolexpr14;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr14 = FALSE ;
  else {
  mu__boolexpr14 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr14)) mu__boolexpr13 = FALSE ;
  else {
  mu__boolexpr13 = (mu_reward) ; 
}
bool mu__boolexpr15;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr15 = FALSE ;
  else {
  mu__boolexpr15 = ((mu_pos_y) == (mu_goal_y)) ; 
}

 		if (std::string(typeid(mu_reward).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_reward)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr16;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr16 = FALSE ;
  else {
  mu__boolexpr16 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr17;
bool mu__boolexpr18;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr18 = FALSE ;
  else {
  mu__boolexpr18 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr18)) mu__boolexpr17 = FALSE ;
  else {
  mu__boolexpr17 = (mu_reward) ; 
}
bool mu__boolexpr19;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr19 = FALSE ;
  else {
  mu__boolexpr19 = ((mu_pos_y) == (mu_goal_y)) ; 
}

 	if (std::string(typeid(mu_pos_x).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_pos_x), std::make_pair(mu_goal_x, 0))); 
	} 
 	if (std::string(typeid(mu_pos_y).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_pos_y), std::make_pair(mu_goal_y, 0))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_pos_x).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_pos_x)); 
 	if (std::string(typeid(mu_pos_y).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_pos_y)); 
 		if (std::string(typeid(mu_reward).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_reward)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr20;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr20 = FALSE ;
  else {
  mu__boolexpr20 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr21;
bool mu__boolexpr22;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr22 = FALSE ;
  else {
  mu__boolexpr22 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr22)) mu__boolexpr21 = FALSE ;
  else {
  mu__boolexpr21 = (mu_reward) ; 
}
bool mu__boolexpr23;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr23 = FALSE ;
  else {
  mu__boolexpr23 = ((mu_pos_y) == (mu_goal_y)) ; 
}

 		if (std::string(typeid(mu_reward).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_reward), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr24;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr24 = FALSE ;
  else {
  mu__boolexpr24 = ((mu_pos_y) == (mu_goal_y)) ; 
}
bool mu__boolexpr25;
bool mu__boolexpr26;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr26 = FALSE ;
  else {
  mu__boolexpr26 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr26)) mu__boolexpr25 = FALSE ;
  else {
  mu__boolexpr25 = (mu_reward) ; 
}
bool mu__boolexpr27;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr27 = FALSE ;
  else {
  mu__boolexpr27 = ((mu_pos_y) == (mu_goal_y)) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;



    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;


    aeffs.push_back(&(mu_mission_completed)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_mission_completed), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_mission_completed)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 1;
    while (what_rule < 2 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr28;
bool mu__boolexpr29;
  if (!((mu_pos_x) == (mu_goal_x))) mu__boolexpr29 = FALSE ;
  else {
  mu__boolexpr29 = ((mu_pos_y) == (mu_goal_y)) ; 
}
  if (!(mu__boolexpr29)) mu__boolexpr28 = FALSE ;
  else {
  mu__boolexpr28 = (mu_reward) ; 
}
	      if (mu__boolexpr28) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 1;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_mission_completed = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {



mu_mission_completed = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_mission_completed = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_mission_completed = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( stop)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase2 ********************/
class RuleBase2
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" down ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr30;
bool mu__boolexpr31;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr31 = FALSE ;
  else {
  mu__boolexpr31 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr31)) mu__boolexpr30 = FALSE ;
  else {
  mu__boolexpr30 = (!(mu_mission_completed)) ; 
}
    return mu__boolexpr30;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr32;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr32 = FALSE ;
  else {
  mu__boolexpr32 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
bool mu__boolexpr33;
bool mu__boolexpr34;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr34 = FALSE ;
  else {
  mu__boolexpr34 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr34)) mu__boolexpr33 = FALSE ;
  else {
  mu__boolexpr33 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr35;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr35 = FALSE ;
  else {
  mu__boolexpr35 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}


    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr36;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr36 = FALSE ;
  else {
  mu__boolexpr36 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
bool mu__boolexpr37;
bool mu__boolexpr38;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr38 = FALSE ;
  else {
  mu__boolexpr38 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr38)) mu__boolexpr37 = FALSE ;
  else {
  mu__boolexpr37 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr39;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr39 = FALSE ;
  else {
  mu__boolexpr39 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr40;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr40 = FALSE ;
  else {
  mu__boolexpr40 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
bool mu__boolexpr41;
bool mu__boolexpr42;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr42 = FALSE ;
  else {
  mu__boolexpr42 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr42)) mu__boolexpr41 = FALSE ;
  else {
  mu__boolexpr41 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr43;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr43 = FALSE ;
  else {
  mu__boolexpr43 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}

 		if (std::string(typeid(mu_mission_completed).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_mission_completed), 0)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr44;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr44 = FALSE ;
  else {
  mu__boolexpr44 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
bool mu__boolexpr45;
bool mu__boolexpr46;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr46 = FALSE ;
  else {
  mu__boolexpr46 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr46)) mu__boolexpr45 = FALSE ;
  else {
  mu__boolexpr45 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr47;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr47 = FALSE ;
  else {
  mu__boolexpr47 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_pos_y));  // assign_pos_y_action_down( mu_pos_y ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_pos_y)); //  assign_pos_y_action_down( mu_pos_y ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 2;
    while (what_rule < 3 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr48;
bool mu__boolexpr49;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr49 = FALSE ;
  else {
  mu__boolexpr49 = (!((mu_ob_y) == ((mu_pos_y) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr49)) mu__boolexpr48 = FALSE ;
  else {
  mu__boolexpr48 = (!(mu_mission_completed)) ; 
}
	      if (mu__boolexpr48) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 2;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_pos_y = assign_pos_y_action_down( mu_pos_y );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( down)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase3 ********************/
class RuleBase3
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" up ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr50;
bool mu__boolexpr51;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr51 = FALSE ;
  else {
  mu__boolexpr51 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr51)) mu__boolexpr50 = FALSE ;
  else {
  mu__boolexpr50 = (!(mu_mission_completed)) ; 
}
    return mu__boolexpr50;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr52;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr52 = FALSE ;
  else {
  mu__boolexpr52 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
bool mu__boolexpr53;
bool mu__boolexpr54;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr54 = FALSE ;
  else {
  mu__boolexpr54 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr54)) mu__boolexpr53 = FALSE ;
  else {
  mu__boolexpr53 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr55;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr55 = FALSE ;
  else {
  mu__boolexpr55 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}


    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr56;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr56 = FALSE ;
  else {
  mu__boolexpr56 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
bool mu__boolexpr57;
bool mu__boolexpr58;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr58 = FALSE ;
  else {
  mu__boolexpr58 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr58)) mu__boolexpr57 = FALSE ;
  else {
  mu__boolexpr57 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr59;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr59 = FALSE ;
  else {
  mu__boolexpr59 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr60;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr60 = FALSE ;
  else {
  mu__boolexpr60 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
bool mu__boolexpr61;
bool mu__boolexpr62;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr62 = FALSE ;
  else {
  mu__boolexpr62 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr62)) mu__boolexpr61 = FALSE ;
  else {
  mu__boolexpr61 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr63;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr63 = FALSE ;
  else {
  mu__boolexpr63 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}

 		if (std::string(typeid(mu_mission_completed).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_mission_completed), 0)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr64;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr64 = FALSE ;
  else {
  mu__boolexpr64 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
bool mu__boolexpr65;
bool mu__boolexpr66;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr66 = FALSE ;
  else {
  mu__boolexpr66 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr66)) mu__boolexpr65 = FALSE ;
  else {
  mu__boolexpr65 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr67;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr67 = FALSE ;
  else {
  mu__boolexpr67 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_pos_y));  // assign_pos_y_action_up( mu_pos_y ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_pos_y)); //  assign_pos_y_action_up( mu_pos_y ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 3;
    while (what_rule < 4 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr68;
bool mu__boolexpr69;
  if (!(!((mu_pos_x) == (mu_ob_x)))) mu__boolexpr69 = FALSE ;
  else {
  mu__boolexpr69 = (!((mu_ob_y) == ((mu_pos_y) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr69)) mu__boolexpr68 = FALSE ;
  else {
  mu__boolexpr68 = (!(mu_mission_completed)) ; 
}
	      if (mu__boolexpr68) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 3;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_pos_y = assign_pos_y_action_up( mu_pos_y );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( up)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase4 ********************/
class RuleBase4
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" left ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr70;
bool mu__boolexpr71;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr71 = FALSE ;
  else {
  mu__boolexpr71 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr71)) mu__boolexpr70 = FALSE ;
  else {
  mu__boolexpr70 = (!(mu_mission_completed)) ; 
}
    return mu__boolexpr70;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr72;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr72 = FALSE ;
  else {
  mu__boolexpr72 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
bool mu__boolexpr73;
bool mu__boolexpr74;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr74 = FALSE ;
  else {
  mu__boolexpr74 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr74)) mu__boolexpr73 = FALSE ;
  else {
  mu__boolexpr73 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr75;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr75 = FALSE ;
  else {
  mu__boolexpr75 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}


    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr76;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr76 = FALSE ;
  else {
  mu__boolexpr76 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
bool mu__boolexpr77;
bool mu__boolexpr78;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr78 = FALSE ;
  else {
  mu__boolexpr78 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr78)) mu__boolexpr77 = FALSE ;
  else {
  mu__boolexpr77 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr79;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr79 = FALSE ;
  else {
  mu__boolexpr79 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr80;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr80 = FALSE ;
  else {
  mu__boolexpr80 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
bool mu__boolexpr81;
bool mu__boolexpr82;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr82 = FALSE ;
  else {
  mu__boolexpr82 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr82)) mu__boolexpr81 = FALSE ;
  else {
  mu__boolexpr81 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr83;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr83 = FALSE ;
  else {
  mu__boolexpr83 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}

 		if (std::string(typeid(mu_mission_completed).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_mission_completed), 0)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr84;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr84 = FALSE ;
  else {
  mu__boolexpr84 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
bool mu__boolexpr85;
bool mu__boolexpr86;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr86 = FALSE ;
  else {
  mu__boolexpr86 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr86)) mu__boolexpr85 = FALSE ;
  else {
  mu__boolexpr85 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr87;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr87 = FALSE ;
  else {
  mu__boolexpr87 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_pos_x));  // assign_pos_x_action_left( mu_pos_x ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_pos_x)); //  assign_pos_x_action_left( mu_pos_x ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 4;
    while (what_rule < 5 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr88;
bool mu__boolexpr89;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr89 = FALSE ;
  else {
  mu__boolexpr89 = (!((mu_ob_x) == ((mu_pos_x) - (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr89)) mu__boolexpr88 = FALSE ;
  else {
  mu__boolexpr88 = (!(mu_mission_completed)) ; 
}
	      if (mu__boolexpr88) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 4;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_pos_x = assign_pos_x_action_left( mu_pos_x );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( left)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase5 ********************/
class RuleBase5
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" right ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr90;
bool mu__boolexpr91;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr91 = FALSE ;
  else {
  mu__boolexpr91 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr91)) mu__boolexpr90 = FALSE ;
  else {
  mu__boolexpr90 = (!(mu_mission_completed)) ; 
}
    return mu__boolexpr90;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr92;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr92 = FALSE ;
  else {
  mu__boolexpr92 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
bool mu__boolexpr93;
bool mu__boolexpr94;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr94 = FALSE ;
  else {
  mu__boolexpr94 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr94)) mu__boolexpr93 = FALSE ;
  else {
  mu__boolexpr93 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr95;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr95 = FALSE ;
  else {
  mu__boolexpr95 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}


    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr96;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr96 = FALSE ;
  else {
  mu__boolexpr96 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
bool mu__boolexpr97;
bool mu__boolexpr98;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr98 = FALSE ;
  else {
  mu__boolexpr98 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr98)) mu__boolexpr97 = FALSE ;
  else {
  mu__boolexpr97 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr99;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr99 = FALSE ;
  else {
  mu__boolexpr99 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}


    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr100;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr100 = FALSE ;
  else {
  mu__boolexpr100 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
bool mu__boolexpr101;
bool mu__boolexpr102;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr102 = FALSE ;
  else {
  mu__boolexpr102 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr102)) mu__boolexpr101 = FALSE ;
  else {
  mu__boolexpr101 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr103;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr103 = FALSE ;
  else {
  mu__boolexpr103 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}

 		if (std::string(typeid(mu_mission_completed).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_mission_completed), 0)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr104;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr104 = FALSE ;
  else {
  mu__boolexpr104 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
bool mu__boolexpr105;
bool mu__boolexpr106;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr106 = FALSE ;
  else {
  mu__boolexpr106 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr106)) mu__boolexpr105 = FALSE ;
  else {
  mu__boolexpr105 = (!(mu_mission_completed)) ; 
}
bool mu__boolexpr107;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr107 = FALSE ;
  else {
  mu__boolexpr107 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_pos_x));  // assign_pos_x_action_right( mu_pos_x ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_pos_x)); //  assign_pos_x_action_right( mu_pos_x ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 5;
    while (what_rule < 6 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr108;
bool mu__boolexpr109;
  if (!(!((mu_pos_y) == (mu_ob_y)))) mu__boolexpr109 = FALSE ;
  else {
  mu__boolexpr109 = (!((mu_ob_x) == ((mu_pos_x) + (1.000000e+00)))) ; 
}
  if (!(mu__boolexpr109)) mu__boolexpr108 = FALSE ;
  else {
  mu__boolexpr108 = (!(mu_mission_completed)) ; 
}
	      if (mu__boolexpr108) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 5;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_pos_x = assign_pos_x_action_right( mu_pos_x );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( right)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
class NextStateGenerator
{
  RuleBase0 R0;
  RuleBase1 R1;
  RuleBase2 R2;
  RuleBase3 R3;
  RuleBase4 R4;
  RuleBase5 R5;
public:
void SetNextEnabledRule(RULE_INDEX_TYPE & what_rule)
{
  category = CONDITION;
  if (what_rule<1)
    { R0.NextRule(what_rule);
      if (what_rule<1) return; }
  if (what_rule>=1 && what_rule<2)
    { R1.NextRule(what_rule);
      if (what_rule<2) return; }
  if (what_rule>=2 && what_rule<3)
    { R2.NextRule(what_rule);
      if (what_rule<3) return; }
  if (what_rule>=3 && what_rule<4)
    { R3.NextRule(what_rule);
      if (what_rule<4) return; }
  if (what_rule>=4 && what_rule<5)
    { R4.NextRule(what_rule);
      if (what_rule<5) return; }
  if (what_rule>=5 && what_rule<6)
    { R5.NextRule(what_rule);
      if (what_rule<6) return; }
}
bool Condition(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.Condition(r-0);
  if (r>=1 && r<=1) return R1.Condition(r-1);
  if (r>=2 && r<=2) return R2.Condition(r-2);
  if (r>=3 && r<=3) return R3.Condition(r-3);
  if (r>=4 && r<=4) return R4.Condition(r-4);
  if (r>=5 && r<=5) return R5.Condition(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking condition for nonexisting rule.");
}
std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.bool_precond_array(r-0);
  if (r>=1 && r<=1) return R1.bool_precond_array(r-1);
  if (r>=2 && r<=2) return R2.bool_precond_array(r-2);
  if (r>=3 && r<=3) return R3.bool_precond_array(r-3);
  if (r>=4 && r<=4) return R4.bool_precond_array(r-4);
  if (r>=5 && r<=5) return R5.bool_precond_array(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::map<mu__real*, std::pair<double,int> > num_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.num_precond_array(r-0);
  if (r>=1 && r<=1) return R1.num_precond_array(r-1);
  if (r>=2 && r<=2) return R2.num_precond_array(r-2);
  if (r>=3 && r<=3) return R3.num_precond_array(r-3);
  if (r>=4 && r<=4) return R4.num_precond_array(r-4);
  if (r>=5 && r<=5) return R5.num_precond_array(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.all_precond_array(r-0);
  if (r>=1 && r<=1) return R1.all_precond_array(r-1);
  if (r>=2 && r<=2) return R2.all_precond_array(r-2);
  if (r>=3 && r<=3) return R3.all_precond_array(r-3);
  if (r>=4 && r<=4) return R4.all_precond_array(r-4);
  if (r>=5 && r<=5) return R5.all_precond_array(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.precond_bool_interference(r-0);
  if (r>=1 && r<=1) return R1.precond_bool_interference(r-1);
  if (r>=2 && r<=2) return R2.precond_bool_interference(r-2);
  if (r>=3 && r<=3) return R3.precond_bool_interference(r-3);
  if (r>=4 && r<=4) return R4.precond_bool_interference(r-4);
  if (r>=5 && r<=5) return R5.precond_bool_interference(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.temporal_constraints(r-0);
  if (r>=1 && r<=1) return R1.temporal_constraints(r-1);
  if (r>=2 && r<=2) return R2.temporal_constraints(r-2);
  if (r>=3 && r<=3) return R3.temporal_constraints(r-3);
  if (r>=4 && r<=4) return R4.temporal_constraints(r-4);
  if (r>=5 && r<=5) return R5.temporal_constraints(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_bool_interference(r-0);
  if (r>=1 && r<=1) return R1.effects_bool_interference(r-1);
  if (r>=2 && r<=2) return R2.effects_bool_interference(r-2);
  if (r>=3 && r<=3) return R3.effects_bool_interference(r-3);
  if (r>=4 && r<=4) return R4.effects_bool_interference(r-4);
  if (r>=5 && r<=5) return R5.effects_bool_interference(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_add_bool_array(r-0);
  if (r>=1 && r<=1) return R1.effects_add_bool_array(r-1);
  if (r>=2 && r<=2) return R2.effects_add_bool_array(r-2);
  if (r>=3 && r<=3) return R3.effects_add_bool_array(r-3);
  if (r>=4 && r<=4) return R4.effects_add_bool_array(r-4);
  if (r>=5 && r<=5) return R5.effects_add_bool_array(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_num_array(r-0);
  if (r>=1 && r<=1) return R1.effects_num_array(r-1);
  if (r>=2 && r<=2) return R2.effects_num_array(r-2);
  if (r>=3 && r<=3) return R3.effects_num_array(r-3);
  if (r>=4 && r<=4) return R4.effects_num_array(r-4);
  if (r>=5 && r<=5) return R5.effects_num_array(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_all_array(r-0);
  if (r>=1 && r<=1) return R1.effects_all_array(r-1);
  if (r>=2 && r<=2) return R2.effects_all_array(r-2);
  if (r>=3 && r<=3) return R3.effects_all_array(r-3);
  if (r>=4 && r<=4) return R4.effects_all_array(r-4);
  if (r>=5 && r<=5) return R5.effects_all_array(r-5);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
void Code(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code(r-5); return; } 
}
void Code_ff(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_ff(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_ff(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_ff(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_ff(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code_ff(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code_ff(r-5); return; } 
}
void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_plus(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_numeric_ff_plus(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_numeric_ff_plus(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_numeric_ff_plus(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code_numeric_ff_plus(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code_numeric_ff_plus(r-5); return; } 
}
void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_minus(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_numeric_ff_minus(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_numeric_ff_minus(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_numeric_ff_minus(r-3); return; } 
  if (r>=4 && r<=4) { R4.Code_numeric_ff_minus(r-4); return; } 
  if (r>=5 && r<=5) { R5.Code_numeric_ff_minus(r-5); return; } 
}
mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_rule_clock_started(r-0); } 
  if (r>=1 && r<=1) { return R1.get_rule_clock_started(r-1); } 
  if (r>=2 && r<=2) { return R2.get_rule_clock_started(r-2); } 
  if (r>=3 && r<=3) { return R3.get_rule_clock_started(r-3); } 
  if (r>=4 && r<=4) { return R4.get_rule_clock_started(r-4); } 
  if (r>=5 && r<=5) { return R5.get_rule_clock_started(r-5); } 
}
std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_clocks(r-0); } 
  if (r>=1 && r<=1) { return R1.get_clocks(r-1); } 
  if (r>=2 && r<=2) { return R2.get_clocks(r-2); } 
  if (r>=3 && r<=3) { return R3.get_clocks(r-3); } 
  if (r>=4 && r<=4) { return R4.get_clocks(r-4); } 
  if (r>=5 && r<=5) { return R5.get_clocks(r-5); } 
}
int Priority(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.Priority(); } 
  if (r>=1 && r<=1) { return R1.Priority(); } 
  if (r>=2 && r<=2) { return R2.Priority(); } 
  if (r>=3 && r<=3) { return R3.Priority(); } 
  if (r>=4 && r<=4) { return R4.Priority(); } 
  if (r>=5 && r<=5) { return R5.Priority(); } 
}
char * Name(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Name(r-0);
  if (r>=1 && r<=1) return R1.Name(r-1);
  if (r>=2 && r<=2) return R2.Name(r-2);
  if (r>=3 && r<=3) return R3.Name(r-3);
  if (r>=4 && r<=4) return R4.Name(r-4);
  if (r>=5 && r<=5) return R5.Name(r-5);
  return NULL;
}
int Duration(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Duration(r-0);
  if (r>=1 && r<=1) return R1.Duration(r-1);
  if (r>=2 && r<=2) return R2.Duration(r-2);
  if (r>=3 && r<=3) return R3.Duration(r-3);
  if (r>=4 && r<=4) return R4.Duration(r-4);
  if (r>=5 && r<=5) return R5.Duration(r-5);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
int Weight(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Weight(r-0);
  if (r>=1 && r<=1) return R1.Weight(r-1);
  if (r>=2 && r<=2) return R2.Weight(r-2);
  if (r>=3 && r<=3) return R3.Weight(r-3);
  if (r>=4 && r<=4) return R4.Weight(r-4);
  if (r>=5 && r<=5) return R5.Weight(r-5);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
 char * PDDLName(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLName(r-0);
  if (r>=1 && r<=1) return R1.PDDLName(r-1);
  if (r>=2 && r<=2) return R2.PDDLName(r-2);
  if (r>=3 && r<=3) return R3.PDDLName(r-3);
  if (r>=4 && r<=4) return R4.PDDLName(r-4);
  if (r>=5 && r<=5) return R5.PDDLName(r-5);
  return NULL;
}
RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLClass(r-0);
  if (r>=1 && r<=1) return R1.PDDLClass(r-1);
  if (r>=2 && r<=2) return R2.PDDLClass(r-2);
  if (r>=3 && r<=3) return R3.PDDLClass(r-3);
  if (r>=4 && r<=4) return R4.PDDLClass(r-4);
  if (r>=5 && r<=5) return R5.PDDLClass(r-5);
Error.Notrace("Internal: NextStateGenerator -- querying PDDL class for nonexisting rule.");
}
};
const RULE_INDEX_TYPE numrules = 6;

/********************
  parameter
 ********************/
#define RULES_IN_WORLD 6


/********************
  Startstate records
 ********************/
/******************** StartStateBase0 ********************/
class StartStateBase0
{
public:
  char * Name(unsigned short r)
  {
    return tsprintf("start");
  }
  void Code(unsigned short r)
  {
mu_TIME = 0.000000e+00;
mu_set_reward ( mu_false );
mu_set_mission_completed ( mu_false );
mu_pos_x = 0.000000e+00;
mu_pos_y = 0.000000e+00;
mu_ob_x = 0.000000e+00;
mu_ob_y = 0.000000e+00;
mu_running_time = 0.000000e+00;
mu_reward = mu_false;
mu_mission_completed = mu_false;
mu_running_time = 0.000000e+00;
mu_pos_x = 0.000000e+00;
mu_pos_y = 0.000000e+00;
mu_ob_x = 1.000000e+00;
mu_ob_y = 0.000000e+00;
mu_all_event_true = mu_true;
mu_g_n = 0;
mu_h_n = 0;
mu_f_n = 0;
  };

};
class StartStateGenerator
{
  StartStateBase0 S0;
public:
void Code(unsigned short r)
{
  if (r<=0) { S0.Code(r-0); return; }
}
char * Name(unsigned short r)
{
  if (r<=0) return S0.Name(r-0);
  return NULL;
}
};
const rulerec startstates[] = {
{ NULL, NULL, NULL, FALSE},
};
unsigned short StartStateManager::numstartstates = 1;

/********************
  Goal records
 ********************/

// WP WP WP GOAL
int mu__goal_110() // Goal "enjoy"
{
bool mu__boolexpr111;
  if (!(mu_mission_completed)) mu__boolexpr111 = FALSE ;
  else {
  mu__boolexpr111 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}
return mu__boolexpr111;
};

  std::set<mu_0_boolean*> get_bool_goal_conditions()
  {
    std::set<mu_0_boolean*> bool_goal_conds;
bool mu__boolexpr112;
  if (!(mu_mission_completed)) mu__boolexpr112 = FALSE ;
  else {
  mu__boolexpr112 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}

 if (std::string(typeid(mu_mission_completed).name()).compare("12mu_0_boolean") == 0)
		bool_goal_conds.insert(&(mu_mission_completed)); 

    return bool_goal_conds;
  }

  std::map<mu__real*, std::pair<double, int> > get_numeric_goal_conditions()
  {
    std::map<mu__real*, std::pair<double, int> > numeric_goal_conds;

    return numeric_goal_conds;
  }

bool mu__condition_113() // Condition for Rule "enjoy"
{
  return mu__goal_110( );
}

bool mu__goal__00(){ return mu__condition_113(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/


// WP WP WP GOAL
const rulerec goals[] = {
{"enjoy", &mu__condition_113, NULL, },
};
const unsigned short numgoals = 1;

/********************
  Metric related stuff
 ********************/
const short metric = -1;

/********************
  Invariant records
 ********************/
int mu__invariant_114() // Invariant "todo bien"
{
bool mu__boolexpr115;
  if (!(mu_all_event_true)) mu__boolexpr115 = FALSE ;
  else {
  mu__boolexpr115 = (!(mu_DAs_violate_duration(  ))) ; 
}
return mu__boolexpr115;
};

bool mu__condition_116() // Condition for Rule "todo bien"
{
  return mu__invariant_114( );
}

bool mu__goal__01(){ return mu__condition_116(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/

const rulerec invariants[] = {
{"todo bien", &mu__condition_116, NULL, },
};
const unsigned short numinvariants = 1;

/********************
  Normal/Canonicalization for scalarset
 ********************/
/*
reward:NoScalarset
pos_y:NoScalarset
ob_y:NoScalarset
TIME:NoScalarset
g_n:NoScalarset
all_event_true:NoScalarset
h_n:NoScalarset
f_n:NoScalarset
ob_x:NoScalarset
pos_x:NoScalarset
running_time:NoScalarset
mission_completed:NoScalarset
*/

/********************
Code for symmetry
 ********************/

/********************
 Permutation Set Class
 ********************/
class PermSet
{
public:
  // book keeping
  enum PresentationType {Simple, Explicit};
  PresentationType Presentation;

  void ResetToSimple();
  void ResetToExplicit();
  void SimpleToExplicit();
  void SimpleToOne();
  bool NextPermutation();

  void Print_in_size()
  { unsigned long ret=0; for (unsigned long i=0; i<count; i++) if (in[i]) ret++; cout << "in_size:" << ret << "\n"; }


  /********************
   Simple and efficient representation
   ********************/
  bool AlreadyOnlyOneRemain;
  bool MoreThanOneRemain();


  /********************
   Explicit representation
  ********************/
  unsigned long size;
  unsigned long count;
  // in will be of product of factorial sizes for fast canonicalize
  // in will be of size 1 for reduced local memory canonicalize
  bool * in;

  // auxiliary for explicit representation

  // in/perm/revperm will be of factorial size for fast canonicalize
  // they will be of size 1 for reduced local memory canonicalize
  // second range will be size of the scalarset
  // procedure for explicit representation
  // General procedure
  PermSet();
  bool In(int i) const { return in[i]; };
  void Add(int i) { for (int j=0; j<i; j++) in[j] = FALSE;};
  void Remove(int i) { in[i] = FALSE; };
};
bool PermSet::MoreThanOneRemain()
{
  int i,j;
  if (AlreadyOnlyOneRemain)
    return FALSE;
  else {
  }
  AlreadyOnlyOneRemain = TRUE;
  return FALSE;
}
PermSet::PermSet()
: Presentation(Simple)
{
  int i,j,k;
  if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];

    // Set perm and revperm

    // setting up combination of permutations
    // for different scalarset
    int carry;
    size = 1;
    count = 1;
    for (i=0; i<1; i++)
      {
        carry = 1;
        in[i]= TRUE;
    }
  }
  else
  {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];
  in[0] = TRUE;
  }
}
void PermSet::ResetToSimple()
{
  int i;

  AlreadyOnlyOneRemain = FALSE;
  Presentation = Simple;
}
void PermSet::ResetToExplicit()
{
  for (int i=0; i<1; i++) in[i] = TRUE;
  Presentation = Explicit;
}
void PermSet::SimpleToExplicit()
{
  int i,j,k;
  int start, class_size;

  // Setup range for mapping

  // To be In or not to be

  // setup explicit representation 
  // Set perm and revperm
  for (i=0; i<1; i++)
    {
      in[i] = TRUE;
    }
  Presentation = Explicit;
  if (args->test_parameter1.value==0) Print_in_size();
}
void PermSet::SimpleToOne()
{
  int i,j,k;
  int class_size;
  int start;


  // Setup range for mapping
  Presentation = Explicit;
}
bool PermSet::NextPermutation()
{
  bool nexted = FALSE;
  int start, end; 
  int class_size;
  int temp;
  int j,k;

  // algorithm
  // for each class
  //   if forall in the same class reverse_sorted, 
  //     { sort again; goto next class }
  //   else
  //     {
  //       nexted = TRUE;
  //       for (j from l to r)
  // 	       if (for all j+ are reversed sorted)
  // 	         {
  // 	           swap j, j+1
  // 	           sort all j+ again
  // 	           break;
  // 	         }
  //     }
if (!nexted) return FALSE;
  return TRUE;
}

/********************
 Symmetry Class
 ********************/
class SymmetryClass
{
  PermSet Perm;
  bool BestInitialized;
  state BestPermutedState;

  // utilities
  void SetBestResult(int i, state* temp);
  void ResetBestResult() {BestInitialized = FALSE;};

public:
  // initializer
  SymmetryClass() : Perm(), BestInitialized(FALSE) {};
  ~SymmetryClass() {};

  void Normalize(state* s);

  void Exhaustive_Fast_Canonicalize(state *s);
  void Heuristic_Fast_Canonicalize(state *s);
  void Heuristic_Small_Mem_Canonicalize(state *s);
  void Heuristic_Fast_Normalize(state *s);

  void MultisetSort(state* s);
};


/********************
 Symmetry Class Members
 ********************/
void SymmetryClass::MultisetSort(state* s)
{
        mu_reward.MultisetSort();
        mu_pos_y.MultisetSort();
        mu_ob_y.MultisetSort();
        mu_TIME.MultisetSort();
        mu_g_n.MultisetSort();
        mu_all_event_true.MultisetSort();
        mu_h_n.MultisetSort();
        mu_f_n.MultisetSort();
        mu_ob_x.MultisetSort();
        mu_pos_x.MultisetSort();
        mu_running_time.MultisetSort();
        mu_mission_completed.MultisetSort();
}
void SymmetryClass::Normalize(state* s)
{
  switch (args->sym_alg.mode) {
  case argsym_alg::Exhaustive_Fast_Canonicalize:
    Exhaustive_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Canonicalize:
    Heuristic_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Small_Mem_Canonicalize:
    Heuristic_Small_Mem_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Normalize:
    Heuristic_Fast_Normalize(s);
    break;
  default:
    Heuristic_Fast_Canonicalize(s);
  }
}

/********************
 Permute and Canonicalize function for different types
 ********************/
void mu_1_real_type::Permute(PermSet& Perm, int i) {};
void mu_1_real_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_real_type::Canonicalize(PermSet& Perm) {};
void mu_1_real_type::SimpleLimit(PermSet& Perm) {};
void mu_1_real_type::ArrayLimit(PermSet& Perm) {};
void mu_1_real_type::Limit(PermSet& Perm) {};
void mu_1_real_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };
void mu_1_integer::Permute(PermSet& Perm, int i) {};
void mu_1_integer::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_integer::Canonicalize(PermSet& Perm) {};
void mu_1_integer::SimpleLimit(PermSet& Perm) {};
void mu_1_integer::ArrayLimit(PermSet& Perm) {};
void mu_1_integer::Limit(PermSet& Perm) {};
void mu_1_integer::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for subrange type.\n"); };
void mu_1_TIME_type::Permute(PermSet& Perm, int i) {};
void mu_1_TIME_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_TIME_type::Canonicalize(PermSet& Perm) {};
void mu_1_TIME_type::SimpleLimit(PermSet& Perm) {};
void mu_1_TIME_type::ArrayLimit(PermSet& Perm) {};
void mu_1_TIME_type::Limit(PermSet& Perm) {};
void mu_1_TIME_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };

/********************
 Auxiliary function for error trace printing
 ********************/
bool match(state* ns, StatePtr p)
{
  unsigned int i;
  static PermSet Perm;
  static state temp;
  StateCopy(&temp, ns);
  if (args->symmetry_reduction.value)
    {
      if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {
        Perm.ResetToExplicit();
        for (i=0; i<Perm.count; i++)
          if (Perm.In(i))
            {
              if (ns != workingstate)
                  StateCopy(workingstate, ns);
              
              mu_reward.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_reward.MultisetSort();
              mu_pos_y.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_pos_y.MultisetSort();
              mu_ob_y.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_ob_y.MultisetSort();
              mu_TIME.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_g_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_all_event_true.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_h_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_f_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_ob_x.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_ob_x.MultisetSort();
              mu_pos_x.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_pos_x.MultisetSort();
              mu_running_time.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_running_time.MultisetSort();
              mu_mission_completed.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_mission_completed.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
      else {
        Perm.ResetToSimple();
        Perm.SimpleToOne();
        if (ns != workingstate)
          StateCopy(workingstate, ns);

          mu_reward.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_reward.MultisetSort();
          mu_pos_y.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_pos_y.MultisetSort();
          mu_ob_y.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_ob_y.MultisetSort();
          mu_TIME.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_TIME.MultisetSort();
          mu_g_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_g_n.MultisetSort();
          mu_all_event_true.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_all_event_true.MultisetSort();
          mu_h_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_h_n.MultisetSort();
          mu_f_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_f_n.MultisetSort();
          mu_ob_x.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_ob_x.MultisetSort();
          mu_pos_x.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_pos_x.MultisetSort();
          mu_running_time.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_running_time.MultisetSort();
          mu_mission_completed.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_mission_completed.MultisetSort();
        if (p.compare(workingstate)) {
          StateCopy(workingstate,&temp); return TRUE; }

        while (Perm.NextPermutation())
          {
            if (ns != workingstate)
              StateCopy(workingstate, ns);
              
              mu_reward.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_reward.MultisetSort();
              mu_pos_y.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_pos_y.MultisetSort();
              mu_ob_y.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_ob_y.MultisetSort();
              mu_TIME.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_g_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_all_event_true.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_h_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_f_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_ob_x.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_ob_x.MultisetSort();
              mu_pos_x.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_pos_x.MultisetSort();
              mu_running_time.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_running_time.MultisetSort();
              mu_mission_completed.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_mission_completed.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
    }
  if (!args->symmetry_reduction.value
      && args->multiset_reduction.value)
    {
      if (ns != workingstate)
          StateCopy(workingstate, ns);
      mu_reward.MultisetSort();
      mu_pos_y.MultisetSort();
      mu_ob_y.MultisetSort();
      mu_TIME.MultisetSort();
      mu_g_n.MultisetSort();
      mu_all_event_true.MultisetSort();
      mu_h_n.MultisetSort();
      mu_f_n.MultisetSort();
      mu_ob_x.MultisetSort();
      mu_pos_x.MultisetSort();
      mu_running_time.MultisetSort();
      mu_mission_completed.MultisetSort();
      if (p.compare(workingstate)) {
        StateCopy(workingstate,&temp); return TRUE; }
      StateCopy(workingstate,&temp);
      return FALSE;
    }
  return (p.compare(ns));
}

/********************
 Canonicalization by fast exhaustive generation of
 all permutations
 ********************/
void SymmetryClass::Exhaustive_Fast_Canonicalize(state* s)
{
  unsigned int i;
  static state temp;
  Perm.ResetToExplicit();

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_reward.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_reward.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_pos_y.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_pos_y.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_ob_y.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_ob_y.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_TIME.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_TIME.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_g_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_g_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_all_event_true.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_all_event_true.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_h_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_h_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_f_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_f_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_ob_x.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_ob_x.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_pos_x.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_pos_x.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_running_time.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_running_time.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_mission_completed.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_mission_completed.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 ********************/
void SymmetryClass::Heuristic_Fast_Canonicalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 and use less local memory
 ********************/
void SymmetryClass::Heuristic_Small_Mem_Canonicalize(state* s)
{
  unsigned long cycle;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Normalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and for all other variables, pick any remaining permutation
 ********************/
void SymmetryClass::Heuristic_Fast_Normalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
  Include
 ********************/
#include "upm_epilog.hpp"
