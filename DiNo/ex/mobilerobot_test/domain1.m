domain: file "domain1.pddl"
problem: file "problem1.pddl"
message: " Time Discretisation = 1.000000"
message: " Digits for representing the integer part of a real =  5.000000"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(10,4);
	integer: -1000..1000;

	 TIME_type: real(12,7);


const 
	 T:1.000000;

	goal_x : 2.00000;
	goal_y : 2.00000;
	reward_x : 4.00000;
	reward_y : 4.00000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	ob_x[pddlname:"ob_x";] :  real_type;
	ob_y[pddlname:"ob_y";] :  real_type;
	pos_x[pddlname:"pos_x";] :  real_type;
	pos_y[pddlname:"pos_y";] :  real_type;
	running_time[pddlname:"running_time";] :  real_type;


	reward[pddlname: "reward";] :  boolean;
	mission_completed[pddlname: "mission_completed";] :  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun increase_running_time_process_grid(running_time : real_type ; T : real_type ; ): real_type "domain1.h" ;
externfun assign_ob_x_event_generate_obstacle(): real_type ;
externfun assign_ob_y_event_generate_obstacle(): real_type ;
externfun assign_pos_x_action_right(pos_x : real_type ; ): real_type ;
externfun assign_pos_x_action_left(pos_x : real_type ; ): real_type ;
externfun assign_pos_y_action_up(pos_y : real_type ; ): real_type ;
externfun assign_pos_y_action_down(pos_y : real_type ; ): real_type ;
procedure set_reward(  value : boolean);
BEGIN
	reward := value;
END;

function get_reward(): boolean;
BEGIN
	return 	reward;
END;

procedure set_mission_completed(  value : boolean);
BEGIN
	mission_completed := value;
END;

function get_mission_completed(): boolean;
BEGIN
	return 	mission_completed;
END;










procedure process_grid (); 
BEGIN
IF ((!(mission_completed))) THEN 
running_time := increase_running_time_process_grid(running_time , T  );

ENDIF ; 

END;

function getreward () : boolean; 
BEGIN
IF ((!(reward)) & ((( pos_x) = (reward_x))) & ((( pos_y) = (reward_y)))) THEN 
reward:= true; 
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;

function generate_obstacle () : boolean; 
BEGIN
IF ((!(mission_completed)) & ((( running_time) = (2.00000)))) THEN 
ob_x := assign_ob_x_event_generate_obstacle();
ob_y := assign_ob_y_event_generate_obstacle();
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   getreward_triggered :  boolean;
   generate_obstacle_triggered :  boolean;
BEGIN
 event_triggered := true;
   getreward_triggered := false;
   generate_obstacle_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! getreward_triggered) then 
   getreward_triggered := getreward();
   event_triggered := event_triggered | getreward_triggered; 
   endif;

   if(! generate_obstacle_triggered) then 
   generate_obstacle_triggered := generate_obstacle();
   event_triggered := event_triggered | generate_obstacle_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_grid_enabled :  boolean;
BEGIN
 process_updated := false; end_while := false;
   process_grid_enabled := false;
while (!end_while) do 
    if ((!(mission_completed)) &  !process_grid_enabled) then
   process_updated := true;
   process_grid();
   process_grid_enabled := true;
   endif;

IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

action rule " right " 
(!((( pos_y) = (ob_y)))) & (!((( ob_x) = ((pos_x) + (1.00000))))) & (!(mission_completed)) ==> 
pddlname: " right"; 
BEGIN
pos_x := assign_pos_x_action_right(pos_x  );

END;

action rule " left " 
(!((( pos_y) = (ob_y)))) & (!((( ob_x) = ((pos_x) - (1.00000))))) & (!(mission_completed)) ==> 
pddlname: " left"; 
BEGIN
pos_x := assign_pos_x_action_left(pos_x  );

END;

action rule " up " 
(!((( pos_x) = (ob_x)))) & (!((( ob_y) = ((pos_y) + (1.00000))))) & (!(mission_completed)) ==> 
pddlname: " up"; 
BEGIN
pos_y := assign_pos_y_action_up(pos_y  );

END;

action rule " down " 
(!((( pos_x) = (ob_x)))) & (!((( ob_y) = ((pos_y) - (1.00000))))) & (!(mission_completed)) ==> 
pddlname: " down"; 
BEGIN
pos_y := assign_pos_y_action_down(pos_y  );

END;

action rule " stop " 
((( pos_x) = (goal_x))) & ((( pos_y) = (goal_y))) & (reward) ==> 
pddlname: " stop"; 
BEGIN
mission_completed:= true; 

END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
set_reward(false);

   set_mission_completed(false);

   pos_x := 0.0 ;

   pos_y := 0.0 ;

   ob_x := 0.0 ;

   ob_y := 0.0 ;





   running_time := 0.0 ;

reward:= false; 
mission_completed:= false; 
running_time := 0.00000;
pos_x := 0.00000;
pos_y := 0.00000;
ob_x := 1.00000;
ob_y := 0.00000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (mission_completed)& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


