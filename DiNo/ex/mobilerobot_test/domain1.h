#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

double round_k_digits(double n, unsigned k){
	double prec = pow(0.1,k);
	double round = (n>0) ? (n+prec/2) : (n-prec/2);
	return round-fmod(round,prec);
}

double ext_assignment(double n){
	return round_k_digits(n,5.000000);
}

double increase_running_time_process_grid(double running_time, double T ) {
	 return round_k_digits(running_time+(( T ) * (1.00000)),5.000000); 
}

double assign_ob_x_event_generate_obstacle() {
	 return round_k_digits(1.00000,5.000000); 
}

double assign_ob_y_event_generate_obstacle() {
	 return round_k_digits(3.00000,5.000000); 
}

double assign_pos_x_action_right(double pos_x ) {
	 return round_k_digits((pos_x) + (1.00000),5.000000); 
}

double assign_pos_x_action_left(double pos_x ) {
	 return round_k_digits((pos_x) - (1.00000),5.000000); 
}

double assign_pos_y_action_up(double pos_y ) {
	 return round_k_digits((pos_y) + (1.00000),5.000000); 
}

double assign_pos_y_action_down(double pos_y ) {
	 return round_k_digits((pos_y) - (1.00000),5.000000); 
}

