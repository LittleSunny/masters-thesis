/******************************
  Program "car.m" compiled by "DiNo Release 1.1"

  DiNo Last Compiled date: "May 28 2018"
 ******************************/

/********************
  Parameter
 ********************/
#define DINO_VERSION "DiNo Release 1.1"
#define MURPHI_DATE "May 28 2018"
#define PROTOCOL_NAME "car"
#define DOMAIN_FILENAME "car.pddl"
#define PROBLEM_FILENAME "prob01.pddl"
#define DISCRETIZATION 0.100000
#define VAL_PATHNAME "/home/sunny/catkin_ws/DiNo/src/DiNo/../VAL-master/validate"
#define BITS_IN_WORLD 245
#define HASHC
#define HAS_CLOCK
const char * const modelmessages[] = { " Time Discretisation = 0.1"," Digits for representing the integer part of a real =  5"," Digits for representing the fractional part of a real =  4" };
const int modelmessagecount = 3;

/********************
  Include
 ********************/
#include "upm_prolog.hpp"

/********************
  Decl declaration
 ********************/

class mu_1_real_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_real_type& val) { return mu__real::operator=((double) val); };
  mu_1_real_type (const char *name, int os): mu__real(7,4,40,name, os) {};
  mu_1_real_type (void): mu__real(7,4,40) {};
  mu_1_real_type (double val): mu__real(7,4,40,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_real_type mu_1_real_type_undefined_var;

class mu_1_integer: public mu__long
{
 public:
  inline int operator=(int val) { return mu__long::operator=(val); };
  inline int operator=(const mu_1_integer& val) { return mu__long::operator=((int) val); };
  mu_1_integer (const char *name, int os): mu__long(-1000, 1000, 11, name, os) {};
  mu_1_integer (void): mu__long(-1000, 1000, 11) {};
  mu_1_integer (int val): mu__long(-1000, 1000, 11, "Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%d",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of subrange decl ***/
mu_1_integer mu_1_integer_undefined_var;

class mu_1_TIME_type: public mu__real
{
 public:
  inline double operator=(double val) { return mu__real::operator=(val); };
  inline double operator=(const mu_1_TIME_type& val) { return mu__real::operator=((double) val); };
  mu_1_TIME_type (const char *name, int os): mu__real(7,2,40,name, os) {};
  mu_1_TIME_type (void): mu__real(7,2,40) {};
  mu_1_TIME_type (double val): mu__real(7,2,40,"Parameter or function result.", 0)
  {
    operator=(val);
  };
  char * Name() { return tsprintf("%le",value()); };
  virtual void Permute(PermSet& Perm, int i);
  virtual void SimpleCanonicalize(PermSet& Perm);
  virtual void Canonicalize(PermSet& Perm);
  virtual void SimpleLimit(PermSet& Perm);
  virtual void ArrayLimit(PermSet& Perm);
  virtual void Limit(PermSet& Perm);
  virtual void MultisetLimit(PermSet& Perm);
  virtual void MultisetSort() {};
  void print_statistic() {};
};

/*** end of real decl ***/
mu_1_TIME_type mu_1_TIME_type_undefined_var;

const double mu_T = +1.000000e-01;
const double mu_down_limit = -1.000000e+00;
const double mu_up_limit = +1.000000e+00;
/*** Variable declaration ***/
mu_0_boolean mu_all_event_true("all_event_true",0);

/*** Variable declaration ***/
mu_1_integer mu_h_n("h_n",2);

/*** Variable declaration ***/
mu_1_integer mu_g_n("g_n",13);

/*** Variable declaration ***/
mu_1_integer mu_f_n("f_n",24);

/*** Variable declaration ***/
mu_1_TIME_type mu_TIME("TIME",35);

/*** Variable declaration ***/
mu_1_real_type mu_a("a",75);

/*** Variable declaration ***/
mu_1_real_type mu_d("d",115);

/*** Variable declaration ***/
mu_1_real_type mu_running_time("running_time",155);

/*** Variable declaration ***/
mu_1_real_type mu_v("v",195);

/*** Variable declaration ***/
mu_0_boolean mu_running("running",235);

/*** Variable declaration ***/
mu_0_boolean mu_stopped("stopped",237);

/*** Variable declaration ***/
mu_0_boolean mu_engineblown("engineblown",239);

/*** Variable declaration ***/
mu_0_boolean mu_transmission_fine("transmission_fine",241);

/*** Variable declaration ***/
mu_0_boolean mu_goal_reached("goal_reached",243);


#include "car.h"

void mu_set_running(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_running.undefine();
else
  mu_running = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_running()
{
return mu_running;
	Error.Error("The end of function get_running reached without returning values.");
};
/*** end function declaration ***/

void mu_set_stopped(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_stopped.undefine();
else
  mu_stopped = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_stopped()
{
return mu_stopped;
	Error.Error("The end of function get_stopped reached without returning values.");
};
/*** end function declaration ***/

void mu_set_engineblown(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_engineblown.undefine();
else
  mu_engineblown = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_engineblown()
{
return mu_engineblown;
	Error.Error("The end of function get_engineblown reached without returning values.");
};
/*** end function declaration ***/

void mu_set_transmission_fine(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_transmission_fine.undefine();
else
  mu_transmission_fine = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_transmission_fine()
{
return mu_transmission_fine;
	Error.Error("The end of function get_transmission_fine reached without returning values.");
};
/*** end function declaration ***/

void mu_set_goal_reached(const mu_0_boolean& mu_value)
{
if (mu_value.isundefined())
  mu_goal_reached.undefine();
else
  mu_goal_reached = mu_value;
};
/*** end procedure declaration ***/

mu_0_boolean mu_get_goal_reached()
{
return mu_goal_reached;
	Error.Error("The end of function get_goal_reached reached without returning values.");
};
/*** end function declaration ***/

void mu_process_moving()
{
if ( mu_running )
{
mu_running_time = increase_running_time_process_moving( mu_running_time, (double)mu_T );
mu_d = increase_d_process_moving( mu_d, (double)mu_T, mu_v );
mu_v = increase_v_process_moving( mu_v, (double)mu_T, mu_a );
}
};
/*** end procedure declaration ***/

void mu_process_windresistance()
{
bool mu__boolexpr0;
  if (!(mu_running)) mu__boolexpr0 = FALSE ;
  else {
  mu__boolexpr0 = ((mu_v) >= (5.000000e+02)) ; 
}
if ( mu__boolexpr0 )
{
mu_v = decrease_v_process_windresistance( mu_v, (double)mu_T );
}
};
/*** end procedure declaration ***/

mu_0_boolean mu_engineexplode()
{
bool mu__boolexpr1;
bool mu__boolexpr2;
  if (!(mu_running)) mu__boolexpr2 = FALSE ;
  else {
  mu__boolexpr2 = ((mu_a) >= (1.000000e+00)) ; 
}
  if (!(mu__boolexpr2)) mu__boolexpr1 = FALSE ;
  else {
  mu__boolexpr1 = ((mu_v) >= (1.000000e+02)) ; 
}
if ( mu__boolexpr1 )
{
mu_engineblown = mu_true;
mu_running = mu_false;
mu_a = assign_a_event_engineexplode(  );
return mu_true;
}
else
{
return mu_false;
}
	Error.Error("The end of function engineexplode reached without returning values.");
};
/*** end function declaration ***/

void mu_event_check()
{
/*** Variable declaration ***/
mu_0_boolean mu_event_triggered("event_triggered",0);

/*** Variable declaration ***/
mu_0_boolean mu_engineexplode_triggered("engineexplode_triggered",2);

mu_event_triggered = mu_true;
mu_engineexplode_triggered = mu_false;
{
  bool mu__while_expr_4;  mu__while_expr_4 = mu_event_triggered;
int mu__counter_3 = 0;
while (mu__while_expr_4) {
if ( ++mu__counter_3 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
mu_event_triggered = mu_false;
if ( !(mu_engineexplode_triggered) )
{
mu_engineexplode_triggered = mu_engineexplode(  );
bool mu__boolexpr5;
  if (mu_event_triggered) mu__boolexpr5 = TRUE ;
  else {
  mu__boolexpr5 = (mu_engineexplode_triggered) ; 
}
mu_event_triggered = mu__boolexpr5;
}
};
mu__while_expr_4 = mu_event_triggered;
}
};
};
/*** end procedure declaration ***/

mu_0_boolean mu_DAs_violate_duration()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_duration_violated("DA_duration_violated",0);

mu_DA_duration_violated = mu_false;
return mu_DA_duration_violated;
	Error.Error("The end of function DAs_violate_duration reached without returning values.");
};
/*** end function declaration ***/

mu_0_boolean mu_DAs_ongoing_in_goal_state()
{
/*** Variable declaration ***/
mu_0_boolean mu_DA_still_ongoing("DA_still_ongoing",0);

mu_DA_still_ongoing = mu_false;
return mu_DA_still_ongoing;
	Error.Error("The end of function DAs_ongoing_in_goal_state reached without returning values.");
};
/*** end function declaration ***/

void mu_apply_continuous_change()
{
/*** Variable declaration ***/
mu_0_boolean mu_process_updated("process_updated",0);

/*** Variable declaration ***/
mu_0_boolean mu_end_while("end_while",2);

/*** Variable declaration ***/
mu_0_boolean mu_process_moving_enabled("process_moving_enabled",4);

/*** Variable declaration ***/
mu_0_boolean mu_process_windresistance_enabled("process_windresistance_enabled",6);

mu_process_updated = mu_false;
mu_end_while = mu_false;
mu_process_moving_enabled = mu_false;
mu_process_windresistance_enabled = mu_false;
{
  bool mu__while_expr_7;  mu__while_expr_7 = !(mu_end_while);
int mu__counter_6 = 0;
while (mu__while_expr_7) {
if ( ++mu__counter_6 > args->loopmax.value )
  Error.Error("Too many iterations in while loop.");
{
bool mu__boolexpr8;
  if (!(mu_running)) mu__boolexpr8 = FALSE ;
  else {
  mu__boolexpr8 = (!(mu_process_moving_enabled)) ; 
}
if ( mu__boolexpr8 )
{
mu_process_updated = mu_true;
mu_process_moving (  );
mu_process_moving_enabled = mu_true;
}
bool mu__boolexpr9;
bool mu__boolexpr10;
  if (!(mu_running)) mu__boolexpr10 = FALSE ;
  else {
  mu__boolexpr10 = ((mu_v) >= (5.000000e+02)) ; 
}
  if (!(mu__boolexpr10)) mu__boolexpr9 = FALSE ;
  else {
  mu__boolexpr9 = (!(mu_process_windresistance_enabled)) ; 
}
if ( mu__boolexpr9 )
{
mu_process_updated = mu_true;
mu_process_windresistance (  );
mu_process_windresistance_enabled = mu_true;
}
if ( !(mu_process_updated) )
{
mu_end_while = mu_true;
}
else
{
mu_process_updated = mu_false;
}
};
mu__while_expr_7 = !(mu_end_while);
}
};
};
/*** end procedure declaration ***/





/********************
  The world
 ********************/
void world_class::clear()
{
  mu_all_event_true.clear();
  mu_h_n.clear();
  mu_g_n.clear();
  mu_f_n.clear();
  mu_TIME.clear();
  mu_a.clear();
  mu_d.clear();
  mu_running_time.clear();
  mu_v.clear();
  mu_running.clear();
  mu_stopped.clear();
  mu_engineblown.clear();
  mu_transmission_fine.clear();
  mu_goal_reached.clear();
}
void world_class::undefine()
{
  mu_all_event_true.undefine();
  mu_h_n.undefine();
  mu_g_n.undefine();
  mu_f_n.undefine();
  mu_TIME.undefine();
  mu_a.undefine();
  mu_d.undefine();
  mu_running_time.undefine();
  mu_v.undefine();
  mu_running.undefine();
  mu_stopped.undefine();
  mu_engineblown.undefine();
  mu_transmission_fine.undefine();
  mu_goal_reached.undefine();
}
void world_class::reset()
{
  mu_all_event_true.reset();
  mu_h_n.reset();
  mu_g_n.reset();
  mu_f_n.reset();
  mu_TIME.reset();
  mu_a.reset();
  mu_d.reset();
  mu_running_time.reset();
  mu_v.reset();
  mu_running.reset();
  mu_stopped.reset();
  mu_engineblown.reset();
  mu_transmission_fine.reset();
  mu_goal_reached.reset();
}
std::vector<mu_0_boolean*> world_class::get_mu_bools()
{
	  std::vector<mu_0_boolean*> awesome;

      awesome.push_back(&(mu_all_event_true));
      awesome.push_back(&(mu_running));
      awesome.push_back(&(mu_stopped));
      awesome.push_back(&(mu_engineblown));
      awesome.push_back(&(mu_transmission_fine));
      awesome.push_back(&(mu_goal_reached));
    return awesome; 
}
std::vector<mu_0_boolean*> world_class::get_mu_bool_arrays()
{
	  std::vector<mu_0_boolean*> var_arrays;
   std::vector<mu_0_boolean*> interm;

    return var_arrays; 
}
std::vector<mu__real*> world_class::get_mu_nums()
{
	  std::vector<mu__real*> awesome;

      awesome.push_back(&(mu_a));
      awesome.push_back(&(mu_d));
      awesome.push_back(&(mu_running_time));
      awesome.push_back(&(mu_v));
    return awesome; 
}
std::vector<mu__real*> world_class::get_mu_num_arrays()
{
	  std::vector<mu__real*> var_arrays;
   std::vector<mu__real*> interm;

    return var_arrays; 
}
//WP WP WP WP WP
double world_class::get_f_val()
{
  double f_val = mu_f_n.value();
  return f_val;
}

//WP WP WP WP WP
void world_class::fire_processes()
{
			mu_process_moving();

			mu_process_windresistance();

}

//WP WP WP WP WP
void world_class::fire_processes_plus()
{
{



 if (mu_running) 


	{
		mu_running_time = increase_running_time_process_moving( mu_running_time, (double)mu_T ); 
		mu_d = increase_d_process_moving( mu_d, (double)mu_T, mu_v ); 
		mu_v = increase_v_process_moving( mu_v, (double)mu_T, mu_a ); 
	}


}
{

bool mu__boolexpr11;
  if (!(mu_running)) mu__boolexpr11 = FALSE ;
  else {
  mu__boolexpr11 = ((mu_v) >= (5.000000e+02)) ; 
}


 if (mu__boolexpr11) 


	{
	}


}
}

//WP WP WP WP WP
void world_class::fire_processes_minus()
{
{

bool mu__boolexpr12;
  if (!(mu_running)) mu__boolexpr12 = FALSE ;
  else {
  mu__boolexpr12 = ((mu_v) >= (5.000000e+02)) ; 
}


 if (mu__boolexpr12) 


	{
		mu_v = decrease_v_process_windresistance( mu_v, (double)mu_T ); 
	}


}
{



 if (mu_running) 


	{
	}


}
}

//WP WP WP WP WP
void world_class::set_f_val()
{
  double f_val = mu_g_n.value() + mu_h_n.value();
  mu_f_n.value(f_val);
}

//WP WP WP WP WP
double world_class::get_h_val()
{
  double h_val = mu_h_n.value();
  return h_val;
}

//WP WP WP WP WP
void world_class::set_h_val()
{
  //	NON-HEURISTIC SEARCH
  // double h_val = 0; 

  //	FF RPG
  //upm_rpg::getInstance().clear_all();
  //double h_val = upm_rpg::getInstance().compute_rpg();


  //	NUMERIC RPG
  //upm_numeric_rpg::getInstance().clear_all();
  //double h_val = upm_numeric_rpg::getInstance().compute_rpg();

  //	TEMPORAL RPG
  upm_staged_rpg::getInstance().clear_all();
  double h_val = upm_staged_rpg::getInstance().compute_rpg();

  mu_h_n.value(h_val);
}

//WP WP WP WP WP
void world_class::set_h_val(int hp)
{
  double h_val = hp; 
  mu_h_n.value(h_val);
}

//WP WP WP WP WP
double world_class::get_g_val()
{
  double g_val = mu_g_n.value();
  return g_val;
}

//WP WP WP WP WP
void world_class::set_g_val(double g_val)
{
  mu_g_n.value(g_val);
}

void world_class::print(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print(target, separator);
  mu_h_n.print(target, separator);
  mu_g_n.print(target, separator);
  mu_f_n.print(target, separator);
  mu_TIME.print(target, separator);
  mu_a.print(target, separator);
  mu_d.print(target, separator);
  mu_running_time.print(target, separator);
  mu_v.print(target, separator);
  mu_running.print(target, separator);
  mu_stopped.print(target, separator);
  mu_engineblown.print(target, separator);
  mu_transmission_fine.print(target, separator);
  mu_goal_reached.print(target, separator);
    num_calls--;
}
}
void world_class::pddlprint(FILE *target, const char *separator)
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_TIME.print(target, separator);
  mu_a.print(target, separator);
  mu_d.print(target, separator);
  mu_running_time.print(target, separator);
  mu_v.print(target, separator);
  mu_running.print(target, separator);
  mu_stopped.print(target, separator);
  mu_engineblown.print(target, separator);
  mu_transmission_fine.print(target, separator);
  mu_goal_reached.print(target, separator);
    num_calls--;
}
}
double world_class::get_clock_value()
{
  return mu_TIME.value();
}
void world_class::print_statistic()
{
  static int num_calls = 0; /* to ward off recursive calls. */
  if ( num_calls == 0 ) {
    num_calls++;
  mu_all_event_true.print_statistic();
  mu_h_n.print_statistic();
  mu_g_n.print_statistic();
  mu_f_n.print_statistic();
  mu_TIME.print_statistic();
  mu_a.print_statistic();
  mu_d.print_statistic();
  mu_running_time.print_statistic();
  mu_v.print_statistic();
  mu_running.print_statistic();
  mu_stopped.print_statistic();
  mu_engineblown.print_statistic();
  mu_transmission_fine.print_statistic();
  mu_goal_reached.print_statistic();
    num_calls--;
}
}
void world_class::print_diff(state *prevstate, FILE *target, const char *separator)
{
  if ( prevstate != NULL )
  {
    mu_all_event_true.print_diff(prevstate,target,separator);
    mu_h_n.print_diff(prevstate,target,separator);
    mu_g_n.print_diff(prevstate,target,separator);
    mu_f_n.print_diff(prevstate,target,separator);
    mu_TIME.print_diff(prevstate,target,separator);
    mu_a.print_diff(prevstate,target,separator);
    mu_d.print_diff(prevstate,target,separator);
    mu_running_time.print_diff(prevstate,target,separator);
    mu_v.print_diff(prevstate,target,separator);
    mu_running.print_diff(prevstate,target,separator);
    mu_stopped.print_diff(prevstate,target,separator);
    mu_engineblown.print_diff(prevstate,target,separator);
    mu_transmission_fine.print_diff(prevstate,target,separator);
    mu_goal_reached.print_diff(prevstate,target,separator);
  }
  else
print(target,separator);
}
void world_class::to_state(state *newstate)
{
  mu_all_event_true.to_state( newstate );
  mu_h_n.to_state( newstate );
  mu_g_n.to_state( newstate );
  mu_f_n.to_state( newstate );
  mu_TIME.to_state( newstate );
  mu_a.to_state( newstate );
  mu_d.to_state( newstate );
  mu_running_time.to_state( newstate );
  mu_v.to_state( newstate );
  mu_running.to_state( newstate );
  mu_stopped.to_state( newstate );
  mu_engineblown.to_state( newstate );
  mu_transmission_fine.to_state( newstate );
  mu_goal_reached.to_state( newstate );
}
void world_class::setstate(state *thestate)
{
}


/********************
  Rule declarations
 ********************/
/******************** RuleBase0 ********************/
class RuleBase0
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" time passing ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
    return mu_true;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;



    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;



    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;



    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;



    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_TIME));  // (mu_TIME) + (mu_T) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_TIME)); //  (mu_TIME) + (mu_T) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 0;
    while (what_rule < 1 )
      {
	if ( ( TRUE  ) ) {
	      if (mu_true) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 0;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_TIME = (mu_TIME) + (mu_T);
mu_event_check (  );
mu_apply_continuous_change (  );
mu_event_check (  );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_TIME = (mu_TIME) + (mu_T);


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 1;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return Duration(r);
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( time passing )");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Clock;
  };

};
/******************** RuleBase1 ********************/
class RuleBase1
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" stop ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr13;
bool mu__boolexpr14;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr14 = FALSE ;
  else {
  mu__boolexpr14 = ((mu_d) >= (5.000000e+00)) ; 
}
  if (!(mu__boolexpr14)) mu__boolexpr13 = FALSE ;
  else {
  mu__boolexpr13 = (!(mu_engineblown)) ; 
}
    return mu__boolexpr13;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr15;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr15 = FALSE ;
  else {
  mu__boolexpr15 = ((mu_d) >= (5.000000e+00)) ; 
}
bool mu__boolexpr16;
bool mu__boolexpr17;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr17 = FALSE ;
  else {
  mu__boolexpr17 = ((mu_d) >= (5.000000e+00)) ; 
}
  if (!(mu__boolexpr17)) mu__boolexpr16 = FALSE ;
  else {
  mu__boolexpr16 = (!(mu_engineblown)) ; 
}
bool mu__boolexpr18;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr18 = FALSE ;
  else {
  mu__boolexpr18 = ((mu_d) >= (5.000000e+00)) ; 
}


    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr19;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr19 = FALSE ;
  else {
  mu__boolexpr19 = ((mu_d) >= (5.000000e+00)) ; 
}
bool mu__boolexpr20;
bool mu__boolexpr21;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr21 = FALSE ;
  else {
  mu__boolexpr21 = ((mu_d) >= (5.000000e+00)) ; 
}
  if (!(mu__boolexpr21)) mu__boolexpr20 = FALSE ;
  else {
  mu__boolexpr20 = (!(mu_engineblown)) ; 
}
bool mu__boolexpr22;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr22 = FALSE ;
  else {
  mu__boolexpr22 = ((mu_d) >= (5.000000e+00)) ; 
}

 	if (std::string(typeid(mu_d).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_d), std::make_pair(5.000000e+00, 1))); 
	} 
 	if (std::string(typeid(mu_v).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_v), std::make_pair(0.000000e+00, 0))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_d).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_d)); 
 	if (std::string(typeid(mu_v).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_v)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr23;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr23 = FALSE ;
  else {
  mu__boolexpr23 = ((mu_d) >= (5.000000e+00)) ; 
}
bool mu__boolexpr24;
bool mu__boolexpr25;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr25 = FALSE ;
  else {
  mu__boolexpr25 = ((mu_d) >= (5.000000e+00)) ; 
}
  if (!(mu__boolexpr25)) mu__boolexpr24 = FALSE ;
  else {
  mu__boolexpr24 = (!(mu_engineblown)) ; 
}
bool mu__boolexpr26;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr26 = FALSE ;
  else {
  mu__boolexpr26 = ((mu_d) >= (5.000000e+00)) ; 
}

 		if (std::string(typeid(mu_engineblown).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_engineblown), 0)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr27;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr27 = FALSE ;
  else {
  mu__boolexpr27 = ((mu_d) >= (5.000000e+00)) ; 
}
bool mu__boolexpr28;
bool mu__boolexpr29;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr29 = FALSE ;
  else {
  mu__boolexpr29 = ((mu_d) >= (5.000000e+00)) ; 
}
  if (!(mu__boolexpr29)) mu__boolexpr28 = FALSE ;
  else {
  mu__boolexpr28 = (!(mu_engineblown)) ; 
}
bool mu__boolexpr30;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr30 = FALSE ;
  else {
  mu__boolexpr30 = ((mu_d) >= (5.000000e+00)) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;



    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;


    aeffs.push_back(&(mu_goal_reached)); //  mu_true 

    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;


    inter_effs.insert(std::make_pair(&(mu_goal_reached), 1)); //  mu_true 

    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_goal_reached)); //  mu_true 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 1;
    while (what_rule < 2 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr31;
bool mu__boolexpr32;
  if (!((mu_v) == (0.000000e+00))) mu__boolexpr32 = FALSE ;
  else {
  mu__boolexpr32 = ((mu_d) >= (5.000000e+00)) ; 
}
  if (!(mu__boolexpr32)) mu__boolexpr31 = FALSE ;
  else {
  mu__boolexpr31 = (!(mu_engineblown)) ; 
}
	      if (mu__boolexpr31) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 1;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_goal_reached = mu_true;
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {



mu_goal_reached = mu_true;


  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_goal_reached = mu_true;


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_goal_reached = mu_true;


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( stop)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase2 ********************/
class RuleBase2
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" decelerate ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr33;
  if (!(mu_running)) mu__boolexpr33 = FALSE ;
  else {
  mu__boolexpr33 = ((mu_a) > (mu_down_limit)) ; 
}
    return mu__boolexpr33;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr34;
  if (!(mu_running)) mu__boolexpr34 = FALSE ;
  else {
  mu__boolexpr34 = ((mu_a) > (mu_down_limit)) ; 
}

 		if (std::string(typeid(mu_running).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_running)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr35;
  if (!(mu_running)) mu__boolexpr35 = FALSE ;
  else {
  mu__boolexpr35 = ((mu_a) > (mu_down_limit)) ; 
}

 	if (std::string(typeid(mu_a).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_a), std::make_pair(mu_down_limit, 3))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_a).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_a)); 
 		if (std::string(typeid(mu_running).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_running)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr36;
  if (!(mu_running)) mu__boolexpr36 = FALSE ;
  else {
  mu__boolexpr36 = ((mu_a) > (mu_down_limit)) ; 
}

 		if (std::string(typeid(mu_running).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_running), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr37;
  if (!(mu_running)) mu__boolexpr37 = FALSE ;
  else {
  mu__boolexpr37 = ((mu_a) > (mu_down_limit)) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_a));  // decrease_a_action_decelerate( mu_a ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_a)); //  decrease_a_action_decelerate( mu_a ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 2;
    while (what_rule < 3 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr38;
  if (!(mu_running)) mu__boolexpr38 = FALSE ;
  else {
  mu__boolexpr38 = ((mu_a) > (mu_down_limit)) ; 
}
	      if (mu__boolexpr38) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 2;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_a = decrease_a_action_decelerate( mu_a );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {



mu_a = decrease_a_action_decelerate( mu_a );


  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( decelerate)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
/******************** RuleBase3 ********************/
class RuleBase3
{
public:
  int Priority()
  {
    return 0;
  }
  char * Name(RULE_INDEX_TYPE r)
  {
    return tsprintf(" accelerate ");
  }
  bool Condition(RULE_INDEX_TYPE r)
  {
bool mu__boolexpr39;
  if (!(mu_running)) mu__boolexpr39 = FALSE ;
  else {
  mu__boolexpr39 = ((mu_a) < (mu_up_limit)) ; 
}
    return mu__boolexpr39;
  }

  std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> preconds;

bool mu__boolexpr40;
  if (!(mu_running)) mu__boolexpr40 = FALSE ;
  else {
  mu__boolexpr40 = ((mu_a) < (mu_up_limit)) ; 
}

 		if (std::string(typeid(mu_running).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_running)); 

    return preconds;
  }

  std::map<mu__real*, std::pair<double, int> > num_precond_array(RULE_INDEX_TYPE r)
  {
    std::map<mu__real*, std::pair<double, int> > preconds;

bool mu__boolexpr41;
  if (!(mu_running)) mu__boolexpr41 = FALSE ;
  else {
  mu__boolexpr41 = ((mu_a) < (mu_up_limit)) ; 
}

 	if (std::string(typeid(mu_a).name()).compare("14mu_1_real_type") == 0){
			preconds.insert(std::make_pair(&(mu_a), std::make_pair(mu_up_limit, 4))); 
	} 

    return preconds;
  }



  std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> preconds;
 	if (std::string(typeid(mu_a).name()).compare("14mu_1_real_type") == 0)
			preconds.push_back(&(mu_a)); 
 		if (std::string(typeid(mu_running).name()).compare("12mu_0_boolean") == 0)
			preconds.push_back(&(mu_running)); 

    return preconds;
  }

  std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*, int> > interference_preconds;

bool mu__boolexpr42;
  if (!(mu_running)) mu__boolexpr42 = FALSE ;
  else {
  mu__boolexpr42 = ((mu_a) < (mu_up_limit)) ; 
}

 		if (std::string(typeid(mu_running).name()).compare("12mu_0_boolean") == 0)
			interference_preconds.insert(std::make_pair(&(mu_running), 1)); 

    return interference_preconds;
  }

  std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
  {
    std::pair<double, double> temporal_cons;

bool mu__boolexpr43;
  if (!(mu_running)) mu__boolexpr43 = FALSE ;
  else {
  mu__boolexpr43 = ((mu_a) < (mu_up_limit)) ; 
}


    return temporal_cons;
  }

  std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__real*> effs;


    effs.push_back(&(mu_a));  // increase_a_action_accelerate( mu_a ) 

    return effs;
  }

  std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu_0_boolean*> aeffs;



    return aeffs;
  }

  std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
  {
    std::set<std::pair<mu_0_boolean*,int> > inter_effs;



    return inter_effs;
  }

  std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
  {
    std::vector<mu__any*> aeffs;
    aeffs.push_back(&(mu_a)); //  increase_a_action_accelerate( mu_a ) 

    return aeffs;
  }

  void NextRule(RULE_INDEX_TYPE & what_rule)
  {
    RULE_INDEX_TYPE r = what_rule - 3;
    while (what_rule < 4 )
      {
	if ( ( TRUE  ) ) {
bool mu__boolexpr44;
  if (!(mu_running)) mu__boolexpr44 = FALSE ;
  else {
  mu__boolexpr44 = ((mu_a) < (mu_up_limit)) ; 
}
	      if (mu__boolexpr44) {
		if ( ( TRUE  ) )
		  return;
		else
		  what_rule++;
	      }
	      else
		what_rule += 1;
	}
	else
	  what_rule += 1;
    r = what_rule - 3;
    }
  }

  void Code(RULE_INDEX_TYPE r)
  {
mu_a = increase_a_action_accelerate( mu_a );
  };

  void Code_ff(RULE_INDEX_TYPE r)
  {





  }

  void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
  {



mu_a = increase_a_action_accelerate( mu_a );


  }

  void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
  {





  }

  mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
  {





  }

std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
  {


 std::map<mu_0_boolean*, mu__real*> pr; 

return pr; 


  }

  int Duration(RULE_INDEX_TYPE r)
  {
    return 0;
  }

  int Weight(RULE_INDEX_TYPE r)
  {
    return 0;
  }

   char * PDDLName(RULE_INDEX_TYPE r)
  {
    return tsprintf("( accelerate)");
  }
   RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
  {
    return RuleManager::Action;
  };

};
class NextStateGenerator
{
  RuleBase0 R0;
  RuleBase1 R1;
  RuleBase2 R2;
  RuleBase3 R3;
public:
void SetNextEnabledRule(RULE_INDEX_TYPE & what_rule)
{
  category = CONDITION;
  if (what_rule<1)
    { R0.NextRule(what_rule);
      if (what_rule<1) return; }
  if (what_rule>=1 && what_rule<2)
    { R1.NextRule(what_rule);
      if (what_rule<2) return; }
  if (what_rule>=2 && what_rule<3)
    { R2.NextRule(what_rule);
      if (what_rule<3) return; }
  if (what_rule>=3 && what_rule<4)
    { R3.NextRule(what_rule);
      if (what_rule<4) return; }
}
bool Condition(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.Condition(r-0);
  if (r>=1 && r<=1) return R1.Condition(r-1);
  if (r>=2 && r<=2) return R2.Condition(r-2);
  if (r>=3 && r<=3) return R3.Condition(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking condition for nonexisting rule.");
}
std::vector<mu_0_boolean*> bool_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.bool_precond_array(r-0);
  if (r>=1 && r<=1) return R1.bool_precond_array(r-1);
  if (r>=2 && r<=2) return R2.bool_precond_array(r-2);
  if (r>=3 && r<=3) return R3.bool_precond_array(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::map<mu__real*, std::pair<double,int> > num_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.num_precond_array(r-0);
  if (r>=1 && r<=1) return R1.num_precond_array(r-1);
  if (r>=2 && r<=2) return R2.num_precond_array(r-2);
  if (r>=3 && r<=3) return R3.num_precond_array(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::vector<mu__any*> all_precond_array(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.all_precond_array(r-0);
  if (r>=1 && r<=1) return R1.all_precond_array(r-1);
  if (r>=2 && r<=2) return R2.all_precond_array(r-2);
  if (r>=3 && r<=3) return R3.all_precond_array(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > precond_bool_interference(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.precond_bool_interference(r-0);
  if (r>=1 && r<=1) return R1.precond_bool_interference(r-1);
  if (r>=2 && r<=2) return R2.precond_bool_interference(r-2);
  if (r>=3 && r<=3) return R3.precond_bool_interference(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::pair<double, double> temporal_constraints(RULE_INDEX_TYPE r)
{
  category = CONDITION;
  if (r<=0) return R0.temporal_constraints(r-0);
  if (r>=1 && r<=1) return R1.temporal_constraints(r-1);
  if (r>=2 && r<=2) return R2.temporal_constraints(r-2);
  if (r>=3 && r<=3) return R3.temporal_constraints(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking preconditions for nonexisting rule.");
}
std::set<std::pair<mu_0_boolean*, int> > effects_bool_interference(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_bool_interference(r-0);
  if (r>=1 && r<=1) return R1.effects_bool_interference(r-1);
  if (r>=2 && r<=2) return R2.effects_bool_interference(r-2);
  if (r>=3 && r<=3) return R3.effects_bool_interference(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu_0_boolean*> effects_add_bool_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_add_bool_array(r-0);
  if (r>=1 && r<=1) return R1.effects_add_bool_array(r-1);
  if (r>=2 && r<=2) return R2.effects_add_bool_array(r-2);
  if (r>=3 && r<=3) return R3.effects_add_bool_array(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__real*> effects_num_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_num_array(r-0);
  if (r>=1 && r<=1) return R1.effects_num_array(r-1);
  if (r>=2 && r<=2) return R2.effects_num_array(r-2);
  if (r>=3 && r<=3) return R3.effects_num_array(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
std::vector<mu__any*> effects_all_array(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.effects_all_array(r-0);
  if (r>=1 && r<=1) return R1.effects_all_array(r-1);
  if (r>=2 && r<=2) return R2.effects_all_array(r-2);
  if (r>=3 && r<=3) return R3.effects_all_array(r-3);
Error.Notrace("Internal: NextStateGenerator -- checking add effects for nonexisting rule.");
}
void Code(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code(r-3); return; } 
}
void Code_ff(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_ff(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_ff(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_ff(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_ff(r-3); return; } 
}
void Code_numeric_ff_plus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_plus(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_numeric_ff_plus(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_numeric_ff_plus(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_numeric_ff_plus(r-3); return; } 
}
void Code_numeric_ff_minus(RULE_INDEX_TYPE r)
{
  if (r<=0) { R0.Code_numeric_ff_minus(r-0); return; } 
  if (r>=1 && r<=1) { R1.Code_numeric_ff_minus(r-1); return; } 
  if (r>=2 && r<=2) { R2.Code_numeric_ff_minus(r-2); return; } 
  if (r>=3 && r<=3) { R3.Code_numeric_ff_minus(r-3); return; } 
}
mu_0_boolean* get_rule_clock_started(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_rule_clock_started(r-0); } 
  if (r>=1 && r<=1) { return R1.get_rule_clock_started(r-1); } 
  if (r>=2 && r<=2) { return R2.get_rule_clock_started(r-2); } 
  if (r>=3 && r<=3) { return R3.get_rule_clock_started(r-3); } 
}
std::map<mu_0_boolean*, mu__real*> get_clocks(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.get_clocks(r-0); } 
  if (r>=1 && r<=1) { return R1.get_clocks(r-1); } 
  if (r>=2 && r<=2) { return R2.get_clocks(r-2); } 
  if (r>=3 && r<=3) { return R3.get_clocks(r-3); } 
}
int Priority(RULE_INDEX_TYPE r)
{
  if (r<=0) { return R0.Priority(); } 
  if (r>=1 && r<=1) { return R1.Priority(); } 
  if (r>=2 && r<=2) { return R2.Priority(); } 
  if (r>=3 && r<=3) { return R3.Priority(); } 
}
char * Name(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Name(r-0);
  if (r>=1 && r<=1) return R1.Name(r-1);
  if (r>=2 && r<=2) return R2.Name(r-2);
  if (r>=3 && r<=3) return R3.Name(r-3);
  return NULL;
}
int Duration(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Duration(r-0);
  if (r>=1 && r<=1) return R1.Duration(r-1);
  if (r>=2 && r<=2) return R2.Duration(r-2);
  if (r>=3 && r<=3) return R3.Duration(r-3);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
int Weight(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.Weight(r-0);
  if (r>=1 && r<=1) return R1.Weight(r-1);
  if (r>=2 && r<=2) return R2.Weight(r-2);
  if (r>=3 && r<=3) return R3.Weight(r-3);
Error.Notrace("Internal: NextStateGenerator -- querying duration for nonexisting rule.");
}
 char * PDDLName(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLName(r-0);
  if (r>=1 && r<=1) return R1.PDDLName(r-1);
  if (r>=2 && r<=2) return R2.PDDLName(r-2);
  if (r>=3 && r<=3) return R3.PDDLName(r-3);
  return NULL;
}
RuleManager::rule_pddlclass PDDLClass(RULE_INDEX_TYPE r)
{
  if (r<=0) return R0.PDDLClass(r-0);
  if (r>=1 && r<=1) return R1.PDDLClass(r-1);
  if (r>=2 && r<=2) return R2.PDDLClass(r-2);
  if (r>=3 && r<=3) return R3.PDDLClass(r-3);
Error.Notrace("Internal: NextStateGenerator -- querying PDDL class for nonexisting rule.");
}
};
const RULE_INDEX_TYPE numrules = 4;

/********************
  parameter
 ********************/
#define RULES_IN_WORLD 4


/********************
  Startstate records
 ********************/
/******************** StartStateBase0 ********************/
class StartStateBase0
{
public:
  char * Name(unsigned short r)
  {
    return tsprintf("start");
  }
  void Code(unsigned short r)
  {
mu_TIME = 0.000000e+00;
mu_set_running ( mu_false );
mu_set_stopped ( mu_false );
mu_set_engineblown ( mu_false );
mu_set_transmission_fine ( mu_false );
mu_set_goal_reached ( mu_false );
mu_d = 0.000000e+00;
mu_v = 0.000000e+00;
mu_a = 0.000000e+00;
mu_running_time = 0.000000e+00;
mu_running = mu_true;
mu_transmission_fine = mu_true;
mu_engineblown = mu_false;
mu_running_time = 0.000000e+00;
mu_d = 0.000000e+00;
mu_a = 0.000000e+00;
mu_v = 0.000000e+00;
mu_all_event_true = mu_true;
mu_g_n = 0;
mu_h_n = 0;
mu_f_n = 0;
  };

};
class StartStateGenerator
{
  StartStateBase0 S0;
public:
void Code(unsigned short r)
{
  if (r<=0) { S0.Code(r-0); return; }
}
char * Name(unsigned short r)
{
  if (r<=0) return S0.Name(r-0);
  return NULL;
}
};
const rulerec startstates[] = {
{ NULL, NULL, NULL, FALSE},
};
unsigned short StartStateManager::numstartstates = 1;

/********************
  Goal records
 ********************/

// WP WP WP GOAL
int mu__goal_45() // Goal "enjoy"
{
bool mu__boolexpr46;
bool mu__boolexpr47;
bool mu__boolexpr48;
bool mu__boolexpr49;
  if (!(mu_goal_reached)) mu__boolexpr49 = FALSE ;
  else {
  mu__boolexpr49 = (!(mu_engineblown)) ; 
}
  if (!(mu__boolexpr49)) mu__boolexpr48 = FALSE ;
  else {
  mu__boolexpr48 = ((mu_running_time) <= (5.000000e+01)) ; 
}
  if (!(mu__boolexpr48)) mu__boolexpr47 = FALSE ;
  else {
  mu__boolexpr47 = (mu_transmission_fine) ; 
}
  if (!(mu__boolexpr47)) mu__boolexpr46 = FALSE ;
  else {
  mu__boolexpr46 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}
return mu__boolexpr46;
};

  std::set<mu_0_boolean*> get_bool_goal_conditions()
  {
    std::set<mu_0_boolean*> bool_goal_conds;
bool mu__boolexpr50;
bool mu__boolexpr51;
bool mu__boolexpr52;
  if (!(mu_goal_reached)) mu__boolexpr52 = FALSE ;
  else {
  mu__boolexpr52 = (!(mu_engineblown)) ; 
}
  if (!(mu__boolexpr52)) mu__boolexpr51 = FALSE ;
  else {
  mu__boolexpr51 = ((mu_running_time) <= (5.000000e+01)) ; 
}
  if (!(mu__boolexpr51)) mu__boolexpr50 = FALSE ;
  else {
  mu__boolexpr50 = (mu_transmission_fine) ; 
}
bool mu__boolexpr53;
bool mu__boolexpr54;
bool mu__boolexpr55;
bool mu__boolexpr56;
  if (!(mu_goal_reached)) mu__boolexpr56 = FALSE ;
  else {
  mu__boolexpr56 = (!(mu_engineblown)) ; 
}
  if (!(mu__boolexpr56)) mu__boolexpr55 = FALSE ;
  else {
  mu__boolexpr55 = ((mu_running_time) <= (5.000000e+01)) ; 
}
  if (!(mu__boolexpr55)) mu__boolexpr54 = FALSE ;
  else {
  mu__boolexpr54 = (mu_transmission_fine) ; 
}
  if (!(mu__boolexpr54)) mu__boolexpr53 = FALSE ;
  else {
  mu__boolexpr53 = (!(mu_DAs_ongoing_in_goal_state(  ))) ; 
}
bool mu__boolexpr57;
bool mu__boolexpr58;
  if (!(mu_goal_reached)) mu__boolexpr58 = FALSE ;
  else {
  mu__boolexpr58 = (!(mu_engineblown)) ; 
}
  if (!(mu__boolexpr58)) mu__boolexpr57 = FALSE ;
  else {
  mu__boolexpr57 = ((mu_running_time) <= (5.000000e+01)) ; 
}
bool mu__boolexpr59;
bool mu__boolexpr60;
bool mu__boolexpr61;
  if (!(mu_goal_reached)) mu__boolexpr61 = FALSE ;
  else {
  mu__boolexpr61 = (!(mu_engineblown)) ; 
}
  if (!(mu__boolexpr61)) mu__boolexpr60 = FALSE ;
  else {
  mu__boolexpr60 = ((mu_running_time) <= (5.000000e+01)) ; 
}
  if (!(mu__boolexpr60)) mu__boolexpr59 = FALSE ;
  else {
  mu__boolexpr59 = (mu_transmission_fine) ; 
}
bool mu__boolexpr62;
  if (!(mu_goal_reached)) mu__boolexpr62 = FALSE ;
  else {
  mu__boolexpr62 = (!(mu_engineblown)) ; 
}
bool mu__boolexpr63;
bool mu__boolexpr64;
  if (!(mu_goal_reached)) mu__boolexpr64 = FALSE ;
  else {
  mu__boolexpr64 = (!(mu_engineblown)) ; 
}
  if (!(mu__boolexpr64)) mu__boolexpr63 = FALSE ;
  else {
  mu__boolexpr63 = ((mu_running_time) <= (5.000000e+01)) ; 
}
bool mu__boolexpr65;
  if (!(mu_goal_reached)) mu__boolexpr65 = FALSE ;
  else {
  mu__boolexpr65 = (!(mu_engineblown)) ; 
}

 if (std::string(typeid(mu_goal_reached).name()).compare("12mu_0_boolean") == 0)
		bool_goal_conds.insert(&(mu_goal_reached)); 
 if (std::string(typeid(mu_transmission_fine).name()).compare("12mu_0_boolean") == 0)
		bool_goal_conds.insert(&(mu_transmission_fine)); 

    return bool_goal_conds;
  }

  std::map<mu__real*, std::pair<double, int> > get_numeric_goal_conditions()
  {
    std::map<mu__real*, std::pair<double, int> > numeric_goal_conds;
 if (std::string(typeid(mu_running_time).name()).compare("14mu_1_real_type") == 0)
		numeric_goal_conds.insert(std::make_pair(&(mu_running_time), std::make_pair(5.000000e+01, 2))); 

    return numeric_goal_conds;
  }

bool mu__condition_66() // Condition for Rule "enjoy"
{
  return mu__goal_45( );
}

bool mu__goal__00(){ return mu__condition_66(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/


// WP WP WP GOAL
const rulerec goals[] = {
{"enjoy", &mu__condition_66, NULL, },
};
const unsigned short numgoals = 1;

/********************
  Metric related stuff
 ********************/
const short metric = -1;

/********************
  Invariant records
 ********************/
int mu__invariant_67() // Invariant "todo bien"
{
bool mu__boolexpr68;
  if (!(mu_all_event_true)) mu__boolexpr68 = FALSE ;
  else {
  mu__boolexpr68 = (!(mu_DAs_violate_duration(  ))) ; 
}
return mu__boolexpr68;
};

bool mu__condition_69() // Condition for Rule "todo bien"
{
  return mu__invariant_67( );
}

bool mu__goal__01(){ return mu__condition_69(); } /* WP WP WP GOAL CONDITION CHECK */ /**** end rule declaration ****/

const rulerec invariants[] = {
{"todo bien", &mu__condition_69, NULL, },
};
const unsigned short numinvariants = 1;

/********************
  Normal/Canonicalization for scalarset
 ********************/
/*
transmission_fine:NoScalarset
stopped:NoScalarset
v:NoScalarset
d:NoScalarset
TIME:NoScalarset
g_n:NoScalarset
all_event_true:NoScalarset
h_n:NoScalarset
f_n:NoScalarset
a:NoScalarset
running_time:NoScalarset
running:NoScalarset
engineblown:NoScalarset
goal_reached:NoScalarset
*/

/********************
Code for symmetry
 ********************/

/********************
 Permutation Set Class
 ********************/
class PermSet
{
public:
  // book keeping
  enum PresentationType {Simple, Explicit};
  PresentationType Presentation;

  void ResetToSimple();
  void ResetToExplicit();
  void SimpleToExplicit();
  void SimpleToOne();
  bool NextPermutation();

  void Print_in_size()
  { unsigned long ret=0; for (unsigned long i=0; i<count; i++) if (in[i]) ret++; cout << "in_size:" << ret << "\n"; }


  /********************
   Simple and efficient representation
   ********************/
  bool AlreadyOnlyOneRemain;
  bool MoreThanOneRemain();


  /********************
   Explicit representation
  ********************/
  unsigned long size;
  unsigned long count;
  // in will be of product of factorial sizes for fast canonicalize
  // in will be of size 1 for reduced local memory canonicalize
  bool * in;

  // auxiliary for explicit representation

  // in/perm/revperm will be of factorial size for fast canonicalize
  // they will be of size 1 for reduced local memory canonicalize
  // second range will be size of the scalarset
  // procedure for explicit representation
  // General procedure
  PermSet();
  bool In(int i) const { return in[i]; };
  void Add(int i) { for (int j=0; j<i; j++) in[j] = FALSE;};
  void Remove(int i) { in[i] = FALSE; };
};
bool PermSet::MoreThanOneRemain()
{
  int i,j;
  if (AlreadyOnlyOneRemain)
    return FALSE;
  else {
  }
  AlreadyOnlyOneRemain = TRUE;
  return FALSE;
}
PermSet::PermSet()
: Presentation(Simple)
{
  int i,j,k;
  if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];

    // Set perm and revperm

    // setting up combination of permutations
    // for different scalarset
    int carry;
    size = 1;
    count = 1;
    for (i=0; i<1; i++)
      {
        carry = 1;
        in[i]= TRUE;
    }
  }
  else
  {

  /********************
   declaration of class variables
  ********************/
  in = new bool[1];
  in[0] = TRUE;
  }
}
void PermSet::ResetToSimple()
{
  int i;

  AlreadyOnlyOneRemain = FALSE;
  Presentation = Simple;
}
void PermSet::ResetToExplicit()
{
  for (int i=0; i<1; i++) in[i] = TRUE;
  Presentation = Explicit;
}
void PermSet::SimpleToExplicit()
{
  int i,j,k;
  int start, class_size;

  // Setup range for mapping

  // To be In or not to be

  // setup explicit representation 
  // Set perm and revperm
  for (i=0; i<1; i++)
    {
      in[i] = TRUE;
    }
  Presentation = Explicit;
  if (args->test_parameter1.value==0) Print_in_size();
}
void PermSet::SimpleToOne()
{
  int i,j,k;
  int class_size;
  int start;


  // Setup range for mapping
  Presentation = Explicit;
}
bool PermSet::NextPermutation()
{
  bool nexted = FALSE;
  int start, end; 
  int class_size;
  int temp;
  int j,k;

  // algorithm
  // for each class
  //   if forall in the same class reverse_sorted, 
  //     { sort again; goto next class }
  //   else
  //     {
  //       nexted = TRUE;
  //       for (j from l to r)
  // 	       if (for all j+ are reversed sorted)
  // 	         {
  // 	           swap j, j+1
  // 	           sort all j+ again
  // 	           break;
  // 	         }
  //     }
if (!nexted) return FALSE;
  return TRUE;
}

/********************
 Symmetry Class
 ********************/
class SymmetryClass
{
  PermSet Perm;
  bool BestInitialized;
  state BestPermutedState;

  // utilities
  void SetBestResult(int i, state* temp);
  void ResetBestResult() {BestInitialized = FALSE;};

public:
  // initializer
  SymmetryClass() : Perm(), BestInitialized(FALSE) {};
  ~SymmetryClass() {};

  void Normalize(state* s);

  void Exhaustive_Fast_Canonicalize(state *s);
  void Heuristic_Fast_Canonicalize(state *s);
  void Heuristic_Small_Mem_Canonicalize(state *s);
  void Heuristic_Fast_Normalize(state *s);

  void MultisetSort(state* s);
};


/********************
 Symmetry Class Members
 ********************/
void SymmetryClass::MultisetSort(state* s)
{
        mu_transmission_fine.MultisetSort();
        mu_stopped.MultisetSort();
        mu_v.MultisetSort();
        mu_d.MultisetSort();
        mu_TIME.MultisetSort();
        mu_g_n.MultisetSort();
        mu_all_event_true.MultisetSort();
        mu_h_n.MultisetSort();
        mu_f_n.MultisetSort();
        mu_a.MultisetSort();
        mu_running_time.MultisetSort();
        mu_running.MultisetSort();
        mu_engineblown.MultisetSort();
        mu_goal_reached.MultisetSort();
}
void SymmetryClass::Normalize(state* s)
{
  switch (args->sym_alg.mode) {
  case argsym_alg::Exhaustive_Fast_Canonicalize:
    Exhaustive_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Canonicalize:
    Heuristic_Fast_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Small_Mem_Canonicalize:
    Heuristic_Small_Mem_Canonicalize(s);
    break;
  case argsym_alg::Heuristic_Fast_Normalize:
    Heuristic_Fast_Normalize(s);
    break;
  default:
    Heuristic_Fast_Canonicalize(s);
  }
}

/********************
 Permute and Canonicalize function for different types
 ********************/
void mu_1_real_type::Permute(PermSet& Perm, int i) {};
void mu_1_real_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_real_type::Canonicalize(PermSet& Perm) {};
void mu_1_real_type::SimpleLimit(PermSet& Perm) {};
void mu_1_real_type::ArrayLimit(PermSet& Perm) {};
void mu_1_real_type::Limit(PermSet& Perm) {};
void mu_1_real_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };
void mu_1_integer::Permute(PermSet& Perm, int i) {};
void mu_1_integer::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_integer::Canonicalize(PermSet& Perm) {};
void mu_1_integer::SimpleLimit(PermSet& Perm) {};
void mu_1_integer::ArrayLimit(PermSet& Perm) {};
void mu_1_integer::Limit(PermSet& Perm) {};
void mu_1_integer::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for subrange type.\n"); };
void mu_1_TIME_type::Permute(PermSet& Perm, int i) {};
void mu_1_TIME_type::SimpleCanonicalize(PermSet& Perm) {};
void mu_1_TIME_type::Canonicalize(PermSet& Perm) {};
void mu_1_TIME_type::SimpleLimit(PermSet& Perm) {};
void mu_1_TIME_type::ArrayLimit(PermSet& Perm) {};
void mu_1_TIME_type::Limit(PermSet& Perm) {};
void mu_1_TIME_type::MultisetLimit(PermSet& Perm)
{ Error.Error("Internal: calling MultisetLimit for real type.\n"); };

/********************
 Auxiliary function for error trace printing
 ********************/
bool match(state* ns, StatePtr p)
{
  unsigned int i;
  static PermSet Perm;
  static state temp;
  StateCopy(&temp, ns);
  if (args->symmetry_reduction.value)
    {
      if (  args->sym_alg.mode == argsym_alg::Exhaustive_Fast_Canonicalize) {
        Perm.ResetToExplicit();
        for (i=0; i<Perm.count; i++)
          if (Perm.In(i))
            {
              if (ns != workingstate)
                  StateCopy(workingstate, ns);
              
              mu_transmission_fine.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_transmission_fine.MultisetSort();
              mu_stopped.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_stopped.MultisetSort();
              mu_v.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_v.MultisetSort();
              mu_d.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_d.MultisetSort();
              mu_TIME.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_g_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_all_event_true.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_h_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_f_n.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_a.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_a.MultisetSort();
              mu_running_time.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_running_time.MultisetSort();
              mu_running.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_running.MultisetSort();
              mu_engineblown.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_engineblown.MultisetSort();
              mu_goal_reached.Permute(Perm,i);
              if (args->multiset_reduction.value)
                mu_goal_reached.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
      else {
        Perm.ResetToSimple();
        Perm.SimpleToOne();
        if (ns != workingstate)
          StateCopy(workingstate, ns);

          mu_transmission_fine.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_transmission_fine.MultisetSort();
          mu_stopped.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_stopped.MultisetSort();
          mu_v.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_v.MultisetSort();
          mu_d.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_d.MultisetSort();
          mu_TIME.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_TIME.MultisetSort();
          mu_g_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_g_n.MultisetSort();
          mu_all_event_true.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_all_event_true.MultisetSort();
          mu_h_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_h_n.MultisetSort();
          mu_f_n.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_f_n.MultisetSort();
          mu_a.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_a.MultisetSort();
          mu_running_time.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_running_time.MultisetSort();
          mu_running.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_running.MultisetSort();
          mu_engineblown.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_engineblown.MultisetSort();
          mu_goal_reached.Permute(Perm,0);
          if (args->multiset_reduction.value)
            mu_goal_reached.MultisetSort();
        if (p.compare(workingstate)) {
          StateCopy(workingstate,&temp); return TRUE; }

        while (Perm.NextPermutation())
          {
            if (ns != workingstate)
              StateCopy(workingstate, ns);
              
              mu_transmission_fine.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_transmission_fine.MultisetSort();
              mu_stopped.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_stopped.MultisetSort();
              mu_v.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_v.MultisetSort();
              mu_d.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_d.MultisetSort();
              mu_TIME.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_TIME.MultisetSort();
              mu_g_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_g_n.MultisetSort();
              mu_all_event_true.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_all_event_true.MultisetSort();
              mu_h_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_h_n.MultisetSort();
              mu_f_n.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_f_n.MultisetSort();
              mu_a.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_a.MultisetSort();
              mu_running_time.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_running_time.MultisetSort();
              mu_running.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_running.MultisetSort();
              mu_engineblown.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_engineblown.MultisetSort();
              mu_goal_reached.Permute(Perm,0);
              if (args->multiset_reduction.value)
                mu_goal_reached.MultisetSort();
            if (p.compare(workingstate)) {
              StateCopy(workingstate,&temp); return TRUE; }
          }
        StateCopy(workingstate,&temp);
        return FALSE;
      }
    }
  if (!args->symmetry_reduction.value
      && args->multiset_reduction.value)
    {
      if (ns != workingstate)
          StateCopy(workingstate, ns);
      mu_transmission_fine.MultisetSort();
      mu_stopped.MultisetSort();
      mu_v.MultisetSort();
      mu_d.MultisetSort();
      mu_TIME.MultisetSort();
      mu_g_n.MultisetSort();
      mu_all_event_true.MultisetSort();
      mu_h_n.MultisetSort();
      mu_f_n.MultisetSort();
      mu_a.MultisetSort();
      mu_running_time.MultisetSort();
      mu_running.MultisetSort();
      mu_engineblown.MultisetSort();
      mu_goal_reached.MultisetSort();
      if (p.compare(workingstate)) {
        StateCopy(workingstate,&temp); return TRUE; }
      StateCopy(workingstate,&temp);
      return FALSE;
    }
  return (p.compare(ns));
}

/********************
 Canonicalization by fast exhaustive generation of
 all permutations
 ********************/
void SymmetryClass::Exhaustive_Fast_Canonicalize(state* s)
{
  unsigned int i;
  static state temp;
  Perm.ResetToExplicit();

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_transmission_fine.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_transmission_fine.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_stopped.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_stopped.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_v.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_v.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_d.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_d.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_TIME.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_TIME.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_g_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_g_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_all_event_true.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_all_event_true.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_h_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_h_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_f_n.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_f_n.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_a.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_a.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_running_time.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_running_time.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_running.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_running.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_engineblown.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_engineblown.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

  StateCopy(&temp, workingstate);
  ResetBestResult();
  for (i=0; i<Perm.count; i++)
    if (Perm.In(i))
      {
        StateCopy(workingstate, &temp);
        mu_goal_reached.Permute(Perm,i);
        if (args->multiset_reduction.value)
          mu_goal_reached.MultisetSort();
        SetBestResult(i, workingstate);
      }
  StateCopy(workingstate, &BestPermutedState);

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 ********************/
void SymmetryClass::Heuristic_Fast_Canonicalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Canonicalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and fast exhaustive generation of
 all permutations for other variables
 and use less local memory
 ********************/
void SymmetryClass::Heuristic_Small_Mem_Canonicalize(state* s)
{
  unsigned long cycle;
  static state temp;

  Perm.ResetToSimple();

};

/********************
 Normalization by fast simple variable canonicalization,
 fast simple scalarset array canonicalization,
 fast restriction on permutation set with simple scalarset array of scalarset,
 and for all other variables, pick any remaining permutation
 ********************/
void SymmetryClass::Heuristic_Fast_Normalize(state* s)
{
  int i;
  static state temp;

  Perm.ResetToSimple();

};

/********************
  Include
 ********************/
#include "upm_epilog.hpp"
