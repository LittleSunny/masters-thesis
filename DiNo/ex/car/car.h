#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

double round_k_digits(double n, unsigned k){
	double prec = pow(0.1,k);
	double round = (n>0) ? (n+prec/2) : (n-prec/2);
	return round-fmod(round,prec);
}

double ext_assignment(double n){
	return round_k_digits(n,2);
}

double increase_running_time_process_moving(double running_time, double T ) {
	 return round_k_digits(running_time+(( T ) * (1.00000)),2); 
}

double increase_d_process_moving(double d, double T, double v ) {
	 return round_k_digits(d+(( T ) * (v)),2); 
}

double increase_v_process_moving(double v, double T, double a ) {
	 return round_k_digits(v+(( T ) * (a)),2); 
}

double decrease_v_process_windresistance(double v, double T ) {
	 return round_k_digits(v-(( T ) * ((0.100000) * (((v) - (500.000)) * ((v) - (500.000))))),2); 
}

double assign_a_event_engineexplode() {
	 return round_k_digits(0.00000,2); 
}

double increase_a_action_accelerate(double a ) {
	 return round_k_digits(a+(1.00000),2); 
}

double decrease_a_action_decelerate(double a ) {
	 return round_k_digits(a-(1.00000),2); 
}

