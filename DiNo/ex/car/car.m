domain: file "car.pddl"
problem: file "prob01.pddl"
message: " Time Discretisation = 0.1"
message: " Digits for representing the integer part of a real =  5"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(7,4);
	integer: -1000..1000;

	 TIME_type: real(7,2);


const 
	 T:0.1;

	down_limit : -1.00000;
	up_limit : 1.00000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	a[pddlname:"a";] :  real_type;
	d[pddlname:"d";] :  real_type;
	running_time[pddlname:"running_time";] :  real_type;
	v[pddlname:"v";] :  real_type;


	running[pddlname: "running";] :  boolean;
	stopped[pddlname: "stopped";] :  boolean;
	engineblown[pddlname: "engineblown";] :  boolean;
	transmission_fine[pddlname: "transmission_fine";] :  boolean;
	goal_reached[pddlname: "goal_reached";] :  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun increase_running_time_process_moving(running_time : real_type ; T : real_type ; ): real_type "car.h" ;
externfun increase_d_process_moving(d : real_type ; T : real_type ; v : real_type ; ): real_type ;
externfun increase_v_process_moving(v : real_type ; T : real_type ; a : real_type ; ): real_type ;
externfun decrease_v_process_windresistance(v : real_type ; T : real_type ; ): real_type ;
externfun assign_a_event_engineexplode(): real_type ;
externfun increase_a_action_accelerate(a : real_type ; ): real_type ;
externfun decrease_a_action_decelerate(a : real_type ; ): real_type ;
procedure set_running(  value : boolean);
BEGIN
	running := value;
END;

function get_running(): boolean;
BEGIN
	return 	running;
END;

procedure set_stopped(  value : boolean);
BEGIN
	stopped := value;
END;

function get_stopped(): boolean;
BEGIN
	return 	stopped;
END;

procedure set_engineblown(  value : boolean);
BEGIN
	engineblown := value;
END;

function get_engineblown(): boolean;
BEGIN
	return 	engineblown;
END;

procedure set_transmission_fine(  value : boolean);
BEGIN
	transmission_fine := value;
END;

function get_transmission_fine(): boolean;
BEGIN
	return 	transmission_fine;
END;

procedure set_goal_reached(  value : boolean);
BEGIN
	goal_reached := value;
END;

function get_goal_reached(): boolean;
BEGIN
	return 	goal_reached;
END;







procedure process_moving (); 
BEGIN
IF ((running)) THEN 
running_time := increase_running_time_process_moving(running_time , T  );
d := increase_d_process_moving(d , T , v  );
v := increase_v_process_moving(v , T , a  );

ENDIF ; 

END;

procedure process_windresistance (); 
BEGIN
IF ((running) & ((( v) >= (500.000)))) THEN 
v := decrease_v_process_windresistance(v , T  );

ENDIF ; 

END;

function engineexplode () : boolean; 
BEGIN
IF ((running) & ((( a) >= (1.00000))) & ((( v) >= (100.000)))) THEN 
engineblown:= true; 
running:= false; 
a := assign_a_event_engineexplode();
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   engineexplode_triggered :  boolean;
BEGIN
 event_triggered := true;
   engineexplode_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! engineexplode_triggered) then 
   engineexplode_triggered := engineexplode();
   event_triggered := event_triggered | engineexplode_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_moving_enabled :  boolean;
   process_windresistance_enabled :  boolean;
BEGIN
 process_updated := false; end_while := false;
   process_moving_enabled := false;
   process_windresistance_enabled := false;
while (!end_while) do 
    if ((running) &  !process_moving_enabled) then
   process_updated := true;
   process_moving();
   process_moving_enabled := true;
   endif;

   if ((running) & ((( v) >= (500.000))) &  !process_windresistance_enabled) then
   process_updated := true;
   process_windresistance();
   process_windresistance_enabled := true;
   endif;

IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

action rule " accelerate " 
(running) & ((( a) < (up_limit))) ==> 
pddlname: " accelerate"; 
BEGIN
a := increase_a_action_accelerate(a  );

END;

action rule " decelerate " 
(running) & ((( a) > (down_limit))) ==> 
pddlname: " decelerate"; 
BEGIN
a := decrease_a_action_decelerate(a  );

END;

action rule " stop " 
((( v) = (0.00000))) & ((( d) >= (5.00000))) & (!(engineblown)) ==> 
pddlname: " stop"; 
BEGIN
goal_reached:= true; 

END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
set_running(false);

   set_stopped(false);

   set_engineblown(false);

   set_transmission_fine(false);

   set_goal_reached(false);

   d := 0.0 ;

   v := 0.0 ;

   a := 0.0 ;



   running_time := 0.0 ;

running:= true; 
transmission_fine:= true; 
engineblown:= false; 
running_time := 0.00000;
d := 0.00000;
a := 0.00000;
v := 0.00000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (goal_reached) & (!(engineblown)) & ((( running_time) <= (50.0000))) & (transmission_fine)& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


