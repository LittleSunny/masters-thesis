(define (domain test1)

(:requirements :strips :typing :fluents :disjunctive-preconditions :durative-actions :negative-preconditions)

(:types
	waypoint
	robot
)

(:predicates
	(robot_at ?v - robot ?wp - waypoint)
	(connected ?from ?to - waypoint)
	(visited ?wp - waypoint)
    (obstacled ?from ?to -waypoint)
)

(:functions
	(distance ?wpa ?wpb - waypoint)
	(total_distance)
)

;; Move between any two waypoints, avoiding terrain
(:action goto_waypoint
	:parameters (?v - robot ?from ?to - waypoint)
	:precondition (and
		 (robot_at ?v ?from) (not (obstacled ?from ?to )))
	:effect (and
		(visited ?to)
		(not (robot_at ?v ?from))
		(robot_at ?v ?to)
		(increase (total_distance) (distance ?from ?to) ))
)

)
