(define (problem testproblem)
(:domain test1)
(:objects
    kenny - robot
    ob1 ob2 -obstacle
    wp0 wp1 wp2 wp3 - waypoint
)
(:init
    (connected wp0 wp1)
    (connected wp0 wp2)
    (connected wp0 wp3)
    (connected wp1 wp0)
    (connected wp1 wp2)
    (connected wp1 wp3)
    (connected wp2 wp0)
    (connected wp2 wp1)
    (connected wp2 wp3)
    (connected wp3 wp0)
    (connected wp3 wp1)
    (connected wp3 wp2)
    (obstacled wp1 wp2)
    (obstacled wp2 wp1)
    (robot_at kenny wp0)
    (= (distance wp0 wp1) 3)
    (= (distance wp0 wp2) 2)
    (= (distance wp0 wp3) 1)
    (= (distance wp1 wp0) 3)
    (= (distance wp1 wp2) 1)
    (= (distance wp1 wp3) 2)
    (= (distance wp2 wp0) 2)
    (= (distance wp2 wp1) 1)
    (= (distance wp2 wp3) 1)
    (= (distance wp3 wp0) 1)
    (= (distance wp3 wp1) 2)
    (= (distance wp3 wp2) 1)
    (= (total_distance) 0)
)
(:goal (and
    (visited wp1)
    (visited wp2)
    (visited wp3)
)))
