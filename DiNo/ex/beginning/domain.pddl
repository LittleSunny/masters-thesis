(define (domain test1)

(:requirements :strips :typing :fluents :disjunctive-preconditions :durative-actions :negative-preconditions)

(:types
	robot
	obstacle
)

(:predicates
	(robot_at ?v - robot ?wp - waypoint)
	(obsacle_at ?o - obstacle ?wp - waypoint)
	(connected ?from ?to - waypoint)
	(visited ?wp - waypoint)
	;;(visited ?ob - obstacle)
    ;;(obstacled ?from ?to -waypoint)
    ;;(mission_complete)
)

(:functions
	(distance ?wpa ?wpb - waypoint)
	(total_distance)
)

;; Move between any two waypoints, avoiding terrain
(:action goto_waypoint
	:parameters (?v - robot ?from ?to - waypoint)
	:precondition (and
		 (robot_at ?v ?from) (not(obstacle_at ?o ?to)))
	:effect (and
		(visited ?to)
		(not (robot_at ?v ?from))
		(robot_at ?v ?to)
		(increase (total_distance) (distance ?from ?to) )
		(increase (waypoint_visited))
)

(:action calculate_distance
    :parameters(?from ?to waypoint)
    


)
