(define (domain test1)

(:requirements :strips :typing :fluents :disjunctive-preconditions :durative-actions :negative-preconditions)

(:types
	waypoint
	robot
)

(:predicates
	(robot_at ?v - robot ?wp - waypoint)
	(connected ?from ?to - waypoint)
	(visited ?wp - waypoint)
    (obstacled ?from ?to -waypoint)
)

(:functions
	(distance ?wpa ?wpb - waypoint)
	(total_distance)
)

;; Move between any two waypoints, avoiding terrain
(:action goto_waypoint
	:parameters (?v - robot ?from ?to - waypoint)
	:precondition (and
		 (robot_at ?v ?from) (not (obstacled ?from ?to )))
	:effect (and
		(visited ?to)
		(not (robot_at ?v ?from))
		(robot_at ?v ?to)
		(increase (total_distance) (distance ?from ?to) ))
)

)
(define (problem testproblem)
(:domain test1)
(:objects
    kenny - robot
    ob1 ob2 -obstacle
    wp0 wp1 wp2 wp3 - waypoint
)
(:init
    (connected wp0 wp1)
    (connected wp0 wp2)
    (connected wp0 wp3)
    (connected wp1 wp0)
    (connected wp1 wp2)
    (connected wp1 wp3)
    (connected wp2 wp0)
    (connected wp2 wp1)
    (connected wp2 wp3)
    (connected wp3 wp0)
    (connected wp3 wp1)
    (connected wp3 wp2)
    (obstacled wp1 wp2)
    (obstacled wp2 wp1)
    (robot_at kenny wp0)
    (= (distance wp0 wp1) 3)
    (= (distance wp0 wp2) 2)
    (= (distance wp0 wp3) 1)
    (= (distance wp1 wp0) 3)
    (= (distance wp1 wp2) 1)
    (= (distance wp1 wp3) 2)
    (= (distance wp2 wp0) 2)
    (= (distance wp2 wp1) 1)
    (= (distance wp2 wp3) 1)
    (= (distance wp3 wp0) 1)
    (= (distance wp3 wp1) 2)
    (= (distance wp3 wp2) 1)
    (= (total_distance) 0)
)
(:goal (and
    (visited wp1)
    (visited wp2)
    (visited wp3)
)))
