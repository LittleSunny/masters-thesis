(define (problem testproblem)
(:domain test1)
(:objects
    ;;kenny - robot
    ;;ob1 ob2 -obstacle
)
(:init
    (= (wp0) [0,0])
    (= (wp1) [0,3])
    (= (wp2) [0,2])
    (= (robot) (wp0))
)   

(:goal (visited wp1)
))
