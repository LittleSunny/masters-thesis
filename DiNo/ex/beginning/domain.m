domain: file "domain.pddl"
problem: file "problem.pddl"
message: " Time Discretisation = 0.1"
message: " Digits for representing the integer part of a real =  5"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(7,4);
	integer: -1000..1000;

	 TIME_type: real(7,2);

	robot : Enum {kenny};
	obstacle : Enum {ob1,ob2};
	waypoint : Enum {wp0,wp1,wp2,wp3};

const 
	 T:0.1;


var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	total_distance[pddlname:"total_distance";] :  real_type;


	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	connected[pddlname: "connected";] : Array [waypoint] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;
	obstacled[pddlname: "obstacled";] : Array [waypoint] of Array [waypoint] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun increase_total_distance_action_goto_waypoint(total_distance : real_type ; distance : real_type ; ): real_type "domain.h" ;
procedure set_robot_at( v : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[v][wp] := value;
END;

function get_robot_at( v : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[v][wp];
END;

procedure set_connected( from : waypoint ; to_ : waypoint ;  value : boolean);
BEGIN
	connected[from][to_] := value;
END;

function get_connected( from : waypoint ; to_ : waypoint): boolean;
BEGIN
	return 	connected[from][to_];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;

procedure set_obstacled( from : waypoint ; to_ : waypoint ;  value : boolean);
BEGIN
	obstacled[from][to_] := value;
END;

function get_obstacled( from : waypoint ; to_ : waypoint): boolean;
BEGIN
	return 	obstacled[from][to_];
END;





procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
BEGIN
 event_triggered := true;
while (event_triggered) do 
 event_triggered := false;
END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;BEGIN
 process_updated := false; end_while := false;
while (!end_while) do 
 IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

ruleset v:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_waypoint " 
(robot_at[v][from]) & (!(obstacled[from][to_])) ==> 
pddlname: " goto_waypoint"; 
BEGIN
visited[to_]:= true; 
robot_at[v][to_]:= true; 
robot_at[v][from]:= false; 
total_distance := increase_total_distance_action_goto_waypoint(total_distance , distance[from][to_]  );

END; 
END; 
END; 
END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for v : robot do 
  for wp : waypoint do 
    set_robot_at(v,wp, false);
END; END;  -- close for
   for from : waypoint do 
     for to_ : waypoint do 
       set_connected(from,to_, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   for from : waypoint do 
     for to_ : waypoint do 
       set_obstacled(from,to_, false);
END; END;  -- close for
   for wpa : waypoint do 
     for wpb : waypoint do 
       distance[wpa][wpb] := 0.0 ;
END; END;  -- close for
   total_distance := 0.0 ;

connected[wp0][wp1]:= true; 
connected[wp0][wp2]:= true; 
connected[wp0][wp3]:= true; 
connected[wp1][wp0]:= true; 
connected[wp1][wp2]:= true; 
connected[wp1][wp3]:= true; 
connected[wp2][wp0]:= true; 
connected[wp2][wp1]:= true; 
connected[wp2][wp3]:= true; 
connected[wp3][wp0]:= true; 
connected[wp3][wp1]:= true; 
connected[wp3][wp2]:= true; 
obstacled[wp1][wp2]:= true; 
obstacled[wp2][wp1]:= true; 
robot_at[kenny][wp0]:= true; 
distance[wp0][wp1] := 3.00000;
distance[wp0][wp2] := 2.00000;
distance[wp0][wp3] := 1.00000;
distance[wp1][wp0] := 3.00000;
distance[wp1][wp2] := 1.00000;
distance[wp1][wp3] := 2.00000;
distance[wp2][wp0] := 2.00000;
distance[wp2][wp1] := 1.00000;
distance[wp2][wp3] := 1.00000;
distance[wp3][wp0] := 1.00000;
distance[wp3][wp1] := 2.00000;
distance[wp3][wp2] := 1.00000;
total_distance := 0.00000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (visited[wp1]) & (visited[wp2]) & (visited[wp3])& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


