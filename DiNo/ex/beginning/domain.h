#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

double round_k_digits(double n, unsigned k){
	double prec = pow(0.1,k);
	double round = (n>0) ? (n+prec/2) : (n-prec/2);
	return round-fmod(round,prec);
}

double ext_assignment(double n){
	return round_k_digits(n,2);
}

double increase_total_distance_action_goto_waypoint(double total_distance, double distance ) {
	 return round_k_digits(total_distance+(distance),2); 
}

