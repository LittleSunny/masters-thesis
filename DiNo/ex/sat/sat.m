domain: file "sat.pddl"
problem: file "prob1.pddl"
message: " Time Discretisation = 0.1"
message: " Digits for representing the integer part of a real =  5"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(7,4);
	integer: -1000..1000;

	 TIME_type: real(7,2);

	destination : Enum {v_182580,v_186403,v_188151,v_195743,v_195417,v_195087,v_197845,v_205448,v_217618};
	spiral : Enum {spiral1,spiral2,spiral3,spiral4,spiral5,spiral6,spiral7,spiral8,spiral9,spiral10};
	waypoint : Enum {origin,s1s,s1e,s2s,s2e,s3s,s3e,s4s,s4e,s5s,s5e,s6s,s6e,s7s,s7e,s8s,s8e,s9s,s9e,s10s,s10e};

const 
	 T:0.1;

	expected_time : 0.00000;
	n_pattern : 0.00000;
	previous_expected_time : 0.00000;
	previous_total_probability : 0.00000;
	total_probability : 0.00000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	heuristic_approximation[pddlname:"heuristic-approximation";] : Array [pattern] of  real_type;
	heuristic_expected[pddlname:"heuristic-expected";] : Array [pattern] of  real_type;
	is_doing[pddlname:"is-doing";] : Array [pattern] of  real_type;
	n_pattern_active[pddlname:"n-pattern-active";] : Array [pattern] of  real_type;
	previous_probability[pddlname:"previous-probability";] : Array [destination] of  real_type;
	probability[pddlname:"probability";] : Array [destination] of  real_type;
	timefor[pddlname:"timefor";] : Array [pattern] of  real_type;
	fly_clock_started[pddlname:"fly";] : Array [waypoint] of Array [waypoint] of  boolean ;
	fly_clock [pddlname:"fly";] : Array [waypoint] of Array [waypoint] of  TIME_type ;


	at[pddlname: "at";] : Array [waypoint] of  boolean;
	active[pddlname: "active";] : Array [pattern] of  boolean;
	beginat[pddlname: "beginat";] : Array [waypoint] of Array [pattern] of  boolean;
	endat[pddlname: "endat";] : Array [waypoint] of Array [pattern] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
procedure set_at( p : waypoint ;  value : boolean);
BEGIN
	at[p] := value;
END;

function get_at( p : waypoint): boolean;
BEGIN
	return 	at[p];
END;

procedure set_active( p : pattern ;  value : boolean);
BEGIN
	active[p] := value;
END;

function get_active( p : pattern): boolean;
BEGIN
	return 	active[p];
END;

procedure set_beginat( w : waypoint ; p : pattern ;  value : boolean);
BEGIN
	beginat[w][p] := value;
END;

function get_beginat( w : waypoint ; p : pattern): boolean;
BEGIN
	return 	beginat[w][p];
END;

procedure set_endat( w : waypoint ; p : pattern ;  value : boolean);
BEGIN
	endat[w][p] := value;
END;

function get_endat( w : waypoint ; p : pattern): boolean;
BEGIN
	return 	endat[w][p];
END;














procedure process_fly( from : waypoint; tto : waypoint);
BEGIN
	 IF (fly_clock_started[from][tto]) THEN 
		 fly_clock[from][tto]:= fly_clock[from][tto] + T ;
	 ENDIF;

END;
function event_fly_failure( from : waypoint; tto : waypoint) : boolean; 
BEGIN
	 IF (fly_clock_started[from][tto])& !((true) ) THEN 
		 fly_clock[from][tto]:= fly_clock[from][tto]+ T ;
		 all_event_true := false ;
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;


procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   event_fly_failure_triggered :  Array [waypoint] of  Array [waypoint] of  boolean;
BEGIN
 event_triggered := true;
   for from : waypoint do 
     for tto : waypoint do 
           event_fly_failure_triggered[from][tto] := false;
           END;END; -- close for
while (event_triggered) do 
 event_triggered := false;
       for from : waypoint do 
         for tto : waypoint do 
           if(! event_fly_failure_triggered[from][tto]) then 
           event_fly_failure_triggered[from][tto] := event_fly_failure(from,tto);
           event_triggered := event_triggered | event_fly_failure_triggered[from][tto]; 
           endif;
END;END; -- close for
END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;
for from : waypoint do 
  for tto : waypoint do 
if (fly_clock[from][tto] > distance[from][tto]) then return true;
 endif;
END; -- close for 
END; -- close for 

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;
for from : waypoint do 
  for tto : waypoint do 
if (	fly_clock_started[from][tto] = true) then return true;
 endif;
END; -- close for 
END; -- close for 

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_fly_enabled :  Array [waypoint] of  Array [waypoint] of  boolean;
BEGIN
 process_updated := false; end_while := false;
   for from : waypoint do 
     for tto : waypoint do 
           process_fly_enabled[from][tto] := false;
           END;END; -- close for
while (!end_while) do 
        for from : waypoint do 
         for tto : waypoint do 
           if ((true)  & fly_clock_started[from][tto] &  !process_fly_enabled[from][tto]) then
           process_updated := true;
           process_fly(from,tto);
           process_fly_enabled[from][tto] := true;
           endif;
END;END; -- close for
IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;



ruleset from:waypoint do 
 ruleset tto:waypoint do 
 durative_start rule " fly_start " 
( !fly_clock_started[from][tto]) & (at[from]) & (!(=[from][tto])) & all_event_true ==> 
pddlname: " fly"; 
BEGIN
fly_clock_started[from][tto]:= true;
at[from]:= false; 

END; 
END; 
END; 



ruleset from:waypoint do 
 ruleset tto:waypoint do 
 durative_end rule " fly_end " 
( fly_clock_started[from][tto]) & (( fly_clock[from][tto]) = (distance[from][tto]))   & ((fly_clock[from][tto])  > 0.0) & all_event_true ==> 
pddlname: " fly"; 
BEGIN
fly_clock_started[from][tto]:= false;
fly_clock[from][tto]:= 0.0;
at[tto]:= true; 

END; 
END; 
END; 


clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;
IF (TIME = 1405.000) THEN
active[spiral1]:= true; 
ENDIF;
IF (TIME = 2379.000) THEN
active[spiral1]:= false; 
ENDIF;
IF (TIME = 1597.000) THEN
active[spiral2]:= true; 
ENDIF;
IF (TIME = 2909.000) THEN
active[spiral2]:= false; 
ENDIF;
IF (TIME = 1703.000) THEN
active[spiral3]:= true; 
ENDIF;
IF (TIME = 2720.000) THEN
active[spiral3]:= false; 
ENDIF;
IF (TIME = 1298.000) THEN
active[spiral4]:= true; 
ENDIF;
IF (TIME = 2200.000) THEN
active[spiral4]:= false; 
ENDIF;
IF (TIME = 2329.000) THEN
active[spiral5]:= true; 
ENDIF;
IF (TIME = 3400.000) THEN
active[spiral5]:= false; 
ENDIF;
IF (TIME = 1867.000) THEN
active[spiral6]:= true; 
ENDIF;
IF (TIME = 2976.000) THEN
active[spiral6]:= false; 
ENDIF;
IF (TIME = 3469.000) THEN
active[spiral7]:= true; 
ENDIF;
IF (TIME = 4840.000) THEN
active[spiral7]:= false; 
ENDIF;
IF (TIME = 2653.000) THEN
active[spiral8]:= true; 
ENDIF;
IF (TIME = 3985.000) THEN
active[spiral8]:= false; 
ENDIF;
IF (TIME = 4016.000) THEN
active[spiral9]:= true; 
ENDIF;
IF (TIME = 5923.000) THEN
active[spiral9]:= false; 
ENDIF;
IF (TIME = 3447.000) THEN
active[spiral10]:= true; 
ENDIF;
IF (TIME = 4902.000) THEN
active[spiral10]:= false; 
ENDIF;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for p : waypoint do 
  set_at(p, false);
END;  -- close for
   for p : pattern do 
     set_active(p, false);
END;  -- close for
   for w : waypoint do 
     for p : pattern do 
       set_beginat(w,p, false);
END; END;  -- close for
   for w : waypoint do 
     for p : pattern do 
       set_endat(w,p, false);
END; END;  -- close for
   for d : destination do 
     probability[d] := 0.0 ;
END;  -- close for
   for d : destination do 
     previous_probability[d] := 0.0 ;
END;  -- close for

   for p : pattern do 
     heuristic_approximation[p] := 0.0 ;
END;  -- close for
   for p : pattern do 
     heuristic_expected[p] := 0.0 ;
END;  -- close for

   for p : pattern do 
     is_doing[p] := 0.0 ;
END;  -- close for
   for p : pattern do 
     timefor[p] := 0.0 ;
END;  -- close for
   for p1 : waypoint do 
     for p2 : waypoint do 
       distance[p1][p2] := 0.0 ;
END; END;  -- close for



   for p : pattern do 
     n_pattern_active[p] := 0.0 ;
END;  -- close for
at[origin]:= true; 
beginat[s1s][spiral1]:= true; 
endat[s1e][spiral1]:= true; 
beginat[s2s][spiral2]:= true; 
endat[s2e][spiral2]:= true; 
beginat[s3s][spiral3]:= true; 
endat[s3e][spiral3]:= true; 
beginat[s4s][spiral4]:= true; 
endat[s4e][spiral4]:= true; 
beginat[s5s][spiral5]:= true; 
endat[s5e][spiral5]:= true; 
beginat[s6s][spiral6]:= true; 
endat[s6e][spiral6]:= true; 
beginat[s7s][spiral7]:= true; 
endat[s7e][spiral7]:= true; 
beginat[s8s][spiral8]:= true; 
endat[s8e][spiral8]:= true; 
beginat[s9s][spiral9]:= true; 
endat[s9e][spiral9]:= true; 
beginat[s10s][spiral10]:= true; 
endat[s10e][spiral10]:= true; 
heuristic_approximation[spiral1] := 0.00100000;
previous_probability[v_182580] := 0.100000;
probability[v_182580] := 0.100000;
previous_probability[v_186403] := 0.100000;
probability[v_186403] := 0.100000;
previous_probability[v_188151] := 0.100000;
probability[v_188151] := 0.100000;
previous_probability[v_195743] := 0.100000;
probability[v_195743] := 0.100000;
previous_probability[v_195417] := 0.100000;
probability[v_195417] := 0.100000;
previous_probability[v_195087] := 0.100000;
probability[v_195087] := 0.100000;
previous_probability[v_197845] := 0.100000;
probability[v_197845] := 0.100000;
previous_probability[v_205448] := 0.100000;
probability[v_205448] := 0.100000;
previous_probability[v_217618] := 0.100000;
probability[v_217618] := 0.100000;
is_doing[spiral1] := 0.00000;
timefor[spiral1] := 299.000;
n_pattern_active[spiral1] := 1892.00;
distance[origin][s1s] := 234.000;
distance[s1e][s1s] := 38.0000;
distance[s1e][s2s] := 269.000;
distance[s1e][s3s] := 31.0000;
distance[s1e][s4s] := 65.0000;
distance[s1e][s5s] := 169.000;
distance[s1e][s6s] := 52.0000;
distance[s1e][s7s] := 286.000;
distance[s1e][s8s] := 168.000;
distance[s1e][s9s] := 342.000;
distance[s1e][s10s] := 278.000;
is_doing[spiral2] := 0.00000;
timefor[spiral2] := 299.000;
n_pattern_active[spiral2] := 2253.00;
distance[origin][s2s] := 232.000;
distance[s2e][s1s] := 208.000;
distance[s2e][s2s] := 38.0000;
distance[s2e][s3s] := 256.000;
distance[s2e][s4s] := 183.000;
distance[s2e][s5s] := 256.000;
distance[s2e][s6s] := 283.000;
distance[s2e][s7s] := 404.000;
distance[s2e][s8s] := 360.000;
distance[s2e][s9s] := 526.000;
distance[s2e][s10s] := 402.000;
is_doing[spiral3] := 0.00000;
timefor[spiral3] := 299.000;
n_pattern_active[spiral3] := 2211.50;
distance[origin][s3s] := 280.000;
distance[s3e][s1s] := 87.0000;
distance[s3e][s2s] := 318.000;
distance[s3e][s3s] := 38.0000;
distance[s3e][s4s] := 115.000;
distance[s3e][s5s] := 167.000;
distance[s3e][s6s] := 20.0000;
distance[s3e][s7s] := 258.000;
distance[s3e][s8s] := 128.000;
distance[s3e][s9s] := 296.000;
distance[s3e][s10s] := 249.000;
is_doing[spiral4] := 0.00000;
timefor[spiral4] := 299.000;
n_pattern_active[spiral4] := 1749.00;
distance[origin][s4s] := 216.000;
distance[s4e][s1s] := 14.0000;
distance[s4e][s2s] := 243.000;
distance[s4e][s3s] := 49.0000;
distance[s4e][s4s] := 38.0000;
distance[s4e][s5s] := 169.000;
distance[s4e][s6s] := 75.0000;
distance[s4e][s7s] := 297.000;
distance[s4e][s8s] := 188.000;
distance[s4e][s9s] := 363.000;
distance[s4e][s10s] := 289.000;
is_doing[spiral5] := 0.00000;
timefor[spiral5] := 299.000;
n_pattern_active[spiral5] := 2864.50;
distance[origin][s5s] := 385.000;
distance[s5e][s1s] := 168.000;
distance[s5e][s2s] := 325.000;
distance[s5e][s3s] := 138.000;
distance[s5e][s4s] := 184.000;
distance[s5e][s5s] := 38.0000;
distance[s5e][s6s] := 135.000;
distance[s5e][s7s] := 127.000;
distance[s5e][s8s] := 95.0000;
distance[s5e][s9s] := 237.000;
distance[s5e][s10s] := 121.000;
is_doing[spiral6] := 0.00000;
timefor[spiral6] := 299.000;
n_pattern_active[spiral6] := 2421.50;
distance[origin][s6s] := 301.000;
distance[s6e][s1s] := 114.000;
distance[s6e][s2s] := 345.000;
distance[s6e][s3s] := 64.0000;
distance[s6e][s4s] := 142.000;
distance[s6e][s5s] := 177.000;
distance[s6e][s6s] := 38.0000;
distance[s6e][s7s] := 252.000;
distance[s6e][s8s] := 116.000;
distance[s6e][s9s] := 277.000;
distance[s6e][s10s] := 243.000;
is_doing[spiral7] := 0.00000;
timefor[spiral7] := 299.000;
n_pattern_active[spiral7] := 4154.50;
distance[origin][s7s] := 526.000;
distance[s7e][s1s] := 306.000;
distance[s7e][s2s] := 470.000;
distance[s7e][s3s] := 264.000;
distance[s7e][s4s] := 326.000;
distance[s7e][s5s] := 178.000;
distance[s7e][s6s] := 248.000;
distance[s7e][s7s] := 38.0000;
distance[s7e][s8s] := 136.000;
distance[s7e][s9s] := 119.000;
distance[s7e][s10s] := 34.0000;
is_doing[spiral8] := 0.00000;
timefor[spiral8] := 299.000;
n_pattern_active[spiral8] := 3319.00;
distance[origin][s8s] := 418.000;
distance[s8e][s1s] := 211.000;
distance[s8e][s2s] := 424.000;
distance[s8e][s3s] := 158.000;
distance[s8e][s4s] := 237.000;
distance[s8e][s5s] := 170.000;
distance[s8e][s6s] := 133.000;
distance[s8e][s7s] := 162.000;
distance[s8e][s8s] := 38.0000;
distance[s8e][s9s] := 159.000;
distance[s8e][s10s] := 151.000;
is_doing[spiral9] := 0.00000;
timefor[spiral9] := 299.000;
n_pattern_active[spiral9] := 4969.50;
distance[origin][s9s] := 591.000;
distance[s9e][s1s] := 384.000;
distance[s9e][s2s] := 587.000;
distance[s9e][s3s] := 331.000;
distance[s9e][s4s] := 409.000;
distance[s9e][s5s] := 305.000;
distance[s9e][s6s] := 306.000;
distance[s9e][s7s] := 195.000;
distance[s9e][s8s] := 197.000;
distance[s9e][s9s] := 38.0000;
distance[s9e][s10s] := 189.000;
is_doing[spiral10] := 0.00000;
timefor[spiral10] := 299.000;
n_pattern_active[spiral10] := 4174.50;
distance[origin][s10s] := 519.000;
distance[s10e][s1s] := 300.000;
distance[s10e][s2s] := 468.000;
distance[s10e][s3s] := 256.000;
distance[s10e][s4s] := 320.000;
distance[s10e][s5s] := 176.000;
distance[s10e][s6s] := 240.000;
distance[s10e][s7s] := 45.0000;
distance[s10e][s8s] := 127.000;
distance[s10e][s9s] := 113.000;
distance[s10e][s10s] := 38.0000;

-- durative action "fly" clock initialization
 for from : waypoint do 
   for tto : waypoint do 
     fly_clock_started[from][tto]:= false;
    fly_clock[from][tto]:= 0.0;
END; END; -- for ends

all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 ((( total_probability) > (0.00000)))& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


