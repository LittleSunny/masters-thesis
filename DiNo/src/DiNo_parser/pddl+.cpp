/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 17 "pddl+.yacc" /* yacc.c:339  */

/*
Error reporting:
Intention is to provide error token on most bracket expressions,
so synchronisation can occur on next CLOSE_BRAC.
Hence error should be generated for innermost expression containing error.
Expressions which cause errors return a NULL values, and parser
always attempts to carry on.
This won't behave so well if CLOSE_BRAC is missing.

Naming conventions:
Generally, the names should be similar to the PDDL2.1 spec.
During development, they have also been based on older PDDL specs,
older PDDL+ and TIM parsers, and this shows in places.

All the names of fields in the semantic value type begin with t_
Corresponding categories in the grammar begin with c_
Corresponding classes have no prefix.

PDDL grammar       yacc grammar      type of corresponding semantic val.  

thing+             c_things          thing_list
(thing+)           c_thing_list      thing_list

*/

#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <ctype.h>

// This is now copied locally to avoid relying on installation 
// of flex++.

//#include "FlexLexer.h"
//#include <FlexLexer.h>

#include "ptree.h"
#include "parse_error.h"

#define YYDEBUG 1 

int yyerror(const char *);


extern int yylex();

using namespace PDDL2UPMurphi_parser;


#line 117 "pddl+.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    OPEN_BRAC = 258,
    CLOSE_BRAC = 259,
    OPEN_SQ = 260,
    CLOSE_SQ = 261,
    DEFINE = 262,
    PDDLDOMAIN = 263,
    REQS = 264,
    EQUALITY = 265,
    STRIPS = 266,
    ADL = 267,
    NEGATIVE_PRECONDITIONS = 268,
    TYPING = 269,
    DISJUNCTIVE_PRECONDS = 270,
    EXT_PRECS = 271,
    UNIV_PRECS = 272,
    QUANT_PRECS = 273,
    COND_EFFS = 274,
    FLUENTS = 275,
    TIME = 276,
    DURATIVE_ACTIONS = 277,
    DURATION_INEQUALITIES = 278,
    CONTINUOUS_EFFECTS = 279,
    DERIVED_PREDICATES = 280,
    TIMED_INITIAL_LITERALS = 281,
    PREFERENCES = 282,
    CONSTRAINTS = 283,
    ACTION = 284,
    PROCESS = 285,
    EVENT = 286,
    DURATIVE_ACTION = 287,
    DERIVED = 288,
    CONSTANTS = 289,
    PREDS = 290,
    FUNCTIONS = 291,
    TYPES = 292,
    ARGS = 293,
    PRE = 294,
    CONDITION = 295,
    PREFERENCE = 296,
    START_PRE = 297,
    END_PRE = 298,
    EFFECTS = 299,
    INITIAL_EFFECT = 300,
    FINAL_EFFECT = 301,
    INVARIANT = 302,
    DURATION = 303,
    AT_START = 304,
    AT_END = 305,
    OVER_ALL = 306,
    AND = 307,
    OR = 308,
    EXISTS = 309,
    FORALL = 310,
    IMPLY = 311,
    NOT = 312,
    WHEN = 313,
    EITHER = 314,
    PROBLEM = 315,
    FORDOMAIN = 316,
    INITIALLY = 317,
    OBJECTS = 318,
    GOALS = 319,
    EQ = 320,
    LENGTH = 321,
    SERIAL = 322,
    PARALLEL = 323,
    METRIC = 324,
    MINIMIZE = 325,
    MAXIMIZE = 326,
    HASHT = 327,
    DURATION_VAR = 328,
    TOTAL_TIME = 329,
    INCREASE = 330,
    DECREASE = 331,
    SCALE_UP = 332,
    SCALE_DOWN = 333,
    ASSIGN = 334,
    GREATER = 335,
    GREATEQ = 336,
    LESS = 337,
    LESSEQ = 338,
    Q = 339,
    COLON = 340,
    ALWAYS = 341,
    SOMETIME = 342,
    WITHIN = 343,
    ATMOSTONCE = 344,
    SOMETIMEAFTER = 345,
    SOMETIMEBEFORE = 346,
    ALWAYSWITHIN = 347,
    HOLDDURING = 348,
    HOLDAFTER = 349,
    ISVIOLATED = 350,
    BOGUS = 351,
    NAME = 352,
    FUNCTION_SYMBOL = 353,
    INTVAL = 354,
    FLOATVAL = 355,
    AT_TIME = 356,
    HYPHEN = 357,
    PLUS = 358,
    MUL = 359,
    DIV = 360,
    UMINUS = 361
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 68 "pddl+.yacc" /* yacc.c:355  */

    parse_category* t_parse_category;

    effect_lists* t_effect_lists;
    effect* t_effect;
    simple_effect* t_simple_effect;
    cond_effect*   t_cond_effect;
    forall_effect* t_forall_effect;
    timed_effect* t_timed_effect;

    quantifier t_quantifier;
    metric_spec*  t_metric;
    optimization t_optimization;

    symbol* t_symbol;
    var_symbol*   t_var_symbol;
    pddl_type*    t_type;
    pred_symbol*  t_pred_symbol;
    func_symbol*  t_func_symbol;
    const_symbol* t_const_symbol;

    parameter_symbol_list* t_parameter_symbol_list;
    var_symbol_list* t_var_symbol_list;
    const_symbol_list* t_const_symbol_list;
    pddl_type_list* t_type_list;

    proposition* t_proposition;
    pred_decl* t_pred_decl;
    pred_decl_list* t_pred_decl_list;
    func_decl* t_func_decl;
    func_decl_list* t_func_decl_list;

    goal* t_goal;
    con_goal * t_con_goal;
    goal_list* t_goal_list;

    func_term* t_func_term;
    assignment* t_assignment;
    expression* t_expression;
    num_expression* t_num_expression;
    assign_op t_assign_op;
    comparison_op t_comparison_op;

    structure_def* t_structure_def;
    structure_store* t_structure_store;

    action* t_action_def;
    event* t_event_def;
    process* t_process_def;
    durative_action* t_durative_action_def;
    derivation_rule* t_derivation_rule;

    problem* t_problem;
    length_spec* t_length_spec;

    domain* t_domain;    

    pddl_req_flag t_pddl_req_flag;

    plan* t_plan;
    plan_step* t_step;

    int ival;
    double fval;

    char* cp;
    int t_dummy;

    var_symbol_table * vtab;

#line 332 "pddl+.cpp" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 349 "pddl+.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  16
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   882

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  107
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  121
/* YYNRULES -- Number of rules.  */
#define YYNRULES  323
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  743

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   361

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   240,   240,   243,   247,   249,   256,   257,   258,   259,
     261,   263,   265,   268,   273,   280,   287,   288,   293,   295,
     300,   302,   310,   318,   320,   328,   333,   335,   339,   341,
     348,   361,   369,   377,   389,   391,   397,   405,   414,   419,
     420,   424,   425,   433,   440,   449,   455,   457,   459,   466,
     472,   476,   480,   484,   489,   496,   501,   503,   507,   509,
     513,   518,   520,   522,   525,   529,   535,   536,   538,   540,
     549,   550,   551,   552,   553,   557,   558,   562,   564,   566,
     573,   574,   575,   577,   581,   583,   591,   593,   601,   606,
     609,   616,   617,   621,   623,   625,   629,   633,   640,   641,
     645,   647,   649,   656,   657,   658,   660,   665,   667,   669,
     671,   673,   678,   684,   690,   695,   696,   700,   701,   703,
     704,   708,   710,   712,   714,   719,   721,   724,   727,   733,
     734,   735,   743,   747,   750,   754,   759,   766,   771,   776,
     781,   786,   788,   790,   792,   794,   799,   801,   803,   805,
     807,   809,   810,   814,   816,   818,   824,   825,   828,   831,
     833,   851,   853,   855,   861,   862,   863,   864,   865,   877,
     879,   881,   888,   890,   892,   894,   898,   903,   905,   907,
     909,   916,   919,   923,   925,   927,   932,   935,   939,   941,
     944,   946,   948,   950,   952,   954,   956,   958,   960,   962,
     967,   969,   973,   975,   978,   981,   984,   990,   993,   997,
    1000,  1004,  1005,  1009,  1016,  1023,  1028,  1033,  1038,  1040,
    1047,  1049,  1056,  1058,  1065,  1067,  1074,  1075,  1079,  1080,
    1081,  1082,  1083,  1087,  1093,  1102,  1113,  1120,  1131,  1137,
    1147,  1153,  1168,  1175,  1177,  1179,  1183,  1185,  1190,  1193,
    1197,  1199,  1201,  1203,  1208,  1213,  1218,  1219,  1221,  1222,
    1224,  1226,  1227,  1228,  1229,  1230,  1232,  1236,  1245,  1248,
    1251,  1253,  1255,  1257,  1259,  1261,  1267,  1271,  1276,  1283,
    1290,  1291,  1292,  1293,  1294,  1296,  1297,  1298,  1301,  1304,
    1307,  1310,  1314,  1316,  1323,  1326,  1330,  1337,  1338,  1343,
    1344,  1345,  1346,  1347,  1349,  1353,  1354,  1355,  1356,  1360,
    1361,  1366,  1367,  1373,  1376,  1378,  1381,  1385,  1389,  1395,
    1399,  1405,  1413,  1414
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "OPEN_BRAC", "CLOSE_BRAC", "OPEN_SQ",
  "CLOSE_SQ", "DEFINE", "PDDLDOMAIN", "REQS", "EQUALITY", "STRIPS", "ADL",
  "NEGATIVE_PRECONDITIONS", "TYPING", "DISJUNCTIVE_PRECONDS", "EXT_PRECS",
  "UNIV_PRECS", "QUANT_PRECS", "COND_EFFS", "FLUENTS", "TIME",
  "DURATIVE_ACTIONS", "DURATION_INEQUALITIES", "CONTINUOUS_EFFECTS",
  "DERIVED_PREDICATES", "TIMED_INITIAL_LITERALS", "PREFERENCES",
  "CONSTRAINTS", "ACTION", "PROCESS", "EVENT", "DURATIVE_ACTION",
  "DERIVED", "CONSTANTS", "PREDS", "FUNCTIONS", "TYPES", "ARGS", "PRE",
  "CONDITION", "PREFERENCE", "START_PRE", "END_PRE", "EFFECTS",
  "INITIAL_EFFECT", "FINAL_EFFECT", "INVARIANT", "DURATION", "AT_START",
  "AT_END", "OVER_ALL", "AND", "OR", "EXISTS", "FORALL", "IMPLY", "NOT",
  "WHEN", "EITHER", "PROBLEM", "FORDOMAIN", "INITIALLY", "OBJECTS",
  "GOALS", "EQ", "LENGTH", "SERIAL", "PARALLEL", "METRIC", "MINIMIZE",
  "MAXIMIZE", "HASHT", "DURATION_VAR", "TOTAL_TIME", "INCREASE",
  "DECREASE", "SCALE_UP", "SCALE_DOWN", "ASSIGN", "GREATER", "GREATEQ",
  "LESS", "LESSEQ", "Q", "COLON", "ALWAYS", "SOMETIME", "WITHIN",
  "ATMOSTONCE", "SOMETIMEAFTER", "SOMETIMEBEFORE", "ALWAYSWITHIN",
  "HOLDDURING", "HOLDAFTER", "ISVIOLATED", "BOGUS", "NAME",
  "FUNCTION_SYMBOL", "INTVAL", "FLOATVAL", "AT_TIME", "HYPHEN", "PLUS",
  "MUL", "DIV", "UMINUS", "$accept", "mystartsymbol", "c_domain",
  "c_preamble", "c_domain_name", "c_domain_require_def", "c_reqs",
  "c_pred_decls", "c_pred_decl", "c_new_pred_symbol", "c_pred_symbol",
  "c_init_pred_symbol", "c_func_decls", "c_func_decl", "c_new_func_symbol",
  "c_typed_var_list", "c_var_symbol_list", "c_typed_consts",
  "c_const_symbols", "c_new_const_symbols", "c_typed_types",
  "c_parameter_symbols", "c_declaration_var_symbol", "c_var_symbol",
  "c_const_symbol", "c_new_const_symbol", "c_either_type",
  "c_new_primitive_type", "c_primitive_type", "c_new_primitive_types",
  "c_primitive_types", "c_init_els", "c_timed_initial_literal",
  "c_effects", "c_effect", "c_a_effect", "c_p_effect", "c_p_effects",
  "c_conj_effect", "c_da_effect", "c_da_effects", "c_timed_effect",
  "c_a_effect_da", "c_p_effect_da", "c_p_effects_da", "c_f_assign_da",
  "c_proc_effect", "c_proc_effects", "c_f_exp_da", "c_binary_expr_da",
  "c_duration_constraint", "c_d_op", "c_d_value", "c_duration_constraints",
  "c_neg_simple_effect", "c_pos_simple_effect", "c_init_neg_simple_effect",
  "c_init_pos_simple_effect", "c_forall_effect", "c_cond_effect",
  "c_assignment", "c_f_exp", "c_f_exp_t", "c_number", "c_f_head",
  "c_ground_f_head", "c_comparison_op", "c_pre_goal_descriptor",
  "c_pref_con_goal", "c_pref_goal", "c_pref_con_goal_list",
  "c_pref_goal_descriptor", "c_constraint_goal_list", "c_constraint_goal",
  "c_goal_descriptor", "c_pre_goal_descriptor_list", "c_goal_list",
  "c_quantifier", "c_forall", "c_exists", "c_proposition",
  "c_derived_proposition", "c_init_proposition", "c_predicates",
  "c_functions_def", "c_constraints_def", "c_constraints_probdef",
  "c_structure_defs", "c_structure_def", "c_rule_head",
  "c_derivation_rule", "c_action_def", "c_event_def", "c_process_def",
  "c_durative_action_def", "c_da_def_body", "c_da_gd", "c_da_gds",
  "c_timed_gd", "c_args_head", "c_require_key", "c_domain_constants",
  "c_type_names", "c_problem", "c_problem_body", "c_objects",
  "c_initial_state", "c_goals", "c_goal_spec", "c_metric_spec",
  "c_length_spec", "c_optimization", "c_ground_f_exp",
  "c_binary_ground_f_exp", "c_binary_ground_f_pexps",
  "c_binary_ground_f_mexps", "c_plan", "c_step_t_d", "c_step_d", "c_step",
  "c_float", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361
};
# endif

#define YYPACT_NINF -524

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-524)))

#define YYTABLE_NINF -70

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      53,    98,   -48,  -524,  -524,    55,    78,  -524,    77,  -524,
     191,   162,   261,    17,    77,    77,  -524,   262,  -524,   199,
    -524,   -29,   286,   301,   323,  -524,   328,    17,  -524,  -524,
     357,   397,  -524,   332,  -524,   676,   431,   448,   448,   448,
     448,   450,  -524,  -524,  -524,  -524,  -524,  -524,   448,   448,
    -524,  -524,   381,  -524,   473,   400,   336,    27,    38,    58,
      81,  -524,   386,   374,   298,  -524,   485,  -524,  -524,  -524,
    -524,  -524,   438,  -524,  -524,  -524,    87,  -524,   494,   542,
     499,   278,   501,   523,   497,   571,   497,   576,   497,   606,
     497,  -524,   608,   435,   386,   613,    91,   616,   605,   617,
     220,   618,   -39,   129,   620,  -524,   625,  -524,  -524,  -524,
    -524,  -524,  -524,  -524,  -524,  -524,  -524,  -524,  -524,  -524,
    -524,  -524,  -524,  -524,  -524,  -524,  -524,  -524,  -524,  -524,
    -524,   620,  -524,  -524,   620,   620,   131,   620,   620,   620,
     131,   131,   131,   629,  -524,  -524,  -524,   630,  -524,   631,
    -524,   637,  -524,   639,  -524,    33,  -524,  -524,   640,  -524,
     559,  -524,  -524,  -524,    96,  -524,  -524,  -524,  -524,    33,
    -524,  -524,  -524,   559,   544,   641,  -524,   654,   656,   233,
     660,   662,  -524,  -524,   620,   666,   620,   620,   620,   131,
     620,   559,   559,   559,   559,   559,   599,  -524,   386,   386,
    -524,   580,   674,   586,   695,  -524,   559,  -524,  -524,   696,
    -524,  -524,  -524,   620,   620,   168,  -524,  -524,  -524,  -524,
    -524,    69,   699,  -524,  -524,  -524,   642,  -524,  -524,  -524,
    -524,  -524,   710,  -524,   711,   713,   620,   620,   714,   715,
     716,   717,   718,   719,  -524,  -524,  -524,  -524,   559,  -524,
      33,  -524,   720,  -524,  -524,  -524,   253,   275,   620,   721,
     208,   412,  -524,    69,  -524,  -524,   559,   632,  -524,  -524,
    -524,   722,   723,  -524,   725,   691,   692,   693,   685,   169,
    -524,   559,   559,  -524,  -524,  -524,  -524,   730,  -524,  -524,
     638,  -524,  -524,  -524,    69,    69,    69,    69,   732,   733,
     734,  -524,  -524,   735,   737,   620,   620,   738,  -524,  -524,
    -524,  -524,  -524,  -524,  -524,   209,   210,    44,    69,    69,
      69,  -524,   620,   739,  -524,   409,   700,  -524,  -524,   701,
     702,   324,  -524,  -524,  -524,  -524,   743,   744,   745,   746,
     747,   272,   739,   739,   748,   739,   739,   739,   739,   739,
     109,  -524,   740,   750,   751,   750,   752,   753,  -524,  -524,
    -524,  -524,   673,   188,  -524,  -524,  -524,  -524,  -524,   387,
    -524,   386,  -524,   247,   190,   737,  -524,  -524,  -524,  -524,
    -524,  -524,  -524,  -524,   620,   754,   317,   559,   183,   755,
    -524,  -524,  -524,  -524,  -524,  -524,   146,   756,   757,   205,
     205,   345,   689,  -524,   760,   761,   762,   495,   763,  -524,
     377,   764,   670,   671,   767,  -524,  -524,    29,   768,   769,
    -524,  -524,  -524,   770,   405,   772,   620,   773,  -524,  -524,
     101,   101,  -524,  -524,   681,   694,  -524,  -524,    69,   306,
    -524,  -524,   267,  -524,  -524,  -524,  -524,   110,  -524,   774,
    -524,   132,  -524,  -524,  -524,  -524,  -524,  -524,   111,   775,
    -524,   533,  -524,  -524,  -524,  -524,   776,  -524,  -524,   737,
     777,   604,   778,   780,  -524,  -524,  -524,   780,   780,  -524,
     129,   781,   780,   559,   396,   375,     3,     3,   724,   726,
     782,  -524,   123,   620,   620,   620,  -524,   783,   785,   785,
    -524,   760,   101,   101,   101,   101,   101,   786,   725,   787,
     439,   559,   789,   101,  -524,  -524,  -524,  -524,   697,  -524,
     790,   687,  -524,  -524,    29,    29,    29,    29,   791,  -524,
     794,  -524,  -524,   101,   101,  -524,  -524,  -524,  -524,  -524,
     796,   797,  -524,  -524,   686,  -524,   798,   799,    69,    69,
    -524,   193,   801,   802,   803,   804,   805,   472,  -524,   619,
     806,  -524,  -524,  -524,  -524,   807,   504,   761,    50,    50,
      69,    69,    69,   559,   808,  -524,  -524,  -524,   809,   708,
     810,   131,   515,   211,   811,  -524,   812,   213,   214,    29,
      29,    29,    29,  -524,  -524,   517,    69,    69,  -524,   750,
      66,  -524,  -524,   813,   814,   815,  -524,  -524,  -524,  -524,
    -524,  -524,  -524,   101,   101,   101,   101,   101,  -524,  -524,
    -524,  -524,   816,   436,   817,   818,   819,   820,   821,   822,
     823,   824,  -524,   826,  -524,   827,  -524,  -524,  -524,  -524,
    -524,  -524,  -524,    29,  -524,    29,  -524,  -524,   -11,  -524,
    -524,  -524,  -524,   828,    69,   758,   829,   830,  -524,   539,
      47,    47,    47,    47,    47,  -524,    66,  -524,  -524,  -524,
    -524,  -524,  -524,  -524,   761,   561,   831,  -524,  -524,  -524,
    -524,   832,   833,  -524,  -524,   447,  -524,  -524,  -524,  -524,
     570,   765,   835,  -524,  -524,  -524,   836,   837,   838,   839,
      82,   840,   130,  -524,   842,  -524,  -524,  -524,    47,    47,
      47,    47,  -524,  -524,  -524,  -524,  -524,  -524,  -524,   725,
     843,   545,   559,    47,    47,    47,    47,   844,  -524,  -524,
     845,   846,   847,   848,   849,  -524,   826,  -524,  -524,  -524,
    -524,   850,  -524
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
     316,     0,     0,   323,   322,     0,     0,     3,   316,   318,
     320,     0,     0,    40,   316,   316,     1,     0,     2,     0,
     313,     0,     0,     0,     0,    51,     0,    40,   315,   314,
       0,     0,   317,     0,     5,     0,     0,     0,     0,     0,
       0,    12,   227,   232,   228,   229,   230,   231,     0,     0,
     321,    39,     0,   319,     0,     0,     0,     0,     0,     0,
       0,   233,    42,     0,     0,    57,     0,     4,     6,     9,
      10,    11,     0,   226,     8,     7,     0,    13,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    52,     0,    38,    42,     0,     0,     0,    19,     0,
       0,     0,    45,     0,     0,   279,     0,    15,    14,   256,
     257,   267,   259,   258,   260,   261,   262,   268,   263,   264,
     266,   265,   269,   270,   271,   272,   273,   274,   275,    16,
     223,     0,   187,   213,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   222,   236,   255,     0,   240,     0,
     238,     0,   242,     0,   276,     0,    41,   219,     0,    22,
      35,   218,    18,   221,     0,   220,    26,   277,    54,     0,
      56,    23,    24,    35,     0,     0,   200,     0,     0,     0,
       0,     0,   156,   157,     0,     0,     0,     0,     0,     0,
       0,    35,    35,    35,    35,    35,     0,    55,    42,    42,
      21,     0,     0,    33,     0,    30,    35,    57,    57,     0,
     210,   210,   214,     0,     0,   168,   164,   165,   166,   167,
      48,     0,     0,   211,   212,   234,     0,   190,   188,   186,
     191,   192,     0,   194,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    59,    37,    36,    49,    35,    20,
       0,    29,     0,    44,    43,   216,     0,     0,     0,     0,
       0,     0,   160,     0,   151,   152,    35,     0,   193,   195,
     196,     0,     0,   199,     0,     0,     0,     0,     0,     0,
      34,    35,    35,    28,   202,   209,   203,     0,   201,   215,
       0,    46,    48,    48,     0,     0,     0,     0,     0,     0,
       0,   197,   198,     0,     0,     0,     0,     0,    53,    58,
      32,    31,   204,    50,    47,     0,     0,     0,     0,     0,
       0,   206,     0,   287,   189,     0,     0,   169,   185,     0,
       0,     0,   245,   159,   158,   146,     0,     0,     0,     0,
       0,     0,   287,   287,     0,   287,   287,   287,   287,   287,
       0,   208,     0,     0,     0,     0,     0,     0,   134,   131,
     130,   129,     0,     0,   148,   147,   149,   150,   205,     0,
      64,    42,   290,     0,     0,     0,   280,   284,   278,   281,
     282,   283,   285,   286,     0,     0,     0,    35,     0,     0,
      70,    72,    71,    74,    73,   136,     0,     0,     0,     0,
       0,     0,     0,   241,     0,     0,     0,     0,     0,   176,
       0,     0,     0,     0,     0,   297,   298,     0,     0,     0,
     183,   170,   207,     0,     0,     0,     0,     0,   235,   116,
       0,     0,   239,   237,     0,     0,   125,   133,     0,     0,
     244,   246,     0,   243,    89,    90,   225,     0,   182,     0,
     224,     0,   289,    63,    62,    61,   138,   288,     0,     0,
     293,     0,   302,   163,   301,   300,     0,   291,   184,     0,
       0,     0,     0,    69,    76,    77,    78,    69,    69,    79,
       0,     0,    69,    35,     0,     0,     0,     0,     0,     0,
       0,   132,     0,     0,     0,     0,   249,     0,     0,     0,
      92,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    35,     0,     0,    25,    64,    48,   295,     0,   296,
       0,     0,    48,    48,     0,     0,     0,     0,     0,   292,
       0,    85,    83,     0,     0,    84,    66,    68,    67,   135,
       0,     0,   114,   115,     0,   155,     0,     0,     0,     0,
     126,     0,     0,     0,     0,     0,     0,     0,    97,     0,
       0,    99,   102,   100,   101,     0,     0,     0,     0,     0,
       0,     0,     0,    35,     0,   172,   174,   181,     0,     0,
       0,     0,     0,     0,     0,   304,     0,     0,     0,     0,
       0,     0,     0,   299,   171,     0,     0,     0,   140,     0,
       0,   112,   113,     0,     0,     0,   254,   250,   251,   252,
     247,   248,   106,     0,     0,     0,     0,     0,    93,    94,
      86,    91,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   173,     0,   137,     0,    65,   217,   294,   303,
     162,   161,   306,   309,   305,   311,   307,   308,     0,    75,
      80,    81,    82,     0,     0,     0,     0,     0,   253,     0,
       0,     0,     0,     0,     0,    88,     0,   142,    95,   143,
      96,   144,   145,   141,     0,     0,     0,    60,   310,   312,
     139,     0,     0,   127,   128,     0,    98,   105,   103,   104,
       0,     0,     0,   117,   119,   120,     0,     0,     0,     0,
       0,     0,     0,   182,     0,   175,   153,   154,     0,     0,
       0,     0,   118,   108,   109,   110,   111,   107,    87,     0,
       0,     0,    35,     0,     0,     0,     0,     0,   177,   179,
       0,     0,     0,     0,     0,   178,     0,   122,   121,   123,
     124,     0,   180
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -524,  -524,  -524,   407,  -524,   566,  -524,   759,  -524,  -524,
     690,  -524,  -524,  -524,  -524,  -171,   598,  -169,   834,   766,
     364,  -232,  -524,  -524,   192,  -524,   -63,  -524,  -154,  -524,
    -524,   340,  -524,  -227,  -350,  -524,  -524,  -524,  -524,  -523,
    -524,  -524,   359,  -524,  -524,   197,   378,  -524,  -224,  -524,
     458,   174,    70,  -524,  -336,  -328,  -524,  -524,  -337,  -322,
    -392,  -218,  -461,  -132,  -230,  -524,  -524,  -313,   496,   127,
     161,  -524,  -524,   -56,   -97,  -524,   655,  -524,   -80,  -524,
    -341,  -524,   355,  -524,  -524,  -524,  -524,  -524,   841,  -524,
    -524,  -524,  -524,  -524,  -524,  -524,  -367,  -524,  -443,   273,
    -524,  -524,  -524,  -524,   344,  -524,  -524,  -524,  -524,  -524,
    -524,  -524,  -406,  -524,   225,   224,   196,  -524,   851,  -524,
     853
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     5,     6,    36,    24,   342,    79,    97,    98,   160,
     220,   516,   100,   166,   206,   202,   203,    92,    26,    93,
     101,   260,   248,   314,   291,    94,   198,   170,   199,   102,
     279,   410,   453,   472,   389,   473,   474,   595,   390,   443,
     566,   444,   560,   561,   659,   562,   397,   484,   692,   693,
     332,   362,   490,   401,   475,   476,   454,   455,   477,   478,
     479,   491,   546,   264,   265,   465,   221,   326,   577,   676,
     510,   327,   179,   303,   328,   386,   256,   222,   427,   224,
     176,   104,   456,    38,    39,    40,   343,    41,    42,    66,
      43,    44,    45,    46,    47,   363,   440,   557,   441,   147,
     129,    48,    49,    18,   344,   345,   346,   375,   347,   348,
     349,   417,   643,   528,   644,   646,     7,     8,     9,    10,
      11
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      82,   143,   209,   263,   184,   398,   544,   175,   188,   189,
     190,   466,   395,   445,   395,   208,   393,   391,   393,   391,
     239,   240,   241,   242,   243,   392,   547,   392,    83,   245,
     246,   394,   461,   394,   178,   252,   196,   180,   181,    85,
     185,   186,   187,   621,   622,   298,   425,   261,   335,   553,
     690,    14,    15,   623,   171,    16,     1,   237,   168,    87,
     315,   316,   418,   169,   533,   534,   504,   505,   506,   261,
       3,     4,   261,   422,     2,   545,   317,   318,   319,   320,
      19,    17,    89,   395,   481,   261,   172,   232,   105,   234,
     235,   236,   158,   238,   223,   299,   282,   204,     2,   336,
     337,   338,   339,   462,   485,    12,   207,   625,   627,   605,
     310,   311,   174,    81,    25,   517,   258,   259,   589,   590,
     591,   592,   545,   229,    84,   309,   551,   463,   182,   183,
     197,   691,   395,    81,   567,    86,   395,   395,   654,   271,
     272,   395,   262,   182,   183,   262,   182,   183,   262,   182,
     183,   701,     3,     4,   682,    88,   530,   395,   395,   285,
     285,   287,   563,   563,   262,   182,   183,   262,   182,   183,
     564,   564,   -23,   308,   445,   445,     3,     4,    90,   518,
     262,   182,   183,   642,   106,   645,   647,   281,   159,   512,
     611,   414,   403,   205,   171,    13,    21,   513,   429,   262,
     486,   487,   411,   652,    20,    27,   384,   508,   329,   330,
      28,    29,   289,   333,   334,   637,   423,   640,   641,    27,
     552,   430,   431,   164,   165,   340,   172,   719,   404,   514,
     182,   183,   405,   515,   492,   424,    81,   228,   133,   645,
     425,   426,   493,   494,   495,   352,   536,    22,   171,   653,
     537,   538,   -23,   385,   395,   540,   174,   284,   395,   650,
     415,   416,   393,   391,    23,   -23,   197,   651,   497,    30,
     359,   392,   568,   569,   570,   571,   572,   394,   174,   286,
     172,    55,   445,   581,   583,   464,   360,   419,   361,    19,
     587,   588,   290,   290,   290,   290,    13,   290,   290,    99,
     369,   -27,   -27,   596,   597,    25,    25,    25,    25,    33,
      25,    25,   541,   409,   412,   413,   498,   499,   395,   500,
     325,   421,   133,   688,    34,   501,    35,   449,   131,   482,
     132,   689,    50,   133,   370,   371,   372,    80,   373,    81,
     578,   374,   502,   503,   504,   505,   506,   492,   331,   436,
     624,   626,   628,   629,   630,   493,   494,   495,   496,   149,
      52,   151,   507,   153,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   356,   357,    95,   358,    96,   624,   626,
     451,   452,   655,   660,   661,   662,   663,   664,   406,   359,
     407,   509,   464,   464,   464,   464,   554,   555,   556,   396,
     542,    78,   631,    53,   -17,   360,   470,   361,   471,   -69,
     -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,
     -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,    54,
     695,   695,   695,   695,   695,    67,   681,   696,   697,   698,
     699,    76,   407,   576,    68,    69,    70,    71,   700,   635,
     350,    35,   574,    72,   409,    74,    75,   464,   464,   464,
     464,   351,   211,   212,   133,   213,   214,    57,    58,    59,
      60,    61,   292,   293,   215,   439,   610,    77,   695,   695,
     695,   695,   338,    91,   723,   724,   725,   726,   103,   216,
     217,   218,   219,   695,   695,   695,   695,   -17,   107,   731,
     732,   733,   734,   130,   425,   144,   172,   442,   620,   292,
     293,   464,   171,   464,   294,   295,   296,   297,   451,   636,
     648,   649,   613,   614,   615,   616,   617,   145,   694,   694,
     694,   694,   694,   292,   293,   146,   447,   155,   294,   295,
     666,   297,   685,   686,   172,   131,   108,   448,   407,   729,
     133,   730,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   253,   254,   434,   435,   148,   694,   694,   694,   694,
     150,   134,   135,   136,   137,   138,   139,   140,   141,   142,
      37,   694,   694,   694,   694,   704,   210,   211,   212,   133,
     213,   214,   702,    37,    37,    37,    37,   520,    96,   215,
     152,   131,   154,   703,    37,    37,   133,   157,   603,   604,
     161,   163,   167,   174,   216,   217,   218,   219,   521,   177,
     522,   523,   191,   192,   193,   524,   525,   526,   527,   128,
     194,   172,   195,   201,   200,   225,   720,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   532,   226,   244,   133,
     227,   425,   426,   727,   230,   409,   231,   292,   293,   171,
     233,   612,   708,   709,   710,   711,   425,   247,   249,   533,
     534,   504,   505,   506,   171,    55,   376,   377,   250,   379,
     380,   381,   382,   383,   613,   614,   615,   616,   617,   251,
     255,   172,   266,   267,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,   268,   269,   172,   270,   273,   274,
     275,   276,   277,   278,   283,   288,   301,   302,    81,   300,
     304,   305,   306,   307,   312,   313,   321,   322,   323,   324,
     325,   331,   341,   387,   353,   354,   355,   364,   365,   366,
     367,   368,   378,   388,   396,   399,   400,   402,   420,   428,
     432,   433,   438,   439,   442,   488,   446,   450,   457,   458,
     459,   460,   467,   468,   469,   480,   483,   511,   489,   519,
     529,   531,   535,   471,   586,   539,   550,   558,   559,   573,
     600,   575,   579,   173,   585,   593,   584,   548,   594,   549,
     598,   599,   601,   602,   551,   514,   606,   607,   608,   609,
     618,   619,   632,   633,   634,   638,   639,   656,   657,   658,
     665,   667,   668,   669,   670,   671,   672,   673,   674,   675,
     682,   677,   680,   683,   684,   705,   706,   707,   712,   713,
     714,   715,   716,   717,   718,   722,   280,   728,   735,   736,
     737,   738,   739,   740,   742,   582,   687,   162,   565,   437,
     156,    51,   543,   741,   721,   408,   257,   580,   678,   679,
       0,     0,     0,    32,    31,     0,     0,     0,     0,     0,
       0,     0,    73
};

static const yytype_int16 yycheck[] =
{
      56,    81,   173,   221,   136,   355,     3,   104,   140,   141,
     142,   417,   353,   405,   355,   169,   353,   353,   355,   355,
     191,   192,   193,   194,   195,   353,   487,   355,     1,   198,
     199,   353,     3,   355,   131,   206,     3,   134,   135,     1,
     137,   138,   139,   566,   567,   263,    57,     3,     4,   492,
       3,    99,   100,     3,    65,     0,     3,   189,    97,     1,
     292,   293,   375,   102,    75,    76,    77,    78,    79,     3,
      99,   100,     3,   386,    21,    72,   294,   295,   296,   297,
       3,     3,     1,   424,   425,     3,    97,   184,     1,   186,
     187,   188,     1,   190,   174,   266,   250,     1,    21,   317,
     318,   319,   320,    74,     3,     7,   169,   568,   569,   552,
     281,   282,     3,     3,    97,     4,   213,   214,   524,   525,
     526,   527,    72,   179,    97,   279,     3,    98,    99,   100,
      97,    84,   473,     3,   501,    97,   477,   478,    72,   236,
     237,   482,    98,    99,   100,    98,    99,   100,    98,    99,
     100,   674,    99,   100,    72,    97,   469,   498,   499,   256,
     257,   258,   498,   499,    98,    99,   100,    98,    99,   100,
     498,   499,     4,     4,   566,   567,    99,   100,    97,    68,
      98,    99,   100,   589,    97,   591,   592,   250,    97,    57,
     557,     1,     4,    97,    65,    97,     5,    65,    52,    98,
     430,   431,   371,   595,     8,    13,    97,    97,   305,   306,
      14,    15,     4,     4,     4,     4,   387,     4,     4,    27,
      97,    75,    76,     3,     4,   322,    97,    97,    40,    97,
      99,   100,    44,   101,    41,    52,     3,     4,    55,   645,
      57,    58,    49,    50,    51,   325,   473,    85,    65,   599,
     477,   478,    84,   350,   595,   482,     3,     4,   599,   595,
      70,    71,   599,   599,     3,    97,    97,   595,     1,     7,
      65,   599,   502,   503,   504,   505,   506,   599,     3,     4,
      97,     9,   674,   513,   516,   417,    81,   384,    83,     3,
     522,   523,    84,    84,    84,    84,    97,    84,    84,     1,
      28,     3,     4,   533,   534,    97,    97,    97,    97,     8,
      97,    97,   483,   369,    67,    68,    49,    50,   659,    52,
       3,     4,    55,   659,     1,    58,     3,   407,    50,   426,
      52,   659,     4,    55,    62,    63,    64,     1,    66,     3,
     511,    69,    75,    76,    77,    78,    79,    41,     3,     4,
     568,   569,   570,   571,   572,    49,    50,    51,    52,    86,
       3,    88,   442,    90,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    49,    50,     1,    52,     3,   596,   597,
       3,     4,   600,   613,   614,   615,   616,   617,     1,    65,
       3,   447,   524,   525,   526,   527,   493,   494,   495,     3,
       4,     1,   573,     6,     4,    81,     1,    83,     3,     4,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    97,
     660,   661,   662,   663,   664,     4,   654,   661,   662,   663,
     664,    60,     3,     4,    37,    38,    39,    40,   666,   581,
      41,     3,   508,     3,   510,    48,    49,   589,   590,   591,
     592,    52,    53,    54,    55,    56,    57,    29,    30,    31,
      32,    33,    97,    98,    65,     3,     4,     4,   708,   709,
     710,   711,   700,    97,   708,   709,   710,   711,     3,    80,
      81,    82,    83,   723,   724,   725,   726,    97,     4,   723,
     724,   725,   726,     4,    57,     4,    97,     3,     4,    97,
      98,   643,    65,   645,   102,   103,   104,   105,     3,     4,
       3,     4,    75,    76,    77,    78,    79,     4,   660,   661,
     662,   663,   664,    97,    98,    38,    41,   102,   102,   103,
     104,   105,     3,     4,    97,    50,     4,    52,     3,     4,
      55,   722,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,   207,   208,   399,   400,     4,   708,   709,   710,   711,
       4,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      24,   723,   724,   725,   726,   675,    52,    53,    54,    55,
      56,    57,    41,    37,    38,    39,    40,    74,     3,    65,
       4,    50,     4,    52,    48,    49,    55,     4,   548,   549,
       4,     4,     4,     3,    80,    81,    82,    83,    95,     4,
      97,    98,     3,     3,     3,   102,   103,   104,   105,    97,
       3,    97,     3,    84,     4,     4,   702,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    52,     3,    59,    55,
       4,    57,    58,   719,     4,   721,     4,    97,    98,    65,
       4,    52,   102,   103,   104,   105,    57,    97,     4,    75,
      76,    77,    78,    79,    65,     9,   342,   343,   102,   345,
     346,   347,   348,   349,    75,    76,    77,    78,    79,     4,
       4,    97,     3,    61,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,     4,     4,    97,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     3,    97,
      39,    39,    39,    48,     4,    97,     4,     4,     4,     4,
       3,     3,     3,     3,    44,    44,    44,     4,     4,     4,
       4,     4,     4,     3,     3,     3,     3,    84,     4,     4,
       4,     4,    73,     3,     3,    84,     4,     4,     4,    99,
      99,     4,     4,     4,     4,     3,     3,     3,    84,     4,
       4,     4,     4,     3,    97,     4,     4,     4,     3,     3,
     104,     4,     3,   103,     4,     4,    99,    73,     4,    73,
       4,     4,     4,     4,     3,    97,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     3,
      72,     4,     4,     4,     4,     4,     4,     4,    73,     4,
       4,     4,     4,     4,     4,     3,   248,     4,     4,     4,
       4,     4,     4,     4,     4,   515,   659,    98,   499,   401,
      94,    27,   484,   736,   703,   369,   211,   512,   643,   645,
      -1,    -1,    -1,    22,    21,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    41
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,    21,    99,   100,   108,   109,   223,   224,   225,
     226,   227,     7,    97,    99,   100,     0,     3,   210,     3,
     223,     5,    85,     3,   111,    97,   125,   131,   223,   223,
       7,   227,   225,     8,     1,     3,   110,   112,   190,   191,
     192,   194,   195,   197,   198,   199,   200,   201,   208,   209,
       4,   125,     3,     6,    97,     9,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,   196,     4,   110,   110,
     110,   110,     3,   195,   110,   110,    60,     4,     1,   113,
       1,     3,   180,     1,    97,     1,    97,     1,    97,     1,
      97,    97,   124,   126,   132,     1,     3,   114,   115,     1,
     119,   127,   136,     3,   188,     1,    97,     4,     4,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    97,   207,
       4,    50,    52,    55,    86,    87,    88,    89,    90,    91,
      92,    93,    94,   185,     4,     4,    38,   206,     4,   206,
       4,   206,     4,   206,     4,   102,   126,     4,     1,    97,
     116,     4,   114,     4,     3,     4,   120,     4,    97,   102,
     134,    65,    97,   117,     3,   181,   187,     4,   181,   179,
     181,   181,    99,   100,   170,   181,   181,   181,   170,   170,
     170,     3,     3,     3,     3,     3,     3,    97,   133,   135,
       4,    84,   122,   123,     1,    97,   121,   133,   135,   122,
      52,    53,    54,    56,    57,    65,    80,    81,    82,    83,
     117,   173,   184,   185,   186,     4,     3,     4,     4,   180,
       4,     4,   181,     4,   181,   181,   181,   170,   181,   122,
     122,   122,   122,   122,    59,   124,   124,    97,   129,     4,
     102,     4,   122,   127,   127,     4,   183,   183,   181,   181,
     128,     3,    98,   168,   170,   171,     3,    61,     4,     4,
       4,   181,   181,     4,     4,     4,     4,     4,     4,   137,
     123,   133,   135,     4,     4,   181,     4,   181,     4,     4,
      84,   131,    97,    98,   102,   103,   104,   105,   168,   122,
      97,     4,     4,   180,    39,    39,    39,    48,     4,   135,
     122,   122,     4,    97,   130,   128,   128,   168,   168,   168,
     168,     4,     4,     4,     4,     3,   174,   178,   181,   181,
     181,     3,   157,     4,     4,     4,   168,   168,   168,   168,
     181,     3,   112,   193,   211,   212,   213,   215,   216,   217,
      41,    52,   185,    44,    44,    44,    49,    50,    52,    65,
      81,    83,   158,   202,     4,     4,     4,     4,     4,    28,
      62,    63,    64,    66,    69,   214,   211,   211,     4,   211,
     211,   211,   211,   211,    97,   181,   182,     3,     3,   141,
     145,   161,   162,   165,   166,   187,     3,   153,   141,     3,
       3,   160,    84,     4,    40,    44,     1,     3,   175,   180,
     138,   124,    67,    68,     1,    70,    71,   218,   174,   181,
       4,     4,   174,   122,    52,    57,    58,   185,     4,    52,
      75,    76,     4,     4,   158,   158,     4,   157,    73,     3,
     203,   205,     3,   146,   148,   167,     4,    41,    52,   185,
       4,     3,     4,   139,   163,   164,   189,     4,    99,    99,
       4,     3,    74,    98,   170,   172,   219,     4,     4,     4,
       1,     3,   140,   142,   143,   161,   162,   165,   166,   167,
       3,   187,   181,     3,   154,     3,   171,   171,    84,    84,
     159,   168,    41,    49,    50,    51,    52,     1,    49,    50,
      52,    58,    75,    76,    77,    78,    79,   185,    97,   180,
     177,     3,    57,    65,    97,   101,   118,     4,    68,     4,
      74,    95,    97,    98,   102,   103,   104,   105,   220,     4,
     174,     4,    52,    75,    76,     4,   140,   140,   140,     4,
     140,   122,     4,   153,     3,    72,   169,   169,    73,    73,
       4,     3,    97,   205,   181,   181,   181,   204,     4,     3,
     149,   150,   152,   161,   162,   149,   147,   203,   171,   171,
     171,   171,   171,     3,   180,     4,     4,   175,   122,     3,
     189,   171,   138,   128,    99,     4,    97,   128,   128,   219,
     219,   219,   219,     4,     4,   144,   171,   171,     4,     4,
     104,     4,     4,   159,   159,   205,     4,     4,     4,     4,
       4,   203,    52,    75,    76,    77,    78,    79,     4,     4,
       4,   146,   146,     3,   168,   169,   168,   169,   168,   168,
     168,   122,     4,     4,     4,   170,     4,     4,     4,     4,
       4,     4,   219,   219,   221,   219,   222,   219,     3,     4,
     161,   162,   167,   141,    72,   168,     4,     4,     4,   151,
     171,   171,   171,   171,   171,     4,   104,     4,     4,     4,
       4,     4,     4,     4,     4,     3,   176,     4,   221,   222,
       4,   168,    72,     4,     4,     3,     4,   152,   161,   162,
       3,    84,   155,   156,   170,   171,   155,   155,   155,   155,
     168,   146,    41,    52,   185,     4,     4,     4,   102,   103,
     104,   105,    73,     4,     4,     4,     4,     4,     4,    97,
     180,   177,     3,   155,   155,   155,   155,   180,     4,     4,
     122,   155,   155,   155,   155,     4,     4,     4,     4,     4,
       4,   176,     4
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   107,   108,   108,   109,   109,   110,   110,   110,   110,
     110,   110,   110,   111,   112,   112,   113,   113,   114,   114,
     115,   115,   116,   117,   117,   118,   119,   119,   120,   120,
     121,   122,   122,   122,   123,   123,   124,   124,   124,   125,
     125,   126,   126,   127,   127,   127,   128,   128,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   136,   137,   137,
     138,   138,   138,   138,   138,   139,   140,   140,   140,   140,
     141,   141,   141,   141,   141,   142,   142,   143,   143,   143,
     144,   144,   144,   144,   145,   145,   146,   146,   146,   146,
     146,   147,   147,   148,   148,   148,   148,   148,   149,   149,
     150,   150,   150,   151,   151,   151,   151,   152,   152,   152,
     152,   152,   153,   153,   153,   154,   154,   155,   155,   155,
     155,   156,   156,   156,   156,   157,   157,   157,   157,   158,
     158,   158,   159,   160,   160,   161,   162,   163,   164,   165,
     166,   167,   167,   167,   167,   167,   168,   168,   168,   168,
     168,   168,   168,   169,   169,   169,   170,   170,   171,   171,
     171,   172,   172,   172,   173,   173,   173,   173,   173,   174,
     174,   174,   175,   175,   175,   175,   175,   176,   176,   176,
     176,   177,   177,   178,   178,   178,   179,   179,   180,   180,
     180,   180,   180,   180,   180,   180,   180,   180,   180,   180,
     181,   181,   181,   181,   181,   181,   181,   182,   182,   183,
     183,   184,   184,   185,   186,   187,   188,   189,   190,   190,
     191,   191,   192,   192,   193,   193,   194,   194,   195,   195,
     195,   195,   195,   196,   197,   198,   198,   199,   199,   200,
     200,   201,   201,   202,   202,   202,   203,   203,   204,   204,
     205,   205,   205,   205,   205,   206,   207,   207,   207,   207,
     207,   207,   207,   207,   207,   207,   207,   207,   207,   207,
     207,   207,   207,   207,   207,   207,   208,   209,   210,   210,
     211,   211,   211,   211,   211,   211,   211,   211,   212,   213,
     214,   215,   216,   216,   217,   217,   217,   218,   218,   219,
     219,   219,   219,   219,   219,   220,   220,   220,   220,   221,
     221,   222,   222,   223,   223,   223,   223,   224,   224,   225,
     225,   226,   227,   227
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     1,     5,     4,     2,     2,     2,     2,
       2,     2,     1,     4,     4,     4,     2,     0,     2,     1,
       4,     3,     1,     1,     1,     1,     2,     0,     4,     3,
       1,     4,     4,     1,     3,     0,     4,     4,     1,     2,
       0,     2,     0,     4,     4,     1,     2,     3,     0,     1,
       1,     1,     1,     4,     1,     1,     2,     0,     2,     0,
       6,     2,     2,     2,     0,     4,     2,     2,     2,     0,
       1,     1,     1,     1,     1,     4,     1,     1,     1,     1,
       2,     2,     2,     0,     4,     4,     4,     7,     5,     1,
       1,     2,     0,     4,     4,     5,     5,     3,     4,     1,
       1,     1,     1,     2,     2,     2,     0,     5,     5,     5,
       5,     5,     5,     5,     4,     2,     0,     1,     2,     1,
       1,     5,     5,     5,     5,     4,     6,     9,     9,     1,
       1,     1,     1,     2,     0,     4,     1,     4,     1,     7,
       5,     5,     5,     5,     5,     5,     4,     5,     5,     5,
       5,     1,     1,     5,     5,     1,     1,     1,     4,     4,
       1,     4,     4,     1,     1,     1,     1,     1,     1,     1,
       4,     7,     4,     5,     4,     7,     1,     4,     5,     4,
       7,     2,     0,     4,     5,     1,     2,     0,     4,     7,
       4,     4,     4,     5,     4,     5,     5,     6,     6,     5,
       1,     4,     4,     4,     5,     7,     5,     2,     0,     2,
       0,     1,     1,     1,     1,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     2,     1,     1,     1,
       1,     1,     1,     1,     5,    12,     4,    12,     4,    12,
       4,    11,     4,     3,     3,     0,     1,     4,     2,     0,
       4,     4,     4,     5,     4,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     4,     4,    12,     5,
       2,     2,     2,     2,     2,     2,     2,     0,     4,     4,
       1,     4,     5,     4,     7,     5,     5,     1,     1,     3,
       1,     1,     1,     4,     3,     3,     3,     3,     3,     1,
       2,     1,     2,     2,     3,     3,     0,     3,     1,     4,
       1,     4,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 240 "pddl+.yacc" /* yacc.c:1646  */
    {top_thing= (yyvsp[-1].t_domain); current_analysis->the_domain= (yyvsp[-1].t_domain); top_thing= (yyvsp[0].t_problem);
    current_analysis->the_problem= (yyvsp[0].t_problem); requires(E_TYPING);
    }
#line 2007 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 3:
#line 243 "pddl+.yacc" /* yacc.c:1646  */
    {top_thing= (yyvsp[0].t_plan); }
#line 2013 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 4:
#line 248 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[-1].t_domain); (yyval.t_domain)->name= (yyvsp[-2].cp);delete [] (yyvsp[-2].cp);}
#line 2019 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 5:
#line 250 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_domain)=static_cast<domain*>(NULL);
       	log_error(E_FATAL,"Syntax error in domain"); }
#line 2026 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 6:
#line 256 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain); (yyval.t_domain)->req= (yyvsp[-1].t_pddl_req_flag);}
#line 2032 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 7:
#line 257 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain); (yyval.t_domain)->types= (yyvsp[-1].t_type_list);}
#line 2038 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 8:
#line 258 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain); (yyval.t_domain)->constants= (yyvsp[-1].t_const_symbol_list);}
#line 2044 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 9:
#line 259 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain); 
                                       (yyval.t_domain)->predicates= (yyvsp[-1].t_pred_decl_list); }
#line 2051 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 10:
#line 261 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain); 
                                       (yyval.t_domain)->functions= (yyvsp[-1].t_func_decl_list); }
#line 2058 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 11:
#line 263 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain);
   										(yyval.t_domain)->constraints = (yyvsp[-1].t_con_goal);}
#line 2065 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 12:
#line 265 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= new domain((yyvsp[0].t_structure_store)); }
#line 2071 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 13:
#line 268 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.cp)=(yyvsp[-1].cp);}
#line 2077 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 14:
#line 274 "pddl+.yacc" /* yacc.c:1646  */
    {
	// Stash in analysis object --- we need to refer to it during parse
	//   but domain object is not created yet,
	current_analysis->req |= (yyvsp[-1].t_pddl_req_flag);
	(yyval.t_pddl_req_flag)=(yyvsp[-1].t_pddl_req_flag);
    }
#line 2088 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 15:
#line 281 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
       log_error(E_FATAL,"Syntax error in requirements declaration.");
       (yyval.t_pddl_req_flag)= 0; }
#line 2096 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 16:
#line 287 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pddl_req_flag)= (yyvsp[-1].t_pddl_req_flag) | (yyvsp[0].t_pddl_req_flag); }
#line 2102 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 17:
#line 288 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pddl_req_flag)= 0; }
#line 2108 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 18:
#line 294 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pred_decl_list)=(yyvsp[0].t_pred_decl_list); (yyval.t_pred_decl_list)->push_front((yyvsp[-1].t_pred_decl));}
#line 2114 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 19:
#line 296 "pddl+.yacc" /* yacc.c:1646  */
    {  (yyval.t_pred_decl_list)=new pred_decl_list;
           (yyval.t_pred_decl_list)->push_front((yyvsp[0].t_pred_decl)); }
#line 2121 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 20:
#line 301 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pred_decl)= new pred_decl((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_var_symbol_list),current_analysis->var_tab_stack.pop());}
#line 2127 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 21:
#line 303 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
        // hope someone makes this error someday
        log_error(E_FATAL,"Syntax error in predicate declaration.");
	(yyval.t_pred_decl)= NULL; }
#line 2136 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 22:
#line 311 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.symbol_put((yyvsp[0].cp));
           current_analysis->var_tab_stack.push(
           				current_analysis->buildPredTab());
           delete [] (yyvsp[0].cp); }
#line 2145 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 23:
#line 318 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.symbol_ref("="); 
	      requires(E_EQUALITY); }
#line 2152 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 24:
#line 320 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2158 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 25:
#line 328 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2164 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 26:
#line 334 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_func_decl_list)=(yyvsp[-1].t_func_decl_list); (yyval.t_func_decl_list)->push_back((yyvsp[0].t_func_decl));}
#line 2170 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 27:
#line 335 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_decl_list)=new func_decl_list; }
#line 2176 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 28:
#line 340 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_func_decl)= new func_decl((yyvsp[-2].t_func_symbol),(yyvsp[-1].t_var_symbol_list),current_analysis->var_tab_stack.pop());}
#line 2182 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 29:
#line 342 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
	 log_error(E_FATAL,"Syntax error in functor declaration.");
	 (yyval.t_func_decl)= NULL; }
#line 2190 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 30:
#line 349 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_symbol)=current_analysis->func_tab.symbol_put((yyvsp[0].cp));
           current_analysis->var_tab_stack.push(
           		current_analysis->buildFuncTab()); 
           delete [] (yyvsp[0].cp); }
#line 2199 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 31:
#line 362 "pddl+.yacc" /* yacc.c:1646  */
    {  
      (yyval.t_var_symbol_list)= (yyvsp[-3].t_var_symbol_list);
      (yyval.t_var_symbol_list)->set_types((yyvsp[-1].t_type));           /* Set types for variables */
      (yyval.t_var_symbol_list)->splice((yyval.t_var_symbol_list)->end(),*(yyvsp[0].t_var_symbol_list));   /* Join lists */ 
      delete (yyvsp[0].t_var_symbol_list);                   /* Delete (now empty) list */
      requires(E_TYPING);
   }
#line 2211 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 32:
#line 370 "pddl+.yacc" /* yacc.c:1646  */
    {  
      (yyval.t_var_symbol_list)= (yyvsp[-3].t_var_symbol_list);
      (yyval.t_var_symbol_list)->set_either_types((yyvsp[-1].t_type_list));    /* Set types for variables */
      (yyval.t_var_symbol_list)->splice((yyval.t_var_symbol_list)->end(),*(yyvsp[0].t_var_symbol_list));   /* Join lists */ 
      delete (yyvsp[0].t_var_symbol_list);                   /* Delete (now empty) list */
      requires(E_TYPING);
   }
#line 2223 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 33:
#line 378 "pddl+.yacc" /* yacc.c:1646  */
    {
       (yyval.t_var_symbol_list)= (yyvsp[0].t_var_symbol_list);
   }
#line 2231 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 34:
#line 390 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_var_symbol_list)=(yyvsp[0].t_var_symbol_list); (yyvsp[0].t_var_symbol_list)->push_front((yyvsp[-1].t_var_symbol)); }
#line 2237 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 35:
#line 391 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_var_symbol_list)= new var_symbol_list; }
#line 2243 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 36:
#line 398 "pddl+.yacc" /* yacc.c:1646  */
    {  
      (yyval.t_const_symbol_list)= (yyvsp[-3].t_const_symbol_list);
      (yyvsp[-3].t_const_symbol_list)->set_types((yyvsp[-1].t_type));           /* Set types for constants */
      (yyvsp[-3].t_const_symbol_list)->splice((yyvsp[-3].t_const_symbol_list)->end(),*(yyvsp[0].t_const_symbol_list)); /* Join lists */ 
      delete (yyvsp[0].t_const_symbol_list);                   /* Delete (now empty) list */
      requires(E_TYPING);
   }
#line 2255 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 37:
#line 406 "pddl+.yacc" /* yacc.c:1646  */
    {  
      (yyval.t_const_symbol_list)= (yyvsp[-3].t_const_symbol_list);
      (yyvsp[-3].t_const_symbol_list)->set_either_types((yyvsp[-1].t_type_list));
      (yyvsp[-3].t_const_symbol_list)->splice((yyvsp[-3].t_const_symbol_list)->end(),*(yyvsp[0].t_const_symbol_list));
      delete (yyvsp[0].t_const_symbol_list);
      requires(E_TYPING);
   }
#line 2267 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 38:
#line 414 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)= (yyvsp[0].t_const_symbol_list);}
#line 2273 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 39:
#line 419 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[0].t_const_symbol_list); (yyvsp[0].t_const_symbol_list)->push_front((yyvsp[-1].t_const_symbol));}
#line 2279 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 40:
#line 420 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=new const_symbol_list;}
#line 2285 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 41:
#line 424 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[0].t_const_symbol_list); (yyvsp[0].t_const_symbol_list)->push_front((yyvsp[-1].t_const_symbol));}
#line 2291 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 42:
#line 425 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=new const_symbol_list;}
#line 2297 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 43:
#line 434 "pddl+.yacc" /* yacc.c:1646  */
    {  
       (yyval.t_type_list)= (yyvsp[-3].t_type_list);
       (yyval.t_type_list)->set_types((yyvsp[-1].t_type));           /* Set types for constants */
       (yyval.t_type_list)->splice((yyval.t_type_list)->end(),*(yyvsp[0].t_type_list)); /* Join lists */ 
       delete (yyvsp[0].t_type_list);                   /* Delete (now empty) list */
   }
#line 2308 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 44:
#line 441 "pddl+.yacc" /* yacc.c:1646  */
    {  
   // This parse needs to be excluded, we think (DPL&MF: 6/9/01)
       (yyval.t_type_list)= (yyvsp[-3].t_type_list);
       (yyval.t_type_list)->set_either_types((yyvsp[-1].t_type_list));
       (yyval.t_type_list)->splice((yyvsp[-3].t_type_list)->end(),*(yyvsp[0].t_type_list));
       delete (yyvsp[0].t_type_list);
   }
#line 2320 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 45:
#line 450 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type_list)= (yyvsp[0].t_type_list); }
#line 2326 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 46:
#line 456 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_parameter_symbol_list)=(yyvsp[-1].t_parameter_symbol_list); (yyval.t_parameter_symbol_list)->push_back((yyvsp[0].t_const_symbol)); }
#line 2332 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 47:
#line 458 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_parameter_symbol_list)=(yyvsp[-2].t_parameter_symbol_list); (yyval.t_parameter_symbol_list)->push_back((yyvsp[0].t_var_symbol)); }
#line 2338 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 48:
#line 459 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_parameter_symbol_list)= new parameter_symbol_list;}
#line 2344 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 49:
#line 466 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_var_symbol)= current_analysis->var_tab_stack.top()->symbol_put((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2350 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 50:
#line 472 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_var_symbol)= current_analysis->var_tab_stack.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2356 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 51:
#line 476 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_const_symbol)= current_analysis->const_tab.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2362 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 52:
#line 480 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_const_symbol)= current_analysis->const_tab.symbol_put((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2368 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 53:
#line 485 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type_list)= (yyvsp[-1].t_type_list); }
#line 2374 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 54:
#line 490 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type)= current_analysis->pddl_type_tab.symbol_ref((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2380 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 55:
#line 497 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type)= current_analysis->pddl_type_tab.symbol_ref((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2386 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 56:
#line 502 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= (yyvsp[-1].t_type_list); (yyval.t_type_list)->push_back((yyvsp[0].t_type));}
#line 2392 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 57:
#line 503 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= new pddl_type_list;}
#line 2398 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 58:
#line 508 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= (yyvsp[-1].t_type_list); (yyval.t_type_list)->push_back((yyvsp[0].t_type));}
#line 2404 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 59:
#line 509 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= new pddl_type_list;}
#line 2410 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 60:
#line 514 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-5].t_effect_lists);
	  (yyval.t_effect_lists)->assign_effects.push_back(new assignment((yyvsp[-2].t_func_term),E_ASSIGN,(yyvsp[-1].t_num_expression)));  
          requires(E_FLUENTS); 
	}
#line 2419 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 61:
#line 519 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->add_effects.push_back((yyvsp[0].t_simple_effect)); }
#line 2425 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 62:
#line 521 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->del_effects.push_back((yyvsp[0].t_simple_effect)); }
#line 2431 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 63:
#line 523 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->timed_effects.push_back((yyvsp[0].t_timed_effect)); }
#line 2437 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 64:
#line 525 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;}
#line 2443 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 65:
#line 530 "pddl+.yacc" /* yacc.c:1646  */
    { requires(E_TIMED_INITIAL_LITERALS); 
   		(yyval.t_timed_effect)=new timed_initial_literal((yyvsp[-1].t_effect_lists),(yyvsp[-2].fval));}
#line 2450 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 66:
#line 535 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[0].t_effect_lists); (yyval.t_effect_lists)->append_effects((yyvsp[-1].t_effect_lists)); delete (yyvsp[-1].t_effect_lists);}
#line 2456 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 67:
#line 536 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[0].t_effect_lists); (yyval.t_effect_lists)->cond_effects.push_front((yyvsp[-1].t_cond_effect)); 
                                      requires(E_COND_EFFS);}
#line 2463 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 68:
#line 538 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[0].t_effect_lists); (yyval.t_effect_lists)->forall_effects.push_front((yyvsp[-1].t_forall_effect));
                                      requires(E_COND_EFFS);}
#line 2470 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 69:
#line 540 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists(); }
#line 2476 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 70:
#line 549 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[0].t_effect_lists);}
#line 2482 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 71:
#line 550 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->add_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2488 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 72:
#line 551 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->del_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2494 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 73:
#line 552 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->cond_effects.push_front((yyvsp[0].t_cond_effect));}
#line 2500 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 74:
#line 553 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->forall_effects.push_front((yyvsp[0].t_forall_effect));}
#line 2506 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 75:
#line 557 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists);}
#line 2512 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 76:
#line 558 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[0].t_effect_lists);}
#line 2518 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 77:
#line 563 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->del_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2524 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 78:
#line 565 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->add_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2530 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 79:
#line 567 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->assign_effects.push_front((yyvsp[0].t_assignment));
         requires(E_FLUENTS);}
#line 2537 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 80:
#line 573 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->del_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2543 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 81:
#line 574 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->add_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2549 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 82:
#line 575 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->assign_effects.push_back((yyvsp[0].t_assignment));
                                     requires(E_FLUENTS); }
#line 2556 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 83:
#line 577 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2562 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 84:
#line 582 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); }
#line 2568 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 85:
#line 584 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_effect_lists)=NULL;
	 log_error(E_FATAL,"Syntax error in (and ...)");
	}
#line 2576 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 86:
#line 592 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); }
#line 2582 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 87:
#line 597 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; 
          (yyval.t_effect_lists)->forall_effects.push_back(
	       new forall_effect((yyvsp[-1].t_effect_lists), (yyvsp[-3].t_var_symbol_list), current_analysis->var_tab_stack.pop())); 
          requires(E_COND_EFFS);}
#line 2591 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 88:
#line 602 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
	  (yyval.t_effect_lists)->cond_effects.push_back(
	       new cond_effect((yyvsp[-2].t_goal),(yyvsp[-1].t_effect_lists)));
          requires(E_COND_EFFS); }
#line 2600 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 89:
#line 607 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=new effect_lists;
          (yyval.t_effect_lists)->timed_effects.push_back((yyvsp[0].t_timed_effect)); }
#line 2607 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 90:
#line 610 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
	  (yyval.t_effect_lists)->assign_effects.push_front((yyvsp[0].t_assignment));
          requires(E_FLUENTS); }
#line 2615 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 91:
#line 616 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyvsp[-1].t_effect_lists)->append_effects((yyvsp[0].t_effect_lists)); delete (yyvsp[0].t_effect_lists); }
#line 2621 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 92:
#line 617 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2627 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 93:
#line 622 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect((yyvsp[-1].t_effect_lists),E_AT_START);}
#line 2633 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 94:
#line 624 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect((yyvsp[-1].t_effect_lists),E_AT_END);}
#line 2639 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 95:
#line 626 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_timed_effect)->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression))); }
#line 2647 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 96:
#line 630 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_timed_effect)->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression))); }
#line 2655 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 97:
#line 634 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_timed_effect)=NULL;
	log_error(E_FATAL,"Syntax error in timed effect"); }
#line 2662 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 98:
#line 640 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists);}
#line 2668 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 99:
#line 641 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[0].t_effect_lists);}
#line 2674 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 100:
#line 646 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->del_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2680 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 101:
#line 648 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->add_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2686 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 102:
#line 650 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->assign_effects.push_front((yyvsp[0].t_assignment));
         requires(E_FLUENTS);}
#line 2693 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 103:
#line 656 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->del_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2699 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 104:
#line 657 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->add_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2705 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 105:
#line 658 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->assign_effects.push_back((yyvsp[0].t_assignment));
                                     requires(E_FLUENTS); }
#line 2712 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 106:
#line 660 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2718 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 107:
#line 666 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_ASSIGN,(yyvsp[-1].t_expression)); }
#line 2724 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 108:
#line 668 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression)); }
#line 2730 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 109:
#line 670 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression)); }
#line 2736 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 110:
#line 672 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_UP,(yyvsp[-1].t_expression)); }
#line 2742 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 111:
#line 674 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_DOWN,(yyvsp[-1].t_expression)); }
#line 2748 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 112:
#line 679 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; 
         timed_effect * te = new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_effect_lists)->timed_effects.push_front(te);
         te->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression))); }
#line 2758 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 113:
#line 685 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; 
         timed_effect * te = new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_effect_lists)->timed_effects.push_front(te);
         te->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression))); }
#line 2768 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 114:
#line 691 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists) = (yyvsp[-1].t_effect_lists);}
#line 2774 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 115:
#line 695 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyvsp[-1].t_effect_lists)->append_effects((yyvsp[0].t_effect_lists)); delete (yyvsp[0].t_effect_lists); }
#line 2780 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 116:
#line 696 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2786 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 117:
#line 700 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_expression);}
#line 2792 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 118:
#line 701 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= new special_val_expr(E_DURATION_VAR);
                    requires( E_DURATION_INEQUALITIES );}
#line 2799 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 119:
#line 703 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)=(yyvsp[0].t_num_expression); }
#line 2805 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 120:
#line 704 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= (yyvsp[0].t_func_term); }
#line 2811 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 121:
#line 709 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new plus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 2817 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 122:
#line 711 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new minus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 2823 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 123:
#line 713 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 2829 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 124:
#line 715 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new div_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 2835 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 125:
#line 720 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)= new conj_goal((yyvsp[-1].t_goal_list)); }
#line 2841 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 126:
#line 722 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)= new timed_goal(new comparison((yyvsp[-4].t_comparison_op),
        			new special_val_expr(E_DURATION_VAR),(yyvsp[-1].t_expression)),E_AT_START); }
#line 2848 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 127:
#line 725 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal) = new timed_goal(new comparison((yyvsp[-5].t_comparison_op),
					new special_val_expr(E_DURATION_VAR),(yyvsp[-2].t_expression)),E_AT_START);}
#line 2855 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 128:
#line 728 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal) = new timed_goal(new comparison((yyvsp[-5].t_comparison_op),
					new special_val_expr(E_DURATION_VAR),(yyvsp[-2].t_expression)),E_AT_END);}
#line 2862 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 129:
#line 733 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_comparison_op)= E_LESSEQ; requires(E_DURATION_INEQUALITIES);}
#line 2868 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 130:
#line 734 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_comparison_op)= E_GREATEQ; requires(E_DURATION_INEQUALITIES);}
#line 2874 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 131:
#line 735 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_comparison_op)= E_EQUALS; }
#line 2880 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 132:
#line 743 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_expression); }
#line 2886 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 133:
#line 748 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyval.t_goal_list)->push_back((yyvsp[0].t_goal)); }
#line 2892 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 134:
#line 750 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)= new goal_list; }
#line 2898 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 135:
#line 755 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[-1].t_proposition)); }
#line 2904 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 136:
#line 760 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[0].t_proposition)); }
#line 2910 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 137:
#line 767 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[-1].t_proposition)); }
#line 2916 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 138:
#line 772 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[0].t_proposition)); }
#line 2922 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 139:
#line 777 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_forall_effect)= new forall_effect((yyvsp[-1].t_effect_lists), (yyvsp[-3].t_var_symbol_list), current_analysis->var_tab_stack.pop());}
#line 2928 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 140:
#line 782 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_cond_effect)= new cond_effect((yyvsp[-2].t_goal),(yyvsp[-1].t_effect_lists)); }
#line 2934 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 141:
#line 787 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_ASSIGN,(yyvsp[-1].t_expression)); }
#line 2940 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 142:
#line 789 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression)); }
#line 2946 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 143:
#line 791 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression)); }
#line 2952 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 144:
#line 793 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_UP,(yyvsp[-1].t_expression)); }
#line 2958 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 145:
#line 795 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_DOWN,(yyvsp[-1].t_expression)); }
#line 2964 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 146:
#line 800 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new uminus_expression((yyvsp[-1].t_expression)); requires(E_FLUENTS); }
#line 2970 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 147:
#line 802 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new plus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_FLUENTS); }
#line 2976 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 148:
#line 804 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new minus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_FLUENTS); }
#line 2982 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 149:
#line 806 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_FLUENTS); }
#line 2988 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 150:
#line 808 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new div_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_FLUENTS); }
#line 2994 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 151:
#line 809 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)=(yyvsp[0].t_num_expression); }
#line 3000 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 152:
#line 810 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= (yyvsp[0].t_func_term); requires(E_FLUENTS); }
#line 3006 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 153:
#line 815 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression(new special_val_expr(E_HASHT),(yyvsp[-1].t_expression)); }
#line 3012 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 154:
#line 817 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-2].t_expression), new special_val_expr(E_HASHT)); }
#line 3018 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 155:
#line 819 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new special_val_expr(E_HASHT); }
#line 3024 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 156:
#line 824 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_num_expression)=new int_expression((yyvsp[0].ival));   }
#line 3030 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 157:
#line 825 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_num_expression)=new float_expression((yyvsp[0].fval)); }
#line 3036 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 158:
#line 829 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3042 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 159:
#line 832 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3048 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 160:
#line 834 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[0].cp)),
                            new parameter_symbol_list); delete [] (yyvsp[0].cp);}
#line 3055 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 161:
#line 852 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3061 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 162:
#line 854 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3067 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 163:
#line 856 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[0].cp)),
                            new parameter_symbol_list); delete [] (yyvsp[0].cp);}
#line 3074 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 164:
#line 861 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_GREATER; }
#line 3080 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 165:
#line 862 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_GREATEQ; }
#line 3086 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 166:
#line 863 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_LESS; }
#line 3092 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 167:
#line 864 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_LESSEQ; }
#line 3098 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 168:
#line 865 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_EQUALS; }
#line 3104 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 169:
#line 878 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= (yyvsp[0].t_goal);}
#line 3110 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 170:
#line 880 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal) = new conj_goal((yyvsp[-1].t_goal_list));}
#line 3116 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 171:
#line 883 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_goal),current_analysis->var_tab_stack.pop());
        requires(E_UNIV_PRECS);}
#line 3123 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 172:
#line 889 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3129 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 173:
#line 891 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-2].cp),(yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3135 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 174:
#line 893 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new conj_goal((yyvsp[-1].t_goal_list));}
#line 3141 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 175:
#line 896 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal)= new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_con_goal),current_analysis->var_tab_stack.pop());
                requires(E_UNIV_PRECS);}
#line 3148 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 176:
#line 899 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = (yyvsp[0].t_con_goal);}
#line 3154 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 177:
#line 904 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3160 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 178:
#line 906 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-2].cp),(yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3166 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 179:
#line 908 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new conj_goal((yyvsp[-1].t_goal_list));}
#line 3172 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 180:
#line 911 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal)= new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_con_goal),current_analysis->var_tab_stack.pop());
                requires(E_UNIV_PRECS);}
#line 3179 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 181:
#line 917 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyvsp[-1].t_goal_list)->push_back((yyvsp[0].t_con_goal));}
#line 3185 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 182:
#line 919 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)= new goal_list;}
#line 3191 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 183:
#line 924 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new preference((yyvsp[-1].t_goal)); requires(E_PREFERENCES);}
#line 3197 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 184:
#line 926 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new preference((yyvsp[-2].cp),(yyvsp[-1].t_goal)); requires(E_PREFERENCES);}
#line 3203 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 185:
#line 928 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)=(yyvsp[0].t_goal);}
#line 3209 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 186:
#line 933 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list) = (yyvsp[-1].t_goal_list); (yyval.t_goal_list)->push_back((yyvsp[0].t_con_goal));}
#line 3215 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 187:
#line 935 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list) = new goal_list;}
#line 3221 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 188:
#line 940 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal)= new conj_goal((yyvsp[-1].t_goal_list));}
#line 3227 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 189:
#line 942 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_con_goal),current_analysis->var_tab_stack.pop());
        requires(E_UNIV_PRECS);}
#line 3234 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 190:
#line 945 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ATEND,(yyvsp[-1].t_goal));}
#line 3240 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 191:
#line 947 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ALWAYS,(yyvsp[-1].t_goal));}
#line 3246 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 192:
#line 949 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_SOMETIME,(yyvsp[-1].t_goal));}
#line 3252 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 193:
#line 951 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_WITHIN,(yyvsp[-1].t_goal),NULL,(yyvsp[-2].t_num_expression)->double_value(),0.0);delete (yyvsp[-2].t_num_expression);}
#line 3258 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 194:
#line 953 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ATMOSTONCE,(yyvsp[-1].t_goal));}
#line 3264 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 195:
#line 955 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_SOMETIMEAFTER,(yyvsp[-1].t_goal),(yyvsp[-2].t_goal));}
#line 3270 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 196:
#line 957 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_SOMETIMEBEFORE,(yyvsp[-1].t_goal),(yyvsp[-2].t_goal));}
#line 3276 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 197:
#line 959 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ALWAYSWITHIN,(yyvsp[-1].t_goal),(yyvsp[-2].t_goal),(yyvsp[-3].t_num_expression)->double_value(),0.0);delete (yyvsp[-3].t_num_expression);}
#line 3282 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 198:
#line 961 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_HOLDDURING,(yyvsp[-1].t_goal),NULL,(yyvsp[-2].t_num_expression)->double_value(),(yyvsp[-3].t_num_expression)->double_value());delete (yyvsp[-3].t_num_expression);delete (yyvsp[-2].t_num_expression);}
#line 3288 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 199:
#line 963 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_HOLDAFTER,(yyvsp[-1].t_goal),NULL,0.0,(yyvsp[-2].t_num_expression)->double_value());delete (yyvsp[-2].t_num_expression);}
#line 3294 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 200:
#line 968 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new simple_goal((yyvsp[0].t_proposition),E_POS);}
#line 3300 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 201:
#line 970 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new neg_goal((yyvsp[-1].t_goal));simple_goal * s = dynamic_cast<simple_goal *>((yyvsp[-1].t_goal));
       if(s && s->getProp()->head->getName()=="=") {requires(E_EQUALITY);} 
       else{requires(E_NEGATIVE_PRECONDITIONS);};}
#line 3308 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 202:
#line 974 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new conj_goal((yyvsp[-1].t_goal_list));}
#line 3314 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 203:
#line 976 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new disj_goal((yyvsp[-1].t_goal_list));
        requires(E_DISJUNCTIVE_PRECONDS);}
#line 3321 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 204:
#line 979 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new imply_goal((yyvsp[-2].t_goal),(yyvsp[-1].t_goal));
        requires(E_DISJUNCTIVE_PRECONDS);}
#line 3328 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 205:
#line 983 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new qfied_goal((yyvsp[-5].t_quantifier),(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_goal),current_analysis->var_tab_stack.pop());}
#line 3334 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 206:
#line 985 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new comparison((yyvsp[-3].t_comparison_op),(yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); 
        requires(E_FLUENTS);}
#line 3341 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 207:
#line 991 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyvsp[-1].t_goal_list)->push_back((yyvsp[0].t_goal));}
#line 3347 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 208:
#line 993 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)= new goal_list;}
#line 3353 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 209:
#line 998 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyvsp[-1].t_goal_list)->push_back((yyvsp[0].t_goal));}
#line 3359 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 210:
#line 1000 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)= new goal_list;}
#line 3365 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 211:
#line 1004 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_quantifier)=(yyvsp[0].t_quantifier);}
#line 3371 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 212:
#line 1005 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_quantifier)=(yyvsp[0].t_quantifier);}
#line 3377 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 213:
#line 1010 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_quantifier)=E_FORALL; 
        current_analysis->var_tab_stack.push(
        		current_analysis->buildForallTab());}
#line 3385 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 214:
#line 1017 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_quantifier)=E_EXISTS;
        current_analysis->var_tab_stack.push(
        	current_analysis->buildExistsTab());}
#line 3393 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 215:
#line 1024 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_proposition)=new proposition((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_parameter_symbol_list));}
#line 3399 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 216:
#line 1029 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_proposition) = new proposition((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_var_symbol_list));}
#line 3405 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 217:
#line 1034 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_proposition)=new proposition((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_parameter_symbol_list));}
#line 3411 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 218:
#line 1039 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pred_decl_list)= (yyvsp[-1].t_pred_decl_list);}
#line 3417 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 219:
#line 1041 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_pred_decl_list)=NULL;
	 log_error(E_FATAL,"Syntax error in (:predicates ...)");
	}
#line 3425 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 220:
#line 1048 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_func_decl_list)= (yyvsp[-1].t_func_decl_list);}
#line 3431 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 221:
#line 1050 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_func_decl_list)=NULL;
	 log_error(E_FATAL,"Syntax error in (:functions ...)");
	}
#line 3439 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 222:
#line 1057 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = (yyvsp[-1].t_con_goal);}
#line 3445 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 223:
#line 1059 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_con_goal)=NULL;
      log_error(E_FATAL,"Syntax error in (:constraints ...)");
      }
#line 3453 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 224:
#line 1066 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = (yyvsp[-1].t_con_goal);}
#line 3459 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 225:
#line 1068 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_con_goal)=NULL;
      log_error(E_FATAL,"Syntax error in (:constraints ...)");
      }
#line 3467 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 226:
#line 1074 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_store)=(yyvsp[-1].t_structure_store); (yyval.t_structure_store)->push_back((yyvsp[0].t_structure_def)); }
#line 3473 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 227:
#line 1075 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_store)= new structure_store; (yyval.t_structure_store)->push_back((yyvsp[0].t_structure_def)); }
#line 3479 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 228:
#line 1079 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_action_def); }
#line 3485 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 229:
#line 1080 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_event_def); requires(E_TIME); }
#line 3491 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 230:
#line 1081 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_process_def); requires(E_TIME); }
#line 3497 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 231:
#line 1082 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_durative_action_def); requires(E_DURATIVE_ACTIONS); }
#line 3503 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 232:
#line 1083 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_derivation_rule); requires(E_DERIVED_PREDICATES);}
#line 3509 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 233:
#line 1087 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy)= 0; 
    	current_analysis->var_tab_stack.push(
    					current_analysis->buildRuleTab());}
#line 3517 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 234:
#line 1098 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_derivation_rule) = new derivation_rule((yyvsp[-2].t_proposition),(yyvsp[-1].t_goal),current_analysis->var_tab_stack.pop());}
#line 3523 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 235:
#line 1110 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_action_def)= current_analysis->buildAction(current_analysis->op_tab.symbol_put((yyvsp[-9].cp)),
			(yyvsp[-6].t_var_symbol_list),(yyvsp[-3].t_goal),(yyvsp[-1].t_effect_lists),
			current_analysis->var_tab_stack.pop()); delete [] (yyvsp[-9].cp); }
#line 3531 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 236:
#line 1114 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
	 log_error(E_FATAL,"Syntax error in action declaration.");
	 (yyval.t_action_def)= NULL; }
#line 3539 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 237:
#line 1127 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_event_def)= current_analysis->buildEvent(current_analysis->op_tab.symbol_put((yyvsp[-9].cp)),
		   (yyvsp[-6].t_var_symbol_list),(yyvsp[-3].t_goal),(yyvsp[-1].t_effect_lists),
		   current_analysis->var_tab_stack.pop()); delete [] (yyvsp[-9].cp);}
#line 3547 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 238:
#line 1132 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
	 log_error(E_FATAL,"Syntax error in event declaration.");
	 (yyval.t_event_def)= NULL; }
#line 3555 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 239:
#line 1144 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_process_def)= current_analysis->buildProcess(current_analysis->op_tab.symbol_put((yyvsp[-9].cp)),
		     (yyvsp[-6].t_var_symbol_list),(yyvsp[-3].t_goal),(yyvsp[-1].t_effect_lists),
                     current_analysis->var_tab_stack.pop()); delete [] (yyvsp[-9].cp);}
#line 3563 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 240:
#line 1148 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
	 log_error(E_FATAL,"Syntax error in process declaration.");
	 (yyval.t_process_def)= NULL; }
#line 3571 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 241:
#line 1160 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_durative_action_def)= (yyvsp[-1].t_durative_action_def);
      (yyval.t_durative_action_def)->name= current_analysis->op_tab.symbol_put((yyvsp[-8].cp));
      (yyval.t_durative_action_def)->symtab= current_analysis->var_tab_stack.pop();
      (yyval.t_durative_action_def)->parameters= (yyvsp[-5].t_var_symbol_list);
      (yyval.t_durative_action_def)->dur_constraint= (yyvsp[-2].t_goal); 
      delete [] (yyvsp[-8].cp);
    }
#line 3583 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 242:
#line 1169 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
	 log_error(E_FATAL,"Syntax error in durative-action declaration.");
	 (yyval.t_durative_action_def)= NULL; }
#line 3591 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 243:
#line 1176 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_durative_action_def)=(yyvsp[-2].t_durative_action_def); (yyval.t_durative_action_def)->effects=(yyvsp[0].t_effect_lists);}
#line 3597 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 244:
#line 1178 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_durative_action_def)=(yyvsp[-2].t_durative_action_def); (yyval.t_durative_action_def)->precondition=(yyvsp[0].t_goal);}
#line 3603 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 245:
#line 1179 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_durative_action_def)= current_analysis->buildDurativeAction();}
#line 3609 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 246:
#line 1184 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)=(yyvsp[0].t_goal); }
#line 3615 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 247:
#line 1186 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)= new conj_goal((yyvsp[-1].t_goal_list)); }
#line 3621 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 248:
#line 1191 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyval.t_goal_list)->push_back((yyvsp[0].t_goal)); }
#line 3627 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 249:
#line 1193 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)= new goal_list; }
#line 3633 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 250:
#line 1198 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new timed_goal((yyvsp[-1].t_goal),E_AT_START);}
#line 3639 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 251:
#line 1200 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new timed_goal((yyvsp[-1].t_goal),E_AT_END);}
#line 3645 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 252:
#line 1202 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new timed_goal((yyvsp[-1].t_goal),E_OVER_ALL);}
#line 3651 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 253:
#line 1204 "pddl+.yacc" /* yacc.c:1646  */
    {timed_goal * tg = dynamic_cast<timed_goal *>((yyvsp[-1].t_goal));
		(yyval.t_goal) = new timed_goal(new preference((yyvsp[-2].cp),tg->clearGoal()),tg->getTime());
			delete tg;
			requires(E_PREFERENCES);}
#line 3660 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 254:
#line 1209 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal) = new preference((yyvsp[-1].t_goal));requires(E_PREFERENCES);}
#line 3666 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 255:
#line 1213 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy)= 0; current_analysis->var_tab_stack.push(
    				current_analysis->buildOpTab());}
#line 3673 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 256:
#line 1218 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_EQUALITY;}
#line 3679 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 257:
#line 1219 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_STRIPS;}
#line 3685 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 258:
#line 1221 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_TYPING;}
#line 3691 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 259:
#line 1223 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_NEGATIVE_PRECONDITIONS;}
#line 3697 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 260:
#line 1225 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_DISJUNCTIVE_PRECONDS;}
#line 3703 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 261:
#line 1226 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_EXT_PRECS;}
#line 3709 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 262:
#line 1227 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_UNIV_PRECS;}
#line 3715 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 263:
#line 1228 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_COND_EFFS;}
#line 3721 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 264:
#line 1229 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_FLUENTS;}
#line 3727 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 265:
#line 1231 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_DURATIVE_ACTIONS;}
#line 3733 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 266:
#line 1232 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_TIME |
                      E_FLUENTS |
                      E_DURATIVE_ACTIONS; }
#line 3741 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 267:
#line 1236 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_STRIPS |
		      E_TYPING | 
		      E_NEGATIVE_PRECONDITIONS |
		      E_DISJUNCTIVE_PRECONDS |
		      E_EQUALITY |
		      E_EXT_PRECS |
		      E_UNIV_PRECS |
		      E_COND_EFFS;}
#line 3754 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 268:
#line 1245 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_EXT_PRECS |
		      E_UNIV_PRECS;}
#line 3761 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 269:
#line 1249 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_DURATION_INEQUALITIES;}
#line 3767 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 270:
#line 1252 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_CONTINUOUS_EFFECTS;}
#line 3773 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 271:
#line 1254 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_DERIVED_PREDICATES;}
#line 3779 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 272:
#line 1256 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_TIMED_INITIAL_LITERALS;}
#line 3785 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 273:
#line 1258 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_PREFERENCES;}
#line 3791 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 274:
#line 1260 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_CONSTRAINTS;}
#line 3797 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 275:
#line 1262 "pddl+.yacc" /* yacc.c:1646  */
    {log_error(E_WARNING,"Unrecognised requirements declaration ");
       (yyval.t_pddl_req_flag)= 0; delete [] (yyvsp[0].cp);}
#line 3804 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 276:
#line 1268 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[-1].t_const_symbol_list);}
#line 3810 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 277:
#line 1272 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)=(yyvsp[-1].t_type_list); requires(E_TYPING);}
#line 3816 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 278:
#line 1282 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[-1].t_problem); (yyval.t_problem)->name = (yyvsp[-7].cp); (yyval.t_problem)->domain_name = (yyvsp[-3].cp);}
#line 3822 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 279:
#line 1284 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_problem)=NULL;
       	log_error(E_FATAL,"Syntax error in problem definition."); }
#line 3829 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 280:
#line 1290 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->req= (yyvsp[-1].t_pddl_req_flag);}
#line 3835 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 281:
#line 1291 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->objects= (yyvsp[-1].t_const_symbol_list);}
#line 3841 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 282:
#line 1292 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->initial_state= (yyvsp[-1].t_effect_lists);}
#line 3847 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 283:
#line 1293 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->the_goal= (yyvsp[-1].t_goal);}
#line 3853 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 284:
#line 1295 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->constraints = (yyvsp[-1].t_con_goal);}
#line 3859 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 285:
#line 1296 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->metric= (yyvsp[-1].t_metric);}
#line 3865 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 286:
#line 1297 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->length= (yyvsp[-1].t_length_spec);}
#line 3871 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 287:
#line 1298 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=new problem;}
#line 3877 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 288:
#line 1301 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[-1].t_const_symbol_list);}
#line 3883 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 289:
#line 1304 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists);}
#line 3889 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 290:
#line 1307 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.vtab) = current_analysis->buildOpTab();}
#line 3895 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 291:
#line 1310 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)=(yyvsp[-1].t_goal);delete (yyvsp[-2].vtab);}
#line 3901 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 292:
#line 1315 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_metric)= new metric_spec((yyvsp[-2].t_optimization),(yyvsp[-1].t_expression)); }
#line 3907 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 293:
#line 1317 "pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; 
        log_error(E_FATAL,"Syntax error in metric declaration.");
        (yyval.t_metric)= NULL; }
#line 3915 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 294:
#line 1324 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_length_spec)= new length_spec(E_BOTH,(yyvsp[-3].ival),(yyvsp[-1].ival));}
#line 3921 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 295:
#line 1327 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_length_spec) = new length_spec(E_SERIAL,(yyvsp[-1].ival));}
#line 3927 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 296:
#line 1331 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_length_spec) = new length_spec(E_PARALLEL,(yyvsp[-1].ival));}
#line 3933 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 297:
#line 1337 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_optimization)= E_MINIMIZE;}
#line 3939 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 298:
#line 1338 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_optimization)= E_MAXIMIZE;}
#line 3945 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 299:
#line 1343 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[-1].t_expression);}
#line 3951 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 300:
#line 1344 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_func_term);}
#line 3957 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 301:
#line 1345 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_num_expression);}
#line 3963 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 302:
#line 1346 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new special_val_expr(E_TOTAL_TIME); }
#line 3969 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 303:
#line 1348 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = new violation_term((yyvsp[-1].cp));}
#line 3975 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 304:
#line 1349 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new special_val_expr(E_TOTAL_TIME); }
#line 3981 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 305:
#line 1353 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new plus_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 3987 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 306:
#line 1354 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new minus_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 3993 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 307:
#line 1355 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 3999 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 308:
#line 1356 "pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new div_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 4005 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 309:
#line 1360 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = (yyvsp[0].t_expression);}
#line 4011 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 310:
#line 1362 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = new plus_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression));}
#line 4017 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 311:
#line 1366 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = (yyvsp[0].t_expression);}
#line 4023 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 312:
#line 1368 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = new mul_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression));}
#line 4029 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 313:
#line 1374 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan)= (yyvsp[0].t_plan); 
         (yyval.t_plan)->push_front((yyvsp[-1].t_step)); }
#line 4036 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 314:
#line 1377 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan) = (yyvsp[0].t_plan);(yyval.t_plan)->insertTime((yyvsp[-1].fval));}
#line 4042 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 315:
#line 1379 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan) = (yyvsp[0].t_plan);(yyval.t_plan)->insertTime((yyvsp[-1].ival));}
#line 4048 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 316:
#line 1381 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan)= new plan;}
#line 4054 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 317:
#line 1386 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)=(yyvsp[0].t_step); 
         (yyval.t_step)->start_time_given=1; 
         (yyval.t_step)->start_time=(yyvsp[-2].fval);}
#line 4062 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 318:
#line 1390 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)=(yyvsp[0].t_step);
	 (yyval.t_step)->start_time_given=0;}
#line 4069 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 319:
#line 1396 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)= (yyvsp[-3].t_step); 
	 (yyval.t_step)->duration_given=1;
         (yyval.t_step)->duration= (yyvsp[-1].fval);}
#line 4077 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 320:
#line 1400 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)= (yyvsp[0].t_step);
         (yyval.t_step)->duration_given=0;}
#line 4084 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 321:
#line 1406 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)= new plan_step( 
              current_analysis->op_tab.symbol_get((yyvsp[-2].cp)), 
	      (yyvsp[-1].t_const_symbol_list)); delete [] (yyvsp[-2].cp);
      }
#line 4093 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 322:
#line 1413 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.fval)= (yyvsp[0].fval);}
#line 4099 "pddl+.cpp" /* yacc.c:1646  */
    break;

  case 323:
#line 1414 "pddl+.yacc" /* yacc.c:1646  */
    {(yyval.fval)= (float) (yyvsp[0].ival);}
#line 4105 "pddl+.cpp" /* yacc.c:1646  */
    break;


#line 4109 "pddl+.cpp" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1417 "pddl+.yacc" /* yacc.c:1906  */


#include <cstdio>
#include <iostream>
int line_no= 1;
using std::istream;
#include "lex.yy.cc"

namespace PDDL2UPMurphi_parser {
extern yyFlexLexer* yfl;
};


int yyerror(const char * s)
{
    return 0;
}

int yylex()
{
    return yfl->yylex();
}
