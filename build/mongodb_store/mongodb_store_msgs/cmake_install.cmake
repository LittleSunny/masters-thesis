# Install script for directory: /home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/sunny/catkin_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs/msg" TYPE FILE FILES
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/msg/StringList.msg"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/msg/StringPair.msg"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/msg/StringPairList.msg"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/msg/SerialisedMessage.msg"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/msg/Insert.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs/srv" TYPE FILE FILES
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/srv/MongoInsertMsg.srv"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/srv/MongoUpdateMsg.srv"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/srv/MongoQueryMsg.srv"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/srv/MongoQuerywithProjectionMsg.srv"
    "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/srv/MongoDeleteMsg.srv"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs/action" TYPE FILE FILES "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/action/MoveEntries.action")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs/msg" TYPE FILE FILES
    "/home/sunny/catkin_ws/devel/share/mongodb_store_msgs/msg/MoveEntriesAction.msg"
    "/home/sunny/catkin_ws/devel/share/mongodb_store_msgs/msg/MoveEntriesActionGoal.msg"
    "/home/sunny/catkin_ws/devel/share/mongodb_store_msgs/msg/MoveEntriesActionResult.msg"
    "/home/sunny/catkin_ws/devel/share/mongodb_store_msgs/msg/MoveEntriesActionFeedback.msg"
    "/home/sunny/catkin_ws/devel/share/mongodb_store_msgs/msg/MoveEntriesGoal.msg"
    "/home/sunny/catkin_ws/devel/share/mongodb_store_msgs/msg/MoveEntriesResult.msg"
    "/home/sunny/catkin_ws/devel/share/mongodb_store_msgs/msg/MoveEntriesFeedback.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs/cmake" TYPE FILE FILES "/home/sunny/catkin_ws/build/mongodb_store/mongodb_store_msgs/catkin_generated/installspace/mongodb_store_msgs-msg-paths.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/include/mongodb_store_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/share/roseus/ros/mongodb_store_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/share/common-lisp/ros/mongodb_store_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/share/gennodejs/ros/mongodb_store_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python" -m compileall "/home/sunny/catkin_ws/devel/lib/python2.7/dist-packages/mongodb_store_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/lib/python2.7/dist-packages/mongodb_store_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/sunny/catkin_ws/build/mongodb_store/mongodb_store_msgs/catkin_generated/installspace/mongodb_store_msgs.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs/cmake" TYPE FILE FILES "/home/sunny/catkin_ws/build/mongodb_store/mongodb_store_msgs/catkin_generated/installspace/mongodb_store_msgs-msg-extras.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs/cmake" TYPE FILE FILES
    "/home/sunny/catkin_ws/build/mongodb_store/mongodb_store_msgs/catkin_generated/installspace/mongodb_store_msgsConfig.cmake"
    "/home/sunny/catkin_ws/build/mongodb_store/mongodb_store_msgs/catkin_generated/installspace/mongodb_store_msgsConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/mongodb_store_msgs" TYPE FILE FILES "/home/sunny/catkin_ws/src/mongodb_store/mongodb_store_msgs/package.xml")
endif()

