# Install script for directory: /home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/sunny/catkin_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs/msg" TYPE FILE FILES
    "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg"
    "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
    "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs/srv" TYPE FILE FILES "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs/action" TYPE FILE FILES "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/action/Plan.action")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs/msg" TYPE FILE FILES
    "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg"
    "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg"
    "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg"
    "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg"
    "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg"
    "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg"
    "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs/cmake" TYPE FILE FILES "/home/sunny/catkin_ws/build/ROSPlan/rosplan_dispatch_msgs/catkin_generated/installspace/rosplan_dispatch_msgs-msg-paths.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/include/rosplan_dispatch_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/share/roseus/ros/rosplan_dispatch_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/share/common-lisp/ros/rosplan_dispatch_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/share/gennodejs/ros/rosplan_dispatch_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python" -m compileall "/home/sunny/catkin_ws/devel/lib/python2.7/dist-packages/rosplan_dispatch_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/sunny/catkin_ws/devel/lib/python2.7/dist-packages/rosplan_dispatch_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/sunny/catkin_ws/build/ROSPlan/rosplan_dispatch_msgs/catkin_generated/installspace/rosplan_dispatch_msgs.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs/cmake" TYPE FILE FILES "/home/sunny/catkin_ws/build/ROSPlan/rosplan_dispatch_msgs/catkin_generated/installspace/rosplan_dispatch_msgs-msg-extras.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs/cmake" TYPE FILE FILES
    "/home/sunny/catkin_ws/build/ROSPlan/rosplan_dispatch_msgs/catkin_generated/installspace/rosplan_dispatch_msgsConfig.cmake"
    "/home/sunny/catkin_ws/build/ROSPlan/rosplan_dispatch_msgs/catkin_generated/installspace/rosplan_dispatch_msgsConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/rosplan_dispatch_msgs" TYPE FILE FILES "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/package.xml")
endif()

