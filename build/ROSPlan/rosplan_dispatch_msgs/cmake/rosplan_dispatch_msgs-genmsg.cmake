# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "rosplan_dispatch_msgs: 10 messages, 1 services")

set(MSG_I_FLAGS "-Irosplan_dispatch_msgs:/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg;-Irosplan_dispatch_msgs:/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg;-Iactionlib_msgs:/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg;-Idiagnostic_msgs:/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genjava REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(rosplan_dispatch_msgs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" ""
)

get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" "rosplan_dispatch_msgs/PlanFeedback:actionlib_msgs/GoalStatus:rosplan_dispatch_msgs/PlanGoal:rosplan_dispatch_msgs/PlanActionFeedback:rosplan_dispatch_msgs/PlanActionGoal:rosplan_dispatch_msgs/PlanActionResult:actionlib_msgs/GoalID:rosplan_dispatch_msgs/PlanResult:std_msgs/Header"
)

get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" "rosplan_dispatch_msgs/PlanFeedback:actionlib_msgs/GoalID:std_msgs/Header:actionlib_msgs/GoalStatus"
)

get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" "rosplan_dispatch_msgs/PlanGoal:actionlib_msgs/GoalID:std_msgs/Header"
)

get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" "diagnostic_msgs/KeyValue"
)

get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" ""
)

get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" "diagnostic_msgs/KeyValue:rosplan_dispatch_msgs/ActionDispatch"
)

get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" "diagnostic_msgs/KeyValue"
)

get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" ""
)

get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" ""
)

get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" NAME_WE)
add_custom_target(_rosplan_dispatch_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rosplan_dispatch_msgs" "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" "rosplan_dispatch_msgs/PlanResult:actionlib_msgs/GoalID:std_msgs/Header:actionlib_msgs/GoalStatus"
)

#
#  langs = gencpp;geneus;genjava;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg;/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Services
_generate_srv_cpp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Module File
_generate_module_cpp(rosplan_dispatch_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(rosplan_dispatch_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(rosplan_dispatch_msgs_generate_messages rosplan_dispatch_msgs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rosplan_dispatch_msgs_gencpp)
add_dependencies(rosplan_dispatch_msgs_gencpp rosplan_dispatch_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rosplan_dispatch_msgs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg;/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Services
_generate_srv_eus(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Module File
_generate_module_eus(rosplan_dispatch_msgs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(rosplan_dispatch_msgs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(rosplan_dispatch_msgs_generate_messages rosplan_dispatch_msgs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_eus _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rosplan_dispatch_msgs_geneus)
add_dependencies(rosplan_dispatch_msgs_geneus rosplan_dispatch_msgs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rosplan_dispatch_msgs_generate_messages_eus)

### Section generating for lang: genjava
### Generating Messages
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg;/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Services
_generate_srv_java(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Module File
_generate_module_java(rosplan_dispatch_msgs
  ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
  "${ALL_GEN_OUTPUT_FILES_java}"
)

add_custom_target(rosplan_dispatch_msgs_generate_messages_java
  DEPENDS ${ALL_GEN_OUTPUT_FILES_java}
)
add_dependencies(rosplan_dispatch_msgs_generate_messages rosplan_dispatch_msgs_generate_messages_java)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_java _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rosplan_dispatch_msgs_genjava)
add_dependencies(rosplan_dispatch_msgs_genjava rosplan_dispatch_msgs_generate_messages_java)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rosplan_dispatch_msgs_generate_messages_java)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg;/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Services
_generate_srv_lisp(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Module File
_generate_module_lisp(rosplan_dispatch_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(rosplan_dispatch_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(rosplan_dispatch_msgs_generate_messages rosplan_dispatch_msgs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rosplan_dispatch_msgs_genlisp)
add_dependencies(rosplan_dispatch_msgs_genlisp rosplan_dispatch_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rosplan_dispatch_msgs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg;/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Services
_generate_srv_nodejs(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Module File
_generate_module_nodejs(rosplan_dispatch_msgs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(rosplan_dispatch_msgs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(rosplan_dispatch_msgs_generate_messages rosplan_dispatch_msgs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rosplan_dispatch_msgs_gennodejs)
add_dependencies(rosplan_dispatch_msgs_gennodejs rosplan_dispatch_msgs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rosplan_dispatch_msgs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg;/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/diagnostic_msgs/cmake/../msg/KeyValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)
_generate_msg_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Services
_generate_srv_py(rosplan_dispatch_msgs
  "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
)

### Generating Module File
_generate_module_py(rosplan_dispatch_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(rosplan_dispatch_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(rosplan_dispatch_msgs_generate_messages rosplan_dispatch_msgs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/srv/PlanningService.srv" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanAction.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionDispatch.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/CompletePlan.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/src/ROSPlan/rosplan_dispatch_msgs/msg/ActionFeedback.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanGoal.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sunny/catkin_ws/devel/share/rosplan_dispatch_msgs/msg/PlanActionResult.msg" NAME_WE)
add_dependencies(rosplan_dispatch_msgs_generate_messages_py _rosplan_dispatch_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rosplan_dispatch_msgs_genpy)
add_dependencies(rosplan_dispatch_msgs_genpy rosplan_dispatch_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rosplan_dispatch_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rosplan_dispatch_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()
if(TARGET diagnostic_msgs_generate_messages_cpp)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp diagnostic_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rosplan_dispatch_msgs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()
if(TARGET diagnostic_msgs_generate_messages_eus)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_eus diagnostic_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genjava_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genjava_INSTALL_DIR}/rosplan_dispatch_msgs
    DESTINATION ${genjava_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_java)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_java actionlib_msgs_generate_messages_java)
endif()
if(TARGET diagnostic_msgs_generate_messages_java)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_java diagnostic_msgs_generate_messages_java)
endif()
if(TARGET std_msgs_generate_messages_java)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_java std_msgs_generate_messages_java)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rosplan_dispatch_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()
if(TARGET diagnostic_msgs_generate_messages_lisp)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp diagnostic_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rosplan_dispatch_msgs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()
if(TARGET diagnostic_msgs_generate_messages_nodejs)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs diagnostic_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rosplan_dispatch_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
if(TARGET diagnostic_msgs_generate_messages_py)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_py diagnostic_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(rosplan_dispatch_msgs_generate_messages_py std_msgs_generate_messages_py)
endif()
