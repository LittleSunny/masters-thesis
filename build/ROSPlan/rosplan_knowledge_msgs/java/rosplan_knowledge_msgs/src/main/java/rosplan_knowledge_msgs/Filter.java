package rosplan_knowledge_msgs;

public interface Filter extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosplan_knowledge_msgs/Filter";
  static final java.lang.String _DEFINITION = "uint8 CLEAR = 0\nuint8 ADD = 1\nuint8 REMOVE = 2\n\nuint8 function\nKnowledgeItem[] knowledge_items\n";
  static final byte CLEAR = 0;
  static final byte ADD = 1;
  static final byte REMOVE = 2;
  byte getFunction();
  void setFunction(byte value);
  java.util.List<rosplan_knowledge_msgs.KnowledgeItem> getKnowledgeItems();
  void setKnowledgeItems(java.util.List<rosplan_knowledge_msgs.KnowledgeItem> value);
}
