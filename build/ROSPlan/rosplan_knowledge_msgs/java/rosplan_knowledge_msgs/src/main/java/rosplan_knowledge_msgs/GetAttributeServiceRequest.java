package rosplan_knowledge_msgs;

public interface GetAttributeServiceRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosplan_knowledge_msgs/GetAttributeServiceRequest";
  static final java.lang.String _DEFINITION = "# PDDL problem generation; service(2/2):\n# Get all facts (or functions) by predicate name (or function name).\n# The results are passed as name/value pairs, where the values need to be parsed by the program.\nstring predicate_name\n";
  java.lang.String getPredicateName();
  void setPredicateName(java.lang.String value);
}
