package rosplan_knowledge_msgs;

public interface KnowledgeItem extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosplan_knowledge_msgs/KnowledgeItem";
  static final java.lang.String _DEFINITION = "# A knowledge item used to represent a piece of the world model in ROSPlan\nuint8 INSTANCE = 0\nuint8 FACT = 1\nuint8 FUNCTION = 2\n\nuint8 knowledge_type\n\n# instance knowledge_type\nstring instance_type\nstring instance_name\n\n# attribute knowledge_type\nstring attribute_name\ndiagnostic_msgs/KeyValue[] values\n\n# function value\nfloat64 function_value\n\n# negative of positive\nbool is_negative";
  static final byte INSTANCE = 0;
  static final byte FACT = 1;
  static final byte FUNCTION = 2;
  byte getKnowledgeType();
  void setKnowledgeType(byte value);
  java.lang.String getInstanceType();
  void setInstanceType(java.lang.String value);
  java.lang.String getInstanceName();
  void setInstanceName(java.lang.String value);
  java.lang.String getAttributeName();
  void setAttributeName(java.lang.String value);
  java.util.List<diagnostic_msgs.KeyValue> getValues();
  void setValues(java.util.List<diagnostic_msgs.KeyValue> value);
  double getFunctionValue();
  void setFunctionValue(double value);
  boolean getIsNegative();
  void setIsNegative(boolean value);
}
