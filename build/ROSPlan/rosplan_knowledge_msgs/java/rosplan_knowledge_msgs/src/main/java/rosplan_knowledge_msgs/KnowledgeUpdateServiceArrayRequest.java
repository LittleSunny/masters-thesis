package rosplan_knowledge_msgs;

public interface KnowledgeUpdateServiceArrayRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosplan_knowledge_msgs/KnowledgeUpdateServiceArrayRequest";
  static final java.lang.String _DEFINITION = "# Knowledge building; service(1/1):\n# Add or remove attributes of a domain predicate or function.\nuint8 ADD_KNOWLEDGE = 0\nuint8 ADD_GOAL = 1\nuint8 REMOVE_KNOWLEDGE = 2\nuint8 REMOVE_GOAL = 3\n\nuint8 update_type\n\nrosplan_knowledge_msgs/KnowledgeItem[] knowledge\n";
  static final byte ADD_KNOWLEDGE = 0;
  static final byte ADD_GOAL = 1;
  static final byte REMOVE_KNOWLEDGE = 2;
  static final byte REMOVE_GOAL = 3;
  byte getUpdateType();
  void setUpdateType(byte value);
  java.util.List<rosplan_knowledge_msgs.KnowledgeItem> getKnowledge();
  void setKnowledge(java.util.List<rosplan_knowledge_msgs.KnowledgeItem> value);
}
