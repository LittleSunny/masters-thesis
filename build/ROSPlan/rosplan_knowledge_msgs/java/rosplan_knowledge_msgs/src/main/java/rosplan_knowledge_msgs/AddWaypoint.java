package rosplan_knowledge_msgs;

public interface AddWaypoint extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosplan_knowledge_msgs/AddWaypoint";
  static final java.lang.String _DEFINITION = "string id\ngeometry_msgs/PoseStamped waypoint\nfloat32 connecting_distance\nint8 occupancy_threshold\nfloat32 x\nfloat32 y\n---\n\n";
}
