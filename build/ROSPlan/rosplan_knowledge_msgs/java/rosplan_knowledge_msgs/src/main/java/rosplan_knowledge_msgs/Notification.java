package rosplan_knowledge_msgs;

public interface Notification extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosplan_knowledge_msgs/Notification";
  static final java.lang.String _DEFINITION = "uint8 ADDED = 0\nuint8 REMOVED = 1\n\nuint8 function\nKnowledgeItem knowledge_item\n";
  static final byte ADDED = 0;
  static final byte REMOVED = 1;
  byte getFunction();
  void setFunction(byte value);
  rosplan_knowledge_msgs.KnowledgeItem getKnowledgeItem();
  void setKnowledgeItem(rosplan_knowledge_msgs.KnowledgeItem value);
}
