package rosplan_knowledge_msgs;

public interface KnowledgeUpdateServiceArray extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosplan_knowledge_msgs/KnowledgeUpdateServiceArray";
  static final java.lang.String _DEFINITION = "# Knowledge building; service(1/1):\n# Add or remove attributes of a domain predicate or function.\nuint8 ADD_KNOWLEDGE = 0\nuint8 ADD_GOAL = 1\nuint8 REMOVE_KNOWLEDGE = 2\nuint8 REMOVE_GOAL = 3\n\nuint8 update_type\n\nrosplan_knowledge_msgs/KnowledgeItem[] knowledge\n---\nbool success\n";
}
