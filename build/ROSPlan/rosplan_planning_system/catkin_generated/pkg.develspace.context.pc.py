# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/include;/home/sunny/catkin_ws/devel/include;/opt/ros/kinetic/include;/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp;/usr/include;/home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/src/VALfiles".split(';') if "/home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/include;/home/sunny/catkin_ws/devel/include;/opt/ros/kinetic/include;/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp;/usr/include;/home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/src/VALfiles" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;actionlib;std_msgs;std_srvs;diagnostic_msgs;rosplan_dispatch_msgs;rosplan_knowledge_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrosplan_planning_system;-lrosplan_action_interface".split(';') if "-lrosplan_planning_system;-lrosplan_action_interface" != "" else []
PROJECT_NAME = "rosplan_planning_system"
PROJECT_SPACE_DIR = "/home/sunny/catkin_ws/devel"
PROJECT_VERSION = "0.0.0"
