# CMake generated Testfile for 
# Source directory: /home/sunny/catkin_ws/src
# Build directory: /home/sunny/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(octomap_mapping/octomap_mapping)
subdirs(robot)
subdirs(ROSPlan/rosplan)
subdirs(ROSPlan/rosplan_demos)
subdirs(ROSPlan/rosplan_rqt)
subdirs(test_world)
subdirs(turtlebot_simulator/turtlebot_gazebo)
subdirs(turtlebot_simulator/turtlebot_simulator)
subdirs(turtlebot_simulator/turtlebot_stage)
subdirs(turtlebot_simulator/turtlebot_stdr)
subdirs(mongodb_store/libmongocxx_ros)
subdirs(ROSPlan/rosplan_dispatch_msgs)
subdirs(ROSPlan/rosplan_knowledge_msgs)
subdirs(mongodb_store/mongodb_store_msgs)
subdirs(mongodb_store/mongodb_store)
subdirs(ROSPlan/rosplan_planning_system)
subdirs(ROSPlan/rosplan_interface_movebase)
subdirs(mongodb_store/mongodb_log)
subdirs(occupancy_grid_utils)
subdirs(octomap_mapping/octomap_server)
subdirs(ROSPlan/rosplan_interface_mapping)
subdirs(rosplan_interface_turtlebot2)
subdirs(ROSPlan/rosplan_knowledge_base)
