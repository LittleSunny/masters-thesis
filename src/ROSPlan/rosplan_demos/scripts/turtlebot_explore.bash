#!/bin/bash
#rosservice call /kcl_rosplan/roadmap_server/create_prm "{nr_waypoints: 20, min_distance: 0.3, casting_distance: 2.0, connecting_distance: 8.0, occupancy_threshold: 100, total_attempts: 10}";
rosservice call /kcl_rosplan/roadmap_server/create_prm "{nr_waypoints: 30, min_distance: 1, casting_distance: 6.0, connecting_distance: 10.0, occupancy_threshold: 50, total_attempts: 20}";
#rosservice call /kcl_rosplan/roadmap_server/ "{nr_waypoints: 3, min_distance: 0.3, casting_distance: 2.0, connecting_distance: 8.0, occupancy_threshold: 10, total_attempts: 10}";
rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 0
  instance_type: 'robot'
  instance_name: 'kenny'
  attribute_name: ''
  function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 0
  instance_type: 'covariance'
  instance_name: 'dummy1'
  attribute_name: ''
  function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 0
  instance_type: 'covariance'
  instance_name: 'dummy2'
  attribute_name: ''
  function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 1
  instance_type: ''
  instance_name: ''
  attribute_name: 'robot_at'
  values:
  - {key: 'r', value: 'kenny'}
  - {key: 'wp', value: 'wp0'}
  function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 2
  instance_type: ''
  instance_name: ''
  attribute_name: 'cov'
  function_value: 1.22";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 2
  instance_type: ''
  instance_name: ''
  attribute_name: 'relativeD'
  function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 2
  instance_type: ''
  instance_name: ''
  attribute_name: 'finalTrace'
  function_value: 0.5";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 2
  instance_type: ''
  instance_name: ''
  attribute_name: 'dFactor'
  function_value: 0.5";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 2
  instance_type: ''
  instance_name: ''
  attribute_name: 'update_covariance'
  function_value: 1.22";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 2
  instance_type: ''
  instance_name: ''
  attribute_name: 'predict_covariance'
  function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
knowledge:
  knowledge_type: 1
  instance_type: ''
  instance_name: ''
  attribute_name: 'observe'
  function_value: 0.0";


rosservice call /kcl_rosplan/update_knowledge_base "update_type: 1
knowledge:
    knowledge_type: 1
    instance_type: ''
    instance_name: ''
    attribute_name: 'robot_at'
    values:
    - {key: 'r', value: 'kenny'}
    - {key: 'wp', value: 'endpoint'}
    function_value: 0.0";
# rosservice call /kcl_rosplan/update_knowledge_base "update_type: 1
#     knowledge:
#         knowledge_type: 1
#         instance_type: ''
#         instance_name: ''
#         attribute_name: 'robot_at'
#         values:
#         - {key: 'r', value: 'kenny'}
#         - {key: 'wp', value: 'wp5'}
#         function_value: 0.0";

rosservice call /kcl_rosplan/update_knowledge_base "update_type: 1
knowledge:
    knowledge_type: 1
    instance_type: ''
    instance_name: ''
    attribute_name: 'lessthan'
    values:
    - {key: 'c', value: '(cov)'}
    - {key: 'f', value: 'finalTrace'}
    function_value: 0.0";
# for i in $(seq 0 $(( $(rosservice call /kcl_rosplan/get_current_instances "type_name: 'waypoint'" | sed 's/wp/\n/g' | wc -l) - 2)) )
# do
# rosservice call /kcl_rosplan/update_knowledge_base "update_type: 1
# knowledge:
#   knowledge_type: 1
#   instance_type: ''
#   instance_name: ''
#   attribute_name: 'visited'
#   values:
#   - {key: 'wp', value: 'wp$i'}
#   function_value: 0.0"
# done;

# rosservice call /kcl_rosplan/update_knowledge_base "update_type: 0
# knowledge:
#   knowledge_type: 1
#   instance_type: ''
#   instance_name: ''
#   attribute_name: 'robot_at'
#   values:
#   - {key: 'v', value: 'kenny'}
#   - {key: 'wp', value: 'wp0'}
#   function_value: 0.0";
#rosservice call /kcl_rosplan/roadmap_server/add_waypoint "{id: 'wp0',  connecting_distance: 16.0, occupancy_threshold: 15, x: -3.0, y: 0.0}";
#rosservice call /kcl_rosplan/roadmap_server/add_waypoint "{id: 'wp1',  connecting_distance: 16.0, occupancy_threshold: 15, x: 3.0, y: 0.0}";
rosservice call /kcl_rosplan/planning_server;
