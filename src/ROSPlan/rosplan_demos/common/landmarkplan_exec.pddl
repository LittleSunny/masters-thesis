domain_pred_update.cpp
domain_pred_update.h
domain_pred_update.m
domain_pred_update.pddl
domain_pred_update_planner
prob01_pred_update.pddl
problem_pred_update_plan.pddl
runplanner.sh

======================================================

DiNo version 1.1
Discretised Nonlinear Heuristic Planner for PDDL+ models with continous processes and events

Copyright (C) 2015
W. Piotrowski, M. Fox, D. Long, D. Magazzeni, F. Mercorio

Call with the -c flag or read the license file for terms
and conditions of use.
Run this program with "-h" for the list of options.
Send bug reports and comments to wiktor.piotrowski@kcl.ac.uk

======================================================

======================================================
Planning configuration

* Source domain: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update.pddl
* Source problem: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/problem_pred_update.pddl
* Planning Mode: Feasible Plan
* SRPG time horizon: 12
* Output format: PDDL+
* Epsilon separation: 0.001
* Output target: "/home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/problem_pred_update_plan.pddl"

* Model: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update
* State size 2433 bits (rounded to 308 bytes).
* Allocated memory: 1000 Megabytes

**  Time Discretisation = 1.000000
**  Digits for representing the integer part of a real =  5.000000
**  Digits for representing the fractional part of a real =  4
======================================================

=== Analyzing model... ===============================

* State Space Expansion Algorithm: SRPG+.
  with symmetry algorithm 3 -- Heuristic Small Memory Normalization
  with permutation trial limit 10.
* Maximum SRPG time horizon: 12.
* Maximum size of the state space: 4005271 states.
  with states hash-compressed to 40 bits.

START H VAL: 4.000
0

======================================================

Model exploration complete (in 1.36 seconds).
	6819 actions fired
	1 start states
	6434 reachable states
	1 goals found


=== Building model dynamics... =======================

* Transition Graph mode: Memory Image
* Maximum size of graph: 4020613 transitions.


======================================================

Model dynamics rebuilding complete (in 1.42 seconds).
	6434 states
	6819 transitions
	out degree: min 0 max 3 avg 1.06


=== Finding control paths... =========================

* Search Algorithm: Feasible Plan.


======================================================

Control paths calculation complete (in 1.42 seconds).
	6434 states
	48 controllable


=== Collecting plans... ==============================

PLAN: S0-R12-S6-R0-S77-R0-S143-R0-S214-R0-S290-R0-S371-R0-S457-R0-S548-R0-S644-R0-S745-R0-S851-R0-S963-R0-S1090-R0-S1211-R0-S1326-R0-S1445-R0-S1568-R0-S1695-R0-S1826-R0-S1961-R0-S2101-R0-S2255-R0-S2402-R0-S2542-R0-S2685-R0-S2831-R0-S2980-R0-S3132-R0-S3287-R0-S3446-R0-S3618-R0-S3782-R0-S3938-R0-S4096-R0-S4256-R0-S4418-R0-S4582-R0-S4748-R0-S4917-R0-S5098-R0-S5270-R0-S5433-R0-S5597-R0-S5762-R0-S5928-R0-S6095-R0-S6263-R6-S6433

======================================================

Plan(s) generation complete (in 1.42 seconds).
	1 plans
	plan length (actions): min 48 max 48 avg 48.00
	plan duration (time): min 0 max 46 avg 46.00
	plan weight: min 0 max 46 avg 46.00


=== Writing final results... =========================

* Output format: PDDL+
* Output target: "/home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/problem_pred_update_plan.pddl".


======================================================

Results Written (in 1.44 seconds).


