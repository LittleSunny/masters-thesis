domain: file "/home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/test/domain_turtlebot_demo.pddl"
problem: file "/home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/common/problem.pddl"
message: " Time Discretisation = 1.000000"
message: " Digits for representing the integer part of a real =  5.000000"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(10,4);
	integer: -1000..1000;

	 TIME_type: real(12,7);

	robot : Enum {kenny};
	waypoint : Enum {wp0,wp1,wp2,wp3,wp4,wp5};

const 
	 T:1.000000;


var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	goto_waypoint_clock_started[pddlname:"goto_waypoint";] : Array [robot] of Array [waypoint] of Array [waypoint] of  boolean ;
	goto_waypoint_clock [pddlname:"goto_waypoint";] : Array [robot] of Array [waypoint] of Array [waypoint] of  TIME_type ;


	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	connected[pddlname: "connected";] : Array [waypoint] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
procedure set_robot_at( v : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[v][wp] := value;
END;

function get_robot_at( v : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[v][wp];
END;

procedure set_connected( from : waypoint ; to_ : waypoint ;  value : boolean);
BEGIN
	connected[from][to_] := value;
END;

function get_connected( from : waypoint ; to_ : waypoint): boolean;
BEGIN
	return 	connected[from][to_];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;


procedure process_goto_waypoint( v : robot; from : waypoint; to_ : waypoint);
BEGIN
	 IF (goto_waypoint_clock_started[v][from][to_]) THEN 
		 goto_waypoint_clock[v][from][to_]:= goto_waypoint_clock[v][from][to_] + T ;
	 ENDIF;

END;
function event_goto_waypoint_failure( v : robot; from : waypoint; to_ : waypoint) : boolean; 
BEGIN
	 IF (goto_waypoint_clock_started[v][from][to_])& !((true)) THEN 
		 goto_waypoint_clock[v][from][to_]:= goto_waypoint_clock[v][from][to_]+ T ;
		 all_event_true := false ;
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;


procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   event_goto_waypoint_failure_triggered :  Array [robot] of  Array [waypoint] of  Array [waypoint] of  boolean;
BEGIN
 event_triggered := true;
   for v : robot do 
     for from : waypoint do 
       for to_ : waypoint do 
               event_goto_waypoint_failure_triggered[v][from][to_] := false;
               END;END;END; -- close for
while (event_triggered) do 
 event_triggered := false;
         for v : robot do 
           for from : waypoint do 
             for to_ : waypoint do 
               if(! event_goto_waypoint_failure_triggered[v][from][to_]) then 
               event_goto_waypoint_failure_triggered[v][from][to_] := event_goto_waypoint_failure(v,from,to_);
               event_triggered := event_triggered | event_goto_waypoint_failure_triggered[v][from][to_]; 
               endif;
END;END;END; -- close for
END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;
for v : robot do 
  for from : waypoint do 
    for to_ : waypoint do 
if (goto_waypoint_clock[v][from][to_] > 10.0000) then return true;
 endif;
END; -- close for 
END; -- close for 
END; -- close for 

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;
for v : robot do 
  for from : waypoint do 
    for to_ : waypoint do 
if (	goto_waypoint_clock_started[v][from][to_] = true) then return true;
 endif;
END; -- close for 
END; -- close for 
END; -- close for 

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_goto_waypoint_enabled :  Array [robot] of  Array [waypoint] of  Array [waypoint] of  boolean;
BEGIN
 process_updated := false; end_while := false;
   for v : robot do 
     for from : waypoint do 
       for to_ : waypoint do 
               process_goto_waypoint_enabled[v][from][to_] := false;
               END;END;END; -- close for
while (!end_while) do 
          for v : robot do 
           for from : waypoint do 
             for to_ : waypoint do 
               if ((true) & goto_waypoint_clock_started[v][from][to_] &  !process_goto_waypoint_enabled[v][from][to_]) then
               process_updated := true;
               process_goto_waypoint(v,from,to_);
               process_goto_waypoint_enabled[v][from][to_] := true;
               endif;
END;END;END; -- close for
IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;



ruleset v:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 durative_start rule " goto_waypoint_start " 
( !goto_waypoint_clock_started[v][from][to_]) & (robot_at[v][from]) & all_event_true ==> 
pddlname: " goto_waypoint"; 
BEGIN
goto_waypoint_clock_started[v][from][to_]:= true;
robot_at[v][from]:= false; 

END; 
END; 
END; 
END; 



ruleset v:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 durative_end rule " goto_waypoint_end " 
( goto_waypoint_clock_started[v][from][to_]) & (( goto_waypoint_clock[v][from][to_]) = (10.0000))  & ((goto_waypoint_clock[v][from][to_])  > 0.0) & all_event_true ==> 
pddlname: " goto_waypoint"; 
BEGIN
goto_waypoint_clock_started[v][from][to_]:= false;
goto_waypoint_clock[v][from][to_]:= 0.0;
robot_at[v][to_]:= true; 
visited[to_]:= true; 

END; 
END; 
END; 
END; 


clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for v : robot do 
  for wp : waypoint do 
    set_robot_at(v,wp, false);
END; END;  -- close for
   for from : waypoint do 
     for to_ : waypoint do 
       set_connected(from,to_, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   for wp1 : waypoint do 
     for wp2 : waypoint do 
       distance[wp1][wp2] := 0.0 ;
END; END;  -- close for
connected[wp0][wp1]:= true; 
connected[wp0][wp2]:= true; 
connected[wp0][wp3]:= true; 
connected[wp0][wp4]:= true; 
connected[wp0][wp5]:= true; 
connected[wp1][wp0]:= true; 
connected[wp1][wp2]:= true; 
connected[wp1][wp3]:= true; 
connected[wp1][wp4]:= true; 
connected[wp1][wp5]:= true; 
connected[wp2][wp0]:= true; 
connected[wp2][wp1]:= true; 
connected[wp2][wp3]:= true; 
connected[wp2][wp4]:= true; 
connected[wp2][wp5]:= true; 
connected[wp3][wp0]:= true; 
connected[wp3][wp1]:= true; 
connected[wp3][wp2]:= true; 
connected[wp3][wp4]:= true; 
connected[wp3][wp5]:= true; 
connected[wp4][wp0]:= true; 
connected[wp4][wp1]:= true; 
connected[wp4][wp2]:= true; 
connected[wp4][wp3]:= true; 
connected[wp4][wp5]:= true; 
connected[wp5][wp0]:= true; 
connected[wp5][wp1]:= true; 
connected[wp5][wp2]:= true; 
connected[wp5][wp3]:= true; 
connected[wp5][wp4]:= true; 
robot_at[kenny][wp0]:= true; 
distance[wp0][wp1] := 5.00000;
distance[wp0][wp2] := 9.48683;
distance[wp0][wp3] := 13.8924;
distance[wp0][wp4] := 18.3848;
distance[wp0][wp5] := 22.6274;
distance[wp1][wp0] := 5.00000;
distance[wp1][wp2] := 5.00000;
distance[wp1][wp3] := 9.89949;
distance[wp1][wp4] := 15.2643;
distance[wp1][wp5] := 19.4165;
distance[wp2][wp0] := 9.48683;
distance[wp2][wp1] := 5.00000;
distance[wp2][wp3] := 5.00000;
distance[wp2][wp4] := 10.7703;
distance[wp2][wp5] := 14.7648;
distance[wp3][wp0] := 13.8924;
distance[wp3][wp1] := 9.89949;
distance[wp3][wp2] := 5.00000;
distance[wp3][wp4] := 6.08276;
distance[wp3][wp5] := 9.84886;
distance[wp4][wp0] := 18.3848;
distance[wp4][wp1] := 15.2643;
distance[wp4][wp2] := 10.7703;
distance[wp4][wp3] := 6.08276;
distance[wp4][wp5] := 4.24264;
distance[wp5][wp0] := 22.6274;
distance[wp5][wp1] := 19.4165;
distance[wp5][wp2] := 14.7648;
distance[wp5][wp3] := 9.84886;
distance[wp5][wp4] := 4.24264;

-- durative action "goto_waypoint" clock initialization
 for v : robot do 
   for from : waypoint do 
     for to_ : waypoint do 
       goto_waypoint_clock_started[v][from][to_]:= false;
      goto_waypoint_clock[v][from][to_]:= 0.0;
END; END; END; -- for ends

all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (robot_at[kenny][wp5])& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


