Run with the -l flag or read the license file for terms
and conditions of use.
Run this program with "-h" for the list of options.
Bugs, questions, and comments should be directed to
"wiktor.piotrowski@kcl.ac.uk".

DiNo/UPMurphi compiler last compiled date: Jul  3 2018
===========================================================================

===========================================================================
DiNo Release 1.1
Discretised Nonlinear Heuristic Planner for PDDL+ models with continous processes and events.

DiNo Release 1.1 :
Copyright (C) 2015: W. Piotrowski, M. Fox, D. Long, D. Magazzeni, F. Mercorio.
DiNo Release 1.1 is based on UPMurphi release 3.0.

Universal Planner Murphi Release 3.0 :
Copyright (C) 2007 - 2015: G. Della Penna, B. Intrigila, D. Magazzeni, F. Mercorio.
Universal Planner Murphi Release 3.0 is based on CMurphi release 5.4.

CMurphi Release 5.4 :
Copyright (C) 2001 - 2003 by E. Tronci, G. Della Penna, B. Intrigila, I. Melatti, M. Zilli.
CMurphi Release 5.4 is based on Murphi release 3.1.

Murphi Release 3.1 :
Copyright (C) 1992 - 1999 by the Board of Trustees of
Leland Stanford Junior University.

===========================================================================
PDDL domain: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT.pddl (not found)
PDDL problem: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/problem_pred_update_RRT.pddl (not found)
DiNo model: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT.m (found)
C++ source: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT.cpp (found)
Executable planner: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT_planner (found)
Compiling model...
Model compilation successful, no errors
C++ source code generated in file /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT.cpp
Compiling executable planner, please wait...
Planner compilation successful, no errors
Executable planner generated in file /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT_planner
Call .//home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT_planner to execute the planner with default options
