(define (domain landmark)

(:requirements :typing :durative-actions :fluents :time :strips  :disjunctive-preconditions :durative-actions
:negative-preconditions :timed-initial-literals)

(:types
  waypoint
  robot
  covariance
)

(:predicates 
       (robot_at ?r - robot ?wp - waypoint)
       (visited ?wp - waypoint)
       (observe)
       (moving ?r - robot ?to - waypoint)
       (connected ?from ?to - waypoint)
       (lessthan ?c ?f - covariance)   
)

(:functions (distance ?wp1 ?wp2 - waypoint) (cov) (counter) (update_covariance) (predict_covariance) (relativeD)  (dFactor)
       (finalTrace)
)  
;; relativeD- a variable to store the distance between wps, dFactor- distance factor, to see how many times kalman prediction to be done
;; cov is the initial covarianc trace, finalTrace- the required trace upon reaching the goal state

;; Move between any two waypoints, along the straight line between the two waypoints

(:action goto_waypoint
  :parameters (?r - robot ?from ?to - waypoint)
  :precondition  (and (robot_at ?r ?from) (observe) (not(robot_at ?r ?to)) (not (visited ?to )) (connected ?from ?to)) 
  :effect (and (not (robot_at ?r ?from)) 
            (assign (relativeD) (distance ?from ?to))
            (moving ?r ?to)
            (not (observe))
            (assign (counter) 0)
                  (assign (update_covariance) 0)
            (assign (predict_covariance) 0)      
            )           
)

(:action reached
    :parameters(?r - robot ?to - waypoint)
    :precondition(and (moving ?r ?to) (<= (relativeD) 0))
    :effect(and (robot_at ?r ?to) (visited ?to) (not (moving ?r ?to)))
)   
  

(:event predict_n_update
  ;;:parameters(?r - robot ?to - waypoint)
  :parameters()
    :precondition(and (> (counter) 0) )
    :effect (and 
              (assign (cov) (update_covariance))
              (assign (counter) 0)
        )
)

;; to calculate the number of prediction steps needed
(:process control
  :parameters()
  :precondition (and  (> (relativeD) (-dFactor)) )
  :effect (and  (decrease (relativeD) (* #t (dFactor)))   (increase (counter) (* #t 1))
)
)
)
(define (problem landmark_task)
(:domain landmark)
(:objects
    dummy1 dummy2 - covariance
    kenny - robot
    endpoint wp0 wp1 wp2 wp3 wp4 wp5 wp6 - waypoint
)
(:init
    (connected endpoint wp1)
    (connected endpoint wp2)
    (connected wp0 wp1)
    (connected wp0 wp2)
    (connected wp0 wp3)
    (connected wp1 wp0)
    (connected wp1 endpoint)
    (connected wp2 wp0)
    (connected wp2 wp4)
    (connected wp2 endpoint)
    (connected wp3 wp0)
    (connected wp3 wp5)
    (connected wp4 wp2)
    (connected wp4 wp6)
    (connected wp5 wp3)
    (connected wp6 wp4)
    (observe)
    (robot_at kenny wp0)
    (= (cov) 1.22)
    (= (dFactor) 0.5)
    (= (distance endpoint wp1) 4.80937)
    (= (distance endpoint wp2) 3.66231)
    (= (distance wp0 wp1) 3.50036)
    (= (distance wp0 wp2) 3.66879)
    (= (distance wp0 wp3) 2.97742)
    (= (distance wp1 wp0) 3.50036)
    (= (distance wp1 endpoint) 4.80937)
    (= (distance wp2 wp0) 3.66879)
    (= (distance wp2 wp4) 7.84235)
    (= (distance wp2 endpoint) 3.66231)
    (= (distance wp3 wp0) 2.97742)
    (= (distance wp3 wp5) 3.75133)
    (= (distance wp4 wp2) 7.84235)
    (= (distance wp4 wp6) 6.15508)
    (= (distance wp5 wp3) 3.75133)
    (= (distance wp6 wp4) 6.15508)
    (= (finalTrace) 30)
    (= (predict_covariance) 0)
    (= (relativeD) 0)
    (= (update_covariance) 1.22)
    (visited wp0)
)
(:goal (and
    (robot_at kenny endpoint)
    (visited endpoint)
    (< (cov) finalTrace)
))
(:metric minimize(total-time)))
