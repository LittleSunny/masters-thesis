domain_pred_update.cpp
domain_pred_update.h
domain_pred_update.m
domain_pred_update.pddl
domain_pred_update_planner
domain_pred_update_RRT_backup.pddl
domain_pred_update_RRT.cpp
domain_pred_update_RRT.h
domain_pred_update_RRT.m
domain_pred_update_RRT.pddl
domain_pred_update_RRT_planner
parsing_file.pddl
plan_1
plan_14226529
plan_14230625
plan_exec.pddl
plan.pddl
prob01_pred_update.pddl
problem_pred_update.pddl
problem_pred_update_plan.pddl
problem_pred_update_RRT.pddl
problem_pred_update_RRT_plan.pddl
runplanner.sh
test1
waypoints_rrt.txt

======================================================

DiNo version 1.1
Discretised Nonlinear Heuristic Planner for PDDL+ models with continous processes and events

Copyright (C) 2015
W. Piotrowski, M. Fox, D. Long, D. Magazzeni, F. Mercorio

Call with the -c flag or read the license file for terms
and conditions of use.
Run this program with "-h" for the list of options.
Send bug reports and comments to wiktor.piotrowski@kcl.ac.uk

======================================================

======================================================
Planning configuration

* Source domain: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT.pddl
* Source problem: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/problem_pred_update_RRT.pddl
* Planning Mode: Feasible Plan
* SRPG time horizon: 12
* Output format: PDDL+
* Epsilon separation: 0.001
* Output target: "/home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/problem_pred_update_RRT_plan.pddl"

* Model: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/domain_pred_update_RRT
* State size 56313 bits (rounded to 7040 bytes).
* Allocated memory: 1000 Megabytes

**  Time Discretisation = 1.000000
**  Digits for representing the integer part of a real =  5.000000
**  Digits for representing the fractional part of a real =  4
======================================================
DEBUG SUNNY EXTERNAL
SUNNY DEBUG this is external solver name /home/sunny/catkin_ws/DiNo/ex/ext_lib/build/libext_lib.so
/home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/waypoints_rrt.txt
/home/sunny/catkin_ws/DiNo/ex/landmark/landmark.txt

=== Analyzing model... ===============================

* State Space Expansion Algorithm: SRPG+.
  with symmetry algorithm 3 -- Heuristic Small Memory Normalization
  with permutation trial limit 10.
* Maximum SRPG time horizon: 12.
* Maximum size of the state space: 185677 states.
  with states hash-compressed to 40 bits.

START H VAL: 3.000
0
1.2277986595
1.2454844354
1.2732930203
1.3112306640
1.3593036167
1.3659514329
1.3664103181
1.3668692032
1.2277701126
1.2285549086
1.2593435367
1.2474892506
1.2284826274
1.2523818423
1.4072673770
1.3469235515
1.3914468776
1.3723745765
1.3469235515
1.3914468776
1.3723745765
1.3469235515
1.3914468776
1.2350867107
1.2374926760
1.2358656834
1.2590547391
1.2321573275
1.2381802162
1.2434214840
1.2565030902
1.3477942511
1.3638987860
1.3654053061
1.3496547448
1.3455882891
1.3812069822
1.3764686417
1.4200378772
1.3471686285
1.3686806018
1.3477942511
1.4072673770
1.3638987860
1.3654053061
1.3496547448
1.3455882891
1.3812069822
1.3764686417
1.4200378772
1.3471686285
1.3686806018
1.3477942511
1.4072673770
1.3638987860
1.3654053061
1.3496547448
1.3455882891
1.3812069822
1.3764686417
1.4200378772
1.3723745765
1.3471686285
1.3686806018
1.3477942511
1.4072673770
1.3469235515
1.3638987860
1.3654053061
1.3914468776
1.3496547448
1.3455882891
1.3812069822
1.3764686417
1.4200378772
1.3723745765
1.3471686285
1.3686806018
1.3477942511
1.4072673770
1.3469235515
1.3638987860
1.3654053061
1.3914468776
1.3496547448
1.3455882891
1.3812069822
1.3764686417
1.4200378772
1.3723745765
1.3471686285
1.3686806018
1.3477942511
1.4072673770
1.3469235515
1.3638987860
1.3654053061
1.3914468776
1.3496547448
1.3455882891
1.3812069822
1.3764686417
1.4200378772
1.3723745765
1.3471686285
1.3686806018
1.3477942511
1.4072673770
1.3469235515
1.3638987860
1.3654053061
1.3914468776
1.3496547448
1.3455882891
1.3812069822
1.3764686417
1.4200378772
1.3723745765
1.3471686285
1.3686806018
1.2277986595
1.2381802162
1.2277701126
1.2350867107
1.2434214840
1.2285549086
1.2593435367
1.2474892506
1.2284826274
1.2374926760
1.2358656834
1.2590547391
1.2523818423
1.2565030902
1.2321573275
1.3673280884
1.2454392915
1.2466803642
1.4226872542
1.2766230445
1.2465660591
1.2843601662
1.4583616736
1.3776859055
1.4270086556
1.3863195214
1.3776859055
1.4270086556
1.3863195214
1.3776859055
1.4270086556
0.0563419850
0.0263216562
0.0308259931
0.0109455997
0.0129820827
0.0216098764
0.0265942241
0.0491177086
0.0109455997
0.0128493507
0.0218120308
0.0271020908
0.0495918321
0.0109455997
0.0128822124
0.0216773341
0.0266873533
0.0492859491
0.0685645391
0.0109455997
0.0128306894
0.0218330841
0.0270957300
0.0506302646
0.0717690814
0.2327749860
0.2945512993
0.3646123883
0.0496268983
0.0690894422
0.0752852036
0.0109455997
0.0131067357
0.0213551180
0.0256604456
0.0299649960
0.0425234608
0.0591770630
0.0799320524
0.0934407780
0.0947555449
0.0964575584

======================================================

Model exploration complete (in 19.88 seconds).
	838 actions fired
	1 start states
	834 reachable states
	1 goals found


=== Building model dynamics... =======================

* Transition Graph mode: Memory Image
* Maximum size of graph: 185706 transitions.


======================================================

Model dynamics rebuilding complete (in 19.94 seconds).
	834 states
	838 transitions
	out degree: min 0 max 3 avg 1.00


=== Finding control paths... =========================

* Search Algorithm: Feasible Plan.


======================================================

Control paths calculation complete (in 19.94 seconds).
	834 states
	18 controllable


=== Collecting plans... ==============================

PLAN: S0-R70-S5-R0-S163-R0-S337-R0-S338-R8-S339-R250-S341-R0-S731-R0-S732-R0-S733-R2-S734-R63-S736-R0-S826-R0-S827-R0-S828-R0-S829-R0-S830-R0-S831-R1-S832

======================================================

Plan(s) generation complete (in 19.94 seconds).
	1 plans
	plan length (actions): min 18 max 18 avg 18.00
	plan duration (time): min 0 max 12 avg 12.00
	plan weight: min 0 max 12 avg 12.00


=== Writing final results... =========================

* Output format: PDDL+
* Output target: "/home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/problem_pred_update_RRT_plan.pddl".


======================================================

Results Written (in 24.46 seconds).


