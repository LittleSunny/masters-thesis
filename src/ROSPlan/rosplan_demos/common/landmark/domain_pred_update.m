domain: file "domain_pred_update.pddl"
problem: file "prob01_pred_update.pddl"
message: " Time Discretisation = 1.000000"
message: " Digits for representing the integer part of a real =  5.000000"
message: " Digits for representing the fractional part of a real =  4"
type
	 real_type: real(10,4);
	integer: -1000..1000;

	 TIME_type: real(12,7);

	robot : Enum {kenny};
	waypoint : Enum {wp0,wp1,wp2,wp3,wp4,wp5};

const 
	 T:1.000000;

	dfactor : 0.500000;
	finaltrace : 0.200000;

var 
	all_event_true: boolean;
	 h_n: integer;
	 g_n: integer;
	 f_n: integer;
	 TIME[pddlname:"upmurphi_global_clock";]:TIME_type;
	counter[pddlname:"counter";] :  real_type;
	cov[pddlname:"cov";] :  real_type;
	distance[pddlname:"distance";] : Array [waypoint] of Array [waypoint] of  real_type;
	predict_covariance[pddlname:"predict_covariance";] :  real_type;
	relatived[pddlname:"relatived";] :  real_type;
	update_covariance[pddlname:"update_covariance";] :  real_type;


	robot_at[pddlname: "robot_at";] : Array [robot] of Array [waypoint] of  boolean;
	visited[pddlname: "visited";] : Array [waypoint] of  boolean;
	observe[pddlname: "observe";] :  boolean;
	moving[pddlname: "moving";] : Array [robot] of Array [waypoint] of  boolean;


-- External function declaration 

externfun ext_assignment(value : real_type) : real_type;
externfun assign_cov_event_predict_n_update(update_covariance : real_type ; ): real_type "domain_pred_update.h" ;
externfun assign_counter_event_predict_n_update(): real_type ;
externfun increase_counter_process_control(counter : real_type ; T : real_type ; ): real_type ;
externfun decrease_relatived_process_control(relatived : real_type ; T : real_type ; dfactor : real_type ; ): real_type ;
externfun assign_relatived_action_goto_waypoint(distance : real_type ; ): real_type ;
externfun assign_counter_action_goto_waypoint(): real_type ;
externfun assign_update_covariance_action_goto_waypoint(): real_type ;
externfun assign_predict_covariance_action_goto_waypoint(): real_type ;
procedure set_robot_at( r : robot ; wp : waypoint ;  value : boolean);
BEGIN
	robot_at[r][wp] := value;
END;

function get_robot_at( r : robot ; wp : waypoint): boolean;
BEGIN
	return 	robot_at[r][wp];
END;

procedure set_visited( wp : waypoint ;  value : boolean);
BEGIN
	visited[wp] := value;
END;

function get_visited( wp : waypoint): boolean;
BEGIN
	return 	visited[wp];
END;

procedure set_observe(  value : boolean);
BEGIN
	observe := value;
END;

function get_observe(): boolean;
BEGIN
	return 	observe;
END;

procedure set_moving( r : robot ; to_ : waypoint ;  value : boolean);
BEGIN
	moving[r][to_] := value;
END;

function get_moving( r : robot ; to_ : waypoint): boolean;
BEGIN
	return 	moving[r][to_];
END;









function predict_n_update () : boolean; 
BEGIN
IF (((( counter) > (0.00000)))) THEN 
cov := assign_cov_event_predict_n_update(update_covariance  );
counter := assign_counter_event_predict_n_update();
		 return true; 
 	 ELSE return false;
	 ENDIF;

END;

procedure process_control (); 
BEGIN
IF (((( relatived) > (-dfactor)))) THEN 
counter := increase_counter_process_control(counter , T  );
relatived := decrease_relatived_process_control(relatived , T , dfactor  );

ENDIF ; 

END;



procedure event_check();
 var -- local vars declaration 
   event_triggered : boolean;
   predict_n_update_triggered :  boolean;
BEGIN
 event_triggered := true;
   predict_n_update_triggered := false;
while (event_triggered) do 
 event_triggered := false;
   if(! predict_n_update_triggered) then 
   predict_n_update_triggered := predict_n_update();
   event_triggered := event_triggered | predict_n_update_triggered; 
   endif;

END; -- close while loop 
END;



 function DAs_violate_duration() : boolean ; 
 var -- local vars declaration 
 DA_duration_violated : boolean;
 BEGIN
 DA_duration_violated := false;

 return DA_duration_violated; 
 END; -- close begin


 function DAs_ongoing_in_goal_state() : boolean ; 
 var -- local vars declaration 
 DA_still_ongoing : boolean;
 BEGIN
 DA_still_ongoing := false;

 return DA_still_ongoing; 
 END; -- close begin


procedure apply_continuous_change();
 var -- local vars declaration 
   process_updated : boolean;
 end_while : boolean;   process_control_enabled :  boolean;
BEGIN
 process_updated := false; end_while := false;
   process_control_enabled := false;
while (!end_while) do 
    if (((( relatived) > (-dfactor))) &  !process_control_enabled) then
   process_updated := true;
   process_control();
   process_control_enabled := true;
   endif;

IF (!process_updated) then
	 end_while:=true;
 else process_updated:=false;
endif;END; -- close while loop 
END;

ruleset r:robot do 
 ruleset from:waypoint do 
 ruleset to_:waypoint do 
 action rule " goto_waypoint " 
(robot_at[r][from]) & (observe) & (!(robot_at[r][to_])) & (!(visited[to_])) ==> 
pddlname: " goto_waypoint"; 
BEGIN
moving[r][to_]:= true; 
robot_at[r][from]:= false; 
observe:= false; 
relatived := assign_relatived_action_goto_waypoint(distance[from][to_]  );
counter := assign_counter_action_goto_waypoint();
update_covariance := assign_update_covariance_action_goto_waypoint();
predict_covariance := assign_predict_covariance_action_goto_waypoint();

END; 
END; 
END; 
END;

ruleset r:robot do 
 ruleset to_:waypoint do 
 action rule " reached " 
(moving[r][to_]) & ((( relatived) <= (0.00000))) ==> 
pddlname: " reached"; 
BEGIN
robot_at[r][to_]:= true; 
visited[to_]:= true; 
moving[r][to_]:= false; 

END; 
END; 
END;

clock rule " time passing " 
 (true) ==> 
BEGIN 
 	TIME := TIME + T;

 	 event_check();
	 apply_continuous_change();
	 event_check();
END;


startstate "start" 
BEGIN 
TIME := 0.0;
for r : robot do 
  for wp : waypoint do 
    set_robot_at(r,wp, false);
END; END;  -- close for
   for wp : waypoint do 
     set_visited(wp, false);
END;  -- close for
   set_observe(false);

   for r : robot do 
     for to_ : waypoint do 
       set_moving(r,to_, false);
END; END;  -- close for
   for wp1 : waypoint do 
     for wp2 : waypoint do 
       distance[wp1][wp2] := 0.0 ;
END; END;  -- close for
   cov := 0.0 ;



   relatived := 0.0 ;

   counter := 0.0 ;

   update_covariance := 0.0 ;

   predict_covariance := 0.0 ;

robot_at[kenny][wp0]:= true; 
observe:= true; 
distance[wp0][wp1] := 5.00000;
distance[wp0][wp2] := 9.48683;
distance[wp0][wp3] := 13.8924;
distance[wp0][wp4] := 18.3848;
distance[wp0][wp5] := 22.6274;
distance[wp1][wp0] := 5.00000;
distance[wp1][wp2] := 5.00000;
distance[wp1][wp3] := 9.89949;
distance[wp1][wp4] := 15.2643;
distance[wp1][wp5] := 19.4165;
distance[wp2][wp0] := 9.48683;
distance[wp2][wp1] := 5.00000;
distance[wp2][wp3] := 5.00000;
distance[wp2][wp4] := 10.7703;
distance[wp2][wp5] := 14.7648;
distance[wp3][wp0] := 13.8924;
distance[wp3][wp1] := 9.89949;
distance[wp3][wp2] := 5.00000;
distance[wp3][wp4] := 6.08276;
distance[wp3][wp5] := 9.84886;
distance[wp4][wp0] := 18.3848;
distance[wp4][wp1] := 15.2643;
distance[wp4][wp2] := 10.7703;
distance[wp4][wp3] := 6.08276;
distance[wp4][wp5] := 4.24264;
distance[wp5][wp0] := 22.6274;
distance[wp5][wp1] := 19.4165;
distance[wp5][wp2] := 14.7648;
distance[wp5][wp3] := 9.84886;
distance[wp5][wp4] := 4.24264;
cov := 1.22000;
relatived := 0.00000;
update_covariance := 1.22000;
predict_covariance := 0.00000;
all_event_true := true;
g_n := 0;
h_n := 0;
f_n := 0;
END; -- close startstate

goal "enjoy" 
 (robot_at[kenny][wp5]) & ((( cov) < (finaltrace)))& !DAs_ongoing_in_goal_state(); 

invariant "todo bien" 
 all_event_true & !DAs_violate_duration();
metric: minimize;


