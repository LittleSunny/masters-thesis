#include "rosplan_interface_mapping/RPRoadmapServer.h"
#include <occupancy_grid_utils/ray_tracer.h>
#include <occupancy_grid_utils/coordinate_conversions.h>

/* implementation of rosplan_interface_mapping::RPRoadmapServer */
namespace KCL_rosplan {

	/* constructor */
	RPRoadmapServer::RPRoadmapServer(ros::NodeHandle &nh)
	 : message_store(nh) {

		// config
		std::string dataPath("common/");
		std::string staticMapService("/static_map");
		nh.param("/rosplan/data_path", data_path, dataPath);
		nh.param("static_map_service", static_map_service, staticMapService);
		nh.param("use_static_map", use_static_map, false);
		nh.param("landmark_path", landmark_path, landmark_path);
		nh.param("goal_point", goal_point, goal_point);
		nh.param("waypoint_output_path", waypoint_output_path, waypoint_output_path);

		nh.param("waypoint_input_file", waypoint_input_file, waypoint_input_file);
		nh.param("problem_input_file", problem_input_file, problem_input_file);
		nh.param("make_RRT_file", make_RRT_file, false);


		// knowledge interface
		update_knowledge_client = nh.serviceClient<rosplan_knowledge_msgs::KnowledgeUpdateService>("/kcl_rosplan/update_knowledge_base");
		
		// visualisati
		waypoints_pub = nh.advertise<visualization_msgs::MarkerArray>("/kcl_rosplan/viz/waypoints", 10, true);
		edges_pub = nh.advertise<visualization_msgs::Marker>("/kcl_rosplan/viz/edges", 10, true);

		// map interface
		map_client = nh.serviceClient<nav_msgs::GetMap>(static_map_service);
	}

	/*------------------*/
	/* callback methods */
	/*------------------*/

	/* update the costmap */
	void RPRoadmapServer::costMapCallback( const nav_msgs::OccupancyGridConstPtr& msg ) {
		cost_map = *msg;
	}

	/* update position of the robot */
	void RPRoadmapServer::odomCallback( const nav_msgs::OdometryConstPtr& msg ) {
		//we assume that the odometry is published in the frame of the base
		base_odom.header = msg->header;
		base_odom.pose.position = msg->pose.pose.position;
		base_odom.pose.orientation = msg->pose.pose.orientation;
	}

	/*-----------*/
	/* build PRM */
	/*-----------*/

	/**
	 * Check if two waypoints can be connected without colliding with any known scenery. The line should not
	 * come closer than @ref{min_width} than any known obstacle.
	 * @param w1 The first waypoint.
	 * @param w2 The second waypoint.
	 * @param threshold A value between -1 and 255 above which a cell is considered to be occupied.
	 * @return True if the waypoints can be connected, false otherwise.
	 */
	bool RPRoadmapServer::canConnect(const geometry_msgs::Point& w1, const geometry_msgs::Point& w2, int threshold) const
	{
		/*
		if (!has_received_occupancy_grid_)
		{
			return true;
		}
		*/
		
		// Check if the turtlebot is going to collide with any known obstacle.
		occupancy_grid_utils::RayTraceIterRange ray_range = occupancy_grid_utils::rayTrace(cost_map.info, w1, w2);
		
		for (occupancy_grid_utils::RayTraceIterator i = ray_range.first; i != ray_range.second; ++i)
		{
			const occupancy_grid_utils::Cell& cell = *i;

			// Check if this cell is occupied.
			if (cost_map.data[cell.x + cell.y * cost_map.info.width] > threshold)
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Generates waypoints and stores them in the knowledge base and scene database
	 */
	bool RPRoadmapServer::generateRoadmap(rosplan_knowledge_msgs::CreatePRM::Request &req, rosplan_knowledge_msgs::CreatePRM::Response &res) {

		ros::NodeHandle nh("~");

		// clear previous roadmap from knowledge base
		ROS_INFO("KCL: (RPRoadmapServer) Cleaning old roadmap");
		rosplan_knowledge_msgs::KnowledgeUpdateService updateSrv;
		updateSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::REMOVE_KNOWLEDGE;
		updateSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::INSTANCE;
		updateSrv.request.knowledge.instance_type = "waypoint";
		update_knowledge_client.call(updateSrv);

		// clear previous roadmap from scene database
		for (std::map<std::string,Waypoint*>::iterator wit=waypoints.begin(); wit!=waypoints.end(); ++wit) {
			message_store.deleteID(db_name_map[wit->first]);
		}
		db_name_map.clear();

		// clear from visualization
		clearMarkerArrays(nh);
 
		// read map
		nav_msgs::OccupancyGrid map;
		if(use_static_map) {
			ROS_INFO("KCL: (RPRoadmapServer) Reading in map");
			nav_msgs::GetMap mapSrv;
			map_client.call(mapSrv);
			map = mapSrv.response.map;
		} else {
			map = cost_map;
		}

		// generate waypoints
		ROS_INFO("KCL: (RPRoadmapServer) Generating roadmap");
		/*
		 * nr_waypoints, number of waypoints to generate before function terminates.
		 * min_distance, the minimum distance allowed between any pair of waypoints.
		 * casting_distance, the maximum distance a waypoint can be cast.
		 * connecting_distance, the maximum distance that can exists between waypoints for them to be connected.
		 * occupancy_threshold, a number between 0 and 255; determines above which value a cell is considered occupied.
		 */
		//createPRM(map, req.nr_waypoints, req.min_distance, req.casting_distance, req.connecting_distance, req.occupancy_threshold, req.total_attempts);
		if(make_RRT_file)
		{
			createRRT(map, req.nr_waypoints, req.min_distance, req.casting_distance, req.connecting_distance, req.occupancy_threshold, req.total_attempts);
		}
		else
		{
			LoadRRT(waypoint_input_file,problem_input_file,map);
		}
		// publish visualization
		publishWaypointMarkerArray(nh);
		publishEdgeMarkerArray(nh);

		// add roadmap to knowledge base and scene database
		ROS_INFO("KCL: (RPRoadmapServer) Adding knowledge");
		for (std::map<std::string,Waypoint*>::iterator wit=waypoints.begin(); wit!=waypoints.end(); ++wit) {

			// instance
			rosplan_knowledge_msgs::KnowledgeUpdateService updateSrv;
			updateSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::ADD_KNOWLEDGE;
			updateSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::INSTANCE;
			updateSrv.request.knowledge.instance_type = "waypoint";
			updateSrv.request.knowledge.instance_name = wit->first;
			update_knowledge_client.call(updateSrv);

			res.waypoints.push_back(wit->first);
			
			// predicates
			for (std::vector<std::string>::iterator nit=wit->second->neighbours.begin(); nit!=wit->second->neighbours.end(); ++nit) {
				rosplan_knowledge_msgs::KnowledgeUpdateService updatePredSrv;
				updatePredSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::ADD_KNOWLEDGE;
				updatePredSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::FACT;
				updatePredSrv.request.knowledge.attribute_name = "connected";
				diagnostic_msgs::KeyValue pairFrom;
				pairFrom.key = "from";
				pairFrom.value = wit->first;
				updatePredSrv.request.knowledge.values.push_back(pairFrom);
				diagnostic_msgs::KeyValue pairTo;
				pairTo.key = "to";
				pairTo.value = *nit;
				updatePredSrv.request.knowledge.values.push_back(pairTo);
				update_knowledge_client.call(updatePredSrv);	
			}

			// functions
			for (std::vector<std::string>::iterator nit=wit->second->neighbours.begin(); nit!=wit->second->neighbours.end(); ++nit) {
				rosplan_knowledge_msgs::KnowledgeUpdateService updateFuncSrv;
				updateFuncSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::ADD_KNOWLEDGE;
				updateFuncSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::FUNCTION;
				updateFuncSrv.request.knowledge.attribute_name = "distance";
				diagnostic_msgs::KeyValue pairFrom;
				pairFrom.key = "wp1";
				pairFrom.value = wit->first;
				updateFuncSrv.request.knowledge.values.push_back(pairFrom);
				diagnostic_msgs::KeyValue pairTo;
				pairTo.key = "wp2";
				pairTo.value = *nit;
				updateFuncSrv.request.knowledge.values.push_back(pairTo);
				double dist = sqrt(
						(wit->second->real_x - waypoints[*nit]->real_x)*(wit->second->real_x - waypoints[*nit]->real_x)
						+ (wit->second->real_y - waypoints[*nit]->real_y)*(wit->second->real_y - waypoints[*nit]->real_y));
				updateFuncSrv.request.knowledge.function_value = dist;
				update_knowledge_client.call(updateFuncSrv);
			}

			//data
			geometry_msgs::PoseStamped pose;
			pose.header.frame_id = map.header.frame_id;
			pose.pose.position.x = wit->second->real_x;
			pose.pose.position.y = wit->second->real_y;
			pose.pose.position.z = 0.0;
			pose.pose.orientation.x = 0.0;;
			pose.pose.orientation.y = 0.0;;
			pose.pose.orientation.z = 0.0;
			pose.pose.orientation.w = 1.0;
			//std::string id(message_store.insertNamed(wit->first, pose));
			std::vector< boost::shared_ptr<geometry_msgs::PoseStamped> > checkResults; 
			if (wit->first == "endpoint")
			 	{
				 	std::stringstream gazeboss;
				 	gazeboss << "rosrun gazebo_ros spawn_model -database checkerboard_plane -sdf -model " << wit->first << " -x " << pose.pose.position.x << " -y " << pose.pose.position.y;
				 	std::string p_s = gazeboss.str();
				 	std::system(p_s.c_str());
				 	ROS_INFO("%s", p_s.c_str());
			 	}
			if(message_store.queryNamed<geometry_msgs::PoseStamped>(wit->first, checkResults))
			{ 
				message_store.updateNamed(wit->first, pose);

			} 
			else
			{
				std::string id(message_store.insertNamed(wit->first, pose));
				db_name_map[wit->first] = id;
			}
			//db_name_map[wit->first] = id;
		}

		// robot start position (TODO remove)
		/*updateSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::ADD_KNOWLEDGE;
		updateSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::FACT;
		updateSrv.request.knowledge.attribute_name = "robot_at";
		diagnostic_msgs::KeyValue pair1, pair2;
		pair1.key = "v";
		pair1.value = "kenny";
		updateSrv.request.knowledge.values.push_back(pair1);
		pair2.key = "wp";
		pair2.value = "wp0";
		updateSrv.request.knowledge.values.push_back(pair2);
		update_knowledge_client.call(updateSrv);
		*/

		ROS_INFO("KCL: (RPRoadmapServer) Done");
		return true;
	}

	/*
	 * Input:
	 * 	nr_waypoints, number of waypoints to generate before function terminates.
	 * 	min_distance, the minimum distance allowed between any pair of waypoints.
	 * 	casting_distance, the maximum distance a waypoint can be cast.
	 * 	connecting_distance, the maximum distance that can exists between waypoints for them to be connected.
	 * 	occupancy_threshold, a number between 0 and 255; determines above which value a cell is considered occupied.
	 * Output: A roadmap G = (V, E)
	 */
	void RPRoadmapServer::createPRM(nav_msgs::OccupancyGrid map, unsigned int nr_waypoints, double min_distance, double casting_distance, double connecting_distance, int occupancy_threshold, int total_attempts) {

		// map info
		int width = map.info.width;
		int height = map.info.height;
		double resolution = map.info.resolution; // m per cell

		if(width==0 || height==0) {
			ROS_INFO("KCL: (RPRoadmapServer) Empty map");
			return;
		}

		// V <-- empty set; E <-- empty set.
		for (std::map<std::string, Waypoint*>::const_iterator ci = waypoints.begin(); ci != waypoints.end(); ++ci) {
			delete (*ci).second;
		}
		waypoints.clear();
		edges.clear();

		// create robot start point
		geometry_msgs::PoseStamped start_pose;
		geometry_msgs::PoseStamped start_pose_transformed;
		start_pose.header = base_odom.header;
		start_pose.pose.position = base_odom.pose.position;
		start_pose.pose.orientation = base_odom.pose.orientation; 
		try {
			tf.waitForTransform( base_odom.header.frame_id, "map", ros::Time::now(), ros::Duration( 500 ) );
			tf.transformPose( "map", start_pose,  start_pose_transformed);
		} catch(tf::LookupException& ex) {
			ROS_ERROR("Lookup Error: %s\n", ex.what());
			return;
		} catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return;
		} catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return;
		}

		occupancy_grid_utils::Cell start_cell = occupancy_grid_utils::pointCell(map.info, start_pose_transformed.pose.position);
		//this gives the starting position of the robot
		Waypoint* start_wp = new Waypoint("wp0", start_cell.x, start_cell.y, map.info);
		waypoints[start_wp->wpID] = start_wp;

		int loop_counter = 0;
		while(waypoints.size() < nr_waypoints && ++loop_counter < total_attempts) {

			// Sample a random waypoint.
			std::map<std::string, Waypoint*>::iterator item = waypoints.begin();
			std::advance(item, rand() % waypoints.size());
			Waypoint* casting_wp = (*item).second;//waypoint from the map
			
			// sample collision-free configuration at random
			int x = rand() % width;
			int y = rand() % height;

			std::stringstream ss;
			ss << "wp" << waypoints.size();
			Waypoint* wp = new Waypoint(ss.str(), x, y, map.info);
			
			// Move the waypoint closer so it's no further than @ref{casting_distance} away from the casting_wp.
			wp->update(*casting_wp, casting_distance, map.info);
			
			// Check whether this waypoint is connected to any of the existing waypoints.
			geometry_msgs::Point p1, p2;
			p1.x = wp->real_x;
			p1.y = wp->real_y;
			
			// Ignore waypoint that are too close to existing waypoints.
			float min_distance_to_other_wp = std::numeric_limits<float>::max();
			for (std::map<std::string, Waypoint*>::const_iterator ci = waypoints.begin(); ci != waypoints.end(); ++ci) {
				float distance = wp->getDistance(*(*ci).second);
				if (distance < min_distance_to_other_wp) 
					min_distance_to_other_wp = distance;
			}
			
			if (min_distance_to_other_wp < min_distance) {
				delete wp;
				continue;
			}
			
			for (std::map<std::string, Waypoint*>::const_iterator ci = waypoints.begin(); ci != waypoints.end(); ++ci) {
				Waypoint* other_wp = (*ci).second;
				p2.x = other_wp->real_x;
				p2.y = other_wp->real_y;
				
				if (wp->getDistance(*other_wp) < connecting_distance && canConnect(p1, p2, occupancy_threshold)) {
					wp->neighbours.push_back(other_wp->wpID);
					other_wp->neighbours.push_back(wp->wpID);
					Edge e(wp->wpID, other_wp->wpID);
					edges.push_back(e);
				}
			}
			
			if (wp->neighbours.size() > 0) {
				waypoints[wp->wpID] = wp;
			}
		}
	}

	void RPRoadmapServer::parse_landmark(double pose[], std::string &landmark_name, std::string line) 
	{

    int curr,next;
    curr=line.find("[");
    
    landmark_name = line.substr(0,curr).c_str();
    
    curr=curr+1;
    next=line.find(",",curr);

    pose[0] = (double)atof(line.substr(curr,next-curr).c_str());
    curr=next+1; next=line.find(",",curr);

    pose[1] = (double)atof(line.substr(curr,next-curr).c_str());
    curr=next+1; next=line.find("]",curr);

    pose[2] = (double)atof(line.substr(curr,next-curr).c_str());
    
 	}

	void RPRoadmapServer::parse_landmark_parameters(std::string parameter_path, std::map<std::string, std::vector<double> > &landmarks)
	{
	    std::string line;
	    double value;
	    int iLine = 0;
	    std::ifstream parametersFile (parameter_path.c_str());

	   
	    std::cout <<parameter_path<< std::endl;
	    //std::initializer_list<string> row = {"a","b","c"};
	    //std::vector<std::initializer_list<string>> distance_vector{{"begin_waypoint", "end_waypoint", "distance"}};
	    if (parametersFile.is_open())
	    {
	      int curr,next;

	        while (getline(parametersFile,line)){
	          curr=line.find("[");
	          std::string name = line.substr(0,curr);
	          //geometry_msgs::PoseStamped pose;
	          double pose[3];
	          std::string wp;
	          parse_landmark(pose, wp, line);
	          //landmarks[wp]= vector<double> {pose[0], pose[1], pose[2]};
	          std::vector<double> position_landmark;
	          position_landmark.clear();
	          position_landmark.push_back(pose[0]);
	          position_landmark.push_back(pose[1]);
	          
	          landmarks[wp]= position_landmark;

	        }  
	          
	    }else{
	        std::cout << "Unable to open the file" << std::endl;
	    }
	}

	void RPRoadmapServer::createRRT(nav_msgs::OccupancyGrid map, unsigned int nr_waypoints, double min_distance, double casting_distance, double connecting_distance, int occupancy_threshold, int total_attempts) {
		//createRRT(map, req.nr_waypoints, req.min_distance, req.casting_distance, req.connecting_distance, req.occupancy_threshold, req.total_attempts);
		srand(time(NULL));
		

		

		/*std::vector<double> v0, v1, v2, v3, v4, v5;
		v0.push_back (0);
		v0.push_back (1.5);
		v1.push_back (1.5);
		v1.push_back (-1);
		v2.push_back (1);
		v2.push_back (-3.5);
		v3.push_back (-1.8);
		v3.push_back (-3.4);
		v4.push_back (-3.2);
		v4.push_back (-1);
		v5.push_back (5);
		v5.push_back (-1);*/

		std::map<std::string, std::vector<double> > landmarks;
		std::map<std::string, bool> touched;
		geometry_msgs::Point p1, p2;

		/*landmarks["lm0"] = std::vector <double> {2,3};
    	landmarks["lm1"] = std::vector <double> {4,6};
    	landmarks["lm2"] = std::vector <double> {6,9};
    	landmarks["lm3"] = std::vector <double> {8,8};
    	landmarks["lm4"] = std::vector <double> {9,10};
    	landmarks["lm5"] = std::vector <double> {6,8};

		landmarks["lm0"] = v0;
    	landmarks["lm1"] = v1;
    	landmarks["lm2"] = v2;
    	landmarks["lm3"] = v3;
    	landmarks["lm4"] = v4;
    	//landmarks["goal"] = v5;
    	landmarks["endpoint"] = v5;


    	std::map<std::string, bool> touched;
    	touched["lm0"] = false;
    	touched["lm1"] = false;
    	touched["lm2"] = false;
    	touched["lm3"] = false;
    	touched["lm4"] = false;
    	//touched["goal"] = false;
    	touched["endpoint"] = false;*/

		parse_landmark_parameters(landmark_path, landmarks);


    	bool pull_next = false;
    	bool push_next = false;
    	std::string push_landmark = "";

		// map info
		int width = map.info.width;
		int height = map.info.height;
		double resolution = map.info.resolution; // m per cell
		double radiuspull = 3*(int)(1.0/resolution);
    	double radiuspush = 1*(int)(1.0/resolution);
		double radiusmid = 2*(int)(1.0/resolution); 

		if(width==0 || height==0) {
			ROS_INFO("KCL: (RPRoadmapServer) Empty map");
			return;
		}

		// V <-- empty set; E <-- empty set.
		for (std::map<std::string, Waypoint*>::const_iterator ci = waypoints.begin(); ci != waypoints.end(); ++ci) {
			delete (*ci).second;
		}
		waypoints.clear();
		edges.clear();

		// create robot start point
		geometry_msgs::PoseStamped start_pose;
		geometry_msgs::PoseStamped start_pose_transformed;
		start_pose.header = base_odom.header;
		start_pose.pose.position = base_odom.pose.position;
		start_pose.pose.orientation = base_odom.pose.orientation; 
		try {
			tf.waitForTransform( base_odom.header.frame_id, "map", ros::Time::now(), ros::Duration( 500 ) );
			tf.transformPose( "map", start_pose,  start_pose_transformed);
		} catch(tf::LookupException& ex) {
			ROS_ERROR("Lookup Error: %s\n", ex.what());
			return;
		} catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return;
		} catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return;
		}

		occupancy_grid_utils::Cell start_cell = occupancy_grid_utils::pointCell(map.info, start_pose_transformed.pose.position);
		//this gives the starting position of the robot
		Waypoint* start_wp = new Waypoint("wp0", start_cell.x, start_cell.y, map.info);
		waypoints[start_wp->wpID] = start_wp;

		//transform landmarks
		std::cout<< "startpoint"<<start_wp->wpID<<","<<start_wp->real_x<<","<<start_wp->real_y<<std::endl;
		if (goal_point != "random")
			{
				double endpoint_pose[3];
				std::string endpoint_wp;
				parse_landmark(endpoint_pose, endpoint_wp, goal_point); 
				std::vector <double> v1;
				v1.push_back(endpoint_pose[0]);
				v1.push_back(endpoint_pose[1]);
				landmarks[endpoint_wp] = v1;
			}
		else
		{
			while(true)
			{
				int x = rand() % (int)((connecting_distance/2.0) /resolution);
				int y = rand() % (int)((connecting_distance/2.0) /resolution);
				if (rand()%3 == 2)
				{
					x = start_cell.x +x;
				}
				else
				{	
					x= start_cell.x -x;
				}

				if (rand()%3 == 2)
				{
					y = start_cell.y +y;
				}
				else
				{	
					y= start_cell.y -y;
				}

				
				p1.x = x;
				p1.y = y;


				if (cost_map.data[p1.x + p1.y * cost_map.info.width] < occupancy_threshold)
				{
					std::vector <double> v1;
					v1.push_back((p1.x-start_cell.x)*resolution);
					v1.push_back((p1.y-start_cell.y)*resolution);
					landmarks["endpoint"] = v1;
					break;
				}
			}
		}

		for (std::map<std::string, std::vector <double> >::iterator ci = landmarks.begin(); ci != landmarks.end(); ++ci)
		{
			ci->second.at(0) = start_cell.x+ ci->second.at(0)*(int)(1.0/resolution);
			ci->second.at(1) = start_cell.y+ ci->second.at(1)*(int)(1.0/resolution);
			Waypoint* lm_wp = new Waypoint(ci->first, ci->second.at(0), ci->second.at(1), map.info);
			std::cout<< "landmark"<<lm_wp->wpID<<","<<lm_wp->real_x<<","<<lm_wp->real_y<<std::endl;
			lm_waypoints[ci->first] = lm_wp;
			touched[ci->first] = false;
			
		}




		//cost to root from the waypoint generally 0 for differentiation we make -1
		std::map<std::string, double> cost_to_root;
		cost_to_root[start_wp->wpID] = -1;

		std::string lm_name = "";

		//iterator to check if the next is in landmark range
		bool has_next = false;

		//int i = 0;
		std::cout<<"start_cell_x"<<start_cell.x<<std::endl;
		std::cout<<"start_cell_y"<<start_cell.y<<std::endl;

		int loop_counter = 0;

		while(waypoints.size() < nr_waypoints) {
			
			// sample collision-free configuration at random
			/*int x = rand() % width;
			int y = rand() % height;*/
			int x = rand() % (int)((connecting_distance/2.0) /resolution);
			int y = rand() % (int)((connecting_distance/2.0) /resolution);

			if (rand()%3 == 2)
				{
					x = start_cell.x +x;
				}
				else
				{	
					x= start_cell.x -x;
				}

				if (rand()%3 == 2)
				{
					y = start_cell.y +y;
				}
				else
				{	
					y= start_cell.y -y;
				}
				std::cout<<"x"<<x<<std::endl;
				std::cout<<"y"<<y<<std::endl;
				//exit(0);
			bool valid_lm = false;
			if(has_next)
			{	
				//has_next = false;
				std::vector<double> lm_pose = landmarks[lm_name];
				x = lm_pose.at(0);
				double x_diff = (rand()/(RAND_MAX/(radiusmid -radiuspush)));
				if (rand()%3 == 2)
				{
					x = x + x_diff;
				}
				else
				{	
					x= x-x_diff;
				}

				y = lm_pose.at(1);
				double y_diff = (rand()/(RAND_MAX/(radiusmid-radiuspush)));
				if (rand()%3 == 2)
				{
					y = y + y_diff;
				}
				else
				{	
					y= y-y_diff;
				}

			}
			else
			{

				for (std::map<std::string, std::vector <double> >::const_iterator ci = landmarks.begin(); ci != landmarks.end(); ++ci)
				{
					lm_name = ci->first;
					std::vector <double > lm_pos = ci->second;
					bool lm_touched = touched[lm_name];

					if (!lm_touched)
					{
						double dist = ((x-lm_pos.at(0))*(x-lm_pos.at(0)))+ ((y-lm_pos.at(1))*(y-lm_pos.at(1)));
						dist = sqrt(dist);

						if (dist< radiuspull && dist > radiuspush)
						{
							valid_lm = true;
							break;
						}

					}

				}
			}

			
			std::stringstream ss;
			ss << "wp" << waypoints.size();
			Waypoint* wp = new Waypoint(ss.str(), x, y, map.info);
					
			// Check whether this waypoint is connected to any of the existing waypoints.
			//geometry_msgs::Point p1, p2;
			p1.x = wp->real_x;
			p1.y = wp->real_y;

			std::vector<std::pair<Waypoint*, double> > n_closest;
			n_closest.clear();
			Waypoint* closest_waypoint;

			float closest_distance= 1000;
			bool valid_waypoint = true;

			for (std::map<std::string, Waypoint*>::const_iterator ci = waypoints.begin(); ci != waypoints.end(); ++ci) {
				float distance = wp->getDistance(*(*ci).second);
				if (distance < connecting_distance)
				{
					double cost = cost_to_root[(*ci).second->wpID];
					if(cost > 0)
					{
						double total_distance = cost+ distance;
						n_closest.push_back(std::make_pair((*ci).second, total_distance));
					}
					else
					{
						n_closest.push_back(std::make_pair((*ci).second, distance));
					}

				}
				
				
				if (distance < closest_distance)
				{
					closest_distance = distance;
					closest_waypoint = (*ci).second;
				}

				if (distance < min_distance) {
				valid_waypoint = false;
				//delete wp;
				//delete closest_waypoint;
				break;
				}
			}
			//if (i == 10)
			//	exit(0);
			if (loop_counter>10000)
				{exit(0);}
			loop_counter++;
			if (!valid_waypoint)
				continue;
	
			//i++;
			//if no parent found
			if (n_closest.size() == 0)
			{
				/*p2.x = closest_waypoint->real_x;
				p2.y = closest_waypoint->real_y;
				if (canConnect(p1, p2, occupancy_threshold))
				{
					double root_cost = cost_to_root[closest_waypoint->wpID];
					if(root_cost > 0)
					{
						cost_to_root[wp->wpID] = wp->getDistance(*closest_waypoint)+ root_cost;
					}
					else
					{
						cost_to_root[wp->wpID] = wp->getDistance(*closest_waypoint);
					}
					wp->neighbours.push_back(closest_waypoint->wpID);
					closest_waypoint->neighbours.push_back(wp->wpID);
					Edge e(wp->wpID, closest_waypoint->wpID);
					edges.push_back(e);
					waypoints[wp->wpID] = wp;
					if (valid_lm)
					{
						has_next = true;
						touched[lm_name]= true;
					}
					//std::cout<<"before continue"<<std::endl;
					//exit(0);
					continue;
				}*/
			}
			else //when parent found
			{
				
				std::sort(n_closest.begin(), n_closest.end(), sort_distance);
				//int i = 0;
				
				for(std::vector<std::pair<Waypoint*, double> >::iterator ci = n_closest.begin(); ci != n_closest.end(); ++ci)
				{
					
					closest_waypoint = ci->first;
					p2.x = closest_waypoint->real_x;
					p2.y = closest_waypoint->real_y;
					/*std::cout<<"before canConnect"<<std::endl;
						std::cout<<"p1,x and y"<<p1.x<<","<< p1.y<<std::endl;

						std::cout<<"p2,x and y"<<p2.x<<","<< p2.y<<std::endl;*/
						
					if (canConnect(p1, p2, occupancy_threshold))
					{

						cost_to_root[wp->wpID] = ci->second;
						wp->neighbours.push_back(closest_waypoint->wpID);
						closest_waypoint->neighbours.push_back(wp->wpID);
						Edge e(wp->wpID, closest_waypoint->wpID);
						edges.push_back(e);
						waypoints[wp->wpID] = wp;
						if (has_next)
						has_next = false;
						if (valid_lm)
						{

							has_next = true;
							touched[lm_name]= true;
						}

						break;
					}
				
				}

				//exit(0);


			}

		// Ignore waypoint that are too close to existing waypoints.
			
		}
		//end of while
		//Waypoint* goal_wp = lm_waypoints["goal"] ;
		Waypoint* goal_wp = lm_waypoints["endpoint"] ;

		p1.x = goal_wp->real_x;
		p1.y = goal_wp->real_y;
		bool goal_added = false;
		//output stream for txt file to use in DINO
		std::ofstream output_waypoint;
		//output_waypoint.open("/home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/landmark/waypoints_rrt.txt");
		output_waypoint.open(waypoint_output_path.c_str());
		output_waypoint<<goal_wp->wpID<<"["<<(goal_wp->grid_x-start_cell.x)*resolution<<","<<(goal_wp->grid_y-start_cell.y)*resolution<<",0.0]\n";
		for (std::map<std::string, Waypoint*>::const_iterator ci = waypoints.begin(); ci != waypoints.end(); ++ci) {
			output_waypoint<<ci->first<<"["<<(ci->second->grid_x-start_cell.x)*resolution<<","<<(ci->second->grid_y-start_cell.y)*resolution<<",0.0]\n";
				float distance = goal_wp->getDistance(*(*ci).second);
				//if (distance < connecting_distance)
				if (distance < casting_distance)
				{
					p2.x = ci->second->real_x;
					p2.y = ci->second->real_y;
					if(canConnect(p1,p2,occupancy_threshold)) 
					{
						goal_wp->neighbours.push_back(ci->first);
						ci->second->neighbours.push_back(goal_wp->wpID);
						Edge e(goal_wp->wpID, ci->first);
						edges.push_back(e);

						if(!goal_added)
						{
							waypoints[goal_wp->wpID] = goal_wp;
							goal_added = true;
						}
					}
				}
		}
		output_waypoint.close();		



	}

	void RPRoadmapServer::LoadRRT(std::string waypointfile, std::string problemfile, nav_msgs::OccupancyGrid map)
	{
		std::ifstream infile(waypointfile.c_str());
		std::string line;
		int curr,next;

		geometry_msgs::PoseStamped start_pose;
		geometry_msgs::PoseStamped start_pose_transformed;
		start_pose.header = base_odom.header;
		start_pose.pose.position = base_odom.pose.position;
		start_pose.pose.orientation = base_odom.pose.orientation; 
		try {
			tf.waitForTransform( base_odom.header.frame_id, "map", ros::Time::now(), ros::Duration( 500 ) );
			tf.transformPose( "map", start_pose,  start_pose_transformed);
		} catch(tf::LookupException& ex) {
			ROS_ERROR("Lookup Error: %s\n", ex.what());
			return;
		} catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return;
		} catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return;
		}

		occupancy_grid_utils::Cell start_cell = occupancy_grid_utils::pointCell(map.info, start_pose_transformed.pose.position);
		//this gives the starting position of the robot
		//Waypoint* start_wp = new Waypoint("wp0", start_cell.x, start_cell.y, map.info);
		//waypoints[start_wp->wpID] = start_wp;	
		double resolution = map.info.resolution;
		std::map<std::string, std::vector<double> > landmarks;
		parse_landmark_parameters(landmark_path, landmarks);

		for (std::map<std::string, std::vector <double> >::iterator ci = landmarks.begin(); ci != landmarks.end(); ++ci)
		{
			ci->second.at(0) = start_cell.x+ ci->second.at(0)*(int)(1.0/resolution);
			ci->second.at(1) = start_cell.y+ ci->second.at(1)*(int)(1.0/resolution);
			Waypoint* lm_wp = new Waypoint(ci->first, ci->second.at(0), ci->second.at(1), map.info);
			std::cout<< "landmark"<<lm_wp->wpID<<","<<lm_wp->real_x<<","<<lm_wp->real_y<<std::endl;
			lm_waypoints[ci->first] = lm_wp;	
		}

		while(std::getline(infile, line)) {
			// read waypoint
			curr=line.find("[");
			std::string name = line.substr(0,curr);

			// data
			geometry_msgs::PoseStamped pose;
			pose.header.frame_id = map.header.frame_id;
			parsePose(pose, line);

			int grid_x = (int)round((pose.pose.position.x/resolution) + start_cell.x);
			int grid_y = (int)round((pose.pose.position.y/resolution) + start_cell.y);		

			Waypoint* wp = new Waypoint(name, grid_x, grid_y, map.info);
			waypoints[wp->wpID] = wp;
			std::stringstream ss;
			ss << "waypoints: x:" << pose.pose.position.x << " , y: " << pose.pose.position.y << "/n";
			std::string p_s = ss.str();
			ROS_INFO("%s", p_s.c_str());

			if(name == "endpoint")
			{
				lm_waypoints["endpoint"] = wp;
			}
		}

		infile.close();
		

		std::ifstream probleminfile(problemfile.c_str());
		while(std::getline(probleminfile, line)) {
			// read problem
			curr=line.find("(connected");
			if(curr>0)
			{
				curr = line.find(" ", curr+1);
				next = line.find(" ", curr+1);
				std::string wp1name = line.substr(curr+1, next-curr-1);

				curr = next;
				next = line.find(")", curr+1);
				std::string wp2name = line.substr(curr+1, next-curr-1);

				Waypoint* wp1 = waypoints[wp1name];
				Waypoint* wp2 = waypoints[wp2name];

				bool connected = false;
				for (std::vector<std::string>::iterator it = wp1->neighbours.begin(); it!= wp1->neighbours.end(); ++it)
				{
					if(*it == wp2name)
					{
						connected = true;
					}
				}

				if(!connected)
				{
					wp1->neighbours.push_back(wp2name);
					wp2->neighbours.push_back(wp1name);
					Edge e(wp1name, wp2name);
					edges.push_back(e);
				}
			}

		
		}
		probleminfile.close();

		
	}

	 void RPRoadmapServer::parsePose(geometry_msgs::PoseStamped &pose, std::string line) {

		int curr,next;
		curr=line.find("[")+1;
		next=line.find(",",curr);

		pose.pose.position.x = (double)atof(line.substr(curr,next-curr).c_str());
		curr=next+1; next=line.find(",",curr);

		pose.pose.position.y = (double)atof(line.substr(curr,next-curr).c_str());
		curr=next+1; next=line.find("]",curr);

		float theta = atof(line.substr(curr,next-curr).c_str());
		tf::Quaternion q;
		q.setEuler(theta, 0 ,0);


		pose.pose.orientation.x = q.x();
		pose.pose.orientation.y = q.z();
		pose.pose.orientation.w = q.w();
		pose.pose.orientation.z = q.y();
	}

	bool RPRoadmapServer::sort_distance(std::pair<Waypoint*, double> i, std::pair<Waypoint*, double> j)
	{
		return (i.second< j.second);
	}



	/**
	 * Connects a new waypoint and stores it in the knowledge base scene database
	 */
	bool RPRoadmapServer::addWaypoint(rosplan_knowledge_msgs::AddWaypoint::Request &req, rosplan_knowledge_msgs::AddWaypoint::Response &res) {

		ros::NodeHandle nh("~");
		
		ROS_INFO("KCL: (RPRoadmapServer) Adding new waypoint");
		
		// read map
		nav_msgs::OccupancyGrid map;
		if(use_static_map) {
			ROS_INFO("KCL: (RPRoadmapServer) Reading in map");
			nav_msgs::GetMap mapSrv;
			map_client.call(mapSrv);
			map = mapSrv.response.map;
		} else {
			map = cost_map;
		}

		// map info
		int width = map.info.width;
		int height = map.info.height;
		double resolution = map.info.resolution; // m per cell

		if(width==0 || height==0) {
			ROS_INFO("KCL: (RPRoadmapServer) Empty map");
			return false;
		}

		// clear old waypoint
		clearWaypoint(req.id);

		geometry_msgs::PoseStamped pose;
			pose.header.frame_id = map.header.frame_id;
			pose.pose.position.x = req.x;
			pose.pose.position.y = req.y;
			pose.pose.position.z = 0.0;
			pose.pose.orientation.x = 0.0;;
			pose.pose.orientation.y = 0.0;;
			pose.pose.orientation.z = 0.0;
			pose.pose.orientation.w = 1.0;
		// add new waypoint
		occupancy_grid_utils::Cell start_cell = occupancy_grid_utils::pointCell(map.info, req.waypoint.pose.position);
		Waypoint* new_wp = new Waypoint(req.id, start_cell.x, start_cell.y, map.info);
		waypoints[new_wp->wpID] = new_wp;

		// connect to neighbours
		geometry_msgs::Point p1, p2;
		p1.x = new_wp->real_x;
		p1.y = new_wp->real_y;

		for (std::map<std::string, Waypoint*>::const_iterator ci = waypoints.begin(); ci != waypoints.end(); ++ci) {
			Waypoint* other_wp = (*ci).second;
			p2.x = other_wp->real_x;
			p2.y = other_wp->real_y;
			
			std::cout << "Try to connect " << new_wp->wpID << " to " << other_wp->wpID << "." << std::endl;
			
			if (new_wp->getDistance(*other_wp) < req.connecting_distance && canConnect(p1, p2, req.occupancy_threshold)) {
				new_wp->neighbours.push_back(other_wp->wpID);
				other_wp->neighbours.push_back(new_wp->wpID);
				Edge e(new_wp->wpID, other_wp->wpID);
				edges.push_back(e);
			} else {
				std::cout << "Do not connect these waypoints because: ";
				if (new_wp->getDistance(*other_wp) < req.connecting_distance) {
					std::cout << "collision detected." << std::endl;
				} else {
					std::cout << "the distance between them is too large. " << new_wp->getDistance(*other_wp) << ">= " <<  req.connecting_distance << "." << std::endl;
				}
			}
		}


		// instance
		rosplan_knowledge_msgs::KnowledgeUpdateService updateSrv;
		updateSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::ADD_KNOWLEDGE;
		updateSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::INSTANCE;
		updateSrv.request.knowledge.instance_type = "waypoint";
		updateSrv.request.knowledge.instance_name = new_wp->wpID;
		if (!update_knowledge_client.call(updateSrv)) {
			ROS_ERROR("Failed to add a new waypoint instance.");
			return false;
		}
		
		// publish visualization
		publishWaypointMarkerArray(nh);
		publishEdgeMarkerArray(nh);
		
		ROS_INFO("Process the %lu neighbours of this new waypoint.", new_wp->neighbours.size());
			
		// predicates
		for (std::vector<std::string>::iterator nit=new_wp->neighbours.begin(); nit!=new_wp->neighbours.end(); ++nit) {
			// connected new->old
			rosplan_knowledge_msgs::KnowledgeUpdateService updatePredSrv;
			updatePredSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::ADD_KNOWLEDGE;
			updatePredSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::FACT;
			updatePredSrv.request.knowledge.attribute_name = "connected";
			diagnostic_msgs::KeyValue pairFrom;
			pairFrom.key = "from";
			pairFrom.value = new_wp->wpID;
			updatePredSrv.request.knowledge.values.push_back(pairFrom);
			diagnostic_msgs::KeyValue pairTo;
			pairTo.key = "to";
			pairTo.value = *nit;
			updatePredSrv.request.knowledge.values.push_back(pairTo);
			update_knowledge_client.call(updatePredSrv);

			// connected old->new
			updatePredSrv.request.knowledge.values.clear();
			pairFrom.value = *nit;
			updatePredSrv.request.knowledge.values.push_back(pairFrom);
			pairTo.value = new_wp->wpID;
			updatePredSrv.request.knowledge.values.push_back(pairTo);
			update_knowledge_client.call(updatePredSrv);	
		}

		// functions
		for (std::vector<std::string>::iterator nit=new_wp->neighbours.begin(); nit!=new_wp->neighbours.end(); ++nit) {

			// distance new->old
			rosplan_knowledge_msgs::KnowledgeUpdateService updateFuncSrv;
			updateFuncSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::ADD_KNOWLEDGE;
			updateFuncSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::FUNCTION;
			updateFuncSrv.request.knowledge.attribute_name = "distance";
			diagnostic_msgs::KeyValue pairFrom;
			pairFrom.key = "wp1";
			pairFrom.value = new_wp->wpID;
			updateFuncSrv.request.knowledge.values.push_back(pairFrom);
			diagnostic_msgs::KeyValue pairTo;
			pairTo.key = "wp2";
			pairTo.value = *nit;
			updateFuncSrv.request.knowledge.values.push_back(pairTo);
			double dist = sqrt(
					(new_wp->real_x - waypoints[*nit]->real_x)*(new_wp->real_x - waypoints[*nit]->real_x)
					+ (new_wp->real_y - waypoints[*nit]->real_y)*(new_wp->real_y - waypoints[*nit]->real_y));
			updateFuncSrv.request.knowledge.function_value = dist;
			update_knowledge_client.call(updateFuncSrv);

			// distance old->new
			updateFuncSrv.request.knowledge.values.clear();			
			pairFrom.value = *nit;
			updateFuncSrv.request.knowledge.values.push_back(pairFrom);
			pairTo.value = new_wp->wpID;
			updateFuncSrv.request.knowledge.values.push_back(pairTo);
			updateFuncSrv.request.knowledge.function_value = dist;
			update_knowledge_client.call(updateFuncSrv);
		}

		//data
			/*geometry_msgs::PoseStamped pose;
			pose.header.frame_id = map.header.frame_id;
			pose.pose.position.x = req.x;
			pose.pose.position.y = req.y;
			pose.pose.position.z = 0.0;
			pose.pose.orientation.x = 0.0;;
			pose.pose.orientation.y = 0.0;;
			pose.pose.orientation.z = 0.0;
			pose.pose.orientation.w = 1.0;*/

				std::vector< boost::shared_ptr<geometry_msgs::PoseStamped> > checkResults;
			if(message_store.queryNamed<geometry_msgs::PoseStamped>(req.id, checkResults))
			 { message_store.updateNamed(req.id, pose);
			 	// std::stringstream gazeboss;
			 	// gazeboss << "rosrun gazebo_ros spawn_model -database checkerboard_plane -sdf -model " << req.id << " -x " << pose.pose.position.x << " -y " << pose.pose.position.y;
			 	//std::string p_s = gazeboss.str();
			 	//std::system(p_s.c_str());
			 	//ROS_INFO("%s", p_s.c_str());
			 	} 
			else { 

				std::string id(message_store.insertNamed(req.id, pose));
				db_name_map[new_wp->wpID] = id;
				}
		ROS_INFO("KCL: (RPRoadmapServer) Finished adding new waypoint");

		return true;
	}

	bool RPRoadmapServer::removeWaypoint(rosplan_knowledge_msgs::RemoveWaypoint::Request &req, rosplan_knowledge_msgs::RemoveWaypoint::Response &res) {

		ros::NodeHandle nh("~");
		if ( clearWaypoint(req.id) ) {
			// publish visualization
			publishWaypointMarkerArray(nh);
			publishEdgeMarkerArray(nh);
		}
		return true;
	}

	bool RPRoadmapServer::clearWaypoint(const std::string &name) {

		if ( db_name_map.count( name ) == 0 ) {
			return false;
		}

		message_store.deleteID(db_name_map[name]);
		db_name_map.erase(name);
		waypoints.erase(name);

		std::vector<Edge>::iterator eit = edges.begin();
		while( eit != edges.end() ) {
			if ( name == eit->start || name == eit->end ) {
				eit = edges.erase( eit );
			} else {
				eit++;
			}
		}
		// remove instance
		rosplan_knowledge_msgs::KnowledgeUpdateService updateSrv;
		updateSrv.request.update_type = rosplan_knowledge_msgs::KnowledgeUpdateService::Request::REMOVE_KNOWLEDGE;
		updateSrv.request.knowledge.knowledge_type = rosplan_knowledge_msgs::KnowledgeItem::INSTANCE;
		updateSrv.request.knowledge.instance_type = "waypoint";
		updateSrv.request.knowledge.instance_name = name;
		if (!update_knowledge_client.call(updateSrv)) {
			ROS_INFO("Failed to remove old waypoint instance.");
		}

		return true;

	}
} // close namespace

	/*-------------*/
	/* Main method */
	/*-------------*/

	int main(int argc, char **argv) {

		// setup ros
		ros::init(argc, argv, "rosplan_roadmap_server");
		ros::NodeHandle nh("~");

		// params
		std::string costMapTopic("/move_base/local_costmap/costmap");
		nh.param("cost_map_topic", costMapTopic, costMapTopic);
		std::string odomTopic("/odom");
		nh.param("odom_topic", odomTopic, odomTopic);

		// init
		KCL_rosplan::RPRoadmapServer rms(nh);
		ros::Subscriber odom_sub = nh.subscribe<nav_msgs::Odometry>(odomTopic, 1, &KCL_rosplan::RPRoadmapServer::odomCallback, &rms);
		ros::Subscriber map_sub = nh.subscribe<nav_msgs::OccupancyGrid>(costMapTopic, 1, &KCL_rosplan::RPRoadmapServer::costMapCallback, &rms);
		ros::ServiceServer createPRMService = nh.advertiseService("/kcl_rosplan/roadmap_server/create_prm", &KCL_rosplan::RPRoadmapServer::generateRoadmap, &rms);
		ros::ServiceServer addWaypointService = nh.advertiseService("/kcl_rosplan/roadmap_server/add_waypoint", &KCL_rosplan::RPRoadmapServer::addWaypoint, &rms);
		ros::ServiceServer removeWaypointService = nh.advertiseService("/kcl_rosplan/roadmap_server/remove_waypoint", &KCL_rosplan::RPRoadmapServer::removeWaypoint, &rms);

		ROS_INFO("KCL: (RPRoadmapServer) Ready to receive. Cost map topic: %s", costMapTopic.c_str());
		ros::spin();
		return 0;
	}
