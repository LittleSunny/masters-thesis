domain_turtlebot_demo.cpp
domain_turtlebot_demo.errors
domain_turtlebot_demo.goals
domain_turtlebot_demo.h
domain_turtlebot_demo.m
domain_turtlebot_demo.pddl
domain_turtlebot_demo_planner
domain_turtlebot_demo.properties
domain_turtlebot_demo.startstates
domain_turtlebot_demo.transitions
runplanner.sh

======================================================

DiNo version 1.1
Discretised Nonlinear Heuristic Planner for PDDL+ models with continous processes and events

Copyright (C) 2015
W. Piotrowski, M. Fox, D. Long, D. Magazzeni, F. Mercorio

Call with the -c flag or read the license file for terms
and conditions of use.
Run this program with "-h" for the list of options.
Send bug reports and comments to wiktor.piotrowski@kcl.ac.uk

======================================================

======================================================
Planning configuration

* Source domain: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/test/domain_turtlebot_demo.pddl
* Source problem: /home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/common/problem.pddl
* Planning Mode: Feasible Plan
* SRPG time horizon: 12
* Output format: PDDL+
* Epsilon separation: 0.001
* Output target: "/home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/common/problem_plan.pddl"

* Model: /home/sunny/catkin_ws/src/ROSPlan/rosplan_demos/common/test/domain_turtlebot_demo
* State size 1227 bits (rounded to 156 bytes).
* Allocated memory: 1000 Megabytes

**  Time Discretisation = 1.000000
**  Digits for representing the integer part of a real =  5.000000
**  Digits for representing the fractional part of a real =  4
======================================================

=== Analyzing model... ===============================

* State Space Expansion Algorithm: SRPG+.
  with symmetry algorithm 3 -- Heuristic Small Memory Normalization
  with permutation trial limit 10.
* Maximum SRPG time horizon: 12.
* Maximum size of the state space: 7479161 states.
  with states hash-compressed to 40 bits.


======================================================

Model exploration complete (in 1.12 seconds).
	4488 actions fired
	1 start states
	4320 reachable states
	1 goals found


=== Building model dynamics... =======================

* Transition Graph mode: Memory Image
* Maximum size of graph: 7532873 transitions.


======================================================

Model dynamics rebuilding complete (in 1.20 seconds).
	4320 states
	4309 transitions
	out degree: min 0 max 3 avg 1.00


=== Finding control paths... =========================

* Search Algorithm: Feasible Plan.


======================================================

Control paths calculation complete (in 1.20 seconds).
	4320 states
	36 controllable


=== Collecting plans... ==============================


======================================================

Plan(s) generation complete (in 1.20 seconds).
	1 plans
	plan length (actions): min 36 max 36 avg 36.00
	plan duration (time): min 0 max 30 avg 30.00
	plan weight: min 0 max 30 avg 30.00


=== Writing final results... =========================

* Output format: PDDL+
* Output target: "/home/sunny/catkin_ws/src/ROSPlan/rosplan_planning_system/common/problem_plan.pddl".


======================================================

Results Written (in 1.20 seconds).


